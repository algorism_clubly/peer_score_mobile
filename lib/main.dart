import 'package:feature_discovery/feature_discovery.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:peer_score/providers/bank_accounts_provider.dart';
import 'package:peer_score/providers/bvn_provider.dart';
import 'package:peer_score/providers/colors.dart';
import 'package:peer_score/providers/contact.dart';
import 'package:peer_score/providers/debts_provider.dart';
import 'package:peer_score/providers/expected_payments_provider.dart';
import 'package:peer_score/providers/loans-provider.dart';
import 'package:peer_score/providers/notifications_provider.dart';
import 'package:peer_score/providers/onboading_provider.dart';
import 'package:peer_score/providers/payment_provider.dart';
import 'package:peer_score/providers/user-provider.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/providers/witness_provider.dart';
import 'package:peer_score/screens/waiting_screen.dart';
import 'package:peer_score/utils/connectivity_singleton.dart';
import 'package:peer_score/utils/custom_page_route.dart' as CustomRoute;
import 'package:peer_score/utils/notification_utils.dart';
import 'package:peer_score/utils/theme_mode.dart';
import 'package:peer_score/utils/theme_mode_singleton.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';

//start of notifications initialization

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();
NotificationAppLaunchDetails notificationAppLaunchDetails;
//set up streams to listen for notification click event

//android
final BehaviorSubject<String> notificationSelectSubject =
    BehaviorSubject<String>();
//ios
final BehaviorSubject<ReceivedNotification> didReceiveLocalNotificationSubject =
    BehaviorSubject<ReceivedNotification>();

//end of notifications integration

//set singletons variables

ConnectionStatusSingleton connectionStatusSingleton =
    ConnectionStatusSingleton.getInstance();

ThemeModeSingleton themeModeSingleton = ThemeModeSingleton.getInstance();

class ReceivedNotification {
  final int id;
  final String title;
  final String body;
  final String payload;

  ReceivedNotification({
    @required this.id,
    @required this.title,
    @required this.body,
    @required this.payload,
  });
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  notificationAppLaunchDetails =
      await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();
  var initializationSettingsAndroid = AndroidInitializationSettings('app_icon');
  //ios
  var initializationSettingsIOS = IOSInitializationSettings(
      onDidReceiveLocalNotification:
          (int id, String title, String body, String payload) async {
    didReceiveLocalNotificationSubject.add(ReceivedNotification(
        id: id, title: title, body: body, payload: payload));
  });
  var initializationSettings = InitializationSettings(
      initializationSettingsAndroid, initializationSettingsIOS);

  await flutterLocalNotificationsPlugin.initialize(initializationSettings,
      onSelectNotification: (String payload) async {
    notificationSelectSubject.add(payload);
  });

  //initialize singletons here
  connectionStatusSingleton.initialize();
  themeModeSingleton.initialize();

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(PeerScore());
  });
}

class PeerScore extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _PeerScoreState createState() => _PeerScoreState();
}

class _PeerScoreState extends State<PeerScore> with WidgetsBindingObserver {
  String prevPayload = "";
  bool isLight = true;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  @override
  void initState() {
    super.initState();
    _configureSelectNotificationSubject();
    _configureThemeModeSubject();
    WidgetsBinding.instance.addObserver(this);
  }

  void _configureThemeModeSubject() {
    themeModeSingleton.themeModeChangeSubject.listen((isLightMode) {
      setState(() {
        isLight = isLightMode;
      });
    });
  }

  void _configureSelectNotificationSubject() {
    notificationSelectSubject.stream.listen((String payload) async {
      //for whatever reasons this event gets called twice everytime why I employ the hack to save previous payload
      //this will also get triggered only when app was not launched from the clicked notification tile
      if ((prevPayload != payload) &&
          (notificationAppLaunchDetails.didNotificationLaunchApp == false)) {
        NotificationUtils.openNotificationPage(payload: payload);
      }
      prevPayload = payload;
    });
    notificationSelectSubject.value = null;
  }

  // @override
  // void didChangeAppLifecycleState(AppLifecycleState state) {
  //   NotificationUtils.appLifecycleState = state;
  // }

  @override
  void didChangePlatformBrightness() {
    super.didChangePlatformBrightness();
    themeModeSingleton.setThemeMode(null);
  }

  @override
  void dispose() {
    notificationSelectSubject.close();
    connectionStatusSingleton.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: GeneralProvider()),
        ChangeNotifierProvider.value(value: ExpectedPaymentsProvider()),
        ChangeNotifierProvider.value(value: DebtsProviver()),
        ChangeNotifierProvider.value(value: ContactProvider()),
        ChangeNotifierProvider.value(value: LoansProvider()),
        ChangeNotifierProvider.value(value: BankAccountProvider()),
        ChangeNotifierProvider.value(value: OnboardingProvider()),
        ChangeNotifierProvider.value(value: UserProvider()),
        ChangeNotifierProvider.value(value: WitnesProvider()),
        ChangeNotifierProvider.value(value: NotificationProvider()),
        ChangeNotifierProvider.value(value: PaymentProvider()),
        ChangeNotifierProvider.value(value: BvnProvider()),
        ChangeNotifierProvider.value(value: AppColorsProvider()),
      ],
      child: FeatureDiscovery(
        child: MaterialApp(
          key: _scaffoldKey,
          debugShowCheckedModeBanner: false,
          title: 'PeerScore',
          theme: isLight
              ? ThemeDesign.lightMode(context: context)
              : ThemeDesign.darkMode(context: context),
          darkTheme: isLight
              ? ThemeDesign.lightMode(context: context)
              : ThemeDesign.darkMode(context: context),
          home: WaitingScreen(),
          onGenerateRoute: CustomRoute.Router.generateRoute,
        ),
      ),
    );
  }
}
