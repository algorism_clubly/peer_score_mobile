import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:peer_score/utils/api/methods.dart';
import 'package:peer_score/utils/api/urls.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BankAccount {
  final String id;
  final String accountNumber;
  final String accountName;
  final String bankName;
  final String bankCode;
  bool isDefault;
  BankAccount(
      {this.accountNumber,
      this.bankCode,
      this.bankName,
      this.accountName,
      this.isDefault,
      this.id});
}

class BankAccountProvider extends ChangeNotifier {
  BankAccount recipienActiveBankAccount;
  List<BankAccount> _bankAccounts = [];

  List<BankAccount> get bankAccounts {
    return _bankAccounts;
  }

  Future<Map<String, dynamic>> resolveAccountNumber(
      {String accountNumber, String bankCode}) async {
    try {
      Dio dio = new Dio();
      var response = await dio.get(
        Urls.ressolveAccountNumber(
            accountNumber: accountNumber, bankCode: bankCode),
        options: Options(
            headers: {
              "Authorization":
                  "Bearer sk_test_7dcf603b2238ea623d4637067155fb6ad5908bbb"
            },
            followRedirects: false,
            validateStatus: (status) {
              return true;
            }),
      );

      return response.data;
    } on DioError catch (err) {
      var error;
      if (DioErrorType.RESPONSE == err.type) {
        error = err.error;
      } else if (DioErrorType.DEFAULT == err.type) {
        if (err.message.contains('SocketException')) {
          error = {"status": "111", "errors": "Connection refused"};
        }
      }
      throw error as Map<String, dynamic>;
    }
  }

  Future<Map<String, dynamic>> getAllAccounts({dynamic body}) async {
    try {
      final sharedPrefs = await SharedPreferences.getInstance();
      final userData =
          json.decode(sharedPrefs.get("userData")) as Map<String, dynamic>;
      var response =
          await UrlMethods.getRequest(url: Urls.getAllAccounts(userData["id"]));
       
      var bankAccounts = response["data"];
      List<BankAccount> tempBanks = [];
      bankAccounts.forEach((account) {
        tempBanks.add(
          BankAccount(
            id: account["id"],
            accountNumber: account["acc_number"],
            accountName: account["account_name"],
            bankName: account["bank_name"],
            isDefault: account["active"],
            bankCode: account["bank_code"],
          ),
        );
      });
      _bankAccounts = tempBanks;
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> getActiveBankAccount({dynamic body}) async {
    try {
      final sharedPrefs = await SharedPreferences.getInstance();
      final userData =
          json.decode(sharedPrefs.get("userData")) as Map<String, dynamic>;
      //get active bank account of logged in user or second party user
       
       
      var response = await UrlMethods.getRequest(
          url: Urls.getActiveAccount(
              body["userId"] == null ? userData["id"] : body["userId"]));
       
      var bankAccount = response["data"]["resp"];
      BankAccount tempActiveBankAccount;
      if (bankAccount != null) {
        tempActiveBankAccount = BankAccount(
          id: bankAccount["id"],
          accountNumber: bankAccount["acc_number"],
          accountName: bankAccount["account_name"],
          bankName: bankAccount["bank_name"],
          isDefault: bankAccount["active"],
          bankCode: bankAccount["bank_code"],
        );

        recipienActiveBankAccount = tempActiveBankAccount;
      }

       
       
      return response;
    } catch (err) {
       
      return err;
    }
  }

  Future<Map<String, dynamic>> addNewAccount({dynamic body}) async {
    try {
      var response =
          await UrlMethods.postRequest(url: Urls.addNewAccount, body: body);
      // getAllAccounts(body: {"userId": body["userId"]});
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> deleteBankAccount({dynamic body}) async {
    try {
      var response =
          await UrlMethods.postRequest(url: Urls.deleteBankAccount, body: body);
      // getAllAccounts(body: {"userId": body["userId"]});
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> changeBankAccountStatus({dynamic body}) async {
    try {
      var response = await UrlMethods.postRequest(
          url: Urls.changeBankAccountStatus, body: body);
      // getAllAccounts(body: {"userId": body["userId"]});
      return response;
    } catch (err) {
      return err;
    }
  }
}
