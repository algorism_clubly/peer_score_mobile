import 'package:flutter/foundation.dart';
import 'package:peer_score/utils/api/methods.dart';
import 'package:peer_score/utils/api/urls.dart';

class BvnProvider extends ChangeNotifier {
  bool _hasBvn = false;
  bool get hasBvn {
    return _hasBvn;
  }

  Future<Map<String, dynamic>> initiateBvn({Map<String, dynamic> body}) async {
    try {
      var response =
          await UrlMethods.postRequest(url: Urls.initiateBvn, body: body);
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> getBvn({String userId}) async {
    try {
      var response = await UrlMethods.getRequest(url: Urls.getBvn(userId));
      if (response["status"] == 200 && response["data"] != null) {
        _hasBvn = true;
      }
      
      
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> confirmBvn({Map<String, dynamic> body}) async {
    try {
      var response =
          await UrlMethods.postRequest(url: Urls.confirmBvn, body: body);
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> deleteBvn({Map<String, dynamic> body}) async {
    try {
      var response =
          await UrlMethods.postRequest(url: Urls.deleteBvn, body: body);
      return response;
    } catch (err) {
      return err;
    }
  }
}
