import 'package:flutter/foundation.dart';
import 'package:peer_score/utils/api/methods.dart';
import 'package:peer_score/utils/api/urls.dart';

class CalenderProvider extends ChangeNotifier {
  bool _hasBvn = false;
  bool get hasBvn {
    return _hasBvn;
  }

  Future<Map<String, dynamic>> updateCredentials(
      {Map<String, dynamic> body}) async {
    try {
      print(body);
      var response =
          await UrlMethods.postRequest(url: Urls.updateCredentials, body: body);
      print("RESPONSE");
      print(response);
      return response;
    } catch (err) {
      print("ERROR___>");
      print(err);
      return err;
    }
  }

  Future<Map<String, dynamic>> getCredentials({String userId}) async {
    try {
      var response =
          await UrlMethods.getRequest(url: Urls.getCredentials(userId));
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> confirmBvn({Map<String, dynamic> body}) async {
    try {
      var response =
          await UrlMethods.postRequest(url: Urls.confirmBvn, body: body);
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> deleteBvn({Map<String, dynamic> body}) async {
    try {
      var response =
          await UrlMethods.postRequest(url: Urls.deleteBvn, body: body);
      return response;
    } catch (err) {
      return err;
    }
  }
}
