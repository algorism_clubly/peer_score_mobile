import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:peer_score/providers/temp-provider.dart';

class AppColorsProvider extends ChangeNotifier {
  static final Color primaryColorLight = Color(0xFF1F3D51);
  static final Color accentColorLight = Color(0xffE78200);
  static final Color primaryColorDark = Color(0xff9db8cd);
  static final Color accentColorDark = Color(0xfff0af4b);

  Color primaryColorText =
      GeneralProvider.isLightMode ? primaryColorLight : Colors.white;
  Color altColorText =
      GeneralProvider.isLightMode ? primaryColorLight : accentColorLight;
  Color refreshIndicator =
      GeneralProvider.isLightMode ? primaryColorLight : Colors.white;

  Color defaultIconColor = GeneralProvider.isLightMode ? null : Colors.white;
  Color buttonIconColor =
      GeneralProvider.isLightMode ? Colors.white : Colors.black;

  Color pageCloseSymbolColor =
      GeneralProvider.isLightMode ? Colors.grey : Colors.white;

  //start of button colors
  Color primaryButtonColorText =
      GeneralProvider.isLightMode ? Colors.white : Colors.white;
  Color primaryButtonColorBg =
      GeneralProvider.isLightMode ? primaryColorLight : primaryColorLight;
  Color accentButtonColorText =
      GeneralProvider.isLightMode ? Colors.white : Colors.white;
  Color transparentPrimaryButtonColorText =
      GeneralProvider.isLightMode ? primaryColorLight : Colors.white;
  Color accentButtonColorBg =
      GeneralProvider.isLightMode ? accentColorLight : accentColorLight;
  Color primaryIndicatorColor =
      GeneralProvider.isLightMode ? Colors.white : primaryColorLight;
  //----sk

  //start of progress indicator colors
  Color primaryProgressValueColor =
      GeneralProvider.isLightMode ? Colors.white : primaryColorLight;
  Color altProgressValueColor =
      GeneralProvider.isLightMode ? primaryColorLight : Colors.white;

  //pincodetextfield colors
  Color pinCodebackgroundColor =
      GeneralProvider.isLightMode ? Colors.white : Color(0xff121212);
  Color pinCodebackgroundColorAltPhone =
      GeneralProvider.isLightMode ? Colors.white : Colors.grey[850];
  Color pinCodeBorderColor =
      GeneralProvider.isLightMode ? Color(0xFFB19CD9) : Colors.grey[850];
  Color pinCodeBorderColorAltPhone =
      GeneralProvider.isLightMode ? Color(0xFFB19CD9) : Colors.white30;

  //feature discovery

  Color primaryFeatureDiscoveryBackground =
      GeneralProvider.isLightMode ? null : Colors.grey[850];

  //contact list
  Color contactListSelectedHighlight =
      GeneralProvider.isLightMode ? primaryColorLight : Colors.grey[900];
  Color contactBadgeColor =
      GeneralProvider.isLightMode ? primaryColorLight : accentColorDark;
  Color contactPhoneColor =
      GeneralProvider.isLightMode ? Color(0xFF797979) : Colors.white70;
  Color contactAmountColor =
      GeneralProvider.isLightMode ? primaryColorLight : Colors.white70;
  Color contactComplianceColor =
      GeneralProvider.isLightMode ? Colors.black : Colors.white;
  Color contactNameColor =
      GeneralProvider.isLightMode ? Colors.black : Colors.white;
  //loan tile
  Color loanTileSelectedContainerCOlor = GeneralProvider.isLightMode
      ? Color(0xffE78200).withOpacity(0.2)
      : Colors.grey[900];
  Color loanTileSelectedContainerBorderCOlor = GeneralProvider.isLightMode
      ? Color(0xffE78200).withOpacity(0.3)
      : Colors.grey[850];
  Color clickedTileBorderColor = GeneralProvider.isLightMode
      ? Color.fromRGBO(103, 102, 165, 0.18)
      : Colors.grey;

  //proof page colors
  Color selectedFileName =
      GeneralProvider.isLightMode ? Color(0xff6766A5) : Colors.white70;
  Color selectItemTitle =
      GeneralProvider.isLightMode ? Color(0xFF888888) : Colors.white70;

  //filter panel
  Color filterPanelCOlor =
      GeneralProvider.isLightMode ? Colors.white : Color(0xff121212);
  Color filterSortIconColor = GeneralProvider.isLightMode ? null : Colors.white;

//change mode bottomsheet
  Color swipeModeIndicatorColor =
      GeneralProvider.isLightMode ? Colors.grey[300] : Colors.grey[850];

//settings page
  Color altPhoneContainerColor =
      GeneralProvider.isLightMode ? Colors.grey[200] : Colors.grey[850];

//starting really needed variables
//summary card
  Color summaryBoxColor = GeneralProvider.isLightMode
      ? Color(0xFFF1F1F1F1)
      : Colors.white.withOpacity(0.05);

  Color navBarSelected =
      GeneralProvider.isLightMode ? Colors.white : primaryColorDark;
  Color navBarUnselected =
      GeneralProvider.isLightMode ? Colors.white60 : Colors.white70;

  //switch
  // Color activeSwitch = GeneralProvider.isLightMode ? primaryColorLight : ;
  Color inactiveSwitch =
      GeneralProvider.isLightMode ? Colors.grey[400] : Colors.grey[400];
  Color toggleInactiveColor =
      GeneralProvider.isLightMode ? Colors.white : Colors.grey[200];
  Color toggleActiveColor =
      GeneralProvider.isLightMode ? Colors.white : primaryColorDark;
}
