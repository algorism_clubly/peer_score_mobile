import 'package:flutter/foundation.dart';
import 'package:peer_score/utils/api/methods.dart';
import 'package:peer_score/utils/api/urls.dart';

class Contact {
  final String id;
  final String firstName;
  final String lastName;
  final String otherName;
  final String phoneNumber;
  final String peerScore;
  final String compliance;
  final String email;
  final String profilePicture;
  final bool isWhatsApp;
  Contact(
      {this.otherName,
      this.peerScore,
      this.phoneNumber,
      this.id,
      this.lastName,
      this.compliance,
      this.email,
      this.firstName,
      this.profilePicture,
      this.isWhatsApp = false});

  Map<String, dynamic> toMap() {
    return {
      "id": id,
      "firstName": firstName,
      "lastName": lastName,
      "otherName": otherName,
      "phoneNumber": phoneNumber,
      "peerScore": peerScore,
      "compliance": compliance,
      "email": email,
      "profilePicture": profilePicture,
      "isWhatsApp": isWhatsApp
    };
  }
}

class ContactProvider extends ChangeNotifier {
  Future<Map<String, dynamic>> syncPhone(
      {List<Map<String, String>> phoneNumbers, String userId}) async {
    try {
      // final sharedPrefs = await SharedPreferences.getInstance();
      // final userData =
      //     json.decode(sharedPrefs.get("userData")) as Map<String, dynamic>;
      //     // 
      //     
      var response = await UrlMethods.postRequest(
        url: Urls.syncPhone,
        body: {
          "userId": userId,
          "user_contacts": phoneNumbers,
        },
      );

      
      

      // 

      // notifyListeners();
      return response;
    } catch (err) {
      
      
      return err;
    }
  }
}
