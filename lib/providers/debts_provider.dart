import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';
import 'package:peer_score/providers/contact.dart';
import 'package:peer_score/providers/expected_payments_provider.dart';
import 'package:peer_score/utils/api/methods.dart';
import 'package:peer_score/utils/api/urls.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DebtsProviver extends ChangeNotifier {
  List<Loan> _debts = [];
  static Function debtsListCb = () {};
  static Loan selectedDebt;
  static String debtPaymentProofFilePath;
  static DateTime rescheduledDate;
  List<Contact> witnesses = [];
  Map<String, dynamic> _debtSummary = {"loanCount": "0", "totalAmount": "0.0"};
  List<Loan> get debts {
    return _debts;
  }

  Map<String, dynamic> get debtSummary {
    Map<String, dynamic> tempDebtSummary = {};
    double totalAmount = 0;
    _debts.forEach((loan) {
      totalAmount += double.parse(loan.amount);
    });
    tempDebtSummary = {
      "debtCount": _debts.length,
      "totalAmount": totalAmount.toString()
    };
    _debtSummary = tempDebtSummary;
    return _debtSummary;
  }

  // void addWitness()

  Future<Map<String, dynamic>> getDebts() async {
    try {
      final sharedPrefs = await SharedPreferences.getInstance();
      final userData =
          json.decode(sharedPrefs.get("userData")) as Map<String, dynamic>;

      var response = await UrlMethods.getRequest(
        url: Urls.getDebts(
          HelperFunctions.serializePhoneNumber(phoneNumber: userData["phone"]),
        ),
      );

      var loansList = response["data"];

      List<Loan> tempLoansList = [];
      if (loansList.length > 0) {
        loansList.forEach(
          (loan) {
            tempLoansList.add(
              Loan(
                id: loan["id"],
                creditor: Contact(
                  id: loan["provider"]["id"],
                  firstName: loan["provider"]["first_name"],
                  lastName: loan["provider"]["last_name"],
                  otherName: loan["provider"]["other_name"],
                  peerScore: "${loan["provider"]["peer_score"]}",
                  compliance: "${loan["provider"]["compliance"]}",
                  email: loan["provider"]["email"],
                  phoneNumber: HelperFunctions.deserializePhoneNumber(
                      phoneNumber: loan["provider"]["phone"]),
                ),
                amount: loan["amount"],
                createdAt: DateFormat("y-M-d").parse(loan["createdAt"]),
                date: DateFormat(
                  "y-M-d",
                ).parse(loan["due_date"]),
                loanActions: loan["loan_action"],
              ),
            );
          },
        );
      }
      //sort by shortest due date first, also aggregrates late loans first
      tempLoansList.sort(
        (a, b) => a.date.difference(b.date).inSeconds,
      );
      _debts = tempLoansList;
      // notifyListeners();
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> offlinePayment({
    String loanId,
    String documentPath
  }) async {
    
    try {
      var response = await UrlMethods.postRequest(
        url: Urls.offlineDebtPayment,
        body: FormData.fromMap({
          // "loan_id": selectedDebt.id,
          "loan_id": loanId,
          "document": await MultipartFile.fromFile(
            documentPath,
            filename: "document",
          )
        }),
      );
      // notifyListeners();
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> reschedule() async {
    try {
      var response = await UrlMethods.postRequest(
        url: Urls.reschedule,
        body: {
          "new_date": rescheduledDate.toIso8601String(),
          "loan_id": selectedDebt.id,
        },
      );
      // notifyListeners();
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> recallAction() async {
    try {
      var response = await UrlMethods.postRequest(
        url: Urls.recallAction,
        body: {
          "loan_id": selectedDebt.id,
        },
      );
      // notifyListeners();
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> badClaim(
      {List<Map<String, dynamic>> documents,
      List<Contact> witnesses,
      String loanId}) async {
    
    
    
    try {
      var response = await UrlMethods.postRequest(
        url: Urls.badClaim,
        body: FormData.fromMap({
          // "loan_id": selectedDebt.id,
          "loan_id": loanId,
          "witnesses": witnesses.length == 0
              ? null
              : witnesses.map((item) => item.id).toList(),
          "document": documents.length == 0
              ? " "
              : await MultipartFile.fromFile(
                  documents[0]["path"],
                  filename: "document",
                )
        }),
      );
      // notifyListeners();
      return response;
    } catch (err) {
      
      return err;
    }
  }
}
