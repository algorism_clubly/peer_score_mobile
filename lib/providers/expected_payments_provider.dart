import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';
import 'package:peer_score/providers/contact.dart';
import 'package:peer_score/utils/api/methods.dart';
import 'package:peer_score/utils/api/urls.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Loan {
  final String id;
  final String status;
  final Contact debtor;
  final Contact creditor;
  final String amount;
  final String privacy;
  final DateTime date;
  final DateTime createdAt;
  final Map<String, dynamic> loanActions;
  final Map<String, dynamic> loanApprovalProof;
  final Map<String, dynamic> badClaimInsistProof;
  final Map<String, dynamic> badClaimInitiateProof;
  Loan(
      {this.debtor,
      this.creditor,
      this.loanActions,
      this.id,
      this.date,
      this.amount,
      this.loanApprovalProof,
      this.badClaimInitiateProof,
      this.badClaimInsistProof,
      this.status,
      this.createdAt,
      this.privacy});
}

class LoanRequest {
  final String id;
  final Contact debtor;
  final Contact creditor;
  final String amount;
  final DateTime dueDate;
  final String privacy;
  final String status;
  final String denialReason;
  LoanRequest(
      {this.debtor,
      this.id,
      this.dueDate,
      this.amount,
      this.privacy,
      this.denialReason,
      this.status,
      this.creditor});
}

class ExpectedPaymentsProvider extends ChangeNotifier {
  List<LoanRequest> _pendingDebtors = [];
  List<Loan> _expectedPayments = [];
  static LoanRequest selectedDebtorLoanRequest;
  static Loan selectedLoan;
  static String loanDenyReason;
  static String loanCloseReason;
  static DateTime newReschduledDate;
  static Function loanRequestCb = () {};
  static Function loansListCb = () {};
  static String loanRequestPaymentProofFilePath;
  Map<String, dynamic> _loanSummary = {"loanCount": "", "totalAmount": ""};

  List<Loan> get expectedPayments {
    return _expectedPayments;
  }

  Map<String, dynamic> get loanSummary {
    Map<String, dynamic> tempLoanSummary = {};
    double totalAmount = 0;
    _expectedPayments.forEach((loan) {
      totalAmount += double.parse(loan.amount);
    });
    tempLoanSummary = {
      "loanCount": _expectedPayments.length,
      "totalAmount": totalAmount.toString()
    };
    _loanSummary = tempLoanSummary;
    return _loanSummary;
  }

  List<LoanRequest> get pendingDebtors {
    return _pendingDebtors;
  }

  // LoanRequest get selectedDebtorLoanRequest {
  //   return _selectedDebtorLoanRequest;
  // }

  // void setSelectedDebtorLoanRequest({LoanRequest loan}) {
  //   _selectedDebtorLoanRequest = loan;
  // }

  // Loan get selectedLoan {
  //   return _selectedLoan;
  // }

  // void setSelectedLoan({Loan loan}) {
  //   _selectedLoan = loan;
  // }

  // void setLoanDenyreason({String reason}) {
  //   _loanDenyReason = reason;
  // }

  Future<Map<String, dynamic>> denyLoanRequest() async {
    try {
      final sharedPrefs = await SharedPreferences.getInstance();
      final userData =
          json.decode(sharedPrefs.get("userData")) as Map<String, dynamic>;
      var response =
          await UrlMethods.postRequest(url: Urls.denyLoanRequest, body: {
        "loan_request_id": selectedDebtorLoanRequest.id,
        "denial_reason": loanDenyReason,
        "userId": userData["id"]
      });
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> approveLoanWithReceipt(
      {String recipientPhone,
      String loanRequestId,
      String documentPath}) async {
    try {
      final sharedPrefs = await SharedPreferences.getInstance();
      final userData =
          json.decode(sharedPrefs.get("userData")) as Map<String, dynamic>;
      var response = await UrlMethods.postRequest(
        url: Urls.approveLoanWithReceipt,
        body: FormData.fromMap(
          {
            // "loan_request_id": _selectedDebtorLoanRequest.id,
            "loan_request_id": loanRequestId,
            "document": await MultipartFile.fromFile(documentPath,
                filename: "document"),
            "recipient_phone": HelperFunctions.serializePhoneNumber(
              phoneNumber: recipientPhone,
            ),
            "userId": userData["id"]
          },
        ),
      );
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> getLoanRequests() async {
    try {
      final sharedPrefs = await SharedPreferences.getInstance();
      final userData =
          json.decode(sharedPrefs.get("userData")) as Map<String, dynamic>;
      var response = await UrlMethods.getRequest(
          url: Urls.getLoanRequests(HelperFunctions.serializePhoneNumber(
              phoneNumber: userData["phone"])));

      var pendingDebtorsList;
      pendingDebtorsList = response["data"];

      List<LoanRequest> tempPendingDebtorsList = [];
      pendingDebtorsList.forEach((loan) {
        tempPendingDebtorsList.add(
          LoanRequest(
            id: loan["id"],
            debtor: Contact(
              id: loan["reciever"]["id"],
              firstName: loan["reciever"]["first_name"],
              lastName: loan["reciever"]["last_name"],
              otherName: loan["reciever"]["other_name"],
              peerScore: "${loan["reciever"]["peer_score"]}",
              compliance: "${loan["reciever"]["compliance"]}",
              email: loan["reciever"]["email"],
              phoneNumber: HelperFunctions.deserializePhoneNumber(
                phoneNumber: loan["reciever"]["phone"],
              ),
            ),
            amount: loan["amount"],
            dueDate: DateFormat(
              "y-M-d",
            ).parse(loan["due_date"]),
          ),
        );
      });
      //sort by shortest due date first
      tempPendingDebtorsList.sort(
        (a, b) => a.dueDate.difference(b.dueDate).inSeconds,
      );
      _pendingDebtors = tempPendingDebtorsList;
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> getLoans() async {
    try {
      final sharedPrefs = await SharedPreferences.getInstance();
      final userData =
          json.decode(sharedPrefs.get("userData")) as Map<String, dynamic>;
      var response = await UrlMethods.getRequest(
        url: Urls.getLoans(
          userData["id"],
        ),
      );

      var loansList = response["data"];
      List<Loan> tempLoansList = [];
      if (loansList.length > 0) {
        loansList.forEach((loan) {
          tempLoansList.add(
            Loan(
              id: loan["id"],
              debtor: Contact(
                id: loan["reciever"] != null ? loan["reciever"]["id"] : null,
                firstName: loan["reciever"] != null
                    ? loan["reciever"]["first_name"]
                    : null,
                lastName: loan["reciever"] != null
                    ? loan["reciever"]["last_name"]
                    : null,
                otherName: loan["reciever"] != null
                    ? loan["reciever"]["other_name"]
                    : null,
                peerScore: loan["reciever"] != null
                    ? "${loan["reciever"]["peer_score"]}"
                    : null,
                compliance: loan["reciever"] != null
                    ? "${loan["reciever"]["compliance"]}"
                    : null,
                email:
                    loan["reciever"] != null ? loan["reciever"]["email"] : null,
                isWhatsApp: loan["reciever"] != null
                    ? loan["reciever"]["whatsapp"]
                    : false,
                phoneNumber: HelperFunctions.deserializePhoneNumber(
                  phoneNumber: loan["reciever"] != null
                      ? loan["reciever"]["phone"]
                      : loan["recipient_phone"],
                ),
              ),
              amount: loan["amount"],
              createdAt: DateFormat("y-M-d").parse(loan["createdAt"]),
              date: DateFormat("y-M-d").parse(loan["due_date"]),
              loanActions: loan["loan_action"],
            ),
          );
        });
      }
      //sort by shortest due date first, also aggregrates late loans first
      tempLoansList.sort(
        (a, b) => a.date.difference(b.date).inSeconds,
      );
      _expectedPayments = tempLoansList;
      getLoanRequests();
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> insistClaim(
      {List<Map<String, dynamic>> documents,
      List<Contact> witnesses,
      String loanId}) async {
    try {
      var response = await UrlMethods.postRequest(
        url: Urls.insistClaim,
        body: FormData.fromMap({
          // "loan_id": _selectedLoan.id,
          "loan_id": loanId,
          "witnesses": witnesses.length == 0
              ? null
              : witnesses.map((item) => item.id).toList(),
          "document": documents.length == 0
              ? " "
              : await MultipartFile.fromFile(
                  documents[0]["path"],
                  filename: "document",
                )
        }),
      );
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> cancelClaim() async {
    try {
      var response = await UrlMethods.postRequest(
        url: Urls.cancelClaim,
        body: {
          "loan_id": selectedLoan.id,
        },
      );
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> confirmReschedule() async {
    try {
      var response = await UrlMethods.postRequest(
        url: Urls.confirmReschedule,
        body: {
          "due_date": newReschduledDate.toIso8601String(),
          "loan_id": selectedLoan.id,
        },
      );
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> declineReschedule() async {
    try {
      var response = await UrlMethods.postRequest(
        url: Urls.declineReschedule,
        body: {
          "due_date": newReschduledDate.toIso8601String(),
          "loan_id": selectedLoan.id,
        },
      );
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> confirmPay() async {
    try {
      var response = await UrlMethods.postRequest(
        url: Urls.confirmPay,
        body: {
          "loan_id": selectedLoan.id,
        },
      );
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> flagPay() async {
    try {
      var response = await UrlMethods.postRequest(
        url: Urls.flagPay,
        body: {
          "loan_id": selectedLoan.id,
        },
      );
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> closeLoan({dynamic body}) async {
    try {
      var response = await UrlMethods.postRequest(url: Urls.closeLoan, body: {
        "loan_id": selectedLoan.id,
        "closure_reason": loanCloseReason
      });

      return response;
    } catch (err) {
      return err;
    }
  }
}
