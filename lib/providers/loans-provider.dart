import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';
import 'package:peer_score/providers/contact.dart';
import 'package:peer_score/providers/expected_payments_provider.dart';
import 'package:peer_score/utils/api/methods.dart';
import 'package:peer_score/utils/api/urls.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:peer_score/utils/googleCalender.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoansProvider extends ChangeNotifier {
  // Contact _contact;
  static Contact selectedContact;
  String _amount;
  DateTime _paymentDate;
  String _requestVisibility;
  List<Loan> _contactOpenLoans = [];
  Map<String, List<Loan>> _calenderLoans = {"debts": [], "payments": []};
  int _unregisteredContactDebtsCount = 0;
  List<Loan> get contactOpenLoans {
    return _contactOpenLoans;
  }

  Map<String, List<Loan>> get calenderLoans {
    return _calenderLoans;
  }

  int get unregisteredContactDebtsCount {
    return _unregisteredContactDebtsCount;
  }

  // Contact get contact {
  //   return _contact;
  // }

  String get amount {
    return _amount;
  }

  String get requestVisibility {
    return _requestVisibility;
  }

  DateTime get paymentDate {
    return _paymentDate;
  }

  void setContact({Contact contact}) {
    // _contact = contact;
    selectedContact = contact;
  }

  void setAmountDateVisibility(
      {String amount, String requestVisibility, DateTime paymentDate}) {
    _amount = amount;
    _requestVisibility = requestVisibility;
    _paymentDate = paymentDate;
  }

  Future<Map<String, dynamic>> createLoanRequest() async {
    try {
      final sharedPrefs = await SharedPreferences.getInstance();
      final userData =
          json.decode(sharedPrefs.get("userData")) as Map<String, dynamic>;
      dynamic postBody = {
        "provider_id": selectedContact.id,
        "recipient_id": userData["id"],
        "amount": _amount,
        "privacy": "nill",
        "due_date": _paymentDate.toIso8601String(),
        "provider_phone": HelperFunctions.serializePhoneNumber(
          phoneNumber: selectedContact.phoneNumber,
        )
      };
      var response = await UrlMethods.postRequest(
          url: Urls.createLoanRequest, body: postBody);
      selectedContact = null;
      _amount = null;
      _paymentDate = null;
      _requestVisibility = null;
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> createRequestPayment() async {
    try {
      final sharedPrefs = await SharedPreferences.getInstance();
      final userData =
          json.decode(sharedPrefs.get("userData")) as Map<String, dynamic>;
      dynamic postBody = {
        "provider_id": userData["id"],
        "recipient_id": selectedContact.id,
        "amount": _amount,
        "privacy": _requestVisibility,
        "due_date": _paymentDate.toIso8601String(),
        "recipient_phone": HelperFunctions.serializePhoneNumber(
          phoneNumber: selectedContact.phoneNumber,
        )
      };

      var response = await UrlMethods.postRequest(
          url: Urls.createRequestPayment, body: postBody);
      //add loan to debtor calender
      // addLoanToUserCalender(
      //   dueDate: _paymentDate.toIso8601String(),
      //   amount: _amount,
      //   debtorId: selectedContact.id,
      //   creditorFirstName: userData["first_name"],
      // );
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> getUserFinanceSummary(
      {String providerId, String recipientPhone}) async {
    try {
      var response = await UrlMethods.getRequest(
          url: Urls.getUserFinanceSummary(providerId, recipientPhone));

      // notifyListeners();
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> getContactOpenLoans({String recipientId}) async {
    try {
      final sharedPrefs = await SharedPreferences.getInstance();
      final userData =
          json.decode(sharedPrefs.get("userData")) as Map<String, dynamic>;

      var response = await UrlMethods.getRequest(
        url: Urls.getContactOpenLoans(
          userData["id"],
          recipientId,
        ),
      );

      var loansList = response["data"];

      List<Loan> tempLoansList = [];
      if (loansList != null && loansList.length > 0) {
        loansList.forEach(
          (loan) {
            tempLoansList.add(
              Loan(
                id: loan["id"],
                privacy: loan["privacy"],
                amount: loan["amount"],
                date: DateFormat(
                  "y-M-d",
                ).parse(loan["due_date"]),
                // loanActions: loan["loan_action"],
              ),
            );
          },
        );
      }
      //sort by shortest due date first, also aggregrates late loans first
      tempLoansList.sort(
        (a, b) => a.date.difference(b.date).inSeconds,
      );
      _contactOpenLoans = tempLoansList;
      // notifyListeners();

      return response;
    } catch (err) {
      return err;
    }
  }

  List<Loan> iterateLoanList({List loans}) {
    List<Loan> loansList = [];
    loans.forEach(
      (loan) {
        loansList.add(
          Loan(
            id: loan["id"],
            privacy: loan["privacy"],
            amount: loan["amount"],
            debtor: Contact(
              id: loan["reciever"]["id"],
              firstName: loan["reciever"]["first_name"],
              lastName: loan["reciever"]["last_name"],
              otherName: loan["reciever"]["other_name"],
              peerScore: "${loan["reciever"]["peer_score"]}",
              compliance: "${loan["reciever"]["compliance"]}",
              email: loan["reciever"]["email"],
              phoneNumber: HelperFunctions.deserializePhoneNumber(
                phoneNumber: loan["reciever"]["phone"],
              ),
            ),
            creditor: Contact(
              id: loan["provider"]["id"],
              firstName: loan["provider"]["first_name"],
              lastName: loan["provider"]["last_name"],
              otherName: loan["provider"]["other_name"],
              peerScore: "${loan["provider"]["peer_score"]}",
              compliance: "${loan["provider"]["compliance"]}",
              email: loan["provider"]["email"],
              phoneNumber: HelperFunctions.deserializePhoneNumber(
                phoneNumber: loan["provider"]["phone"],
              ),
            ),
            date: DateFormat(
              "y-M-d",
            ).parse(loan["due_date"]),
            // loanActions: loan["loan_action"],
          ),
        );
      },
    );

    return loansList;
  }

  Future<Map<String, dynamic>> getCalenderLoans(
      {Map<String, dynamic> body}) async {
    try {
      print("DATA GOING");
      print(body);
      var response =
          await UrlMethods.postRequest(url: Urls.getCalenderLoans, body: body);
      var loansResp = response["data"];
      print("LOANS LIST CAL");
      print(loansResp);
      Map<String, List<Loan>> tempCalenderLoansList = {
        "debts": [],
        "payments": []
      };
      tempCalenderLoansList["debts"] =
          iterateLoanList(loans: loansResp["debts"]);
      tempCalenderLoansList["payments"] =
          iterateLoanList(loans: loansResp["payments"]);

      _calenderLoans = tempCalenderLoansList;
      // notifyListeners();

      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> getUnregisteredContactOpenLoans(
      {String recipientPhone}) async {
    try {
      final sharedPrefs = await SharedPreferences.getInstance();
      final userData =
          json.decode(sharedPrefs.get("userData")) as Map<String, dynamic>;

      var response = await UrlMethods.getRequest(
        url: Urls.getUnregisteredContactOpenLoans(
          userData["id"],
          recipientPhone,
        ),
      );

      var loansList;
      if (response["data"] != null) {
        loansList = response["data"]["openLoans"];
        _unregisteredContactDebtsCount = response["data"]["totalDebts"];
      }

      List<Loan> tempLoansList = [];
      if (loansList != null && loansList.length > 0) {
        loansList.forEach(
          (loan) {
            tempLoansList.add(
              Loan(
                id: loan["id"],
                privacy: loan["privacy"],
                amount: loan["amount"],

                date: DateFormat(
                  "y-M-d",
                ).parse(loan["due_date"]),
                // loanActions: loan["loan_action"],
              ),
            );
          },
        );
      }
      //sort by shortest due date first, also aggregrates late loans first
      tempLoansList.sort(
        (a, b) => a.date.difference(b.date).inSeconds,
      );
      _contactOpenLoans = tempLoansList;
      // notifyListeners();

      return response;
    } catch (err) {
      return err;
    }
  }
}
