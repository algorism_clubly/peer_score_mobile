import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:peer_score/providers/expected_payments_provider.dart';
import 'package:peer_score/providers/witness_provider.dart';
import 'package:peer_score/utils/api/methods.dart';
import 'package:peer_score/utils/api/urls.dart';
import 'package:peer_score/utils/notification_utils.dart';
import 'package:peer_score/utils/socket.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Notification {
  final String id;
  final String title;
  final String type;
  bool seen;
  final DateTime createdDate;
  final Loan loan;
  final LoanRequest loanRequest;
  final Claim witness;
  final bool archived;
  Notification(
      {this.title,
      this.id,
      this.seen,
      this.type,
      this.createdDate,
      this.loan,
      this.loanRequest,
      this.witness,
      this.archived});
}

class NotificationProvider extends ChangeNotifier {
  static String notificationIdFromDeviceTray;
  static bool isOpened = false;
  List<Notification> _notifications = [];
  int unreadNotificationsCount = 0;
  List<Notification> get notifications {
    return _notifications;
  }

  Future<Map<String, dynamic>> getNotifications(
      {dynamic body, bool isArchive = false}) async {
    try {
      final sharedPrefs = await SharedPreferences.getInstance();
      final userData =
          json.decode(sharedPrefs.get("userData")) as Map<String, dynamic>;

      var response = await UrlMethods.getRequest(
        url: isArchive
            ? Urls.getArchivedNotifications(userData["id"])
            : Urls.getNotifications(
                userData["id"],
              ),
      );
      var notificationsData = response["data"];
      List<Notification> tempNotifications = [];
      notificationsData.forEach((notification) {
        tempNotifications.add(NotificationUtils.getFullNotificationObject(
            notification: notification));
      });
      _notifications = tempNotifications;
      sharedPrefs.setString("notifications", json.encode(notificationsData));
      SocketConnection.getInstance().updateUnreadNotificationCount();
      return response;
    } catch (err) {
      

      return err;
    }
  }

  Future<Map<String, dynamic>> markNotificationAsSeen({dynamic body}) async {
    try {
      var response = await UrlMethods.postRequest(
          url: Urls.markNotificationAsSeen, body: body);

      final sharedPrefs = await SharedPreferences.getInstance();
      var notificationsData = json.decode(sharedPrefs.get("notifications"));
      notificationsData = notificationsData.map((item) {
        if (item["id"] == body["notification_id"]) {
          item["seen"] = true;
        }
        return item;
      }).toList();
      sharedPrefs.setString("notifications", json.encode(notificationsData));
      SocketConnection.getInstance().updateUnreadNotificationCount();
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> markNotificationAsArchive({dynamic body}) async {
    try {
      var response = await UrlMethods.postRequest(
          url: Urls.markNotificationAsArchive, body: body);

      final sharedPrefs = await SharedPreferences.getInstance();
      var notificationsData = json.decode(sharedPrefs.get("notifications"));
      notificationsData = notificationsData.map((item) {
        if (item["id"] == body["notification_id"]) {
          item["archived"] = true;
        }
        return item;
      }).toList();
      sharedPrefs.setString("notifications", json.encode(notificationsData));
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> unArchiveNotification({dynamic body}) async {
    try {
      var response = await UrlMethods.postRequest(
          url: Urls.unArchiveNotification, body: body);

      final sharedPrefs = await SharedPreferences.getInstance();
      var notificationsData = json.decode(sharedPrefs.get("notifications"));
      notificationsData = notificationsData.map((item) {
        if (item["id"] == body["notification_id"]) {
          item["archived"] = false;
        }
        return item;
      }).toList();
      sharedPrefs.setString("notifications", json.encode(notificationsData));
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> archiveAll({dynamic body}) async {
    try {
      var response =
          await UrlMethods.postRequest(url: Urls.archiveAll, body: body);
      final sharedPrefs = await SharedPreferences.getInstance();
      var notificationsData = json.decode(sharedPrefs.get("notifications"));
      notificationsData = notificationsData.map((item) {
        item["archived"] = true;
        return item;
      }).toList();
      sharedPrefs.setString("notifications", json.encode(notificationsData));
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> readAll({dynamic body}) async {
    try {
      var response =
          await UrlMethods.postRequest(url: Urls.readAll, body: body);
      final sharedPrefs = await SharedPreferences.getInstance();
      var notificationsData = json.decode(sharedPrefs.get("notifications"));
      notificationsData = notificationsData.map((item) {
        item["seen"] = true;
        return item;
      }).toList();
      sharedPrefs.setString("notifications", json.encode(notificationsData));
      return response;
    } catch (err) {
      return err;
    }
  }
}
