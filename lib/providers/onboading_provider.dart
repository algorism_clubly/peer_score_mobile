import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:peer_score/utils/api/methods.dart';
import 'package:peer_score/utils/api/urls.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OnboardingProvider extends ChangeNotifier {
  Map<String, dynamic> _onboadingUserData = {};

  Future<Map<String, dynamic>> get onboadingUserData async {
    final sharedPrefs = await SharedPreferences.getInstance();
    return json.decode(sharedPrefs.get("userData")) as Map<String, dynamic>;
  }

  Future<Map<String, dynamic>> sendPhoneNumber({dynamic body}) async {
    try {
      var response =
          await UrlMethods.postRequest(url: Urls.sendPhoneNumber, body: body);
      // notifyListeners();
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> verifyOtp({dynamic body}) async {
    try {
      
      var response =
          await UrlMethods.postRequest(url: Urls.verifyOtp, body: body);
          
          
      return response;
    } catch (err) {
      
      return err;
    }
  }

  Future<Map<String, dynamic>> createPin({dynamic body}) async {
    try {
      var response =
          await UrlMethods.postRequest(url: Urls.createPassword, body: body);
      final sharedPrefs = await SharedPreferences.getInstance();
      
      
      sharedPrefs.setString(
          "userData", json.encode(response["data"]["completedUser"]));
      sharedPrefs.setString("token", response["data"]["token"]);
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> updateFirstName({dynamic body}) async {
    try {
      var response =
          await UrlMethods.postRequest(url: Urls.updateFirstName, body: body);
      final sharedPrefs = await SharedPreferences.getInstance();
      sharedPrefs.setString("userData", json.encode(response["data"]));
      return response;
    } catch (err) {
      return err;
    }
  }
}
