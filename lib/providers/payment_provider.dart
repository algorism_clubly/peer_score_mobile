import 'package:flutter/foundation.dart';
import 'package:peer_score/utils/api/methods.dart';
import 'package:peer_score/utils/api/urls.dart';

class PaymentProvider extends ChangeNotifier {
  Future<Map<String, dynamic>> initiatePayment({dynamic body}) async {
    try {
      var response =
          await UrlMethods.postRequest(url: Urls.initiatePayment, body: body);
      return response;
    } catch (err) {
      print(err);
      return err;
    }
  }

  Future<Map<String, dynamic>> confirmPayment({dynamic body}) async {
    try {
      var response =
          await UrlMethods.postRequest(url: Urls.confirmPayment, body: body);
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> getPaystackKey({dynamic body}) async {
    try {
      var response = await UrlMethods.getRequest(url: Urls.getPaystackKey);

      return response;
    } catch (err) {
      return err;
    }
  }
}
