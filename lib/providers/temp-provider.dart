import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:peer_score/providers/contact.dart';

class GeneralProvider with ChangeNotifier {
  static bool isNewUser = false;
  static GlobalKey<ScaffoldState> scaffoldKeyRef;
  GlobalKey<ScaffoldState> _scaffoldKey;
  static GlobalKey<ScaffoldState> profileScaffoldKey;
  static TextEditingController currentFormFieldController;
  //saving the currently focused textfield in any of the pageview to be unfocused on pagechange
  //**textfields wont dispose naturally on pagechange hence this hack */
  static FocusNode currentFocusedNode;
  static Function({int index, int pageStackIntialIndex}) switchMainPage;
  static int currentSelectedPageIndex = 0;
  //this function will ensure that when the navigator button is clicked the particular stack will reset to its first page
  static Function({int index}) switchSubStackIndex;
  double _bottomNavHeight = 52.0;
  List<Contact> _selectedContacts = [];
  List<Map<String, dynamic>> _selectedFilesData = [];
  String _currentAltPhoneNumber = "";
  static String resetPasswordInitilzationPage = "";
  static String successPageMessage = "";

  //this saves the status of modals in any page (opened/close)
  static bool isModalOpened = false;

  //save the selected themeMode
  static bool isLightMode = true;
  static String selectedMode;

  //below variables used to initiate methods from widgets called from notifications page
  static var widgetType;
  static String selectedItemId;

  int _requestedPageIndex;
  int _requestedPageStackIndex;

  GlobalKey<ScaffoldState> get getAppScaffoldKey {
    return _scaffoldKey;
  }

  String get getCurrentAltPhone {
    return _currentAltPhoneNumber;
  }

  double get getBottomNavHeight {
    return _bottomNavHeight;
  }

  List<Contact> get getSelectedContatcts {
    return _selectedContacts;
  }

  List<Map<String, dynamic>> get getSelectedFilesData {
    return _selectedFilesData;
  }

  void clearSelectedContacts() {
    _selectedContacts = [];
  }

  void clearSelectedFiles() {
    _selectedFilesData = [];
  }

  void updatedSelectedContacts(List<Contact> selectedContacts) {
    _selectedContacts = selectedContacts;
  }

  void updatedSelectedFiles(Map<String, dynamic> fileData) {
    _selectedFilesData.add(fileData);
  }

  void setCurrentAltPhone({String phoneNumber}) {
    _currentAltPhoneNumber = phoneNumber;
  }

  void setScaffoldKey(GlobalKey<ScaffoldState> scaffoldKey) {
    _scaffoldKey = scaffoldKey;
    scaffoldKeyRef = scaffoldKey;
  }

  Map<String, int> get getPageAndStackIndex {
    return {
      "pageIndex": _requestedPageIndex,
      "stackIndex": _requestedPageStackIndex
    };
  }

  void setPageAndStackIndex({int pageIndex, int stackIndex}) {
    _requestedPageIndex = pageIndex;
    _requestedPageStackIndex = stackIndex;
  }

  void clearPageAndStackIndex() {
    _requestedPageIndex = null;
    _requestedPageStackIndex = null;
  }
}
