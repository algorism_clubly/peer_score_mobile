import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/utils/api/methods.dart';
import 'package:peer_score/utils/api/urls.dart';
import 'package:peer_score/utils/socket.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserProvider extends ChangeNotifier {
  List<Map<String, dynamic>> _alternatePhoneNumbers = [];
  static String tempUserId;
  Function profilePageCb;

  List<Map<String, dynamic>> get alternatePhoneNumbers {
    return _alternatePhoneNumbers;
  }

  Future<Map<String, dynamic>> get userData async {
    final sharedPrefs = await SharedPreferences.getInstance();
    var getUserData = sharedPrefs.containsKey("userData")
        ? json.decode(sharedPrefs.get("userData")) as Map<String, dynamic>
        : null;
    //little delay here for waiting screen
    if (getUserData == null) {
      return Future.delayed(
        Duration(seconds: 1),
        () {
          return null;
        },
      );
    } else {
      return json.decode(sharedPrefs.get("userData")) as Map<String, dynamic>;
    }
  }

  Future<Map<String, dynamic>> updateUserInfo({dynamic body}) async {
    try {
      var response =
          await UrlMethods.postRequest(url: Urls.updateUserInfo, body: body);
      final sharedPrefs = await SharedPreferences.getInstance();
      sharedPrefs.setString("userData", json.encode(response["data"]));
      return response;
    } catch (err) {
      return err;
    }
  }

  
  Future<Map<String, dynamic>> uploadProfilePicture({dynamic body}) async {
    try {
      var response = await UrlMethods.postRequest(
          url: Urls.uploadProfilePicture, body: body);

      final sharedPrefs = await SharedPreferences.getInstance();
      sharedPrefs.setString("userData", json.encode(response["data"]));
      // notifyListeners();
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> getOtpForAlternatePhone({dynamic body}) async {
    try {
      var response = await UrlMethods.postRequest(
          url: Urls.getOtpForAlternatePhone, body: body);
      // notifyListeners();

      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> getOtpForPinReset({dynamic body}) async {
    try {
      var response =
          await UrlMethods.postRequest(url: Urls.getOtpForPinReset, body: body);
      // notifyListeners();

      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> getAllAlternatePhones({String userId}) async {
    try {
      var response =
          await UrlMethods.getRequest(url: Urls.getAllAlternatePhones(userId));
      // var response = await UrlMethods.getRequest(
      //     url: "http://10.0.2.2:3030/alternate_phone/get-all/$userId");
      var alternameNumbers = response["data"];
      List<Map<String, dynamic>> tempAlternameNumbers = [];
      alternameNumbers.forEach((number) {
        tempAlternameNumbers.add(number);
      });
      _alternatePhoneNumbers = tempAlternameNumbers;
      // notifyListeners();
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> createAlternatePhone({dynamic body}) async {
    try {
      var response = await UrlMethods.postRequest(
        url: Urls.createAlternatePhone,
        body: body,
      );
      getAllAlternatePhones(userId: body["userId"]);
      // notifyListeners();
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> deleteAlternatePhone({dynamic body}) async {
    try {
      var response = await UrlMethods.postRequest(
          url: Urls.deleteAlternatePhone, body: body);
      getAllAlternatePhones(userId: body["userId"]);
      // notifyListeners();
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> login({dynamic body}) async {
    try {
      var response = await UrlMethods.postRequest(url: Urls.login, body: body);
      
      final sharedPrefs = await SharedPreferences.getInstance();
      var currentUserData = await userData;

      sharedPrefs.setString("userData", json.encode(response["data"]["data"]));
      sharedPrefs.setString("token", response["data"]["token"]);

      //check if new different user and clear saved contact

      //
      //
      //
      if (currentUserData != null &&
          (currentUserData["phone"] != body["phone"])) {
        if (sharedPrefs.containsKey("contacts")) {
          sharedPrefs.remove("contacts");
        }
        if (sharedPrefs.containsKey("notifications")) {
          sharedPrefs.remove("notifications");
        }
      }
      // notifyListeners();
      return response;
    } catch (err) {
      
      return err;
    }
  }

  void logout() async {
    try {
      final sharedPrefs = await SharedPreferences.getInstance();
      sharedPrefs.setString("token", "-");
      sharedPrefs.remove("notifications");
      //set the genralscaffoldkey to null
      GeneralProvider.scaffoldKeyRef = null;
      SocketConnection.getInstance().destroySocket();
      notifyListeners();
    } catch (err) {}
  }

  Future<bool> verifyToken() async {
    return Future.delayed(Duration(seconds: 2), () async {
      try {
        final sharedPrefs = await SharedPreferences.getInstance();
        var token = sharedPrefs.get("token");
        var response = await UrlMethods.postRequest(
            url: Urls.verifyToken, body: {"token": token});

        return true;
      } catch (err) {
        return false;
      }
    });
  }

  Future<bool> getUserId({String phoneNumber}) async {
    try {
      var response = await UrlMethods.postRequest(
          url: Urls.getUserId, body: {"phone": phoneNumber});

      tempUserId = response["data"]["id"];
      // notifyListeners();
      return true;
    } catch (err) {
      return false;
    }
  }

  Future<Map<String, dynamic>> getUpdatedUserData({String userId}) async {
    try {
      var response = await UrlMethods.postRequest(
          url: Urls.getUserData, body: {"userId": userId});
      final sharedPrefs = await SharedPreferences.getInstance();
      sharedPrefs.setString("userData", json.encode(response["data"]));
      return response;
    } catch (err) {
      return err;
    }
  }

  Future<Map<String, dynamic>> updatePassword(
      {String pin, String userId}) async {
    try {
      var response = await UrlMethods.postRequest(
        url: Urls.updatePassword,
        body: {
          "userId": userId,
          "password": pin,
        },
      );
      // notifyListeners();
      return response;
    } catch (err) {
      return err;
    }
  }
}
