import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:peer_score/providers/contact.dart';
import 'package:peer_score/providers/expected_payments_provider.dart';
import 'package:peer_score/utils/api/methods.dart';
import 'package:peer_score/utils/api/urls.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Claim {
  final String id;
  final Contact winessProvider;
  final Contact claimInitiator;
  final Contact witness;
  final Loan loan;
  final String amount;
  // final DateTime date;
  final String type;
  final String verdict;
  Claim({
    this.winessProvider,
    this.claimInitiator,
    this.witness,
    this.id,
    this.type,
    this.amount,
    this.loan,
    this.verdict
  });
}

class WitnesProvider extends ChangeNotifier {
  List<Claim> _claims = [];
  static Claim selectedClaim;
  List<Claim> get claims {
    return _claims;
  }

  Future<Map<String, dynamic>> getClaims() async {
    try {
      final sharedPrefs = await SharedPreferences.getInstance();
      final userData = json.decode(sharedPrefs.get("userData"));

      var response =
          await UrlMethods.getRequest(url: Urls.getClaimsList(userData["id"]));
      
      
      var list = response["data"];
      List<Claim> tempList = [];
      if (list.length > 0) {
        list.forEach(
          (claim) {
            Map<String, dynamic> witnessProvider;
            Map<String, dynamic> claimInitiator;
            if (claim["type"] == "insist") {
              witnessProvider = claim["provider"];
              claimInitiator = claim["reciever"];
            } else {
              witnessProvider = claim["reciever"];
              claimInitiator = claim["provider"];
            }
            tempList.add(
              Claim(
                  id: claim["id"],
                  winessProvider: Contact(
                    id: witnessProvider["id"],
                    firstName: witnessProvider["first_name"],
                    lastName: witnessProvider["last_name"],
                    otherName: witnessProvider["other_name"],
                    profilePicture: witnessProvider["profile_picture"],
                    peerScore: "${witnessProvider["peer_score"]}",
                    compliance: "${witnessProvider["compliance"]}",
                    email: witnessProvider["email"],
                    phoneNumber: HelperFunctions.deserializePhoneNumber(
                      phoneNumber: witnessProvider["phone"],
                    ),
                  ),
                  claimInitiator: Contact(
                    id: claimInitiator["id"],
                    firstName: claimInitiator["first_name"],
                    lastName: claimInitiator["last_name"],
                    otherName: claimInitiator["other_name"],
                    profilePicture: claimInitiator["profile_picture"],
                    peerScore: "${claimInitiator["peer_score"]}",
                    compliance: "${claimInitiator["compliance"]}",
                    email: claimInitiator["email"],
                    phoneNumber: HelperFunctions.deserializePhoneNumber(
                      phoneNumber: claimInitiator["phone"],
                    ),
                  ),
                  type: claim["type"],
                  loan: Loan(
                    id: claim["loan"]["id"],
                    amount: claim["loan"]["amount"],
                    date: DateFormat(
                      "yyyy-mm-dd",
                    ).parse(claim["loan"]["due_date"]).add(
                          Duration(days: 1),
                        ),
                  )
                  // amount: claim["amount"],
                  // date: DateFormat(
                  //   "yyyy-mm-dd",
                  // ).parse(loan["due_date"]).add(
                  //       Duration(days: 1),
                  //     ),
                  // loanActions: loan["loan_action"],
                  ),
            );
          },
        );
      }
      
      
      _claims = tempList;
      // notifyListeners();
      return response;
    } catch (err) {
      
      
      return err;
    }
  }

  Future<Map<String, dynamic>> passVerdict({String verdict}) async {
    try {
      var response = await UrlMethods.postRequest(
          url: Urls.passVerdict,
          body: {"verdict": verdict, "verdict_id": selectedClaim.id});
      return response;
    } catch (err) {
      return err;
    }
  }
}
