import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/providers/user-provider.dart';
import 'package:peer_score/screens/main_screen.dart';
import 'package:peer_score/screens/otp.dart';
import 'package:peer_score/screens/phone.dart';
import 'package:peer_score/providers/colors.dart';
import 'package:peer_score/utils/contry_codes.dart';
import 'package:peer_score/utils/custom_raised_button.dart';
import 'package:peer_score/utils/dropdown_field.dart';
import 'package:peer_score/utils/enums.dart';
import 'package:peer_score/utils/err_text_widget.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:peer_score/utils/main_nav_bar.dart';
import 'package:peer_score/utils/progress_indicator.dart';
import 'package:peer_score/utils/socket.dart';
import 'package:peer_score/utils/text_form_field.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  static const String routeName = "/login";
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool isLightMode = GeneralProvider.isLightMode;
  TextEditingController _phoneNumberController;
  FocusNode _phoneNumberFocusNode;
  TextEditingController _pinController;
  FocusNode _pinFocusNode;
  bool isSending = false;
  bool isBooting = false;
  String defaultValue = "+234";
  String _phoneErrText = "";
  bool _phoneErrVisible = false;
  String _pinErrText = "";
  bool _pinErrVisible = false;
  String _submitErrText = "";
  bool _submitErrVisible = false;
  Map<String, dynamic> userData;
  var _connectionStatusSubscription;

  @override
  void initState() {
    super.initState();
    _connectionStatusSubscription =
        HelperFunctions.subscribeToNetworkNotifier(context: context);
    _phoneNumberController = TextEditingController();
    _phoneNumberFocusNode = FocusNode();
    _pinController = TextEditingController();
    _pinFocusNode = FocusNode();
    _phoneNumberController.addListener(() {
      if (_phoneNumberController.text.length > 0) {
        setState(() {
          _phoneErrText = "";
          _phoneErrVisible = false;
        });
      }
    });
    _pinController.addListener(() {
      if (_pinController.text.length > 0) {
        setState(() {
          _pinErrText = "";
          _pinErrVisible = false;
        });
      }
    });
    Provider.of<UserProvider>(context, listen: false).userData.then((data) {
      setState(() {
        userData = data;
      });
    });
  }

  void _onCountryCodeChanged(String value) {
    setState(() {
      defaultValue = value;
    });
  }

  void _onSubmit() async {
    var phoneValidateResult =
        HelperFunctions.validatePhone(phoneNumber: _phoneNumberController.text);
    if (!phoneValidateResult["status"]) {
      setState(() {
        _phoneErrText = phoneValidateResult["message"];
        _phoneErrVisible = true;
      });
      return;
    }

    var pinValidateResult =
        HelperFunctions.validatePin(pin: _pinController.text);
    if (!pinValidateResult["status"]) {
      setState(() {
        _pinErrText = pinValidateResult["message"];
        _pinErrVisible = true;
      });
      return;
    }
    setState(() {
      _phoneNumberFocusNode.unfocus();
      _pinFocusNode.unfocus();
      isSending = true;
    });
    var phoneNumber = HelperFunctions.serializePhoneNumber(
      phoneNumber: _phoneNumberController.text,
    );
    var sendResponse = await Provider.of<UserProvider>(context, listen: false)
        .login(body: {"phone": phoneNumber, "password": _pinController.text});

    if (sendResponse["status"] == 200 || sendResponse["status"] == 201) {
      setState(() {
        isSending = false;
        isBooting = true;
      });
      //detect if different user
      if (userData != null) {
        String oldUserPhone = HelperFunctions.serializePhoneNumber(
          phoneNumber: userData["phone"],
        );
        if (oldUserPhone != phoneNumber) {
          GeneralProvider.isNewUser = true;
          HelperFunctions.setAutoSync(value: false);
        }
      }

      SocketConnection.getInstance().startSocket(userId: sendResponse["data"]["data"]["id"]);
      SocketConnection.getInstance().updateUnreadNotificationCount();
      await HelperFunctions.startupApiCalls(context: context);
      Navigator.of(context).pushReplacementNamed(MainScreen.routeName);
    } else {
      setState(() {
        isSending = false;
        _submitErrText = sendResponse["errors"];
        _submitErrVisible = true;
      });
    }
  }

  void _forgotPin() async {
    var phoneValidateResult =
        HelperFunctions.validatePhone(phoneNumber: _phoneNumberController.text);
    if (!phoneValidateResult["status"]) {
      setState(() {
        _phoneErrText = phoneValidateResult["message"];
        _phoneErrVisible = true;
      });
      return;
    }
    setState(() {
      _phoneNumberFocusNode.unfocus();
      _pinFocusNode.unfocus();
    });
    var phoneNumber = HelperFunctions.serializePhoneNumber(
      phoneNumber: _phoneNumberController.text,
    );
    Provider.of<UserProvider>(context, listen: false)
        .getUserId(phoneNumber: phoneNumber);
    //get pin reset otp here
    Provider.of<UserProvider>(context, listen: false).getOtpForPinReset(
      body: {"phone": phoneNumber},
    );
    GeneralProvider.resetPasswordInitilzationPage = LoginScreen.routeName;
    Navigator.of(context).pushReplacementNamed(OtpScreen.routeName);
  }

  @override
  void dispose() {
    super.dispose();
    _phoneNumberController.dispose();
    _phoneNumberFocusNode.dispose();
    _pinController.dispose();
    _pinFocusNode.dispose();
    _connectionStatusSubscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    var height = HelperFunctions.getDeviceUsabaleHeigth(context: context);
    var appColors =  Provider.of<AppColorsProvider>(context, listen: false);
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          // physics: NeverScrollableScrollPhysics(),
          child: Stack(
            children: <Widget>[
              Container(
                height: height,
                width: double.infinity,
                padding: EdgeInsets.symmetric(
                  horizontal: 30,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 20),
                    HelperFunctions.buildLogo(context: context),
                    Spacer(),
                    // SizedBox(height: 100),
                    Text(
                      "Welcome to PeerScore",
                      style: Theme.of(context)
                          .textTheme
                          .body2
                          .copyWith(fontSize: 20, height: 0.74),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      "Enter your phone number and 6 digits pin to access your account",
                      style: Theme.of(context).textTheme.body1.copyWith(
                            fontWeight: FontWeight.w300,
                            // fontSize: 11,
                          ),
                    ),
                    SizedBox(
                      height: 18,
                    ),
                    Text(
                      "Phone number",
                      style: Theme.of(context).textTheme.body2.copyWith(
                            fontWeight: FontWeight.normal,
                            height: 0.73,
                          ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    DropDownField(
                      textEditingController: _phoneNumberController,
                      focusNode: _phoneNumberFocusNode,
                      textInputType: TextInputType.number,
                      defaultItem: defaultValue,
                      onChanged: (value) => _onCountryCodeChanged(value),
                      items: ["+234"],
                      hintText: "Phone number",
                      height: 40,
                      onTap: () {
                        setState(() {
                          _phoneErrText = "";
                          _phoneErrVisible = false;
                        });
                      },
                      inputFormatters: [
                        WhitelistingTextInputFormatter.digitsOnly,
                      ],
                    ),
                    ErrText(
                      errText: _phoneErrText,
                      vissibility: _phoneErrVisible,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      "Pin",
                      style: Theme.of(context).textTheme.body2.copyWith(
                            fontWeight: FontWeight.normal,
                            height: 0.73,
                          ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    CustomTextField(
                      hintText: "6-digit pin",
                      height: 40,
                      textInputType: TextInputType.number,
                      obscureText: true,
                      textEditingController: _pinController,
                      focusNode: _pinFocusNode,
                      onChanged: (value) {
                        if (value.length >= 6) {
                          var pin = _pinController.text;
                          pin = pin.substring(0, 6);
                          _pinController.text = pin;
                          _pinFocusNode.unfocus();
                        }
                      },
                      inputFormatters: [
                        WhitelistingTextInputFormatter.digitsOnly,
                        LengthLimitingTextInputFormatter(6)
                      ],
                    ),
                    ErrText(
                      errText: _pinErrText,
                      vissibility: _pinErrVisible,
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.bottomRight,
                          child: InkWell(
                            onTap: () {
                              return isSending
                                  ? () {}
                                  : Navigator.of(context).pushReplacementNamed(
                                      PhoneNumberScreen.routeName,
                                      arguments: {"isSlideBackward": false});
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(3),
                              child: Text(
                                "Sign up",
                                style:
                                    Theme.of(context).textTheme.body1.copyWith(
                                          fontWeight: FontWeight.normal,
                                          color: Theme.of(context).primaryColor,
                                        ),
                              ),
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.bottomRight,
                          child: InkWell(
                            onTap: () {
                              return isSending ? () {} : _forgotPin();
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(3),
                              child: Text(
                                "Forgot pin?",
                                style: Theme.of(context)
                                    .textTheme
                                    .body1
                                    .copyWith(
                                        fontWeight: FontWeight.normal,
                                        color: Theme.of(context).primaryColor),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    ErrText(
                      errText: _submitErrText,
                      vissibility: _submitErrVisible,
                    ),

                    Spacer(
                      flex: 1,
                    ),
                    // SizedBox(height: 60),
                    Container(
                      margin: EdgeInsets.only(bottom: 30),
                      child: CustomRaisedButton(
                        title: isSending ? "" : "Login",
                        onTapAction: () {
                          isSending ? () {} : _onSubmit();
                        },
                        indicator: isSending
                            ? CustomProgressIndicator(
                                // color: appColors.primaryProgressValueColor,
                              )
                            : AnimatedContainer(
                                duration: Duration(milliseconds: 200),
                                child: Container(
                                  margin: EdgeInsets.only(left: 8),
                                  child: FaIcon(
                                    FontAwesomeIcons.lightSignIn,
                                    color: appColors.buttonIconColor,
                                    size: 15,
                                  ),
                                ),
                              ),
                      ),
                    ),
                    Spacer(
                      flex: 3,
                    ),
                  ],
                ),
              ),
              if (isBooting)
                HelperFunctions.loadingScreenAfterLogin(context: context),
            ],
          ),
        ),
      ),
    );
  }
}
