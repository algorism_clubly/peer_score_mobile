import 'package:flutter/material.dart';
import 'package:peer_score/providers/colors.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/utils/connectivity_singleton.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:peer_score/utils/snackbar.dart';
import 'package:peer_score/widgets/calender/loan_calender.dart';
import 'package:peer_score/widgets/contacts/contacts_stack.dart';
import 'package:peer_score/widgets/dashboard/dashboard_stack.dart';
import 'package:peer_score/widgets/debts/debts_stack.dart';
import 'package:peer_score/widgets/expected_payment/expected_payment_stack.dart';
import 'package:provider/provider.dart';

class MainScreen extends StatefulWidget {
  static const routeName = '/main';
  final int mainPageIndex;
  final int subPageIndex;
  MainScreen({this.mainPageIndex = 0, this.subPageIndex = 0});
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  bool isLightMode = GeneralProvider.isLightMode;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  PageController pageController;

  var currentPage = 0;
  int _pageStackIntialIndex = 0;
  var _connectionStatusSubscription;
  @override
  void initState() {
    pageController = PageController(
      initialPage: widget.mainPageIndex,
    );
    currentPage = widget.mainPageIndex;
    _pageStackIntialIndex = widget.subPageIndex;

    Provider.of<GeneralProvider>(context, listen: false)
        .setScaffoldKey(_scaffoldKey);
    GeneralProvider.switchMainPage = switchMainPage;
    _connectionStatusSubscription =
        HelperFunctions.subscribeToNetworkNotifier(context: context);
    // switchMainPage(
    //     index: widget.mainPageIndex, pageStackIntialIndex: widget.subPageIndex);
    super.initState();
  }

  void switchMainPage({int index, int pageStackIntialIndex = 0}) {
    if (GeneralProvider.currentFocusedNode != null) {
      GeneralProvider.currentFocusedNode.unfocus();
    }
    GeneralProvider.currentSelectedPageIndex = index;
    setState(() {
      currentPage = index;
      _pageStackIntialIndex = pageStackIntialIndex;
      pageController.jumpToPage(index);
    });
  }

  Widget buildBottomNavIcon(
      {String imagePath, int pageNumber, String caption}) {
    var appColors = Provider.of<AppColorsProvider>(context);
    return InkWell(
      splashColor: Colors.grey[850],
      focusColor: Colors.grey[850],
      customBorder: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(50),
      ),
      onTap: () {
        switchMainPage(index: pageNumber);
        GeneralProvider.switchSubStackIndex();
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            // margin: EdgeInsets.only(bottom: 1),
            height: 22,
            child: Image.asset(
              imagePath,
              color: pageNumber == currentPage
                  ? appColors.navBarSelected
                  : appColors.navBarUnselected,
              // color: Colors.white,
              width: 22,
            ),
          ),
          SizedBox(
            height: 0.7,
          ),
          Text(
            caption,
            style: Theme.of(context).textTheme.body1.copyWith(
                  fontWeight: FontWeight.w300,
                  color: pageNumber == currentPage
                      ? appColors.navBarSelected
                      : appColors.navBarUnselected,
                  // color: Colors.white,
                ),
          )
        ],
      ),
    );
  }

  @override
  void dispose() {
    _connectionStatusSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var height = HelperFunctions.getDeviceUsabaleHeigth(context: context);
    // final requestedPageIndex =
    //     Provider.of<GeneralProvider>(context, listen: false).getPageAndStackIndex["pageIndex"];
    // if (requestedPageIndex != null) {
    //   setState(() {
    //     currentPage = requestedPageIndex;
    //     pageController.jumpToPage(currentPage);
    //   });
    // }

    return Scaffold(
      key: _scaffoldKey,
      body: SafeArea(
          child: SingleChildScrollView(
        child: Container(
          height: height,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Expanded(
                child: PageView(
                  controller: pageController,
                  physics: NeverScrollableScrollPhysics(),
                  children: <Widget>[
                    DashBoardStack(initialIndex: _pageStackIntialIndex),
                    ExpectedPaymentStack(
                      initialIndex: _pageStackIntialIndex,
                      // switchMainPage: (index) => switchMainPage(index: index),
                    ),
                    DebtStack(
                        // switchMainPage: (index) => switchMainPage(index: index),
                        ),
                    ContactListStack(
                        // switchMainPage: (index) => switchMainPage(index: index),
                        ),
                    LoanCalender()
                  ],
                ),
              ),
              // Divider(
              //   indent: 0,
              //   endIndent: 0,
              //   thickness: 0.5,
              //   color: Colors.white,
              // ),
              Container(
                height:
                    Provider.of<GeneralProvider>(context).getBottomNavHeight,
                child: Stack(
                  children: <Widget>[
                    // if (!isLightMode)
                    //   Container(
                    //     decoration: BoxDecoration(
                    //         border: Border(
                    //             top: BorderSide(
                    //       width: 0.2,
                    //       color: Colors.white,
                    //     ))),
                    //   ),
                    Container(
                      // padding: EdgeInsets.symmetric(vertical: 5),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(25),
                            topRight: Radius.circular(25)),
                        color: GeneralProvider.isLightMode
                            ? Theme.of(context).primaryColor
                            : Colors.white.withOpacity(0.12),
                      ),
                      child: Row(
                        // mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          // buildBottomNavIcon(
                          //     imagePath: 'public/images/profile.png',
                          //     pageNumber: 0,
                          //     caption: "Profile"),
                          Expanded(
                            child: buildBottomNavIcon(
                                imagePath: 'public/images/home.png',
                                pageNumber: 0,
                                caption: "Home"),
                          ),
                          Expanded(
                            child: buildBottomNavIcon(
                                imagePath: 'public/images/money-in.png',
                                pageNumber: 1,
                                caption: "Debtors"),
                          ),
                          Expanded(
                            child: buildBottomNavIcon(
                                imagePath: 'public/images/money-out.png',
                                pageNumber: 2,
                                caption: "Creditors"),
                          ),
                          Expanded(
                            child: buildBottomNavIcon(
                              imagePath:
                                  'public/images/empty-lists/contact.png',
                              caption: "Contacts",
                              pageNumber: 3,
                            ),
                          ),
                          Expanded(
                            child: buildBottomNavIcon(
                              imagePath: 'public/images/calender_new.png',
                              caption: "Calendar",
                              pageNumber: 4,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      )),
    );
  }
}
