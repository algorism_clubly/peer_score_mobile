import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:peer_score/providers/colors.dart';
import 'package:peer_score/providers/onboading_provider.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/screens/main_screen.dart';
import 'package:peer_score/utils/custom_raised_button.dart';
import 'package:peer_score/utils/err_text_widget.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:peer_score/utils/progress_indicator.dart';
import 'package:peer_score/utils/socket.dart';
import 'package:peer_score/utils/text_form_field.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NameScreen extends StatefulWidget {
  static const String routeName = '/name';
  @override
  _NameScreenState createState() => _NameScreenState();
}

class _NameScreenState extends State<NameScreen> {
  FocusNode _nameFocusNode;
  TextEditingController _nameController;
  Map<String, dynamic> _onboardingUserData = {};
  String _submitErrText = "";
  bool _submitErrVisible = false;
  bool isSending = false;
  bool isBooting = false;
  var _connectionStatusSubscription;
  @override
  void initState() {
    super.initState();
    _connectionStatusSubscription =
        HelperFunctions.subscribeToNetworkNotifier(context: context);
    _nameFocusNode = FocusNode();
    _nameController = TextEditingController();

    _nameController.addListener(() {
      if (_nameController.text.length > 0) {
        setState(() {
          _submitErrText = "";
          _submitErrVisible = false;
        });
      }
    });

    SharedPreferences.getInstance().then((sharedPrefs) {
      _onboardingUserData =
          json.decode(sharedPrefs.get("userData")) as Map<String, dynamic>;
    });
  }

  @override
  void dispose() {
    super.dispose();
    _nameController.dispose();
    _nameFocusNode.dispose();
    _connectionStatusSubscription.cancel();
  }

  void _onSubmit() async {
    setState(() {
      _submitErrText = "";
      _submitErrVisible = false;
      isSending = true;
    });
    if (_nameController.text.isEmpty) {
      setState(() {
        _submitErrText = "Please provide your first name";
        _submitErrVisible = false;
        isSending = false;
      });
      return;
    }
    final updateNameResp =
        await Provider.of<OnboardingProvider>(context, listen: false)
            .updateFirstName(
      body: {
        "userId": _onboardingUserData["id"],
        "first_name": _nameController.text
      },
    );
    if (updateNameResp["status"] == 200 || updateNameResp["status"] == 201) {
      setState(() {
        isSending = false;
        isBooting = true;
      });

      GeneralProvider.isNewUser = true;
      HelperFunctions.setAutoSync(value: false);
      SocketConnection.getInstance().startSocket(userId: _onboardingUserData["id"]);
      SocketConnection.getInstance().updateUnreadNotificationCount();
      await HelperFunctions.startupApiCalls(context: context);
      Navigator.of(context).pushReplacementNamed(MainScreen.routeName);
    } else {
      setState(() {
        _submitErrText = updateNameResp["errors"];
        _submitErrVisible = true;
        isSending = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var height = HelperFunctions.getDeviceUsabaleHeigth(context: context);

    var appColors = Provider.of<AppColorsProvider>(context, listen: false);
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Stack(
            children: <Widget>[
              Container(
                height: height,
                width: double.infinity,
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    // ConstrainedBox(
                    //   constraints: BoxConstraints(
                    //     minHeight: 70,
                    //     maxHeight: 150,
                    //   ),
                    // ),
                    Spacer(
                      flex: 1,
                    ),
                    Center(
                      child: Text("Let's Know you",
                          style: Theme.of(context)
                              .textTheme
                              .body2
                              .copyWith(fontSize: 20, height: 0.74)),
                    ),
                    Spacer(
                      flex: 1,
                    ),
                    Text(
                      "Hey! PeerScore needs a way to identify you",
                      style: Theme.of(context).textTheme.body2.copyWith(
                            color: Theme.of(context).primaryColor,
                            fontWeight: FontWeight.normal,
                          ),
                    ),
                    SizedBox(height: 8),
                    Text(
                      "PeerScore would like to meet you.",
                      style: Theme.of(context).textTheme.body1.copyWith(
                            color: Color(0xFF979696),
                            fontWeight: FontWeight.bold,
                          ),
                    ),
                    SizedBox(
                      height: 45,
                    ),
                    CustomTextField(
                      height: 40,
                      hintText: "Enter first name",
                      textEditingController: _nameController,
                      inputFormatters: [
                        WhitelistingTextInputFormatter(
                          RegExp("[a-zA-Z]"),
                        ),
                      ],
                    ),
                    ErrText(
                      errText: _submitErrText,
                      vissibility: _submitErrVisible,
                    ),
                    SizedBox(height: 50),
                    CustomRaisedButton(
                      title: isSending ? "" : "Complete",
                      color: appColors.primaryButtonColorText,
                      bgColor: appColors.primaryButtonColorBg,
                      indicator: isSending
                          ? CustomProgressIndicator(
                              color: appColors.primaryProgressValueColor,
                            )
                          : Text(""),
                      onTapAction: () {
                        isSending ? null : _onSubmit();
                      },
                    ),
                    Spacer(
                      flex: 3,
                    ),
                  ],
                ),
              ),
              if (isBooting)
                HelperFunctions.loadingScreenAfterLogin(context: context),
            ],
          ),
        ),
      ),
    );
  }
}
