import 'dart:convert';

import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:peer_score/main.dart';
import 'package:peer_score/providers/colors.dart';
import 'package:peer_score/providers/contact.dart';
import 'package:peer_score/providers/notifications_provider.dart' as Note;
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/providers/user-provider.dart';
import 'package:peer_score/utils/custom_switch.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:peer_score/utils/main_nav_bar.dart';
import 'package:peer_score/utils/modal_actions.dart';
import 'package:peer_score/utils/notification_utils.dart';
import 'package:peer_score/utils/progress_indicator.dart';
import 'package:peer_score/utils/snackbar.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NotificationScreen extends StatefulWidget {
  static const String routeName = "/notification-screen";
  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  bool isLoading = false;
  bool pullRefresh = false;
  bool isArchived = false;
  bool isFetchingNew = false;
  bool markingAllOrArchiving = false;
  List<Note.Notification> notifications = [];
  Map<String, dynamic> userData = {"first_name": ""};
  String notificationIdFromDeviceTray;
  @override
  void initState() {
    super.initState();
    BackButtonInterceptor.add(myInterceptor);
    flutterLocalNotificationsPlugin.cancelAll();
    Note.NotificationProvider.isOpened = true;
    if (this.mounted) {
      setState(() {
        isLoading = true;
        notificationIdFromDeviceTray =
            Note.NotificationProvider.notificationIdFromDeviceTray;
      });
    }
    HelperFunctions.openNotificationModalWhenClickedFromTray =
        automaticllyOpenNotificationModal;
    Provider.of<UserProvider>(context, listen: false).userData.then((data) {
      if (this.mounted) {
        setState(() {
          userData = data;
          getNotifications();
          SchedulerBinding.instance.addPostFrameCallback((_) {
            if (notificationIdFromDeviceTray != null) {
              automaticllyOpenNotificationModal();
            }
          });
        });
      }
    });
  }

  void onBackButton() {
    Navigator.of(context).pop();
    Note.NotificationProvider.isOpened = false;
    GeneralProvider.widgetType = null;
  }

  bool myInterceptor(bool stopDefaultButtonEvent) {
    onBackButton();
    return true;
  }

  Future getNotifications({bool isArchive = false}) async {
    if (this.mounted) {
      if (this.mounted) {
        setState(() {
          isLoading = true;
        });
      }
      //device storage option//

      try {
        final sharedPrefs = await SharedPreferences.getInstance();
        var notificationsData = json.decode(sharedPrefs.get("notifications"));
        List<Note.Notification> tempNotifications = [];
        notificationsData.forEach((notification) {
          tempNotifications.add(
            NotificationUtils.getFullNotificationObject(
              notification: notification,
            ),
          );
        });
        if (this.mounted) {
          if (tempNotifications.length > 0) {
            setState(() {
              isLoading = false;
              isFetchingNew = false;
              notifications = tempNotifications;

              // for archived notifications here
              if (isArchive) {
                notifications.retainWhere((item) => item.archived == true);
              } else {
                notifications.retainWhere((item) => item.archived == false);
              }
              notifications.sort(
                (a, b) => b.createdDate.difference(a.createdDate).inSeconds,
              );
            });
          }

          final getNotificationsResponse =
              await Provider.of<Note.NotificationProvider>(context,
                      listen: false)
                  .getNotifications(isArchive: isArchive);
          if (getNotificationsResponse["status"] == 200 ||
              getNotificationsResponse["status"] == 201) {
            if (this.mounted) {
              setState(() {
                isLoading = false;
                isFetchingNew = false;
                notifications = Provider.of<Note.NotificationProvider>(context,
                        listen: false)
                    .notifications;
                notifications.sort((a, b) =>
                    b.createdDate.difference(a.createdDate).inSeconds);
              });
            }
          }
        }
      } catch (err) {
        if (this.mounted) {
          setState(() {
            isLoading = false;
          });
        }
        CustomSnackBar.snackBar(
          title: "something went wrong please try again",
          scaffoldState: _scaffoldKey,
          context: context,
        );
      }

      //fresh list option from db//

      // final getNotificationsResponse =
      //     await Provider.of<Note.NotificationProvider>(context, listen: false)
      //         .getNotifications(isArchive: isArchive);
      // if (getNotificationsResponse["status"] == 200 ||
      //     getNotificationsResponse["status"] == 201) {
      //   if (this.mounted) {
      //     setState(() {
      //       isLoading = false;
      //       notifications =
      //           Provider.of<Note.NotificationProvider>(context, listen: false)
      //               .notifications;
      //       notifications.sort(
      //           (a, b) => b.createdDate.difference(a.createdDate).inSeconds);
      //     });
      //   }
      // } else {
      //   if (this.mounted) {
      //     setState(() {
      //       isLoading = false;
      //     });
      //   }
      //   CustomSnackBar.snackBar(
      //     title: getNotificationsResponse["errors"],
      //     scaffoldState: _scaffoldKey,
      //     context: context,
      //   );
      // }
    }
  }

  void automaticllyOpenNotificationModal() async {
    //get updated list first
    await getNotifications();
    if (GeneralProvider.isModalOpened) {
      Navigator.of(context).pop();
    }
    var notificationIdFromDeviceTray =
        Note.NotificationProvider.notificationIdFromDeviceTray;
    var notification = notifications.firstWhere((notification) {
      return notification.id == notificationIdFromDeviceTray;
    }, orElse: () {});
    var notificationIndex = notifications.indexWhere((notification) {
      return notification.id == notificationIdFromDeviceTray;
    });
    if (notification != null && notificationIndex != null) {
      onNotificationClicked(
        title: notification.title,
        type: notification.type,
        index: notificationIndex,
      );
      markAsRead(notificationId: notification.id, index: notificationIndex);
    }
    Note.NotificationProvider.notificationIdFromDeviceTray = null;
  }

  void markAsRead({String notificationId, int index}) async {
    if (this.mounted) {
      final markAsReadresponse =
          await Provider.of<Note.NotificationProvider>(context, listen: false)
              .markNotificationAsSeen(
                  body: {"notification_id": notificationId});
      if (markAsReadresponse["status"] == 200 ||
          markAsReadresponse["status"] == 201) {
        if (this.mounted) {
          setState(() {
            notifications[index].seen = true;
            if (!isArchived) {}
          });
        }
      } else {
        CustomSnackBar.snackBar(
          title: markAsReadresponse["errors"],
          scaffoldState: _scaffoldKey,
          context: context,
        );
      }
    }
  }

  void archiveOperation({String notificationId, int index}) async {
    if (this.mounted) {
      final response = isArchived
          ? await Provider.of<Note.NotificationProvider>(context, listen: false)
              .unArchiveNotification(
              body: {"notification_id": notificationId},
            )
          : await Provider.of<Note.NotificationProvider>(context, listen: false)
              .markNotificationAsArchive(
              body: {"notification_id": notificationId},
            );
      if (response["status"] == 200 || response["status"] == 201) {
        if (this.mounted) {
          setState(() {
            notifications.removeAt(index);
          });
        }
      } else {
        CustomSnackBar.snackBar(
          title: response["errors"],
          scaffoldState: _scaffoldKey,
          context: context,
        );
      }
    }
  }

  void archiveAll() async {
    if (this.mounted) {
      if (this.mounted) {
        setState(() {
          markingAllOrArchiving = true;
        });
      }
      final archiveAllResponse =
          await Provider.of<Note.NotificationProvider>(context, listen: false)
              .archiveAll(body: {"userId": userData["id"]});
      if (archiveAllResponse["status"] == 200 ||
          archiveAllResponse["status"] == 201) {
        if (this.mounted) {
          setState(() {
            notifications = [];
            markingAllOrArchiving = false;
          });
        }
      } else {
        if (this.mounted) {
          setState(() {
            markingAllOrArchiving = false;
          });
        }
        CustomSnackBar.snackBar(
          title: archiveAllResponse["errors"],
          scaffoldState: _scaffoldKey,
          context: context,
        );
      }
    }
  }

  void markAllAsRead() async {
    if (this.mounted) {
      if (this.mounted) {
        setState(() {
          markingAllOrArchiving = true;
        });
      }
      final archiveAllResponse =
          await Provider.of<Note.NotificationProvider>(context, listen: false)
              .readAll(body: {"userId": userData["id"]});
      if (archiveAllResponse["status"] == 200 ||
          archiveAllResponse["status"] == 201) {
        if (this.mounted) {
          setState(() {
            markingAllOrArchiving = false;
            notifications = notifications.map((item) {
              item.seen = true;
              return item;
            }).toList();
          });
        }
      } else {
        if (this.mounted) {
          setState(() {
            markingAllOrArchiving = false;
          });
        }
        CustomSnackBar.snackBar(
          title: archiveAllResponse["errors"],
          scaffoldState: _scaffoldKey,
          context: context,
        );
      }
    }
  }

  void onArchiveClick() {
    getNotifications(isArchive: isArchived);
  }

  void onNotificationClicked({String title, String type, int index}) {
    Map<String, dynamic> contentObj;
    String content;
    contentObj = NotificationUtils.getNotificationBody(
        notification: notifications[index], userId: userData["id"]);
    content = contentObj["content"];
    ModalActions.notification(
      context: context,
      title: title,
      content: content,
      showPage: contentObj["showPage"],
      mainPageIndex: contentObj["mainPageIndex"],
      subPageIndex: contentObj["subPageIndex"],
      proofLink: NotificationUtils.getNotificationAttachedProofLink(
        notification: notifications[index],
      ),
    );
  }

  Widget buildNotificationListTile({
    String title,
    String notificationId,
    bool seen,
    DateTime createdDate,
    int index,
    String type,
    Contact instigator,
  }) {
    var appColors = Provider.of<AppColorsProvider>(context, listen: false);
    return InkWell(
      onTap: () {
        onNotificationClicked(title: title, type: type, index: index);
        markAsRead(notificationId: notificationId, index: index);
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 15),
        width: double.infinity,
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(color: Colors.grey, width: 0.5),
          ),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            CircleAvatar(
                backgroundColor: GeneralProvider.isLightMode
                    ? Colors.grey[50]
                    : Colors.white.withOpacity(0.05),
                child: HelperFunctions.buildLogo(context: context, height: 25)

                // Image.asset(
                //   "public/images/new_logo.png",
                //   height: 25,
                // ),
                ),
            SizedBox(
              width: 8,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    child: Row(
                      children: <Widget>[
                        CircleAvatar(
                          backgroundColor: seen
                              ? Colors.transparent
                              : Theme.of(context).primaryColor,
                          radius: 3,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          "$title - ",
                          style: Theme.of(context)
                              .textTheme
                              .body1
                              .copyWith(fontWeight: FontWeight.w600),
                        ),
                        Expanded(
                          child: Text(
                            "${HelperFunctions.getContactFullName(contact: instigator)}",
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "${HelperFunctions.getNotificationLapsePeriod(createdDate: createdDate)}",
                        ),
                        // if (!isArchived)
                        InkWell(
                          onTap: () => archiveOperation(
                              notificationId: notificationId, index: index),
                          customBorder: CircleBorder(),
                          child: Image.asset(
                            isArchived
                                ? "public/images/unarchived.png"
                                : "public/images/archive-small.png",
                            height: 24,
                            color: appColors.defaultIconColor,
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    BackButtonInterceptor.remove(myInterceptor);
  }

  @override
  Widget build(BuildContext context) {
    var appColors = Provider.of<AppColorsProvider>(context, listen: false);
    return Scaffold(
      key: _scaffoldKey,
      body: SafeArea(
          child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(vertical: 30),
            child: MainNavBar(
              onNotificationPage: true,
              // leadingImagePath: 'public/images/close-page.png',
              // leadingHeight: 20,
              // leadingWidth: 40,
              // notificationImagePath: 'public/images/notification.png',
              leadingWidget: Container(
                // padding: const EdgeInsets.only(left: 15),
                child: InkWell(
                  onTap: () {
                    onBackButton();
                  },
                  customBorder: CircleBorder(),
                  child: CircleAvatar(
                    backgroundColor: Colors.transparent,
                    child: FaIcon(
                      FontAwesomeIcons.lightTimes,
                      size: 26,
                      color: appColors.pageCloseSymbolColor,
                    ),
                  ),
                ),
              ),
              subNav: true,
              title: "Notifications",
              // leadingPressAction: () => Navigator.of(context).pop(),
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 10),
            margin: EdgeInsets.symmetric(
              horizontal: 30,
            ),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(color: Colors.grey, width: 0.5),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                if (isFetchingNew)
                  CustomProgressIndicator(
                    size: 10,
                    color: Theme.of(context).primaryColor,
                  ),
                Spacer(),
                Row(
                  children: <Widget>[
                    Text("Archived",
                        style: Theme.of(context)
                            .textTheme
                            .body2
                            .copyWith(fontWeight: FontWeight.w400)),
                    SizedBox(
                      width: 5,
                    ),
                    CustomSwitch(
                      value: isArchived,
                      onClick: (value) {
                        if (this.mounted) {
                          setState(() {
                            isArchived = value;
                          });
                        }
                        onArchiveClick();
                      },
                      padding: EdgeInsets.all(5),
                    ),
                  ],
                )
              ],
            ),
          ),
          Expanded(
            child: Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  child: HelperFunctions.renderListItemsAndRefresh(
                      context: context,
                      pullRefresh: pullRefresh,
                      isLoading: isLoading,
                      refresh: () {
                        if (this.mounted) {
                          setState(() {
                            pullRefresh = true;
                          });
                        }
                        return getNotifications();
                      },
                      itemsCount: notifications.length,
                      itemsBuilder: ListView.builder(
                        itemBuilder: (ctx, index) {
                          return buildNotificationListTile(
                            index: index,
                            type: notifications[index].type,
                            notificationId: notifications[index].id,
                            title: notifications[index].title,
                            seen: notifications[index].seen,
                            createdDate: notifications[index].createdDate,
                            instigator:
                                NotificationUtils.getNotificationInstigator(
                                    notification: notifications[index],
                                    userId: userData["id"]),
                          );
                        },
                        itemCount: notifications.length,
                      ),
                      emptyListIconPath:
                          "public/images/empty-lists/${isArchived ? "archive.png" : "notifications.png"}",
                      emptyListText:
                          "${isArchived ? "You have no archived notifications." : "You have no new notifications at this time"}"),
                ),
                if (markingAllOrArchiving)
                  Center(
                    child: CustomProgressIndicator(
                      color: Theme.of(context).primaryColor,
                      size: 40,
                    ),
                  )
              ],
            ),
          ),
          if (notifications.length != 0)
            Container(
              padding: EdgeInsets.symmetric(vertical: 10),
              margin: EdgeInsets.symmetric(
                horizontal: 30,
              ),
              decoration: BoxDecoration(
                border: Border(
                  top: BorderSide(color: Colors.grey, width: 0.5),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  InkWell(
                    onTap: () => markAllAsRead(),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "Mark all as read",
                        style: Theme.of(context).textTheme.body2.copyWith(
                            color: Theme.of(context).primaryColor,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                  if (!isArchived)
                    InkWell(
                      onTap: () => archiveAll(),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          "Archive all",
                          style: Theme.of(context).textTheme.body2.copyWith(
                              color: Theme.of(context).primaryColor,
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                ],
              ),
            )
        ],
      )),
    );
  }
}
