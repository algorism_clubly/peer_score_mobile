import 'package:flutter/material.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/screens/main_screen.dart';
import 'package:peer_score/screens/phone.dart';
import 'package:peer_score/utils/functions.dart';

class OnboadingScreen extends StatefulWidget {
  static const routeName = "/onboarding-screen";
  @override
  _OnboadingScreenState createState() => _OnboadingScreenState();
}

class _OnboadingScreenState extends State<OnboadingScreen> {
  PageController _pageController = PageController();
  int pageNumber = 0;

  AnimatedContainer buildScrollIndicator(int index) {
    //
    return AnimatedContainer(
      duration: Duration(seconds: 10),
      curve: Curves.bounceIn,
      child: Container(
        width: pageNumber == index ? 40 : 14,
        height: 5,
        decoration: BoxDecoration(
          color: pageNumber == index ? Color(0xFF93FF97) : Color(0xFFbcffbd),
        ),
      ),
    );
  }

  Container buildSplashScreen(
      {int index,
      BuildContext context,
      String title,
      String imagePath,
      String content}) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 30, horizontal: 30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          SizedBox(
            height: 50,
          ),
          Text(
            title,
            style: Theme.of(context).textTheme.body2.copyWith(
                fontSize: 18, height: 0.72, fontWeight: FontWeight.normal),
          ),
          SizedBox(
            height: 15,
          ),
          Text(
            content,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.body1.copyWith(
                  fontWeight: FontWeight.w300,
                ),
          ),
          SizedBox(),
          Expanded(
            child: SizedBox(
              height: 400,
              child: Image.asset(imagePath),
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              buildScrollIndicator(0),
              SizedBox(
                width: 5,
              ),
              buildScrollIndicator(1),
              SizedBox(
                width: 5,
              ),
              buildScrollIndicator(2)
            ],
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var height = HelperFunctions.getDeviceUsabaleHeigth(context: context);
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          physics: NeverScrollableScrollPhysics(),
          child: Container(
            height: height,
            child: Column(
              children: <Widget>[
                Expanded(
                  child: PageView(
                    // physics: BouncingScrollPhysics(),
                    controller: _pageController,
                    onPageChanged: (index) {
                      setState(() {
                        pageNumber = index;
                      });
                    },
                    children: <Widget>[
                      buildSplashScreen(
                        index: 0,
                        context: context,
                        title: "Find debtors",
                        content:
                            "Find your debtors on PeerScore and recieve payment",
                        imagePath: "public/images/splash1.png",
                      ),
                      buildSplashScreen(
                        index: 1,
                        context: context,
                        title: "Manage debts",
                        content:
                            "Manage your debts on PeerScore and always be on track",
                        imagePath: "public/images/splash2.png",
                      ),
                      buildSplashScreen(
                        index: 2,
                        context: context,
                        title: "Monitor credit score",
                        content:
                            "PeerScore gives you a credit score based on your activities on the app, pay debts on-time and enjoy freedom",
                        imagePath: "public/images/splash3.png",
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      FlatButton(
                        onPressed: () {
                          Navigator.of(context).pushReplacementNamed(
                              PhoneNumberScreen.routeName);
                        },
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        child: Text(
                          pageNumber == 2 ? "" : "Skip",
                          style: Theme.of(context).textTheme.body1.copyWith(
                              color: Theme.of(context).accentColor,
                              fontWeight: FontWeight.w600,
                              height: 0.75),
                        ),
                      ),
                      FlatButton(
                        onPressed: () {
                          setState(() {
                            if (pageNumber != 2) {
                              pageNumber += 1;
                            } else if (pageNumber == 2) {
                              Navigator.of(context)
                                  .popAndPushNamed(PhoneNumberScreen.routeName);
                            }
                            _pageController.animateToPage(pageNumber,
                                duration: Duration(milliseconds: 400),
                                curve: Curves.linear);
                          });
                        },
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Text(
                          pageNumber == 2 ? "lets go" : "Next",
                          style: Theme.of(context).textTheme.body1.copyWith(
                              color: Theme.of(context).primaryColor,
                              fontWeight: FontWeight.w600,
                              height: 0.75),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
