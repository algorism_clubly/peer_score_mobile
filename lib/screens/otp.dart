import 'dart:convert';

import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:peer_score/providers/colors.dart';
import 'package:peer_score/providers/onboading_provider.dart';
import 'package:peer_score/providers/user-provider.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/screens/login.dart';
import 'package:peer_score/screens/phone.dart';
import 'package:peer_score/screens/pin.dart';
import 'package:peer_score/utils/custom_raised_button.dart';
import 'package:peer_score/utils/enums.dart';
import 'package:peer_score/utils/err_text_widget.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:peer_score/utils/main_nav_bar.dart';
import 'package:peer_score/utils/progress_indicator.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OtpScreen extends StatefulWidget {
  static const String routeName = '/otp';

  final bool isAltPhone;
  Function altPhoneAction;
  Function altPhoneNav;
  OtpScreen({this.isAltPhone = false, this.altPhoneAction, this.altPhoneNav});
  @override
  _OtpScreenState createState() => _OtpScreenState();
}

class _OtpScreenState extends State<OtpScreen> {
  OnboardingScreens baseInitiator;
  String leadingActionPageRoute;
  Map<String, dynamic> _onboardingUserData = {};
  String _otp = "";
  bool isSending = false;
  String errText;
  TextEditingController _otpController;
  String _submitErrText = "";
  bool _submitErrVisible = false;
  var _connectionStatusSubscription;
  @override
  void initState() {
    super.initState();
    _connectionStatusSubscription =
        HelperFunctions.subscribeToNetworkNotifier(context: context);
    BackButtonInterceptor.add(myInterceptor);

    leadingActionPageRoute = GeneralProvider.resetPasswordInitilzationPage;
    _otpController = TextEditingController();

    _otpController.addListener(() {
      if (_otpController.text.length > 0) {
        setState(() {
          _submitErrText = "";
          _submitErrVisible = false;
        });
      }
    });

    // FocusScope.of(context).requestFocus(_box1FocusNode);
    SharedPreferences.getInstance().then((sharedPrefs) {
      //
      _onboardingUserData = sharedPrefs.containsKey("userData")
          ? json.decode(sharedPrefs.get("userData")) as Map<String, dynamic>
          : null;
    });
  }

  bool myInterceptor(bool stopDefaultButtonEvent) {
    Navigator.of(context).pushReplacementNamed(leadingActionPageRoute,
        arguments: {"isSlideBackward": false});
    return true;
  }

  @override
  void dispose() {
    BackButtonInterceptor.remove(myInterceptor);
    _connectionStatusSubscription.cancel();
    super.dispose();
  }

  void _onSubmit() async {
    setState(() {
      _submitErrText = "";
      _submitErrVisible = false;
    });
    var otpValidationResult = HelperFunctions.validateOtp(otp: _otp);
    if (!otpValidationResult["status"]) {
      setState(() {
        _submitErrText = otpValidationResult["message"];
        _submitErrVisible = true;
      });
      return;
    }
    setState(() {
      isSending = true;
    });
    var sendResponse;
    if (widget.isAltPhone) {
      var alterPhone;
      alterPhone = Provider.of<GeneralProvider>(context, listen: false)
          .getCurrentAltPhone;
      sendResponse = await Provider.of<UserProvider>(context, listen: false)
          .createAlternatePhone(
        body: {
          "phone":
              HelperFunctions.serializePhoneNumber(phoneNumber: alterPhone),
          "userId": _onboardingUserData["id"],
          "code": _otp
        },
      );
    } else {
      if (leadingActionPageRoute == LoginScreen.routeName ||
          leadingActionPageRoute == PhoneNumberScreen.routeName) {
        var userId = UserProvider.tempUserId;

        sendResponse =
            await Provider.of<OnboardingProvider>(context, listen: false)
                .verifyOtp(
          body: {"userId": userId, "code": _otp},
        );
      } else {
        sendResponse =
            await Provider.of<OnboardingProvider>(context, listen: false)
                .verifyOtp(
          body: {"userId": _onboardingUserData["id"], "code": _otp},
        );
      }
    }

    if (sendResponse["status"] == 200 || sendResponse["status"] == 201) {
      setState(() {
        isSending = false;
      });
      if (widget.isAltPhone) {
        widget.altPhoneAction();
      } else {
        Navigator.of(context).pushReplacementNamed(PinScreen.routeName);
      }
    } else {
      setState(() {
        _submitErrText = sendResponse["errors"];
        _submitErrVisible = true;
        _otpController.text = "";
        isSending = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var height = HelperFunctions.getDeviceUsabaleHeigth(context: context);

    var appColors = Provider.of<AppColorsProvider>(context, listen: false);
    return Scaffold(
      backgroundColor: widget.isAltPhone && !GeneralProvider.isLightMode
          ? Colors.grey[850]
          : null,
      body: SafeArea(
        child: SingleChildScrollView(
          physics: NeverScrollableScrollPhysics(),
          child: Container(
            height: widget.isAltPhone ? 300 : height,
            child: Column(
              children: <Widget>[
                if (!widget.isAltPhone)
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 30),
                    child: MainNavBar(
                      leadingImagePath: 'public/images/arrow-back.png',
                      // notificationImagePath: '',
                      leadingPressAction: () {
                        Navigator.of(context).pushReplacementNamed(
                            leadingActionPageRoute,
                            arguments: {"isSlideBackward": false});
                      },
                    ),
                  ),
                Expanded(
                  child: Container(
                    width: double.infinity,
                    padding: EdgeInsets.symmetric(
                        horizontal: widget.isAltPhone ? 0 : 30,
                        vertical: widget.isAltPhone ? 10 : 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        if (!widget.isAltPhone) Spacer(),
                        if (!widget.isAltPhone)
                          Align(
                            alignment: Alignment.center,
                            child: Text(
                              "Verify OTP",
                              style: Theme.of(context)
                                  .textTheme
                                  .body2
                                  .copyWith(fontSize: 20),
                            ),
                          ),

                        if (!widget.isAltPhone)
                          Spacer(
                            flex: 2,
                          ),
                        // Expanded(
                        //   child: ConstrainedBox(
                        //     constraints: BoxConstraints(
                        //       // minHeight: 40,
                        //       maxHeight: 60,
                        //     ),
                        //   ),
                        // ),
                        Text(
                          "Enter OTP",
                          style: Theme.of(context).textTheme.body2.copyWith(
                                fontWeight: FontWeight.normal,
                                color: Theme.of(context).primaryColor,
                              ),
                        ),
                        SizedBox(height: 8),
                        Text(
                          "Enter the 4 digit pin sent to your number",
                          style: Theme.of(context).textTheme.body2.copyWith(
                              color: GeneralProvider.isLightMode
                                  ? Color(0xFF979696)
                                  : Colors.white70,
                              fontWeight: FontWeight.normal,
                              height: 0.73),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        PinCodeTextField(
                          length: 4,
                          onChanged: (value) {
                            setState(() {
                              _otp = value;
                            });
                          },
                          controller: _otpController,
                          backgroundColor: Colors.transparent,
                          textStyle: Theme.of(context)
                              .textTheme
                              .body2
                              .copyWith(fontSize: 20),
                          // backgroundColor: Colors.grey[50],
                          shape: PinCodeFieldShape.box,
                          fieldWidth: 48,
                          // fieldWidth: widget.isAltPhone ? 60 : 65,
                          // fieldHeight: widget.isAltPhone ? 60 : 65,
                          animationType: AnimationType.slide,
                          // borderRadius: BorderRadius.circular(10),
                          textInputType: TextInputType.number,

                          inactiveColor: widget.isAltPhone
                              ? appColors.pinCodeBorderColorAltPhone
                              : appColors.pinCodeBorderColor,
                          inactiveFillColor: widget.isAltPhone
                              ? appColors.pinCodeBorderColorAltPhone
                              : appColors.pinCodeBorderColor,
                          activeColor: widget.isAltPhone
                              ? appColors.pinCodeBorderColorAltPhone
                              : appColors.pinCodeBorderColor,
                          borderRadius: BorderRadius.circular(4.0),

                          borderWidth: GeneralProvider.isLightMode ? 1 : 1.5,
                        ),
                        ErrText(
                          errText: _submitErrText,
                          vissibility: _submitErrVisible,
                        ),
                        // SizedBox(height: widget.isAltPhone ? 40 : 60),
                        Spacer(
                          flex: 2,
                        ),
                        // Expanded(
                        //   child: ConstrainedBox(
                        //     constraints: BoxConstraints(
                        //       // minHeight: 40,
                        //       maxHeight: 60,
                        //     ),
                        //   ),
                        // ),
                        CustomRaisedButton(
                          title: isSending ? "" : "verify",
                          color: appColors.primaryButtonColorText,
                          bgColor: appColors.primaryButtonColorBg,
                          indicator: isSending
                              ? CustomProgressIndicator(
                                  color: appColors.primaryProgressValueColor,
                                )
                              : Text(""),
                          onTapAction: () {
                            isSending ? null : _onSubmit();
                          },
                        ),
                        Spacer(
                          flex: 5,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
