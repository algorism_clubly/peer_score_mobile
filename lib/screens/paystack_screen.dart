import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class PaystackScreen extends StatefulWidget {
  static const String routeName = "/paystack-screen";
  const PaystackScreen({Key key, this.title, this.arguments}) : super(key: key);
  final String title;
  final Map<String, dynamic> arguments;

  @override
  _PaystackScreenState createState() => _PaystackScreenState();
}

class _PaystackScreenState extends State<PaystackScreen> {
  final flutterWebViewPlugin = FlutterWebviewPlugin();

  @override
  void initState() {
    BackButtonInterceptor.add(myInterceptor);
    super.initState();
  }

  void onclose() async {
    Navigator.of(context).pop();
  }

  bool myInterceptor(bool stopDefaultButtonEvent) {
    if (stopDefaultButtonEvent) return false;
    onclose();
    return true;
  }

  @override
  void dispose() {
    BackButtonInterceptor.remove(myInterceptor);
    flutterWebViewPlugin.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      url: widget.arguments["url"],
      appBar: AppBar(
        backgroundColor: Colors.white,
        automaticallyImplyLeading: false,
        title: Text(
          "Pay with paystack",
          style: Theme.of(context)
              .textTheme
              .body2
              .copyWith(fontSize: 20, color: Colors.black),
        ),
        bottom: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.white,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              IconButton(
                icon: const Icon(
                  Icons.close,
                  color: Colors.black,
                ),
                onPressed: () {
                  onclose();
                },
              ),
            ],
          ),
        ),
      ),
      withZoom: true,
      withLocalStorage: true,
      hidden: false,
    );
  }
}
