import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:peer_score/providers/colors.dart';
import 'package:peer_score/providers/onboading_provider.dart';
import 'package:peer_score/providers/user-provider.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/screens/login.dart';
import 'package:peer_score/screens/otp.dart';
import 'package:peer_score/utils/contry_codes.dart';
import 'package:peer_score/utils/custom_checkbox.dart';
import 'package:peer_score/utils/custom_raised_button.dart';
import 'package:peer_score/utils/dropdown_field.dart';
import 'package:peer_score/utils/enums.dart';
import 'package:peer_score/utils/err_text_widget.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:peer_score/utils/modal_actions.dart';
import 'package:peer_score/utils/progress_indicator.dart';
import 'package:provider/provider.dart';

class PhoneNumberScreen extends StatefulWidget {
  static const String routeName = "/phone-number";
  final bool isAltPhone;
  Function altPhoneAction;
  PhoneNumberScreen({this.isAltPhone = false, this.altPhoneAction});
  @override
  _PhoneNumberScreenState createState() => _PhoneNumberScreenState();
}

class _PhoneNumberScreenState extends State<PhoneNumberScreen> {
  bool isLightMode = GeneralProvider.isLightMode;
  TextEditingController _phoneNumberController;
  FocusNode _phoneNumberFocusNode;
  String defaultValue = "+234";
  bool onCLickNext = false;
  bool isSending = false;
  String _submitErrText = "";
  bool _submitErrVisible = false;
  String _phoneErrText = "";
  bool _phoneErrVisible = false;
  Map<String, dynamic> userData = {};
  bool whatsAppCheck = false;
  var _connectionStatusSubscription;
  @override
  void initState() {
    super.initState();
    _connectionStatusSubscription =
        HelperFunctions.subscribeToNetworkNotifier(context: context);
    _phoneNumberController = TextEditingController();
    _phoneNumberFocusNode = FocusNode();
    Provider.of<UserProvider>(context, listen: false).userData.then((data) {
      userData = data;
      setState(() {});
    });
  }

  @override
  void dispose() {
    super.dispose();
    _phoneNumberController.dispose();
    _phoneNumberFocusNode.dispose();
    _connectionStatusSubscription.cancel();
  }

  void _onCountryCodeChanged(String value) {
    setState(() {
      defaultValue = value;
    });
  }

  void _whatsAppCheck(bool value) {
    setState(() {
      whatsAppCheck = value;
    });
  }

  void _onSubmit() async {
    _phoneNumberFocusNode.unfocus();
    setState(() {
      _submitErrText = "";
      _submitErrVisible = false;
    });
    var phoneValidateResult =
        HelperFunctions.validatePhone(phoneNumber: _phoneNumberController.text);
    if (!phoneValidateResult["status"]) {
      setState(() {
        _phoneErrText = phoneValidateResult["message"];
        _phoneErrVisible = true;
      });
      return;
    }

    //if using for alternate phone addition, checking if provided number is the active users number

    if (widget.isAltPhone &&
        (HelperFunctions.serializePhoneNumber(
                phoneNumber: _phoneNumberController.text) ==
            userData["phone"])) {
      setState(() {
        _phoneErrText = "Phone number cannot be set as alternate number";
        _phoneErrVisible = true;
      });

      return;
    }
    setState(() {
      isSending = true;
    });
    var phoneNumber = HelperFunctions.serializePhoneNumber(
      phoneNumber: _phoneNumberController.text,
    );
    var sendResponse;
    if (widget.isAltPhone) {
      // userData =
      //     await Provider.of<UserProvider>(context, listen: false).userData;
      sendResponse = await Provider.of<UserProvider>(context, listen: false)
          .getOtpForAlternatePhone(
        body: {"phone": phoneNumber, "userId": userData["id"]},
      );
      
      
    } else {
      sendResponse =
          await Provider.of<OnboardingProvider>(context, listen: false)
              .sendPhoneNumber(
        body: {"phone": phoneNumber, "whatsapp": whatsAppCheck},
      );
    }

    if (sendResponse["status"] == 200 || sendResponse["status"] == 201) {
      setState(() {
        isSending = false;
      });

      if (widget.isAltPhone) {
        Provider.of<GeneralProvider>(context, listen: false)
            .setCurrentAltPhone(phoneNumber: _phoneNumberController.text);
        widget.altPhoneAction();
      } else {
        UserProvider.tempUserId = sendResponse["data"]["id"];
        GeneralProvider.resetPasswordInitilzationPage =
            PhoneNumberScreen.routeName;
        Navigator.of(context).pushReplacementNamed(OtpScreen.routeName);
      }
      _phoneNumberController.text = null;
    } else {
      setState(() {
        isSending = false;
        if (widget.isAltPhone) {
          _phoneErrText = sendResponse["errors"];
          _phoneErrVisible = true;
        } else {
          _submitErrText = sendResponse["errors"];
          _submitErrVisible = true;
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var height = HelperFunctions.getDeviceUsabaleHeigth(context: context);

    var appColors = Provider.of<AppColorsProvider>(context, listen: false);
    return Scaffold(
      backgroundColor: widget.isAltPhone && !GeneralProvider.isLightMode
          ? Colors.grey[850]
          : null,
      body: SafeArea(
        child: SingleChildScrollView(
          physics: NeverScrollableScrollPhysics(),
          child: Container(
            height: widget.isAltPhone ? 300 : height,
            width: double.infinity,
            padding: EdgeInsets.symmetric(
                horizontal: widget.isAltPhone ? 0 : 30,
                vertical: widget.isAltPhone ? 10 : 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                if (!widget.isAltPhone)
                  Spacer(
                    flex: 3,
                  ),
                if (!widget.isAltPhone)
                  Center(
                    child: Text("Let's Get Started",
                        style: Theme.of(context)
                            .textTheme
                            .body2
                            .copyWith(fontSize: 20, height: 0.74)),
                  ),
                if (!widget.isAltPhone) Spacer(),
                Text(
                  widget.isAltPhone
                      ? "Enter an alternate phone number"
                      : "Enter your phone number",
                  style: Theme.of(context).textTheme.body2.copyWith(
                        color: Theme.of(context).primaryColor,
                        fontWeight: FontWeight.normal,
                      ),
                ),
                SizedBox(height: 8),
                RichText(
                    text: TextSpan(
                        style: Theme.of(context).textTheme.body1.copyWith(
                              fontWeight: FontWeight.w300,
                            ),
                        children: [
                      TextSpan(
                          text: "NB:",
                          style: Theme.of(context)
                              .textTheme
                              .body1
                              .copyWith(fontWeight: FontWeight.bold)),
                      TextSpan(
                        text:
                            "  PeerScore would send in an OTP for verification, ensure you are using a phone number you can easily access",
                      )
                    ])),
                SizedBox(
                  height: 30,
                ),
                DropDownField(
                  textEditingController: _phoneNumberController,
                  focusNode: _phoneNumberFocusNode,
                  textInputType: TextInputType.number,
                  defaultItem: defaultValue,
                  onChanged: (value) => _onCountryCodeChanged(value),
                  items: ["+234"],
                  hintText: "Phone number",
                  height: 40,
                  borderColor: widget.isAltPhone && !GeneralProvider.isLightMode
                      ? Colors.white30
                      : null,
                  onTap: () {
                    setState(() {
                      _phoneErrText = "";
                      _phoneErrVisible = false;
                    });
                  },
                  onFieldSubmitted: (value) => _onSubmit(),
                  inputFormatters: [
                    WhitelistingTextInputFormatter.digitsOnly,
                  ],
                ),
                ErrText(
                  errText: _phoneErrText,
                  vissibility: _phoneErrVisible,
                ),
                SizedBox(
                  height: 10,
                ),
                if (!widget.isAltPhone)
                  FittedBox(
                    child: Row(
                      children: <Widget>[
                        CustomCheckBox(
                          value: whatsAppCheck,
                          onClick: _whatsAppCheck,
                          title: "Is this number available on WhatsApp?",
                        ),
                        SizedBox(width: 12),
                        InkWell(
                          onTap: () => ModalActions.showMoreInfo(
                            context: context,
                            title: "WhatsApp availability",
                            content:
                                "Payment requests made to you can be completed by sending a message to you on WhatsApp, please ensure that the number is available before confirmation.",
                          ),
                          child: HelperFunctions.buildQuestionMark()
                        )
                      ],
                    ),
                  ),
                SizedBox(height: 55),
                CustomRaisedButton(
                  title: isSending ? "" : "Next",
                  // color: appColors.primaryButtonColorText,
                  // bgColor: appColors.primaryButtonColorBg,
                  onTapAction: () {
                    isSending ? () {} : _onSubmit();
                  },
                  indicator: isSending
                      ? CustomProgressIndicator(
                          // color: appColors.primaryProgressValueColor,
                          )
                      : AnimatedContainer(
                          duration: Duration(milliseconds: 200),
                          child: Container(
                            margin: EdgeInsets.only(left: 15),
                            child: FaIcon(
                                Platform.isIOS
                                    ? const IconData(0xf3d6,
                                        fontFamily: "CupertinoIcons",
                                        fontPackage: "cupertino_icons")
                                    : FontAwesomeIcons.longArrowAltRight,
                                color: appColors.buttonIconColor),
                          ),
                        ),
                ),
                ErrText(
                  errText: _submitErrText,
                  vissibility: _submitErrVisible,
                ),
                if (!widget.isAltPhone)
                  Spacer(
                    flex: 4,
                  ),
                if (!widget.isAltPhone)
                  Align(
                    child: FlatButton(
                      onPressed: isSending
                          ? () {}
                          : () => Navigator.of(context)
                              .pushReplacementNamed(LoginScreen.routeName),
                      child: Text(
                        "I already have an account",
                        style: Theme.of(context).textTheme.body1.copyWith(
                              color: Theme.of(context).primaryColor,
                              fontWeight: FontWeight.normal,
                            ),
                      ),
                    ),
                  )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
