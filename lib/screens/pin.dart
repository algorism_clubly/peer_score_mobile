import 'dart:convert';

import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:peer_score/providers/colors.dart';
import 'package:peer_score/providers/onboading_provider.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/providers/user-provider.dart';
import 'package:peer_score/screens/login.dart';
import 'package:peer_score/screens/name.dart';
import 'package:peer_score/screens/welcome_back.dart';
import 'package:peer_score/utils/custom_raised_button.dart';
import 'package:peer_score/utils/enums.dart';
import 'package:peer_score/utils/err_text_widget.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:peer_score/utils/main_nav_bar.dart';
import 'package:peer_score/utils/progress_indicator.dart';
import 'package:peer_score/utils/text_form_field.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PinScreen extends StatefulWidget {
  static const String routeName = '/pin';
  @override
  _PinScreenState createState() => _PinScreenState();
}

class _PinScreenState extends State<PinScreen> {
  String leadingActionPageRoute;
  OnboardingScreens baseInitiator;
  FocusNode _pinFocusNode;
  FocusNode _confirmPinFocusNode;
  TextEditingController _pinController;
  TextEditingController _confirmPinController;
  String _pin = "";
  String _confirmPin = "";
  String errText;
  bool isSending = false;
  String _submitErrText = "";
  bool _submitErrVisible = false;
  Map<String, dynamic> _onboardingUserData = {};
  String headerText = "Setup your Pin";

  var _connectionStatusSubscription;

  @override
  void initState() {
    super.initState();

    _connectionStatusSubscription =
        HelperFunctions.subscribeToNetworkNotifier(context: context);
    BackButtonInterceptor.add(myInterceptor);
    leadingActionPageRoute = GeneralProvider.resetPasswordInitilzationPage;
    _pinFocusNode = FocusNode();
    _confirmPinFocusNode = FocusNode();
    _pinController = TextEditingController();
    _confirmPinController = TextEditingController();
    _pinController.addListener(() {
      if (_pinController.text.length > 0) {
        setState(() {
          _submitErrText = "";
          _submitErrVisible = false;
        });
      }
    });

    _confirmPinController.addListener(() {
      if (_confirmPinController.text.length > 0) {
        setState(() {
          _submitErrText = "";
          _submitErrVisible = false;
        });
      }
    });

    SharedPreferences.getInstance().then((sharedPrefs) {
      //
      _onboardingUserData =
          json.decode(sharedPrefs.get("userData")) as Map<String, dynamic>;
    });
  }

  bool myInterceptor(bool stopDefaultButtonEvent) {
    Navigator.of(context).pushReplacementNamed(leadingActionPageRoute,
        arguments: {"isSlideBackward": false});
    return true;
  }

  @override
  void dispose() {
    BackButtonInterceptor.remove(myInterceptor);
    _connectionStatusSubscription.cancel();
    super.dispose();
  }

  void _onSubmit() async {
    setState(() {
      _submitErrText = "";
      _submitErrVisible = false;
    });
    //check pin length

    // if (_pin.trim() != _confirmPin.trim()) {
    //   setState(() {
    //     _submitErrText = "Pin does not match";
    //     _submitErrVisible = true;
    //     isSending = false;
    //   });
    //   return;
    // }

    var pinCreationValidationResult =
        HelperFunctions.validatePinCreation(pin: _pin, confirmPin: _confirmPin);
    if (!pinCreationValidationResult["status"]) {
      setState(() {
        _submitErrText = pinCreationValidationResult["message"];
        _submitErrVisible = true;
      });
      return;
    }
    setState(() {
      isSending = true;
    });
    var pinResp;
    if (leadingActionPageRoute == WelcomeBackScreen.routeName ||
        leadingActionPageRoute == LoginScreen.routeName) {
      if (leadingActionPageRoute == LoginScreen.routeName) {
        var userId = UserProvider.tempUserId;
        pinResp = await Provider.of<UserProvider>(context, listen: false)
            .updatePassword(pin: _pinController.text, userId: userId);
      } else {
        pinResp = await Provider.of<UserProvider>(context, listen: false)
            .updatePassword(
                pin: _pinController.text, userId: _onboardingUserData["id"]);
      }
    } else {
      var userId = UserProvider.tempUserId;
      pinResp = await Provider.of<OnboardingProvider>(context, listen: false)
          .createPin(
        body: {"userId": userId, "password": _pinController.text},
      );
    }
    if (pinResp["status"] == 200 || pinResp["status"] == 201) {
      setState(() {
        isSending = false;
      });
      if (leadingActionPageRoute == WelcomeBackScreen.routeName ||
          leadingActionPageRoute == LoginScreen.routeName) {
        Navigator.of(context).pushReplacementNamed(leadingActionPageRoute);
      } else {
        Navigator.of(context).pushReplacementNamed(NameScreen.routeName);
      }
    } else {
      setState(() {
        errText = pinResp["errors"];
        _submitErrText = pinResp["errors"];
        _submitErrVisible = true;
        isSending = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var height = HelperFunctions.getDeviceUsabaleHeigth(context: context);
    var appColors = Provider.of<AppColorsProvider>(context, listen: false);
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          // physics: NeverScrollableScrollPhysics(),
          child: Container(
            height: height,
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.symmetric(vertical: 30),
                  child: MainNavBar(
                    leadingImagePath: 'public/images/arrow-back.png',
                    leadingPressAction: () => Navigator.of(context)
                        .pushReplacementNamed(leadingActionPageRoute,
                            arguments: {"isSlideBackward": false}),
                  ),
                ),
                Expanded(
                  child: Container(
                    width: double.infinity,
                    padding: EdgeInsets.symmetric(horizontal: 30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        // SizedBox(height: 100),
                        // ConstrainedBox(
                        //   constraints: BoxConstraints(
                        //     minHeight: 20,
                        //     maxHeight: 100,
                        //   ),
                        // ),
                        Spacer(
                          flex: 1,
                        ),
                        Center(
                          child: Text("Setup your Pin",
                              style: Theme.of(context)
                                  .textTheme
                                  .body2
                                  .copyWith(fontSize: 20, height: 0.74)),
                        ),
                        Spacer(
                          flex: 2,
                        ),
                        Text(
                          "Create Pin",
                          style: Theme.of(context).textTheme.body2.copyWith(
                                color: Theme.of(context).primaryColor,
                                fontWeight: FontWeight.normal,
                                height: 0.73,
                              ),
                        ),
                        SizedBox(height: 8),
                        Text(
                            "Create your 6 digit pin, ensure you remember it."),
                        SizedBox(
                          height: 15,
                        ),
                        PinCodeTextField(
                          length: 6,
                          onChanged: (value) {
                            setState(() {
                              _pin = value;
                            });
                          },
                          controller: _pinController,
                          backgroundColor: Colors.transparent,
                          textStyle: Theme.of(context)
                              .textTheme
                              .body2
                              .copyWith(fontSize: 20),
                          shape: PinCodeFieldShape.box,
                          // fieldWidth: 48,
                          fieldHeight: 44,
                          animationType: AnimationType.slide,
                          // borderRadius: BorderRadius.circular(10),
                          textInputType: TextInputType.number,
                          inactiveColor: appColors.pinCodeBorderColor,
                          inactiveFillColor: appColors.pinCodeBorderColor,
                          activeColor: appColors.pinCodeBorderColor,
                          borderRadius: BorderRadius.circular(4),
                          borderWidth: GeneralProvider.isLightMode ? 1 : 1.5,
                          obsecureText: true,
                        ),
                        // CustomTextField(
                        //   textEditingController: _pinController,
                        //   obscureText: true,
                        // ),
                        ConstrainedBox(
                          constraints: BoxConstraints(
                            minHeight: 15,
                            maxHeight: 40,
                          ),
                        ),
                        // SizedBox(
                        //   height: 40,
                        // ),
                        Text(
                          "Confirm Pin",
                          style: Theme.of(context).textTheme.body2.copyWith(
                                color: Theme.of(context).primaryColor,
                                fontWeight: FontWeight.normal,
                                height: 0.73,
                              ),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        PinCodeTextField(
                          length: 6,
                          onChanged: (value) {
                            setState(() {
                              _confirmPin = value;
                            });
                          },
                          controller: _confirmPinController,
                          backgroundColor: appColors.pinCodebackgroundColor,
                          textStyle: Theme.of(context)
                              .textTheme
                              .body2
                              .copyWith(fontSize: 20),
                          shape: PinCodeFieldShape.box,
                          // fieldWidth: 48,
                          fieldHeight: 44,
                          animationType: AnimationType.slide,
                          // borderRadius: BorderRadius.circular(10),
                          textInputType: TextInputType.number,
                          inactiveColor: appColors.pinCodeBorderColor,
                          inactiveFillColor: appColors.pinCodeBorderColor,
                          activeColor: appColors.pinCodeBorderColor,
                          borderRadius: BorderRadius.circular(4),
                          borderWidth: GeneralProvider.isLightMode ? 1 : 1.5,
                          obsecureText: true,
                        ),
                        ErrText(
                          errText: _submitErrText,
                          vissibility: _submitErrVisible,
                        ),
                        // SizedBox(height: 50),
                        Spacer(
                          flex: 2,
                        ),
                        CustomRaisedButton(
                          title: isSending ? "" : "Save",
                          color: appColors.primaryButtonColorText,
                          bgColor: appColors.primaryButtonColorBg,
                          indicator: isSending
                              ? CustomProgressIndicator(
                                  color: appColors.primaryProgressValueColor,
                                )
                              : Text(""),
                          onTapAction: () {
                            isSending ? () {} : _onSubmit();
                          },
                        ),
                        Spacer(
                          flex: 5,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
