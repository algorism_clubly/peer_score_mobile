import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/utils/connectivity_singleton.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:peer_score/utils/snackbar.dart';
import 'package:peer_score/widgets/profile/bank_accounts.dart';
import 'package:peer_score/widgets/profile/bnv.dart';
import 'package:peer_score/widgets/profile/new_account_page.dart';
import 'package:peer_score/widgets/profile/privacy_page.dart';
import 'package:peer_score/widgets/profile/profile_page.dart';
import 'package:peer_score/widgets/profile/settings_page.dart';
import 'package:provider/provider.dart';

class ProfileScreen extends StatefulWidget {
  static const String routeName = "/profile-screen";
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  String settingType = "";
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  PageController _pageController = PageController();
  var _connectionStatusSubscription;
  @override
  void initState() {
    super.initState();
    GeneralProvider.profileScaffoldKey = _scaffoldKey;
    _connectionStatusSubscription =
        HelperFunctions.subscribeToNetworkNotifier(context: context);
  }

  void _switchPage(int toPageNumber) {
    _pageController.animateToPage(toPageNumber,
        duration: Duration(milliseconds: 200), curve: Curves.linear);
  }

  void _goBack(int toPageNumber) {
    _pageController.animateToPage(toPageNumber,
        duration: Duration(milliseconds: 200), curve: Curves.linear);
  }

  @override
  void dispose() {
    _connectionStatusSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SafeArea(
        child: PageView(
          physics: NeverScrollableScrollPhysics(),
          controller: _pageController,
          children: <Widget>[
            ProfilePage(
              switchTo: () => _switchPage(1),
            ),
            SettingsPage(
              switchTo: (type) {
                setState(() {
                  settingType = type;
                });
                _switchPage(2);
              },
              navBarLeadingFn: () => _goBack(0),
            ),
            //bvn
            if (settingType == "bvn")
              BvnPage(
                navBarLeadingFn: () => _goBack(1),
              ),
            //managing bank accounts
            if (settingType == "account")
              BankAccountsPage(
                switchTo: () => _switchPage(3),
                navBarLeadingFn: () => _goBack(1),
              ),
            if (settingType == "account")
              NewbankAccount(
                switchTo: () => _switchPage(2),
                navBarLeadingFn: () => _goBack(2),
              ),
            //end of managing bank accounts
            if (settingType == "privacy")
              PrivacyPage(
                navBarLeadingFn: () => _goBack(1),
              )
          ],
        ),
      ),
    );
  }
}
