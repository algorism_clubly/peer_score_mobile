import 'package:flutter/material.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/providers/user-provider.dart';
import 'package:peer_score/screens/main_screen.dart';
import 'package:peer_score/screens/onboarding_screen.dart';
import 'package:peer_score/screens/welcome_back.dart';
import 'package:peer_score/utils/connectivity_singleton.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:peer_score/utils/snackbar.dart';
import 'package:peer_score/utils/socket.dart';
import 'package:provider/provider.dart';
// import 'package:peer_score/utils/progress_indicator.dart';

class WaitingScreen extends StatefulWidget {
  static const routeName = '/waiting';
  final bool checkToken;
  WaitingScreen({this.checkToken = true});
  @override
  _WaitingScreenState createState() => _WaitingScreenState();
}

class _WaitingScreenState extends State<WaitingScreen>
    with TickerProviderStateMixin {
  AnimationController _innerSpinnerController;
  AnimationController _outterSpinnerController;

  var _connectionStatusSubscription;

  @override
  void initState() {
    super.initState();

    _connectionStatusSubscription =
        HelperFunctions.subscribeToNetworkNotifier(context: context);
    _innerSpinnerController = AnimationController(
      duration: Duration(milliseconds: 3000),
      vsync: this,
    );
    _outterSpinnerController = AnimationController(
      duration: Duration(milliseconds: 3000),
      vsync: this,
    );
    _innerSpinnerController.repeat();
    _outterSpinnerController.repeat();

    navigateToRoute();
  }

  navigateToRoute() async {
    var userData =
        await Provider.of<UserProvider>(context, listen: false).userData;
    if (userData == null) {
      Navigator.of(context).pushReplacementNamed(OnboadingScreen.routeName);
    } else {
      bool isTokenValid =
          await Provider.of<UserProvider>(context, listen: false).verifyToken();
      if (isTokenValid) {
        await HelperFunctions.startupApiCalls(context: context);
        SocketConnection.getInstance().startSocket(userId: userData["id"]);
        SocketConnection.getInstance().updateUnreadNotificationCount();
        Navigator.of(context).pushReplacementNamed(MainScreen.routeName);
      } else {
        Navigator.of(context).pushReplacementNamed(WelcomeBackScreen.routeName);
      }
    }
  }

  @override
  void dispose() {
    _innerSpinnerController.dispose();
    _outterSpinnerController.dispose();
    _connectionStatusSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Stack(
        alignment: AlignmentDirectional.center,
        children: <Widget>[
          RotationTransition(
            turns: Tween(begin: 1.0, end: 0.0).animate(_innerSpinnerController),
            child: SizedBox(
              height: 30,
              width: 30,
              child: Image.asset("public/images/logos/inner.png"),
            ),
          ),
          RotationTransition(
            turns:
                Tween(begin: 0.0, end: 1.0).animate(_outterSpinnerController),
            child: SizedBox(
              height: 100,
              width: 100,
              child: Image.asset(
                "public/images/logos/outer.png",
                // color: !GeneralProvider.isLightMode ? Colors.grey[500] : null,
                color: Theme.of(context).primaryColor,
              ),
            ),
          )
        ],
      )),
    );
  }
}
