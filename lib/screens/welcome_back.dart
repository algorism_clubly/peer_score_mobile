import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:peer_score/providers/colors.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/providers/user-provider.dart';
import 'package:peer_score/screens/login.dart';
import 'package:peer_score/screens/main_screen.dart';
import 'package:peer_score/screens/otp.dart';
import 'package:peer_score/screens/phone.dart';
import 'package:peer_score/utils/contry_codes.dart';
import 'package:peer_score/utils/custom_raised_button.dart';
import 'package:peer_score/utils/dropdown_field.dart';
import 'package:peer_score/utils/enums.dart';
import 'package:peer_score/utils/err_text_widget.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:peer_score/utils/main_nav_bar.dart';
import 'package:peer_score/utils/progress_indicator.dart';
import 'package:peer_score/utils/socket.dart';
import 'package:peer_score/utils/text_form_field.dart';
import 'package:provider/provider.dart';

class WelcomeBackScreen extends StatefulWidget {
  static const String routeName = "/welcome-back";
  @override
  _WelcomeBackScreenState createState() => _WelcomeBackScreenState();
}

class _WelcomeBackScreenState extends State<WelcomeBackScreen> {
  bool isLightMode = GeneralProvider.isLightMode;
  TextEditingController _pinController;
  FocusNode _pinFocusNode;
  String _pinErrText = "";
  bool _pinErrVisible = false;
  String _submitErrText = "";
  bool _submitErrVisible = false;
  bool isSending = false;
  bool isBooting = false;
  Map<String, dynamic> userData = {"phone": "", "first_name": ""};

  var _connectionStatusSubscription;
  @override
  void initState() {
    super.initState();
    // BackButtonInterceptor.add(myInterceptor);

    _connectionStatusSubscription =
        HelperFunctions.subscribeToNetworkNotifier(context: context);
    _pinController = TextEditingController();
    _pinFocusNode = FocusNode();

    Provider.of<UserProvider>(context, listen: false).userData.then((data) {
      setState(() {
        userData = data;
      });
    });
  }

// bool myInterceptor(bool stopDefaultButtonEvent) {
//     Navigator.of(context).pop();
//     return true;
//   }

  @override
  void dispose() {
    super.dispose();
    //  BackButtonInterceptor.remove(myInterceptor);
    _pinController.dispose();
    _pinFocusNode.dispose();
    _connectionStatusSubscription.cancel();
  }

  void _onSubmit() async {
    setState(() {
      _pinErrText = "";
      _pinErrVisible = false;
      _submitErrText = "";
      _submitErrVisible = false;
    });
    var pinValidateResult =
        HelperFunctions.validatePin(pin: _pinController.text);
    if (!pinValidateResult["status"]) {
      setState(() {
        _pinErrText = pinValidateResult["message"];
        _pinErrVisible = true;
      });
      return;
    }
    setState(() {
      _pinFocusNode.unfocus();
      isSending = true;
    });
    var phoneNumber = HelperFunctions.serializePhoneNumber(
      phoneNumber: userData["phone"],
    );
    var sendResponse = await Provider.of<UserProvider>(context, listen: false)
        .login(body: {"phone": phoneNumber, "password": _pinController.text});

    if (sendResponse["status"] == 200 || sendResponse["status"] == 201) {
      setState(() {
        isSending = false;
        isBooting = true;
      });
     
      SocketConnection.getInstance()
          .startSocket(userId: sendResponse["data"]["data"]["id"]);
      SocketConnection.getInstance().updateUnreadNotificationCount();
      await HelperFunctions.startupApiCalls(context: context);
      Navigator.of(context).pushReplacementNamed(MainScreen.routeName);
    } else {
      setState(() {
        isSending = false;
        _submitErrText = sendResponse["errors"];
        _submitErrVisible = true;
      });
    }
  }

  void _forgotPin() async {
    var phoneNumber = HelperFunctions.serializePhoneNumber(
      phoneNumber: userData["phone"],
    );
    Provider.of<UserProvider>(context, listen: false)
        .getUserId(phoneNumber: phoneNumber);

    GeneralProvider.resetPasswordInitilzationPage = WelcomeBackScreen.routeName;
    //get pin reset otp here
    Provider.of<UserProvider>(context, listen: false).getOtpForPinReset(
      body: {"phone": phoneNumber},
    );
    Navigator.of(context).pushReplacementNamed(OtpScreen.routeName);
  }

  @override
  Widget build(BuildContext context) {
    var height = HelperFunctions.getDeviceUsabaleHeigth(context: context);
    var appColors = Provider.of<AppColorsProvider>(context, listen: false);
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          // physics: NeverScrollableScrollPhysics(),
          child: Stack(
            children: <Widget>[
              Container(
                width: double.infinity,
                height: height,
                padding: EdgeInsets.symmetric(
                  horizontal: 30,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 20),
                    HelperFunctions.buildLogo(context: context),
                    // ConstrainedBox(
                    //   constraints: BoxConstraints(
                    //     minHeight: 50,
                    //     maxHeight: 100,
                    //   ),
                    // ),
                    Spacer(),
                    // Expanded(
                    //     child: Container(
                    //         constraints: BoxConstraints(maxHeight: 100))),
                    Text(
                      "Welcome back ${userData["first_name"] == null ? "" : userData["first_name"]}!",
                      style: Theme.of(context).textTheme.body2.copyWith(
                            fontSize: 20,
                            height: 0.74,
                          ),
                    ),
                    SizedBox(height: 8),
                    if (userData["phone"] != "")
                      Text(
                        "******${HelperFunctions.deserializePhoneNumber(phoneNumber: userData["phone"]).substring(7)}",
                        style: Theme.of(context).textTheme.body2.copyWith(
                            fontWeight: FontWeight.w500, letterSpacing: 1),
                      ),
                    SizedBox(
                      height: 40,
                    ),
                    Text(
                      "Enter your 6 digits pin to access your account",
                      style: Theme.of(context).textTheme.body1.copyWith(
                            fontWeight: FontWeight.w300,
                          ),
                    ),
                    SizedBox(
                      height: 18,
                    ),
                    Text(
                      "Enter Pin",
                      style: Theme.of(context).textTheme.body2.copyWith(
                            fontWeight: FontWeight.normal,
                            height: 0.73,
                          ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    CustomTextField(
                      hintText: "6 digit pin",
                      textInputType: TextInputType.number,
                      obscureText: true,
                      textEditingController: _pinController,
                      focusNode: _pinFocusNode,
                      onChanged: (value) {
                        if (value.length >= 6) {
                          var pin = _pinController.text;
                          pin = pin.substring(0, 6);
                          _pinController.text = pin;
                          _pinFocusNode.unfocus();
                        }
                      },
                      inputFormatters: [
                        WhitelistingTextInputFormatter.digitsOnly,
                        LengthLimitingTextInputFormatter(6)
                      ],
                    ),
                    ErrText(
                      errText: _pinErrText,
                      vissibility: _pinErrVisible,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.bottomRight,
                          child: InkWell(
                            onTap: () {
                              return isSending
                                  ? () {}
                                  : Navigator.of(context).pushReplacementNamed(
                                      LoginScreen.routeName,
                                      arguments: {"isSlideBackward": false});
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(3),
                              child: Text(
                                "Login as another user",
                                style:
                                    Theme.of(context).textTheme.body1.copyWith(
                                          fontWeight: FontWeight.normal,
                                          color: Theme.of(context).primaryColor,
                                        ),
                              ),
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.bottomRight,
                          child: InkWell(
                            onTap: () {
                              return isSending ? () {} : _forgotPin();
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(3),
                              child: Text(
                                "Forgot pin?",
                                style: Theme.of(context)
                                    .textTheme
                                    .body1
                                    .copyWith(
                                        fontWeight: FontWeight.normal,
                                        color: Theme.of(context).primaryColor),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    ErrText(
                      errText: _submitErrText,
                      vissibility: _submitErrVisible,
                    ),
                    Spacer(
                      flex: 1,
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 30),
                      child: CustomRaisedButton(
                        title: isSending ? "" : "Login",
                        // color: appColors.primaryButtonColorText,
                        // bgColor: appColors.primaryButtonColorBg,
                        onTapAction: () {
                          isSending ? () {} : _onSubmit();
                        },
                        indicator: isSending
                            ? CustomProgressIndicator(
                                // color: appColors.primaryProgressValueColor,
                                )
                            : AnimatedContainer(
                                duration: Duration(milliseconds: 200),
                                child: Container(
                                  margin: EdgeInsets.only(left: 8),
                                  child: FaIcon(
                                    FontAwesomeIcons.lightSignIn,
                                    color: appColors.buttonIconColor,
                                    size: 15,
                                  ),
                                ),
                              ),
                      ),
                    ),
                    Spacer(
                      flex: 3,
                    ),
                  ],
                ),
              ),
              if (isBooting)
                HelperFunctions.loadingScreenAfterLogin(context: context),
            ],
          ),
        ),
      ),
    );
  }
}
