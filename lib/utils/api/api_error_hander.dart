import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:peer_score/screens/welcome_back.dart';
import 'package:peer_score/utils/connectivity_singleton.dart';
import 'package:peer_score/utils/functions.dart';

class ApiErrorHandler {
  static getErrorObject({dynamic error}) {
    var err;
    if (DioErrorType.RESPONSE == error.type) {
      err = error.error as Map<String, dynamic>;
      //incase of 401, goto login page
      if (err["status"] == 401) {
        var appriopriateKey = HelperFunctions.getAppriopriateScaffoldKey();
        Navigator.of(appriopriateKey.currentContext).pushReplacementNamed(
          WelcomeBackScreen.routeName,
        );
      }
    } else if (DioErrorType.DEFAULT == error.type) {
      if (error.message.contains('SocketException')) {
        err = {
          "status": "111",
          "errors": "Something went wrong please try again"
        };
      } else if (error.message.contains('HttpException')) {
        err = {
          "status": "500",
          "errors": "Something went wrong please try again"
        };
      }
    } else {
      err = error;
    }

    return err as Map<String, dynamic>;
  }

  static checkInternetConnectivity() async {
    ConnectionStatusSingleton connectionStatusSingleton =
        ConnectionStatusSingleton.getInstance();

    return await connectionStatusSingleton.checkConnection();
  }
}
