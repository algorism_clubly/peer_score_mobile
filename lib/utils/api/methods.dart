import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:peer_score/utils/api/api_error_hander.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UrlMethods {
  static Future getRequest(
      {String url, Map<String, String> customHeaders}) async {
    try {
      bool getCOnnectionStatus =
          await ApiErrorHandler.checkInternetConnectivity();
      if (!getCOnnectionStatus)
        throw DioError(
          error: {
            "status": 111,
            "errors": "Something went wrong, please try again."
          },
          type: DioErrorType.RESPONSE,
        );
      Dio dio = new Dio();
      final sharedPrefs = await SharedPreferences.getInstance();
      final token = sharedPrefs.containsKey("token")
          ? sharedPrefs.get("token")
          : json.encode("");
      var getResponse = await dio.get(
        url,
        options: Options(
            headers: customHeaders != null
                ? customHeaders
                : {
                    "token": token,
                    "Cache-Control": "no-cache",
                    "secret_key": "consolidatedEntry",
                  },
            followRedirects: false,
            validateStatus: (status) {
              return true;
            }),
      );
      Map<String, dynamic> getRes = getResponse.data;
      if (getRes["status"] != "false") {
        return {"status": getResponse.statusCode, "data": getRes["data"]};
      }
      throw DioError(
        error: {"status": getResponse.statusCode, "errors": getRes["message"]},
        type: DioErrorType.RESPONSE,
      );
    } on DioError catch (err) {
      throw ApiErrorHandler.getErrorObject(error: err);
    }
  }

  static Future<Map<String, dynamic>> postRequest(
      {String url, dynamic body}) async {
    try {
      bool getCOnnectionStatus =
          await ApiErrorHandler.checkInternetConnectivity();
      if (!getCOnnectionStatus)
        throw DioError(
          error: {
            "status": 111,
            "errors": "Something went wrong, please try again."
          },
          type: DioErrorType.RESPONSE,
        );

      Dio dio = new Dio();
      final sharedPrefs = await SharedPreferences.getInstance();
      final token = sharedPrefs.containsKey("token")
          ? sharedPrefs.get("token")
          : json.encode("");
      var postResponse = await dio.post(
        url,
        options: Options(
          headers: {
            "token": token,
            "Cache-Control": "no-cache",
            "secret_key": "consolidatedEntry",
          },
          followRedirects: false,
          validateStatus: (status) {
            return true;
          },
        ),
        data: body,
      );

      print("POST RESPONSE");
      print(postResponse.data);
      Map<String, dynamic> postRes = postResponse.data;
      if (postRes["status"] != "false") {
        return {"status": postResponse.statusCode, "data": postRes["data"]};
      }
      throw DioError(
        error: {
          "status": postResponse.statusCode,
          "errors": postRes["message"]
        },
        type: DioErrorType.RESPONSE,
      );
      // throw {"status": postResponse.statusCode, "errors": postRes["message"]};
    } on DioError catch (err) {
      throw ApiErrorHandler.getErrorObject(error: err);
    }
  }
}
