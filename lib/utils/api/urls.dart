class Urls {
  //test localhost
  // static String baseUrl = 'http://10.0.2.2:3030';
  //test real device(same network)
  // static String baseUrl = 'http://192.168.0.107:8000';
  //live(old)
  // static String baseUrl = 'http://34.244.112.245';
  // static String live = "http://18.202.243.149";
  static String live = "https://api.peerscore.ng";

  static String staging = "https://staging-peerscore.herokuapp.com";

  static String baseUrl = staging;
  static String login = baseUrl + "/users/login";
  static String verifyToken = baseUrl + "/passwords/verify-token";
  static Function ressolveAccountNumber =
      ({String accountNumber, String bankCode}) {
    return 'https://api.paystack.co/bank/resolve?account_number=$accountNumber&bank_code=$bankCode';
  };
  static String sendPhoneNumber = baseUrl + '/users/step_one';
  static String verifyOtp = baseUrl + '/otps/step_two';
  static String createPassword = baseUrl + '/passwords/create';
  static String updateFirstName = baseUrl + '/users/update_fname';
  static String updateUserInfo = baseUrl + '/users/update';
  static String syncPhone = baseUrl + '/users/sync';
  static String addNewAccount = baseUrl + '/accounts/create';
  static String getUserId = baseUrl + '/users/alien';
  static String getUserData = baseUrl + '/users/dash-info';
  static String updatePassword = baseUrl + '/passwords/reset';
  static Function getAllAccounts =
      (userId) => baseUrl + '/accounts/get_all/$userId';
  static Function getActiveAccount =
      (userId) => baseUrl + '/accounts/get_active/$userId';
  static String deleteBankAccount = baseUrl + '/accounts/delete';
  static String changeBankAccountStatus = baseUrl + '/accounts/activate';
  static String getOtpForAlternatePhone = baseUrl + '/otps/create_alternate';
  static String getOtpForPinReset = baseUrl + '/otps/reset-otp';
  static String createAlternatePhone = baseUrl + '/alternate_phone/create';
  static Function getAllAlternatePhones =
      (userId) => baseUrl + '/alternate_phone/get-all/$userId';
  static String deleteAlternatePhone = baseUrl + '/alternate_phone/delete';
  static String uploadProfilePicture = baseUrl + '/upload/image';
  static String createLoanRequest = baseUrl + '/loan-requests/create';
  static Function getLoanRequests =
      (providerPhone) => baseUrl + '/loan-requests/get-requests/$providerPhone';
  static String denyLoanRequest = baseUrl + '/loan-requests/deny';
  static String approveLoanWithReceipt = baseUrl + '/loan-requests/approve';
  static String createRequestPayment = baseUrl + '/loan/create';
  static String closeLoan = baseUrl + '/loan/close';
  static Function getLoans =
      (providerId) => baseUrl + '/loan/get-loans/$providerId';
  static Function getDebts =
      (recipientPhone) => baseUrl + '/loan/get-debts/$recipientPhone';
  static Function getContactOpenLoans = (userId, recipientId) =>
      baseUrl + '/loan/get-open-loans/$userId/$recipientId';
  static Function getUnregisteredContactOpenLoans = (userId, recipientPhone) =>
      baseUrl + '/loan/get-hazy-loans/$userId/$recipientPhone';
  static String offlineDebtPayment = baseUrl + '/loan/loan-paid';
  static String badClaim = baseUrl + '/loan/bad-claim';
  static String insistClaim = baseUrl + '/loan/insist';
  static String cancelClaim = baseUrl + '/loan/close-claim';
  static String reschedule = baseUrl + '/loan/re-schedule';
  static String confirmReschedule = baseUrl + '/loan/confirm-rs';
  static String declineReschedule = baseUrl + '/loan/decline-rs';
  static String confirmPay = baseUrl + '/loan/confirm-pay';
  static String flagPay = baseUrl + '/loan/flag-pay';
  static String recallAction = baseUrl + '/loan/recall-action';
  static Function getUserFinanceSummary = (providerId, recipientPhone) =>
      baseUrl + '/loan/get-finance/$providerId/$recipientPhone';
  static Function getClaimsList =
      (userId) => baseUrl + '/witness/get-all/$userId';
  static String passVerdict = baseUrl + '/witness/verdict';
  static Function getNotifications =
      (userId) => baseUrl + '/notifications/get-all/$userId';
  static Function getArchivedNotifications =
      (userId) => baseUrl + '/notifications/get-archived/$userId';
  static String markNotificationAsSeen = baseUrl + '/notifications/seen';
  static String markNotificationAsArchive = baseUrl + '/notifications/archive';
  static String unArchiveNotification = baseUrl + '/notifications/unarchive';
  static String archiveAll = baseUrl + '/notifications/archive-all';
  static String readAll = baseUrl + '/notifications/real-all';
  static String initiatePayment = baseUrl + '/payment/initiate';
  static String confirmPayment = baseUrl + '/payment/confirm';
  static String getPaystackKey = baseUrl + '/payment/get-active-key';
  static String initiateBvn = baseUrl + '/bvn/initiate';
  static String confirmBvn = baseUrl + '/bvn/confirm';
  static String deleteBvn = baseUrl + '/bvn/delete';
  static Function getBvn = (userId) => baseUrl + '/bvn/get-bvn/$userId';
  static String updateCredentials = baseUrl + '/google/calendar';
  static Function getCredentials = (userId) => baseUrl + '/google/fetch/$userId';
  static String getCalenderLoans = baseUrl + '/loan/calender';
}
