import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:rxdart/rxdart.dart';

class ConnectionStatusSingleton {
  static final ConnectionStatusSingleton _instance =
      ConnectionStatusSingleton._internal();
  ConnectionStatusSingleton._internal();

  static ConnectionStatusSingleton getInstance() => _instance;

  BehaviorSubject<bool> connectivityChangeSubject = BehaviorSubject();

  Connectivity _connectivity = Connectivity();
  var networkSubscription;
  void initialize() {
    networkSubscription = _connectivity.onConnectivityChanged.listen(
      (ConnectivityResult result) {
        if (result == ConnectivityResult.none) {
          connectivityChangeSubject.add(false);
        } else {
          checkConnection();
        }
      },
    );
  }

  Future<bool> checkConnection() async {
    bool connectionStatus;

    try {
      final result = await InternetAddress.lookup('example.com');
      
      
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        connectionStatus = true;
      } else {
        connectionStatus = false;
      }
    } on SocketException catch (err) {
      
      
      connectionStatus = false;
    }
    connectivityChangeSubject.add(connectionStatus);
    return connectionStatus;
  }

  void dispose() {
    connectivityChangeSubject.close();
    networkSubscription.cancel();
  }
}
