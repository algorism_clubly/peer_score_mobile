import 'package:flutter/material.dart';
import 'package:peer_score/providers/temp-provider.dart';

class CustomCheckBox extends StatefulWidget {
  final String title;
  final bool value;
  final Function onClick;
  bool isChecked;
  CustomCheckBox({this.title, this.onClick, this.value}) {
    isChecked = value;
  }
  @override
  _CustomCheckBoxState createState() => _CustomCheckBoxState();
}

class _CustomCheckBoxState extends State<CustomCheckBox> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        setState(() {
          widget.isChecked = !widget.isChecked;
        });
        widget.onClick(widget.isChecked);
      },
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            width: 20,
            height: 20,
            padding: EdgeInsets.all(2.3),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(2),
                color: widget.isChecked ? Color(0xFF73E5AE) : Colors.grey[300]),
            child: widget.isChecked
                ? SizedBox(
                    width: 8,
                    height: 8,
                    child: Image.asset(
                      "public/images/checkbox.png",
                    ),
                  )
                : Text(""),
          ),
          SizedBox(
            width: 10,
          ),
          if (widget.title != null)
            Text(
              widget.title,
              style: Theme.of(context).textTheme.body1.copyWith(
                    // fontSize: 11,
                    fontWeight: FontWeight.bold,
                    // height: 0.73,
                    color: GeneralProvider.isLightMode
                        ? Color(0xFF3D3D3D)
                        : Colors.white.withOpacity(0.7),
                  ),
            ),
        ],
      ),
    );
  }
}
