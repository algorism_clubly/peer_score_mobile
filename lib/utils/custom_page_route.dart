import 'package:flutter/material.dart';
import 'package:peer_score/screens/login.dart';
import 'package:peer_score/screens/main_screen.dart';
import 'package:peer_score/screens/name.dart';
import 'package:peer_score/screens/notification_screen.dart';
import 'package:peer_score/screens/onboarding_screen.dart';
import 'package:peer_score/screens/otp.dart';
import 'package:peer_score/screens/paystack_screen.dart';
import 'package:peer_score/screens/phone.dart';
import 'package:peer_score/screens/pin.dart';
import 'package:peer_score/screens/profile_screen.dart';
import 'package:peer_score/screens/waiting_screen.dart';
import 'package:peer_score/screens/welcome_back.dart';

class MyCustomPageAnimation extends MaterialPageRoute {
  final bool isSlideBackward;
  MyCustomPageAnimation(
      {WidgetBuilder builder,
      RouteSettings settings,
      this.isSlideBackward = true})
      : super(builder: builder, settings: settings);
  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    return SlideTransition(
        position: Tween<Offset>(
                begin: isSlideBackward ? Offset(1, 0) : Offset(-1, 0),
                end: Offset.zero)
            .animate(animation),
        child: child);
  }
}

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    //for main page nav
    int mainPageIndex = 0;
    int subPageIndex = 0;
    bool isSlideBackward = true;
    final arguments = settings.arguments as Map<String, dynamic>;

    
    

    if (arguments != null) {
      if (arguments["isSlideBackward"] != null) {
        isSlideBackward = arguments["isSlideBackward"];
      }

      //set mainPage stack InitialIndex here if accessed from a differet screen via notifications
      if (arguments["mainPageIndex"] != null)
        mainPageIndex = arguments["mainPageIndex"];
      if (arguments["subPageIndex"] != null)
        subPageIndex = arguments["subPageIndex"];
    }

    switch (settings.name) {
      case MainScreen.routeName:
        return MyCustomPageAnimation(
            builder: (ctx) => MainScreen(
                  mainPageIndex: mainPageIndex,
                  subPageIndex: subPageIndex,
                ),
            settings: settings,
            isSlideBackward: isSlideBackward);
      case PinScreen.routeName:
        return MyCustomPageAnimation(
            builder: (ctx) => PinScreen(), isSlideBackward: isSlideBackward);
      case OtpScreen.routeName:
        return MyCustomPageAnimation(
            builder: (ctx) => OtpScreen(), isSlideBackward: isSlideBackward);

      case PhoneNumberScreen.routeName:
        return MyCustomPageAnimation(
            builder: (ctx) => PhoneNumberScreen(),
            isSlideBackward: isSlideBackward);
      case NameScreen.routeName:
        return MyCustomPageAnimation(
            builder: (ctx) => NameScreen(), isSlideBackward: isSlideBackward);
      case LoginScreen.routeName:
        return MyCustomPageAnimation(
            builder: (ctx) => LoginScreen(), isSlideBackward: isSlideBackward);
      case WelcomeBackScreen.routeName:
        return MyCustomPageAnimation(
            builder: (ctx) => WelcomeBackScreen(),
            isSlideBackward: isSlideBackward);
      case WaitingScreen.routeName:
        return MyCustomPageAnimation(
            builder: (ctx) => WaitingScreen(),
            isSlideBackward: isSlideBackward);
      case ProfileScreen.routeName:
        return MyCustomPageAnimation(
            builder: (ctx) => ProfileScreen(),
            isSlideBackward: isSlideBackward);
      case NotificationScreen.routeName:
        return MyCustomPageAnimation(
            builder: (ctx) => NotificationScreen(),
            isSlideBackward: isSlideBackward);
      case OnboadingScreen.routeName:
        return MyCustomPageAnimation(
            builder: (ctx) => OnboadingScreen(),
            isSlideBackward: isSlideBackward);
      case PaystackScreen.routeName:
        return MyCustomPageAnimation(
            builder: (ctx) => PaystackScreen(
                  arguments: arguments,
                ),
            isSlideBackward: isSlideBackward);
    }
  }
}
