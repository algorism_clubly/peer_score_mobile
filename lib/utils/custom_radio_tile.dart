import 'package:flutter/material.dart';
import 'package:peer_score/providers/temp-provider.dart';

class CustomRadioTile extends StatelessWidget {
  final int value;
  final int selectedValue;
  final String title;
  final Function onSelect;
  final TextStyle style;
  CustomRadioTile(
      {this.value, this.selectedValue, this.title, this.onSelect, this.style});
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        unselectedWidgetColor: Theme.of(context).primaryColor,
      ),
      child: Row(
        children: <Widget>[
          Container(
            height: 15,
            width: 15,
            alignment: Alignment.centerLeft,
            child: Radio(
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
              value: value,
              groupValue: selectedValue,
              activeColor: Theme.of(context).primaryColor,
              onChanged: (value) => onSelect(value),
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: InkWell(
              onTap: () => onSelect(value),
              child: Container(
                height: 20,
                child: FittedBox(
                  alignment: Alignment.centerLeft,
                  fit: BoxFit.none,
                  child: Text(
                    title,
                    style: style != null
                        ? style
                        : Theme.of(context).textTheme.body1.copyWith(
                              fontWeight: FontWeight.w300,
                            ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
