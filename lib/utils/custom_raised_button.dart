import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:peer_score/providers/temp-provider.dart';

class CustomRaisedButton extends StatelessWidget {
  final String title;
  final double width;
  final double height;
  final double fontSize;
  final FontWeight fontWeight;
  final Widget indicator;
  final Widget leadingWidget;
  final MainAxisAlignment mainAxisAlignment;
  final Function onTapAction;
  final Color bgColor;
  final Color color;
  final String leadingIconPath;
  final BorderRadius borderRadius;

  CustomRaisedButton(
      {this.indicator,
      this.title,
      this.onTapAction,
      this.bgColor,
      this.color,
      this.leadingIconPath,
      this.width,
      this.borderRadius,
      this.height,
      this.fontSize,
      this.fontWeight,
      this.leadingWidget,
      this.mainAxisAlignment});
  @override
  Widget build(BuildContext context) {
    // Color themeTextColor = color == null ? Colors.white : color;
    // Color themeBgColor = bgColor == null ? Theme.of(context).primaryColor : bgColor;

    return InkWell(
      onTap: onTapAction,
      child: Container(
        height: height != null ? height : 40,
        width: width != null ? width : 150,
        // padding: EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(
            color: bgColor != null ? bgColor : Theme.of(context).primaryColor,
            borderRadius:
                borderRadius != null ? borderRadius : BorderRadius.circular(4)),
        child: Row(
          mainAxisAlignment: mainAxisAlignment != null
              ? mainAxisAlignment
              : MainAxisAlignment.center,
          children: <Widget>[
            if (leadingWidget != null) leadingWidget,
            if (leadingIconPath != null)
              SizedBox(
                height: 20,
                child: Image.asset(leadingIconPath),
              ),
            if (leadingIconPath != null || leadingWidget != null)
              SizedBox(
                width: 8,
              ),
           if(title != "" && title != null) FittedBox(
              child: Text(
                title,
                style: Theme.of(context).textTheme.body1.copyWith(
                      color: color != null ? color : GeneralProvider.isLightMode ? Colors.white : Colors.black,
                      fontSize: fontSize != null ? fontSize : 14,
                      fontWeight:
                          fontWeight != null ? fontWeight : FontWeight.w600,
                    ),
              ),
            ),
            if (indicator != null) indicator
          ],
        ),
      ),
    );
  }
}

class CustomUploadButton extends StatelessWidget {
  final String leadingIconPath;
  final double width;
  final double height;
  final Color bgColor;
  final String title;
  final Function onClick;
  final IconData leadingIcon;
  final TextStyle style;
  final Widget indicator;
  CustomUploadButton(
      {this.bgColor,
      this.leadingIconPath,
      this.onClick,
      this.title,
      this.width = 119,
      this.height = 33,
      this.style,
      this.leadingIcon,
      this.indicator});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onClick,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4),
        child: Container(
          width: width,
          height: height,
          constraints: BoxConstraints(maxHeight: height),
          decoration: BoxDecoration(color: bgColor),
          child: Stack(
            children: <Widget>[
              Container(
                width: height,
                constraints: BoxConstraints(maxHeight: height),
                color: Color.fromRGBO(0, 0, 0, 0.28),
                alignment: Alignment.center,
                child: leadingIcon != null
                    ? FaIcon(
                        leadingIcon,
                        color: Colors.white,
                      )
                    : SizedBox(
                        width: 12,
                        child: Image.asset(leadingIconPath),
                      ),
              ),
              Align(
                alignment: Alignment.center,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(left: width <= 119 ? 20 : 0),
                      child: Text(
                        title,
                        style: style != null
                            ? style
                            : Theme.of(context).textTheme.body1.copyWith(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 10,
                                  color: Colors.white,
                                ),
                      ),
                    ),
                    if (indicator != null) indicator
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
