import 'package:flutter/material.dart';
import 'package:peer_score/providers/colors.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:provider/provider.dart';

class CustomSwitch extends StatefulWidget {
  final bool value;
  final double height;
  final double width;
  final Function(bool value) onClick;
  final EdgeInsetsGeometry padding;
  double toggeleWidth;
  CustomSwitch(
      {this.value,
      this.height = 18,
      this.width = 32,
      this.onClick,
      this.padding}) {
    toggeleWidth = height - 5;
  }
  @override
  _CustomSwitchState createState() => _CustomSwitchState();
}

class _CustomSwitchState extends State<CustomSwitch> {
  @override
  Widget build(BuildContext context) {
    var appColors = Provider.of<AppColorsProvider>(context);
    return InkWell(
      onTap: () {
        widget.onClick(!widget.value);
      },
      child: Container(
        padding: widget.padding != null ? widget.padding : null,
        child: Container(
          width: widget.width,
          height: widget.height,
          child: Stack(
            alignment: AlignmentDirectional.center,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  color: widget.value
                      ? AppColorsProvider.primaryColorLight
                      : appColors.inactiveSwitch,
                  // : Color(0xFFB6B6B6),
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
              TweenAnimationBuilder(
                duration: Duration(milliseconds: 200),
                tween: Tween<double>(
                    begin: widget.value
                        ? 2
                        : (widget.width - (widget.toggeleWidth + 2)),
                    end: widget.value
                        ? (widget.width - (widget.toggeleWidth + 2))
                        : 2),
                builder: (_, togglePosition, child) {
                  return Positioned(
                    left: togglePosition,
                    child: Container(
                      height: widget.toggeleWidth,
                      width: widget.toggeleWidth,
                      decoration: BoxDecoration(
                          color: widget.value ? appColors.toggleActiveColor : appColors.toggleInactiveColor  ,
                          shape: BoxShape.circle),
                    ),
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
