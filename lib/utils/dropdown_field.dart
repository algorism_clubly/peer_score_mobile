import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:peer_score/providers/temp-provider.dart';

class DropDownField extends StatefulWidget {
  final String defaultItem;
  final List<dynamic> items;
  final String hintText;
  final bool dropDownOnly;
  final double borderRadius;
  final double height;
  final Color borderColor;
  final Function onChanged;
  final Function onTextInput;
  final TextEditingController textEditingController;
  final FocusNode focusNode;
  final TextInputType textInputType;
  final Function onFieldSubmitted;
  final Function onTap;
  final List<TextInputFormatter> inputFormatters;
  var _value = "";
  DropDownField({
    this.defaultItem,
    this.items,
    this.hintText,
    this.dropDownOnly = false,
    this.borderColor,
    this.borderRadius,
    this.height,
    this.onChanged,
    this.textEditingController,
    this.focusNode,
    this.textInputType,
    this.onFieldSubmitted,
    this.onTap,
    this.inputFormatters,
    this.onTextInput
  }) {
    _value = defaultItem;
  }

  @override
  _DropDownFieldState createState() => _DropDownFieldState();
}

class _DropDownFieldState extends State<DropDownField> {
  bool isLightMode = GeneralProvider.isLightMode;
  void _onSelectBank(String value) {
    setState(() {
      widget._value = value;
      widget.onChanged(value);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.height != null ? widget.height : 46,
      child: TextFormField(
        controller: widget.textEditingController,
        inputFormatters:
            widget.inputFormatters != null ? widget.inputFormatters : null,
        focusNode: widget.focusNode,
        keyboardType: widget.textInputType != null
            ? widget.textInputType
            : TextInputType.multiline,
        onFieldSubmitted: (value) {
          if (widget.onFieldSubmitted != null) {
            widget.onFieldSubmitted(value);
          }
        },
        onTap: widget.onTap != null ? widget.onTap : () {},
        onChanged: (value) => widget.onTextInput != null ? widget.onTextInput(value) : null,
        style: Theme.of(context).textTheme.body1,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.zero,
          hintText: widget.hintText,
          hintStyle: Theme.of(context).textTheme.body2.copyWith(
                fontWeight: FontWeight.w300,
                color: Color(0xFF9C9C9C),
              ),
          prefixIcon: Container(
            width: widget.dropDownOnly ? double.infinity : 85,
            height: 30,
            padding:
                EdgeInsets.symmetric(horizontal: widget.dropDownOnly ? 20 : 12),
            margin: EdgeInsets.only(right: widget.dropDownOnly ? 0 : 20),
            decoration: BoxDecoration(
              border: Border(
                right: widget.dropDownOnly
                    ? BorderSide.none
                    : BorderSide(
                        width: 1.5,
                        color: widget.borderColor != null
                            ? widget.borderColor
                            : GeneralProvider.isLightMode
                                ? Color(0xFFB19CD9)
                                : Colors.grey[850],
                      ),
              ),
            ),
            child: DropdownButton(
              underline: Text(""),
              icon: FaIcon(
                Platform.isIOS
                    ? const IconData(0xf3d0,
                        fontFamily: "CupertinoIcons",
                        fontPackage: "cupertino_icons")
                    : FontAwesomeIcons.chevronDown,
                size: Platform.isIOS ? 10 : 8,
                color: GeneralProvider.isLightMode ? Colors.black : Colors.white
              ),
              isExpanded: true,
              value: widget._value,
              items: widget.items
                  .map(
                    (currency) => DropdownMenuItem(
                      value: currency,
                      child: FittedBox(
                        child: Padding(
                          padding: const EdgeInsets.only(right: 10),
                          child: Text(
                            currency,
                            style: Theme.of(context).textTheme.body1.copyWith(
                                // fontWeight: FontWeight.normal,
                                ),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ),
                    ),
                  )
                  .toList(),
              onChanged: (value) => _onSelectBank(value),
            ),
          ),
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  width: GeneralProvider.isLightMode ? 1 : 1.5,
                  color: widget.borderColor != null
                      ? widget.borderColor
                      : GeneralProvider.isLightMode
                          ? Color(0xFFB19CD9)
                          : Colors.grey[850]),
              borderRadius: BorderRadius.circular(
                  widget.borderRadius != null ? widget.borderRadius : 4)),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
                width: GeneralProvider.isLightMode ? 1 : 1.5,
                color: widget.borderColor != null
                    ? widget.borderColor
                    : GeneralProvider.isLightMode
                        ? Color(0xFFB19CD9)
                        : Colors.grey[850]),
            borderRadius: BorderRadius.circular(
                widget.borderRadius != null ? widget.borderRadius : 4),
          ),
        ),
      ),
    );
  }
}
