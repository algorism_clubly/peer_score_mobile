enum OnboardingScreens { otp, pin, welcomeBack, login, number, name }

enum ContactlistFilterOptions { all, peerScoreMembers, nonPeerScoreMembers }

enum ContactlistSortOptions { complianceDesc, complianceAsc }

enum LoanlistFilterOptions {
  all,
  paid,
  overdue,
  reschedule,
  flagged,
  createdRange,
  dueRange
}

enum LoanlistSortOptions {
  duedateAsc,
  duedateDesc,
  amountAsc,
  amountDesc,
  createdDateAsc,
  createdDateDesc
}


enum MainPageStackEnum {
  dashboard, debtors, creditors, contacts 
}

enum DashboardStackEnum {
  dashboardPage, witnessList, votePage 
}

enum ExpectedPaymentsStackEnum {
  debtorsPage, loanRequests, 
}

enum CreditorsStackEnum {
  creditorsPage, 
}