import 'package:flutter/material.dart';

class ErrText extends StatelessWidget {
  final bool vissibility;
  final String errText;
  ErrText({this.errText, this.vissibility});
  @override
  Widget build(BuildContext context) {
    return vissibility == true
        ? Container(
            margin: EdgeInsets.only(top: 10),
            child: Text(
              errText,
              style: Theme.of(context).textTheme.body1.copyWith(
                    fontWeight: FontWeight.normal,
                    color: Theme.of(context).errorColor,
                  ),
            ),
          )
        : SizedBox(
            height: 0,
          );
  }
}
