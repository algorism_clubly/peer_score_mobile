import 'dart:io';

import 'package:flutter/scheduler.dart';
import 'package:flutter_paystack/flutter_paystack.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:peer_score/providers/bank_accounts_provider.dart';
import 'package:peer_score/providers/bvn_provider.dart';
import 'package:peer_score/providers/colors.dart';
import 'package:peer_score/providers/contact.dart';
import 'package:flutter/material.dart';
import 'package:peer_score/providers/expected_payments_provider.dart';
import 'package:peer_score/providers/notifications_provider.dart';
import 'package:peer_score/providers/payment_provider.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/screens/paystack_screen.dart';
import 'package:peer_score/utils/animatedcount.dart';
import 'package:peer_score/utils/connectivity_singleton.dart';
import 'package:peer_score/utils/custom_raised_button.dart';
import 'package:peer_score/utils/enums.dart';
import 'package:peer_score/utils/err_text_widget.dart';
import 'package:peer_score/utils/googleCalender.dart';
import 'package:peer_score/utils/modal_actions.dart';
import 'package:peer_score/utils/progress_indicator.dart';
import 'package:peer_score/utils/snackbar.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:timeago/timeago.dart' as timeago;
// import 'package:sms_maintained/sms.dart';
import 'package:date_range_picker/date_range_picker.dart' as DateRagePicker;
import 'package:url_launcher/url_launcher.dart';

class HelperFunctions {
  static List<Contact> searchContacts(
      {String searchValue, List<Contact> contactList}) {
    searchValue = searchValue.trim();
    bool searchInt;
    final stringRegEx = RegExp(r"^[a-zA-Z]+$", multiLine: true);
    final intRegEx = RegExp(r"^[0-9]+$", multiLine: true);
    if (stringRegEx.hasMatch(searchValue)) {
      searchInt = false;
    } else if (intRegEx.hasMatch(searchValue)) {
      searchInt = true;
    }
    List<Contact> tempContactList = [];
    //return all data if search criteria is null
    if (searchInt == null && searchValue.isEmpty) {
      contactList.forEach((contact) {
        tempContactList.add(contact);
      });
    } else if (searchInt) {
      contactList.forEach((contact) {
        if (contact.phoneNumber
            .toLowerCase()
            .contains(searchValue.toLowerCase())) tempContactList.add(contact);
      });
    } else {
      contactList.forEach((contact) {
        if (HelperFunctions.getContactFullName(contact: contact)
            .toLowerCase()
            .contains(searchValue.toLowerCase())) tempContactList.add(contact);
      });
    }
    return tempContactList;
  }

  static List<Loan> searchLoans(
      {String searchValue, List<Loan> loanList, bool isDebt = true}) {
    //search criteria
    //-name
    //-amount
    //-phone number

    searchValue = searchValue.trim();
    bool searchingNum;
    final stringRegEx = RegExp(r"^[a-zA-Z]+$", multiLine: true);
    final intRegEx = RegExp(r"^[0-9]+$", multiLine: true);

    if (stringRegEx.hasMatch(searchValue)) {
      searchingNum = false;
    } else if (intRegEx.hasMatch(searchValue)) {
      searchingNum = true;
    }
    List<Loan> tempLoanList = [];
    //return all data if search criteria is null
    if (searchingNum == null && searchValue.isEmpty) {
      loanList.forEach((loan) {
        tempLoanList.add(loan);
      });
    } else if (searchingNum) {
      loanList.forEach((loan) {
        //first check for amount match, if no match then proceed search phone
        String phoneNumber =
            isDebt ? loan.debtor.phoneNumber : loan.creditor.phoneNumber;
        if (loan.amount.toLowerCase().contains(
              searchValue.toLowerCase(),
            )) {
          tempLoanList.add(loan);
        } else if (phoneNumber
            .toLowerCase()
            .contains(searchValue.toLowerCase())) {
          tempLoanList.add(loan);
        }
      });
    } else {
      //search for the name here
      loanList.forEach((loan) {
        if (HelperFunctions.getContactFullName(
                contact: isDebt ? loan.debtor : loan.creditor)
            .toLowerCase()
            .contains(searchValue.toLowerCase())) tempLoanList.add(loan);
      });
    }

    return tempLoanList;
  }

  static List<Loan> sortLoans({dynamic type, List<Loan> loanList}) {
    switch (type) {
      case LoanlistSortOptions.amountAsc:
        loanList.sort(
          (a, b) => double.parse(a.amount).compareTo(
            double.parse(b.amount),
          ),
        );
        break;
      case LoanlistSortOptions.amountDesc:
        loanList.sort(
          (a, b) => double.parse(b.amount).compareTo(
            double.parse(a.amount),
          ),
        );
        break;
      case LoanlistSortOptions.createdDateAsc:
        loanList.sort(
          (a, b) => a.createdAt.difference(b.createdAt).inSeconds,
        );
        break;
      case LoanlistSortOptions.createdDateDesc:
        loanList.sort(
          (a, b) => b.createdAt.difference(a.createdAt).inSeconds,
        );
        break;
      case LoanlistSortOptions.duedateAsc:
        loanList.sort(
          (a, b) => a.date.difference(b.date).inSeconds,
        );
        break;
      case LoanlistSortOptions.duedateDesc:
        loanList.sort(
          (a, b) => b.date.difference(a.date).inSeconds,
        );
        break;
      default:
    }

    return loanList;
  }

  static Future<List<Loan>> filterLoans({
    dynamic type,
    dynamic sortType,
    List<Loan> loanList,
    BuildContext context,
    Function resultCb,
  }) async {
    bool status = true;
    String message;
    List<Loan> loanListCopy = [...loanList];
    //filter section
    switch (type) {
      case LoanlistFilterOptions.all:
        // loanList.sort(
        //     (a, b) => double.parse(a.amount).compareTo(double.parse(b.amount)));
        break;
      case LoanlistFilterOptions.overdue:
        loanListCopy = loanListCopy
            .where((loan) =>
                HelperFunctions.getLoanPeriodInfo(dueDate: loan.date)
                    .contains("late"))
            .toList();
        break;
      case LoanlistFilterOptions.flagged:
        loanListCopy = loanListCopy
            .where(
              (loan) => (loan.loanActions != null &&
                  loan.loanActions['type'] == 'bad'),
            )
            .toList();
        break;
      case LoanlistFilterOptions.reschedule:
        loanListCopy = loanListCopy
            .where((loan) => (loan.loanActions != null &&
                loan.loanActions['type'] == 'shift'))
            .toList();
        break;
      case LoanlistFilterOptions.paid:
        loanListCopy = loanListCopy
            .where((loan) => (loan.loanActions != null &&
                loan.loanActions['type'] == 'paid'))
            .toList();
        break;
      case LoanlistFilterOptions.createdRange:
        var firstDate = DateTime(DateTime.now().year - 10);
        var lastDate = DateTime(DateTime.now().year + 10);
        var initialDate = DateTime.now();
        Theme(data: ThemeData.dark(), child: Container());
        final List<DateTime> picked = await DateRagePicker.showDatePicker(
          context: context,
          initialFirstDate: initialDate,
          initialLastDate: initialDate,
          firstDate: firstDate,
          lastDate: lastDate,
        );

        DateTime firstdate;
        DateTime seconddate;
        if (picked != null) {
          firstdate = picked[0];
          seconddate = picked.length == 1 ? picked[0] : picked[1];
          loanListCopy = loanListCopy
              .where(
                (loan) =>
                    loan.createdAt.isAfter(firstdate) &&
                    loan.createdAt.isBefore(seconddate),
              )
              .toList();
          message =
              "Loans created between ${DateFormat.yMMMMd().format(firstdate)} and ${DateFormat.yMMMMd().format(seconddate)}";
        } else {
          status = false;
        }
        break;
      case LoanlistFilterOptions.dueRange:
        var firstDate = DateTime(DateTime.now().year - 10);
        var lastDate = DateTime(DateTime.now().year + 10);
        var initialDate = DateTime.now();
        // Theme(
        //   data: ThemeData.dark(),
        //   child: Builder(builder: (context) {
        //     return Container();
        //   }),
        // );
        final List<DateTime> picked = await DateRagePicker.showDatePicker(
          context: context,
          initialFirstDate: initialDate,
          initialLastDate: initialDate,
          firstDate: firstDate,
          lastDate: lastDate,
        );
        DateTime firstdate;
        DateTime seconddate;
        if (picked != null) {
          firstdate = picked[0];
          seconddate = picked.length == 1 ? picked[0] : picked[1];

          loanListCopy = loanListCopy
              .where((loan) =>
                  loan.date.isAfter(firstdate) &&
                  loan.date.isBefore(seconddate))
              .toList();
          message =
              "Loans due between ${DateFormat('dd-MM-y').format(firstdate)} and ${DateFormat('dd-MM-y').format(seconddate)}";
        } else {
          status = false;
        }
        break;
      default:
        break;
    }
    //sort section

    resultCb(
        status, sortLoans(type: sortType, loanList: loanListCopy), message);
    // return loanListCopy;
  }

  static String getContactFullName({Contact contact}) {
    return (contact.firstName != null
            ? toBeginningOfSentenceCase(contact.firstName)
            : "") +
        (contact.otherName != null
            ? (" " + toBeginningOfSentenceCase(contact.otherName))
            : "") +
        (contact.lastName != null
            ? (" " + toBeginningOfSentenceCase(contact.lastName))
            : "");
  }

  static String serializePhoneNumber({String phoneNumber}) {
    phoneNumber.trim();

    //if phone not serializable return
    // if(phoneNumber.length < 3)

    //if space separated
    if (phoneNumber.split(' ').length > 0) {
      phoneNumber = phoneNumber.split(' ').join();
    }
    //if hyphen separated
    if (phoneNumber.split("-").length > 0) {
      phoneNumber = phoneNumber.split('-').join();
    }
    if (phoneNumber.split('')[0] == '0') {
      phoneNumber = phoneNumber.substring(1);
    }
    if (phoneNumber.substring(0, 4) == "+234") {
      phoneNumber = phoneNumber.substring(4);
    }
    if (phoneNumber.substring(0, 3) == "234") {
      phoneNumber = phoneNumber.substring(3);
    }

    return "+234" + phoneNumber;
  }

  static String deserializePhoneNumber({String phoneNumber}) {
    phoneNumber = phoneNumber.replaceFirst("+234", "0");
    return phoneNumber;
  }

  static Map<String, dynamic> validatePhone({String phoneNumber}) {
    final phoneRegEx = RegExp(r"^\+[0-9]{13}$", multiLine: true);
    phoneNumber = phoneNumber.trim();
    if (phoneNumber.isEmpty) {
      return {"status": false, "message": "Please enter phone number"};
    }
    if (phoneNumber.split('')[0] == '0') {
      phoneNumber = phoneNumber.substring(1);
    }
    var phoneMaxLength = 10;
    // phoneNumber.split('')[0] == '0' ? phoneMaxLength = 11 : phoneMaxLength = 10;
    if (phoneNumber.length < phoneMaxLength) {
      return {"status": false, "message": "Please enter a valid phone number"};
    }
    // Validate phone number to be within network phoneNumber prefixes
    var providedProvidedPhonePrefix = phoneNumber.substring(0, 2);
    var prefixes = ["70", "80", "81", "90","91"];
    if (!prefixes.contains(providedProvidedPhonePrefix)) {
      return {
        "status": false,
        "message": "Please enter a valid Nigerian phone number"
      };
    }

    var serializedPhoneNumber = serializePhoneNumber(phoneNumber: phoneNumber);
    //14 long characters here including +234
    if (serializedPhoneNumber.length > 14) {
      return {"status": false, "message": "Please enter a valid phone number"};
    }

    if (!phoneRegEx.hasMatch(serializedPhoneNumber)) {
      return {"status": false, "message": "Please enter a valid phone number"};
    }

    return {"status": true, "message": "Validated"};
  }

  static Map<String, dynamic> validatePin({String pin}) {
    String currentPin = pin;
    currentPin = currentPin.trim();
    final pinRegEx = RegExp(r"^[0-9]{6}$", multiLine: true);
    if (currentPin.isEmpty) {
      return {"status": false, "message": "Pin cannot be empty"};
    }
    if (currentPin.length < 6) {
      return {"status": false, "message": "Pin must be six digits long"};
    }
    if (!pinRegEx.hasMatch(currentPin)) {
      return {"status": false, "message": "Invalid pin"};
    }

    return {"status": true, "message": "Pin Validated"};
  }

  static Map<String, dynamic> validateOtp({String otp}) {
    String currentOtp = otp;
    currentOtp = currentOtp.trim();
    final pinRegEx = RegExp(r"^[0-9]{4}$", multiLine: true);
    if (currentOtp.isEmpty) {
      return {"status": false, "message": "OTP cannot be empty"};
    }
    if (currentOtp.length < 4) {
      return {"status": false, "message": "OTP must be four digits long"};
    }
    if (!pinRegEx.hasMatch(currentOtp)) {
      return {"status": false, "message": "Invalid OTP"};
    }

    return {"status": true, "message": "OTP Validated"};
  }

  static Map<String, dynamic> validatePinCreation(
      {String pin, String confirmPin}) {
    pin = pin.trim();
    confirmPin = confirmPin.trim();
    final pinRegEx = RegExp(r"^[0-9]{6}$", multiLine: true);

    if (pin.isEmpty) {
      return {"status": false, "message": "Enter your 6 digit pin"};
    }
    if (confirmPin.isEmpty) {
      return {"status": false, "message": "Enter a pin same as above"};
    }
    if (!pinRegEx.hasMatch(pin)) {
      return {"status": false, "message": "Pin must be 6 digits long"};
    }
    if (!pinRegEx.hasMatch(confirmPin)) {
      return {"status": false, "message": "Pin must be 6 digits long"};
    }
    if (pin != confirmPin) {
      return {"status": false, "message": "Pin does not match"};
    }

    return {"status": true, "message": "Pin Validated"};
  }

  static String getFormattedAmount({String amount}) {
    return NumberFormat.currency(
            symbol: NumberFormat().simpleCurrencySymbol("NGN"))
        .format(num.parse(amount));
  }

  static String getLoanPeriodInfo({DateTime dueDate}) {
    var dateString = DateFormat('dd-MM-y').format(dueDate);
    List<String> dateBD = dateString.split("-");
    DateTime formattedDate = DateTime(
        num.parse(dateBD[2]), num.parse(dateBD[1]), num.parse(dateBD[0]));
    var days = DateTime.now().difference(formattedDate).inDays.toString();
    var parsedDays = double.parse(days);
    var daysToMonth = parsedDays * 0.0328767;
    var dateInfo = "";
    if ((daysToMonth / 12) >= 1) {
      var yearCount = (daysToMonth / 12).ceil();
      dateInfo = "${yearCount} year${yearCount > 1 ? "s" : ""} late";
    } else if (daysToMonth >= 1) {
      dateInfo =
          "${daysToMonth.ceil()} month${daysToMonth.ceil() > 1 ? "s" : ""} late";
    } else if (parsedDays > 1) {
      dateInfo =
          "${parsedDays.ceil()} day${parsedDays.ceil() > 1 ? "s" : ""} late";
    } else {
      dateInfo = "Active";
    }
    return "$dateInfo";
  }

  static String getNotificationLapsePeriod({DateTime createdDate}) {
    return timeago.format(createdDate);
  }

  static double getDeviceUsabaleHeigth({BuildContext context}) {
    var deviceSize = MediaQuery.of(context).size;
    var deicePaddingInsets = MediaQuery.of(context).padding;

    var height =
        deviceSize.height - deicePaddingInsets.top - deicePaddingInsets.bottom;

    return height;
  }

  // static dynamic sendSms(
  //     {String recipientPhone,
  //     String messageText,
  //     BuildContext context,
  //     SimCard selectedSimCard,
  //     Function smsCb}) async {
  //   PermissionStatus permission = await Permission.sms.request();
  //   if (permission != null) {
  //     if (permission == PermissionStatus.permanentlyDenied) {
  //       smsCb(false);
  //       return openAppSettings();
  //     } else if (permission == PermissionStatus.denied) {
  //       smsCb(false);
  //       return await Permission.sms.request();
  //     }
  //     var status = false;
  //     SmsSender sender = SmsSender();
  //     SmsMessage message = SmsMessage(recipientPhone, messageText);
  //     sender.sendSms(message, simCard: selectedSimCard);
  //     message.onStateChanged.listen((state) async {
  //       if (state == SmsMessageState.Sent) {
  //         status = true;
  //         smsCb(status);
  //       } else if (state == SmsMessageState.Delivered) {
  //         status = true;
  //       } else if (state == SmsMessageState.Fail) {
  //         status = false;
  //         smsCb(status);
  //       }
  //     });
  //   }
  // }

  static Future<DateTime> getSelectedDate(
      {BuildContext context,
      DateTime initialDate,
      DateTime firstDate,
      DateTime lastDate}) async {
    return showDatePicker(
        context: context,
        initialDate: initialDate,
        firstDate: firstDate,
        lastDate: lastDate,
        builder: (ctx, _) {
          return Theme(
              data: !GeneralProvider.isLightMode
                  ? ThemeData.dark()
                      .copyWith(accentColor: Color(0xffE78200).withOpacity(0.8))
                  : Theme.of(context),
              child: _);
        });
  }

  static Widget buildLoanTile({
    BuildContext context,
    Contact contact,
    String amount,
    String type,
    DateTime dueDate,
    String itemId,
    String selectedItemId,
    bool outlineCOlor = false,
    bool debt = false,
    Map<String, dynamic> action,
    Function onOutlineClicked,
  }) {
    Color moneyBagColor = Theme.of(context).primaryColor;
    if (contact.id == null) {
      moneyBagColor = Colors.grey[600];
    } else if (type == "loan request") {
      moneyBagColor = Color.fromRGBO(74, 58, 24, 0.8);
    } else if (HelperFunctions.getLoanPeriodInfo(dueDate: dueDate)
        .contains("late")) {
      moneyBagColor = Color(0xffF6B52F);
    }
    if (debt == false && action != null) {
      switch (action["type"]) {
        case "shift":
          moneyBagColor = Colors.blue;
          break;
        case "paid":
          moneyBagColor = Colors.green;
          break;
        case "bad":
          moneyBagColor = Colors.red;
          break;
        // default:
        //   moneyBagColor = Theme.of(context).primaryColor;
      }
    }
    var appColors = Provider.of<AppColorsProvider>(context, listen: false);
    return Stack(
      children: <Widget>[
        if (itemId == selectedItemId)
          Container(
            height: 77,
            decoration: BoxDecoration(
              color: appColors.loanTileSelectedContainerCOlor,
              border: Border.all(
                width: 1,
                color: appColors.loanTileSelectedContainerBorderCOlor,
              ),
            ),
          ),
        InkWell(
          onTap: () => onOutlineClicked(),
          child: Container(
            height: 77,
            margin: EdgeInsets.symmetric(horizontal: 30),
            decoration: BoxDecoration(
              border: itemId == selectedItemId
                  ? null
                  : Border(
                      bottom: BorderSide(
                        width: GeneralProvider.isLightMode ? 1 : 0.3,
                        color: appColors.clickedTileBorderColor,
                      ),
                    ),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Stack(
                  alignment: AlignmentDirectional.center,
                  children: <Widget>[
                    SizedBox(
                      height: 45,
                      width: 35,
                      child: Image.asset(
                        "public/images/plain-money-bag.png",
                        color: moneyBagColor,
                      ),
                    ),
                    Positioned(
                      bottom: 2,
                      child: SizedBox(
                        height: 20,
                        width: 16,
                        child: Image.asset(
                          "public/images/naira-symbol.png",
                          color: Colors.white,
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(width: 10),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      if (HelperFunctions.getContactFullName(
                              contact: contact) !=
                          "")
                        Text(
                          HelperFunctions.getContactFullName(contact: contact),
                          style: Theme.of(context).textTheme.body1.copyWith(
                                fontWeight: FontWeight.w300,
                              ),
                          overflow: TextOverflow.ellipsis,
                        ),
                      Text(
                        HelperFunctions.deserializePhoneNumber(
                            phoneNumber: contact.phoneNumber),
                        style: Theme.of(context).textTheme.body1.copyWith(
                              fontWeight: FontWeight.w300,
                            ),
                        overflow: TextOverflow.ellipsis,
                      ),
                      Text(
                        HelperFunctions.getFormattedAmount(amount: amount),
                        style: Theme.of(context).textTheme.body2.copyWith(
                              fontWeight: FontWeight.bold,
                              color: Theme.of(context).primaryColor,
                            ),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                ),
                // Spacer(),
                Container(
                  margin: EdgeInsets.only(right: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      if (type == "loan request")
                        Text(
                          "Pay back",
                          style: Theme.of(context).textTheme.body1.copyWith(
                              fontWeight: FontWeight.w600,
                              fontSize: 12,
                              // color: Color(0xFF6C6C6C),
                              color: GeneralProvider.isLightMode
                                  ? Color(0xFF6C6C6C)
                                  : Colors.white60),
                        ),
                      Text(
                        DateFormat('dd-MM-y').format(dueDate),
                        style: Theme.of(context).textTheme.body1.copyWith(
                              fontWeight: FontWeight.w600,
                              fontSize: 12,
                              // color: Color(0xFF6C6C6C),
                              color: GeneralProvider.isLightMode
                                  ? Color(0xFF6C6C6C)
                                  : Colors.white60,
                            ),
                      ),
                      if (type != "loan request")
                        Text(
                          // debtorsList[index]["late_info"],
                          HelperFunctions.getLoanPeriodInfo(dueDate: dueDate),
                          // DateTime.now().difference(debtorsList[index].date).inDays.toString(),
                          style: Theme.of(context).textTheme.body1.copyWith(
                                fontWeight: FontWeight.w600,
                                fontSize: 12,
                                // color: Color(0xFF6C6C6C),
                                color: GeneralProvider.isLightMode
                                    ? Color(0xFF6C6C6C)
                                    : Colors.white60,
                              ),
                        ),
                    ],
                  ),
                ),
                if (action != null)
                  InkWell(
                    onTap: () => onOutlineClicked(),
                    child: SizedBox(
                      height: 24,
                      width: 24,
                      child: Image.asset(
                        "public/images/outline-more.png",
                        color: (outlineCOlor == true && action["type"] != null)
                            ? Color(0xffFF0000)
                            : null,
                      ),
                    ),
                  ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  static Widget renderListItemsAndRefresh(
      {BuildContext context,
      bool pullRefresh,
      bool isLoading,
      int itemsCount,
      Function refresh,
      Widget itemsBuilder,
      String emptyListIconPath,
      String emptyListText}) {
    var appColors = Provider.of<AppColorsProvider>(context, listen: false);
    return Container(
      child: pullRefresh == false && isLoading
          ? Center(
              child: CustomProgressIndicator(
                size: 40,
                color: Theme.of(context).primaryColor,
                // color: appColors.altProgressValueColor,
              ),
            )
          : itemsCount == 0
              ? RefreshIndicator(
                  color: Theme.of(context).primaryColor,
                  onRefresh: () => refresh(),
                  child: Container(
                    alignment: AlignmentDirectional.center,
                    child: SingleChildScrollView(
                      physics: AlwaysScrollableScrollPhysics(),
                      child: Container(
                        width: double.infinity,
                        child: IntrinsicHeight(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(
                                height: 100,
                                width: 100,
                                child: Image.asset(
                                  emptyListIconPath,
                                  fit: BoxFit.contain,
                                ),
                              ),
                              SizedBox(
                                height: 15,
                              ),
                              Text(
                                emptyListText,
                                style: Theme.of(context).textTheme.body2,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              : RefreshIndicator(
                  // color: appColors.refreshIndicator,
                  color: Theme.of(context).primaryColor,
                  onRefresh: () => refresh(),
                  child: itemsBuilder),
    );
  }

  static Widget buildFilterPanel(
      {BuildContext context,
      String optionalFilterMessage,
      List<dynamic> filterOptions,
      List<dynamic> sortOptions,
      int selectedFilterValue,
      int selectedSortValue,
      Function sortList,
      Function filterList}) {
    var appColors = Provider.of<AppColorsProvider>(context, listen: false);
    return Container(
      padding: EdgeInsets.symmetric(vertical: 5, horizontal: 30),
      decoration: BoxDecoration(
        color: appColors.filterPanelCOlor,
        boxShadow: !GeneralProvider.isLightMode
            ? null
            : [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.12),
                  spreadRadius: 0,
                  blurRadius: 15,
                  offset: Offset(0, 20),
                )
              ],
        border: GeneralProvider.isLightMode
            ? null
            : Border(
                bottom: BorderSide(
                  width: 0.2,
                  color: Colors.white,
                ),
              ),
      ),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                // flex: 2,
                child: Align(
                    alignment: Alignment.topLeft,
                    child: Theme(
                        data: !GeneralProvider.isLightMode
                            ? ThemeData.dark().copyWith(
                                accentColor: Color(0xffE78200).withOpacity(0.8),
                              )
                            : Theme.of(context),
                        child: Builder(builder: (ctx) {
                          //need this context (ctx) to theme the date range picker
                          return FlatButton(
                            materialTapTargetSize:
                                MaterialTapTargetSize.shrinkWrap,
                            onPressed: () => ModalActions.flagSort(
                              context: context,
                              title: "Filter options",
                              itemList: filterOptions,
                              selectedValue: selectedFilterValue,
                              onSelectOption: (selectedIndex) {
                                filterList(index: selectedIndex, context: ctx);
                              },
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    FaIcon(
                                      FontAwesomeIcons.lightSlidersH,
                                      size: 20,
                                      color: appColors.filterSortIconColor,
                                    ),
                                    SizedBox(
                                      width: 8,
                                    ),
                                    Text(
                                      "Filter options",
                                      style: Theme.of(context)
                                          .textTheme
                                          .body1
                                          .copyWith(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w400,
                                          ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                FittedBox(
                                  child: Text(
                                    filterOptions[selectedFilterValue],
                                    style: Theme.of(context)
                                        .textTheme
                                        .body1
                                        .copyWith(),
                                  ),
                                )
                              ],
                            ),
                            padding: const EdgeInsets.all(5),
                          );
                        }))),
              ),
              SizedBox(width: 5),
              FlatButton(
                onPressed: () => ModalActions.flagSort(
                  context: context,
                  title: "Sort options",
                  itemList: sortOptions,
                  selectedValue: selectedSortValue,
                  onSelectOption: (selectedIndex) {
                    sortList(index: selectedIndex);
                  },
                ),
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                padding: const EdgeInsets.all(5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        FaIcon(
                          FontAwesomeIcons.solidSort,
                          size: 20,
                          color: appColors.filterSortIconColor,
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Text(
                          "Sort options",
                          style: Theme.of(context).textTheme.body1.copyWith(
                              fontSize: 14, fontWeight: FontWeight.w400),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    FittedBox(
                      child: Text(
                        sortOptions[selectedSortValue],
                        style: Theme.of(context).textTheme.body1.copyWith(),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
          if (optionalFilterMessage != null)
            Divider(
              indent: 0,
              endIndent: 0,
            ),
          if (optionalFilterMessage != null)
            Align(
              alignment: Alignment.centerLeft,
              child: FittedBox(
                child: Text(
                  optionalFilterMessage,
                  style:
                      Theme.of(context).textTheme.body1.copyWith(fontSize: 11),
                ),
              ),
            )
        ],
      ),
    );
  }

  static Widget buildSummaryCard(
      {BuildContext context,
      String title,
      num number,
      String amount,
      Color borderColor,
      Function onTap}) {
    var appColors = Provider.of<AppColorsProvider>(context, listen: false);

    return InkWell(
      onTap: () => onTap != null ? onTap() : () {},
      child: Container(
        // width: 123,
        constraints: BoxConstraints(
          maxHeight: 115.0,
        ),
        padding: EdgeInsets.symmetric(vertical: 5, horizontal: 8),
        decoration: BoxDecoration(
          color: appColors.summaryBoxColor,
          borderRadius: BorderRadius.circular(8),
          border: GeneralProvider.isLightMode
              ? Border.all(color: borderColor, width: 0.3)
              : null,
        ),

        child: Column(
          children: <Widget>[
            Expanded(
              child: Align(
                alignment: Alignment.topLeft,
                child: FittedBox(
                  child: Text(
                    title,
                    style: Theme.of(context)
                        .textTheme
                        .body1
                        .copyWith(fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: FittedBox(
                child: AnimatedCount(
                  count: number,
                  style: Theme.of(context)
                      .textTheme
                      .headline
                      .copyWith(fontWeight: FontWeight.w300, fontSize: 36),
                  duration: Duration(milliseconds: 1000),
                ),
              ),
            ),
            // Spacer(),
            if (amount != null)
              Expanded(
                child: Align(
                  alignment: Alignment.bottomRight,
                  child: FittedBox(
                    child: Text(
                      "$amount",
                      style: Theme.of(context)
                          .textTheme
                          .body2
                          .copyWith(fontWeight: FontWeight.w300),
                    ),
                  ),
                ),
              )
          ],
        ),
      ),
    );
  }

  static getUserCompletionArchAngleFactor(
      {BuildContext context, Map<String, dynamic> userData}) {
    var totalScore = 0;
    if (userData["email"] != null && userData["email"] != " ")
      totalScore = totalScore + 10;
    if (userData["first_name"] != null && userData["first_name"] != " ")
      totalScore = totalScore + 10;
    if (userData["last_name"] != null && userData["last_name"] != " ")
      totalScore = totalScore + 10;
    if (userData["other_name"] != null && userData["other_name"] != " ")
      totalScore = totalScore + 10;
    if (userData["profile_picture"] != null &&
        userData["profile_picture"] != " ") totalScore = totalScore + 10;
    if (Provider.of<BankAccountProvider>(context, listen: false)
            .bankAccounts
            .length >
        0) totalScore = totalScore + 10;
    if (Provider.of<BvnProvider>(context, listen: false).hasBvn)
      totalScore = totalScore + 10;

    return (totalScore / 70) * 2;
  }

  static Future<void> startupApiCalls({BuildContext context}) async {
    Provider.of<BankAccountProvider>(context, listen: false).getAllAccounts();

    Provider.of<NotificationProvider>(context, listen: false)
        .getNotifications();
  }

  static void payWithPaystack({
    BuildContext context,
    dynamic loan,
    Map<String, dynamic> userData,
    bool isLoanRequest = true,
    Function initializingFailedCb,
    Function onPayStackResponse,
  }) async {
    var recipient = isLoanRequest ? loan.debtor : loan.creditor;

    final initiateResponseResponse =
        await Provider.of<PaymentProvider>(context, listen: false)
            .initiatePayment(body: {
      "nuban_account": Provider.of<BankAccountProvider>(context, listen: false)
          .recipienActiveBankAccount
          .id,
      "loan_request_id": isLoanRequest ? loan.id : null,
      "loan_id": !isLoanRequest ? loan.id : null,
      "email": recipient.email,
      "userId": userData["id"]
    });

    if (initiateResponseResponse["status"] == 200 ||
        initiateResponseResponse["status"] == 201) {
      Navigator.of(context).pushNamed(PaystackScreen.routeName, arguments: {
        "url": initiateResponseResponse["data"]["paystackResponse2"]["data"]
            ["authorization_url"],
      }).then((value) async {
        final confirmPaymentResponse =
            await Provider.of<PaymentProvider>(context, listen: false)
                .confirmPayment(body: {
          "reference": initiateResponseResponse["data"]["paystackResponse2"]
              ["data"]["reference"],
          "recipient_phone": HelperFunctions.serializePhoneNumber(
            phoneNumber: recipient.phoneNumber,
          ),
          "userId": userData["id"]
        });

        if (confirmPaymentResponse["status"] == 200 ||
            confirmPaymentResponse["status"] == 201) {
          //add loan to debtor calender
          // addLoanToUserCalender(
          //   dueDate: loan.dueDate.toIso8601String(),
          //   amount: loan.amount,
          //   debtorId: loan.debtor.id,
          //   creditorFirstName: userData["first_name"],
          // );
          onPayStackResponse(true);
        } else {
          CustomSnackBar.snackBar(
            title: confirmPaymentResponse["errors"],
            scaffoldState: Provider.of<GeneralProvider>(context, listen: false)
                .getAppScaffoldKey,
            context: context,
          );
          onPayStackResponse(false);
        }
      });
    } else {
      CustomSnackBar.snackBar(
        title: "Something went wrong, please try again.",
        scaffoldState: Provider.of<GeneralProvider>(context, listen: false)
            .getAppScaffoldKey,
        context: context,
      );
      initializingFailedCb("Something went wrong, please try again.");
    }
  }

  static getAppriopriateScaffoldKey() {
    if (GeneralProvider.scaffoldKeyRef != null) {
      return GeneralProvider.profileScaffoldKey == null
          ? GeneralProvider.scaffoldKeyRef
          : GeneralProvider.profileScaffoldKey;
    }
  }

  static dynamic subscribeToNetworkNotifier({BuildContext context}) {
    var connectionStatusSingleton = ConnectionStatusSingleton.getInstance();
    var _connectionStatusSubscription;
    _connectionStatusSubscription = connectionStatusSingleton
        .connectivityChangeSubject
        .listen((bool status) {
      if (!status)
        return CustomSnackBar.snackBar(
            context: context, title: "You are not connected");
    });

    return _connectionStatusSubscription;
  }

  static Widget loadingScreenAfterLogin({BuildContext context}) {
    return Container(
      height: MediaQuery.of(context).size.height,
      color: Colors.white38,
      child: Center(
        child: CustomProgressIndicator(
          color: Theme.of(context).primaryColor,
          size: 40,
          strokeWidth: 3,
        ),
      ),
    );
  }

  static void setAutoSync({bool value}) async {
    final sharedPrefs = await SharedPreferences.getInstance();
    sharedPrefs.setBool("isAutoSync", value);
  }

  static Future<bool> getAutoSyncValue() async {
    bool value = false;
    final sharedPrefs = await SharedPreferences.getInstance();
    if (sharedPrefs.containsKey("isAutoSync")) {
      value = sharedPrefs.getBool("isAutoSync");
    } else {
      value = false;
    }
    return value;
  }

  static Widget divider() {
    return Divider(
      thickness: 1.2,
      height: 0.0,
      color: !GeneralProvider.isLightMode ? Colors.grey[850] : null,
    );
  }

  static Widget buildLogo(
      {BuildContext context, double height = 60, double width = 60}) {
    return Container(
      height: height,
      width: width,
      child: Stack(
        alignment: AlignmentDirectional.center,
        children: <Widget>[
          Image.asset(
            "public/images/logos/outer.png",
            fit: BoxFit.contain,
            color: Theme.of(context).primaryColor,
          ),
          Image.asset(
            "public/images/logos/inner.png",
            height: height * 0.25,
            fit: BoxFit.contain,
          ),
        ],
      ),
    );
  }

  static Widget buildQuestionMark() {
    return CircleAvatar(
      radius: 8.5,
      backgroundColor: GeneralProvider.isLightMode
          ? Color.fromRGBO(103, 102, 165, 0.8)
          : Color(0xff8081b5),
      child: FaIcon(
        FontAwesomeIcons.solidQuestion,
        size: 10,
        color: GeneralProvider.isLightMode ? Colors.white : Colors.black,
      ),
    );
  }

  static Widget buildUserBadge({BuildContext context}) {
    var appColors = Provider.of<AppColorsProvider>(context, listen: false);
    return FaIcon(
      FontAwesomeIcons.solidStar,
      size: 12,
      color: appColors.contactBadgeColor,
    );
  }

  static Widget buildSendMessage(
      {BuildContext context,
      bool hasWhatsapp = false,
      Function onSmsClicked,
      Function whatsappHandler,
      Function loanHandler,
      String smsErrText,
      bool smsErrVisible,
      String whatsappErrText,
      bool whatsappErrVisible,
      bool isSmsSending,
      bool isWhatsappSending,
      bool isLoanClosing,
      String loanErrText,
      bool loanErrVisible,
      String title,
      bool isLoan}) {
    var appColors = Provider.of<AppColorsProvider>(context, listen: false);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        SizedBox(
          height: 5,
        ),
        Text(
          title,
          style: Theme.of(context).textTheme.body1.copyWith(
                fontWeight: FontWeight.w300,
                // fontSize: 11,
              ),
        ),
        SizedBox(
          height: 15,
        ),
        ErrText(
          errText: smsErrText,
          vissibility: smsErrVisible,
        ),
        SizedBox(
          height: 5,
        ),
        CustomUploadButton(
          title: isSmsSending ? "" : "SendSMS",
          height: 40,
          width: double.infinity,
          style: Theme.of(context).textTheme.body1.copyWith(
                color: appColors.primaryButtonColorText,
                fontWeight: FontWeight.w600,
              ),
          leadingIcon: FontAwesomeIcons.lightCommentsAlt,
          bgColor: appColors.primaryButtonColorBg,
          indicator: isSmsSending
              ? CustomProgressIndicator(
                  color: appColors.primaryProgressValueColor,
                )
              : Text(""),
          onClick: () => isSmsSending ? null : onSmsClicked(),
        ),
        //show only for android

        // if (Platform.isAndroid)
        //   InkWell(
        //     onTap: () => ModalActions.changeSIm(
        //         context: context,
        //         onSelect: (simCard) => onSmsClicked(simCard: simCard)),
        //     child: Align(
        //       alignment: Alignment.topRight,
        //       child: Container(
        //         padding: EdgeInsets.all(5),
        //         child: Text(
        //           "Use another sim",
        //           style: Theme.of(context).textTheme.body1.copyWith(
        //               color: Theme.of(context).primaryColor,
        //               fontWeight: FontWeight.w600),
        //         ),
        //       ),
        //     ),
        //   ),
        SizedBox(height: 10),
        if (hasWhatsapp)
          Row(
            children: <Widget>[
              Expanded(
                child: Divider(
                  thickness: 1,
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: Text(
                  "OR",
                  style: Theme.of(context)
                      .textTheme
                      .body2
                      .copyWith(fontWeight: FontWeight.w600),
                ),
              ),
              Expanded(
                child: Divider(
                  thickness: 1,
                ),
              ),
            ],
          ),
        if (hasWhatsapp) SizedBox(height: 15),
        if (hasWhatsapp)
          CustomUploadButton(
            title: isWhatsappSending ? "" : "WhatsApp",
            height: 40,
            width: double.infinity,
            leadingIcon: FontAwesomeIcons.whatsapp,
            style: Theme.of(context)
                .textTheme
                .body1
                .copyWith(color: Colors.white, fontWeight: FontWeight.w600),
            indicator: isWhatsappSending ? CustomProgressIndicator() : Text(""),
            bgColor: Color(0xff77E77B),
            onClick: () => isWhatsappSending ? null : whatsappHandler(),
          ),
        if (hasWhatsapp)
          ErrText(
            errText: whatsappErrText,
            vissibility: whatsappErrVisible,
          ),
        if (isLoan)
          SizedBox(
            height: 15,
          ),
        if (isLoan)
          CustomRaisedButton(
            onTapAction: () => isLoanClosing ? null : loanHandler(),
            indicator: isLoanClosing ? CustomProgressIndicator() : Text(""),
            bgColor: Colors.transparent,
            color: Theme.of(context).accentColor,
            title: isLoanClosing ? "" : "Close loan",
            width: 100,
          ),
      ],
    );
  }

  static handlePostFrameEventFromNotificationForListItems({
    dynamic widgetType,
    List items,
    Function executable,
    ScrollController scrollController,
  }) {
    // SchedulerBinding.instance.addPostFrameCallback((_) {
    print("FROM POP");
    print(widgetType);
    print(GeneralProvider.widgetType);
    print(GeneralProvider.selectedItemId);
    if (widgetType == GeneralProvider.widgetType) {
      int getIndex =
          items.indexWhere((item) => item.id == GeneralProvider.selectedItemId);
      print(getIndex);
      //scroll the page to make selected Item first
      //height of each tile is 77
      scrollController.jumpTo(77.0 * getIndex);
      executable(getIndex);
      GeneralProvider.widgetType = null;
    }
    // });
  }

  //this function is initiated from notification_screen.dart file
  static Function openNotificationModalWhenClickedFromTray;
}
