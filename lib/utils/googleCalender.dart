import 'dart:convert';

import 'package:googleapis/calendar/v3.dart' as googleCal;
import 'package:googleapis_auth/auth_io.dart';
import 'package:http/src/client.dart';
import 'package:intl/intl.dart';
import 'package:peer_score/providers/calender_provider.dart';
import 'package:peer_score/providers/contact.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:url_launcher/url_launcher.dart';

const _scopes = const [googleCal.CalendarApi.CalendarScope];
var _clientIdStr =
    "254817545309-bithohopbgt41ji72a47gn9r2l89kud3.apps.googleusercontent.com";
var _clientId = new ClientId(_clientIdStr, "");

Future<bool> checkUserCalendaData({String userId}) async {
  var res = await CalenderProvider().getCredentials(userId: userId);
  print("RES FROM CALENDERCHECK");
  print(res);
  if (res["status"] == 200) {
    var data = res["data"];
    if (data == null) {
      return false;
    } else {
      if (data["calendar_data"] == null) return false;
      return true;
    }
  } else {
    return false;
  }
}

Future<bool> removeUserCalenderData({String userId}) async {
  var res = await CalenderProvider()
      .updateCredentials(body: {"userId": userId, "calendar_data": null});
  if (res["status"] == 200) {
    return true;
  } else {
    return false;
  }
}

void promptCalender(urlString) async {
  if (await canLaunch(urlString)) {
    await launch(urlString);
  } else {
    print("Cannot launch");
  }
}

Future<bool> addUserCalenderData({String userId}) async {
  var client = await clientViaUserConsent(_clientId, _scopes, promptCalender);
  var res = await CalenderProvider().updateCredentials(body: {
    "userId": userId,
    "calendar_data": jsonEncode(
      {
        "type": client.credentials.accessToken.type,
        "data": client.credentials.accessToken.data,
        "expiry": client.credentials.accessToken.expiry.toIso8601String(),
        "refreshToken": client.credentials.refreshToken
      },
    )
  });
  if (res["status"] == 200) {
    return true;
  } else {
    return false;
  }
}

Future<void> addLoanToUserCalender(
    {String dueDate,
    String amount,
    String debtorId,
    String creditorFirstName}) async {
  var calendaData;
  var res = await CalenderProvider().getCredentials(userId: debtorId);
  if (res["status"] == 200) {
    var data = res["data"];
    if (data == null) {
      return;
    } else {
      if (data["calendar_data"] == null) return;
      calendaData = jsonDecode(data["calendar_data"]);
    }
  } else {
    return;
  }
  AccessCredentials newCredentials = await refreshCredentials(
    _clientId,
    AccessCredentials(
      AccessToken(
        calendaData["type"],
        calendaData["data"],
        DateTime.tryParse(
          calendaData["expiry"],
        ),
      ),
      calendaData["refreshToken"],
      _scopes,
    ),
    Client(),
  );
  String calendarId = "primary";
  googleCal.Event event = googleCal.Event();

  event.summary =
      "${toBeginningOfSentenceCase(creditorFirstName)}'s ${HelperFunctions.getFormattedAmount(amount: amount)} Loan Payment - PeerScore";
  googleCal.EventDateTime start = new googleCal.EventDateTime();
  start.timeZone = "GMT+01:00";
  start.dateTime = DateTime.tryParse(dueDate);
  // DateFormat().parse(dueDate);
  event.start = start;
  googleCal.EventDateTime end = new googleCal.EventDateTime();
  end.timeZone = "GMT+01:00";
  end.dateTime = DateTime.tryParse(dueDate);
  // end.dateTime = DateFormat().parse(dueDate);
  event.end = end;
  event.reminders = googleCal.EventReminders.fromJson({
    "useDefault": false,
    "overrides": [
      {
        "method": "popup",
        "minutes": 20160,
      },
      {
        "method": "popup",
        "minutes": 10080,
      },
      {
        "method": "popup",
        "minutes": 0,
      }
    ]
  });
  var newClientObj = authenticatedClient(Client(), newCredentials);

  var eventStatus = await googleCal.CalendarApi(newClientObj)
      .events
      .insert(event, calendarId);
  if (eventStatus.status == "confirmed") {
    print("DONE SUCCESSFULLY");
  } else {
    print("Could not add");
  }
}
