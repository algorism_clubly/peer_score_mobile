import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:flutter/material.dart';
import 'package:peer_score/providers/notifications_provider.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/screens/notification_screen.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:peer_score/utils/notification_utils.dart';
import 'package:peer_score/utils/socket.dart';
import 'package:provider/provider.dart';

class MainNavBar extends StatefulWidget {
  final String leadingImagePath;
  final String notificationImagePath;
  final Function leadingPressAction;
  final bool subNav;
  final bool useImageColor;
  final double leadingHeight;
  final double leadingWidth;
  final Widget leadingWidget;
  final String title;
  final bool onNotificationPage;
  MainNavBar(
      {this.leadingImagePath,
      this.notificationImagePath,
      this.subNav = false,
      this.leadingPressAction,
      this.useImageColor = false,
      this.leadingHeight,
      this.leadingWidth,
      this.leadingWidget,
      this.onNotificationPage = false,
      this.title});
  @override
  _MainNavBarState createState() => _MainNavBarState();
}

class _MainNavBarState extends State<MainNavBar> {
  int notificationsCount = 0;
  @override
  void initState() {
    super.initState();
    SocketConnection.getInstance()
        .notificationChangeSubject
        .listen((value) {
      if (this.mounted) {
        setState(() {
          notificationsCount = value;
        });
      }
    });
  }

  

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Row(
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          if (widget.leadingWidget != null) widget.leadingWidget,
          if (widget.leadingWidget == null)
            InkWell(
              customBorder: CircleBorder(),
              onTap: () => widget.leadingPressAction(),
              child: CircleAvatar(
                backgroundColor: Colors.transparent,
                child: SizedBox(
                  width: widget.leadingWidth != null ? widget.leadingWidth : 16,
                  height:
                      widget.leadingHeight != null ? widget.leadingHeight : 12,
                  child: widget.leadingImagePath != null
                      ? Image.asset(
                          widget.leadingImagePath,
                          fit: BoxFit.contain,
                          color: widget.useImageColor
                              ? null
                              : widget.subNav
                                  ? Colors.black
                                  : GeneralProvider.isLightMode
                                      ? Theme.of(context).primaryColor
                                      : Colors.white,
                        )
                      : Text(""),
                ),
              ),
            ),
          if (widget.title != null)
            Container(
                margin: EdgeInsets.only(left: 10),
                child: Text(
                  widget.title,
                  style: Theme.of(context).textTheme.body2.copyWith(
                        fontWeight: FontWeight.w600,
                      ),
                )),
          Spacer(),
          if (widget.notificationImagePath != null)
            DescribedFeatureOverlay(
              featureId: "notification",
              title: Text("Notifications"),
              description: Text(
                  "Access and manage notifications on all activities performed on the app"),
              tapTarget: Image.asset(
                widget.notificationImagePath,
                fit: BoxFit.contain,
                color: Colors.grey,
              ),
              backgroundColor:
                  !GeneralProvider.isLightMode ? Colors.grey[850] : null,
              contentLocation: ContentLocation.below,
              onDismiss: () async {
                return false;
              },
              onComplete: () async {
                return true;
              },
              child: InkWell(
                onTap: () => Navigator.of(context)
                    .pushNamed(NotificationScreen.routeName),
                customBorder: CircleBorder(),
                child: CircleAvatar(
                  backgroundColor: Colors.transparent,
                  child: Stack(
                    alignment: AlignmentDirectional.center,
                    children: <Widget>[
                      Positioned(
                        // right: 0,
                        top: 10,
                        child: SizedBox(
                          width: 18,
                          height: 20,
                          child: Image.asset(
                            widget.notificationImagePath,
                            fit: BoxFit.contain,
                            color: Colors.grey,
                          ),
                        ),
                      ),
                      if (notificationsCount > 0)
                        Positioned(
                          right: 6,
                          top: 1,
                          child: Container(
                            height: 16,
                            width: 16,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.red,
                            ),
                            alignment: Alignment.center,
                            child: FittedBox(
                              child: Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: Text(
                                  notificationsCount.toString(),
                                  style: Theme.of(context)
                                      .textTheme
                                      .body1
                                      .copyWith(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w700),
                                ),
                              ),
                            ),
                          ),
                        ),
                    ],
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }
}
