import 'dart:io';

import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rounded_date_picker/rounded_picker.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:peer_score/providers/bank_accounts_provider.dart';
import 'package:peer_score/providers/colors.dart';
import 'package:peer_score/providers/contact.dart';
import 'package:peer_score/providers/debts_provider.dart';
import 'package:peer_score/providers/expected_payments_provider.dart';
import 'package:peer_score/providers/loans-provider.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/providers/user-provider.dart';
import 'package:peer_score/screens/main_screen.dart';
import 'package:peer_score/screens/otp.dart';
import 'package:peer_score/screens/phone.dart';
import 'package:peer_score/utils/contry_codes.dart';
import 'package:peer_score/utils/custom_radio_tile.dart';
import 'package:peer_score/utils/custom_raised_button.dart';
import 'package:peer_score/utils/dropdown_field.dart';
import 'package:peer_score/utils/enums.dart';
import 'package:peer_score/utils/err_text_widget.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:peer_score/utils/googleCalender.dart';
import 'package:peer_score/utils/notification_view_singleton.dart';
import 'package:peer_score/utils/progress_indicator.dart';
import 'package:peer_score/utils/snackbar.dart';
import 'package:peer_score/utils/text_form_field.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
// import 'package:sms_maintained/sms.dart';
import 'package:url_launcher/url_launcher.dart';

class ModalActions {
  static void infoDialog(
      {BuildContext context,
      String leadingIconPath,
      String title,
      String content,
      String buttonText,
      Function altAction}) {
    modalDesignWrapper(
      context: context,
      title: title,
      child: IntrinsicHeight(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(height: 5),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Text(
                content,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.body1.copyWith(
                      fontWeight: FontWeight.w300,
                    ),
              ),
            ),
            SizedBox(height: 35),
            CustomRaisedButton(
              bgColor: Theme.of(context).accentColor,
              title: buttonText != null ? buttonText : "close",
              onTapAction: () {
                Navigator.of(context).pop();
                if (altAction != null) {
                  altAction();
                }
              },
            )
          ],
        ),
      ),
    );
  }

  static void provideProofModal(
      {BuildContext context, Function proceed, Function cancel}) {
    bool isSending = false;
    String _submitErrText = "";
    bool _submitErrVisible = false;
    modalDesignWrapper(
      context: context,
      title: "Wrong claim",
      child: StatefulBuilder(
        builder: (ctx, setState) {
          void onSubmit() async {
            setState(() {
              _submitErrText = "";
              _submitErrVisible = false;
            });
            isSending = true;

            var denyResponse = await Provider.of<ExpectedPaymentsProvider>(
                    context,
                    listen: false)
                .cancelClaim();
            if (denyResponse["status"] == 200 ||
                denyResponse["status"] == 201) {
              setState(() {
                isSending = false;
                _submitErrText = "";
                _submitErrVisible = false;
              });
              Navigator.of(ctx).pop();
              showSuccess(
                title: "Loan withdrawn",
                context: context,
                content:
                    "This loan has been marked wrong and permanently removed.",
                onOk: ExpectedPaymentsProvider.loansListCb,
              );
            } else {
              setState(() {
                isSending = false;
                _submitErrText = denyResponse["errors"];
                _submitErrVisible = true;
              });
            }
          }

          var appColors =
              Provider.of<AppColorsProvider>(context, listen: false);
          return Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                Text(
                  "This claim has been raised as a bad claim by the defaulter, kindly provide proof of loan or close the request",
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.body1.copyWith(
                        fontWeight: FontWeight.w300,
                      ),
                ),
                SizedBox(
                  height: 20,
                ),
                CustomRaisedButton(
                  // bgColor: Color(0xffE78200),
                  bgColor: Theme.of(context).accentColor,
                  title: "Provide proof",
                  onTapAction: () {
                    Navigator.of(ctx).pop();
                    proceed();
                  },
                ),
                CustomRaisedButton(
                  title: isSending ? "" : "Cancel claim",
                  bgColor: Colors.transparent,
                  color: Theme.of(context).primaryColor,
                  onTapAction: () {
                    isSending ? () {} : onSubmit();
                  },
                  indicator: isSending
                      ? CustomProgressIndicator(
                          color: Theme.of(context).primaryColor,
                        )
                      : Text(""),
                ),
                ErrText(
                  errText: _submitErrText,
                  vissibility: _submitErrVisible,
                )
              ],
            ),
          );
        },
      ),
    );
  }

  static void confirmPayment(
      {BuildContext context, Function proceed, Function cancel}) {
    bool isSending = false;
    String _submitErrText = "";
    bool _submitErrVisible = false;
    showDialog(
        context: context,
        builder: (ctx) {
          return Dialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(6)),
              child: StatefulBuilder(builder: (ctx, setState) {
                void onSubmit() async {
                  setState(() {
                    _submitErrText = "";
                    _submitErrVisible = false;
                  });
                  isSending = true;

                  var denyResponse =
                      await Provider.of<DebtsProviver>(context, listen: false)
                          .reschedule();
                  if (denyResponse["status"] == 200 ||
                      denyResponse["status"] == 201) {
                    setState(() {
                      isSending = false;
                      _submitErrText = "";
                      _submitErrVisible = false;
                    });
                    Navigator.of(ctx).pop();
                    showSuccess(
                        title: "Loan withdrawn",
                        context: context,
                        content:
                            "This loan has been marked wrong and permanently removed.",
                        onOk: () {});
                  } else {
                    setState(() {
                      isSending = false;
                      _submitErrText = denyResponse["errors"];
                      _submitErrVisible = true;
                    });
                  }
                }

                return Container(
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                  height: 210,
                  width: 300,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "Loan funded",
                            style: Theme.of(context).textTheme.body2.copyWith(
                                fontSize: 18, fontWeight: FontWeight.normal),
                          ),
                          SizedBox(
                            width: 2,
                          ),
                          SizedBox(
                            height: 20,
                            width: 20,
                            child: Image.asset("public/images/pay-icon.png"),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: Text(
                          "This loan has been funded, kindly confirm payment",
                          textAlign: TextAlign.center,
                          style: Theme.of(context).textTheme.body1.copyWith(
                                fontWeight: FontWeight.w300,
                              ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      CustomRaisedButton(
                        title: "Confirm",
                        onTapAction: () {
                          Navigator.of(ctx).pop();
                          // proceed(2);
                        },
                      ),
                      CustomRaisedButton(
                        title: isSending ? "" : "Flag",
                        bgColor: Colors.transparent,
                        color: Theme.of(context).primaryColor,
                        onTapAction: () {
                          isSending ? () {} : onSubmit();
                        },
                        indicator: isSending
                            ? CustomProgressIndicator(
                                color: Theme.of(context).primaryColor,
                              )
                            : Text(""),
                      ),
                      ErrText(
                        errText: _submitErrText,
                        vissibility: _submitErrVisible,
                      )
                    ],
                  ),
                );
              }));
        });
  }

  static void rescheduleDate(
      {BuildContext context,
      String leadingIconPath,
      String title,
      String content,
      String buttonText,
      Function altAction}) {
    bool isSending = false;
    String _submitErrText = "";
    bool _submitErrVisible = false;
    modalDesignWrapper(
      context: context,
      title: title,
      child: StatefulBuilder(builder: (ctx, setState) {
        void onSubmit() async {
          setState(() {
            _submitErrText = "";
            _submitErrVisible = false;
          });
          isSending = true;

          var denyResponse =
              await Provider.of<DebtsProviver>(context, listen: false)
                  .reschedule();
          if (denyResponse["status"] == 200 || denyResponse["status"] == 201) {
            setState(() {
              isSending = false;
              _submitErrText = "";
              _submitErrVisible = false;
            });
            Navigator.of(ctx).pop();
            showSuccess(
                title: "Payment rescheduled",
                context: context,
                content: "Request sent, awaiting confirmation from lender",
                onOk: DebtsProviver.debtsListCb);
          } else {
            setState(() {
              isSending = false;
              _submitErrText = denyResponse["errors"];
              _submitErrVisible = true;
            });
          }
        }

        var appColors = Provider.of<AppColorsProvider>(context, listen: false);
        return IntrinsicHeight(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              SizedBox(height: 5),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Text(
                  content,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.body1.copyWith(
                        fontWeight: FontWeight.w300,
                      ),
                ),
              ),
              SizedBox(height: 35),
              ErrText(
                errText: _submitErrText,
                vissibility: _submitErrVisible,
              ),
              SizedBox(height: 5),
              CustomRaisedButton(
                bgColor: Theme.of(context).accentColor,
                title: isSending ? "" : "Confirm",
                onTapAction: () {
                  onSubmit();
                },
                indicator: isSending ? CustomProgressIndicator() : Text(""),
              ),
              CustomRaisedButton(
                title: "Change date",
                bgColor: Colors.transparent,
                color: Theme.of(context).primaryColor,
                onTapAction: () => {
                  Navigator.of(ctx).pop(),
                  openRescheduledDataPicker(context: context)
                },
              ),
            ],
          ),
        );
      }),
    );
  }

  static void confirmReschedule(
      {BuildContext context,
      String leadingIconPath,
      String title,
      String content,
      String buttonText,
      String newDate,
      Function altAction}) {
    bool isSending = false;
    String _submitErrText = "";
    bool _submitErrVisible = false;
    bool isCanceling = false;
    String _cancelErrText = "";
    bool _cancelErrVisible = false;
    modalDesignWrapper(
      context: context,
      title: title,
      child: StatefulBuilder(builder: (ctx, setState) {
        void onSubmit({bool confirm = true}) async {
          setState(() {
            if (confirm) {
              _submitErrText = "";
              _submitErrVisible = false;
              isSending = true;
            } else {
              _cancelErrText = "";
              _cancelErrVisible = false;
              isCanceling = true;
            }
          });

          var response;

          if (confirm) {
            response = await Provider.of<ExpectedPaymentsProvider>(context,
                    listen: false)
                .confirmReschedule();
          } else {
            response = await Provider.of<ExpectedPaymentsProvider>(context,
                    listen: false)
                .declineReschedule();
          }
          if (response["status"] == 200 || response["status"] == 201) {
            setState(() {
              isSending = false;
              _submitErrText = "";
              _submitErrVisible = false;
            });
            Navigator.of(ctx).pop();
            showSuccess(
              title: confirm ? "Rescheduled confirmed" : "Rescheduled rejected",
              context: context,
              content: confirm
                  ? "Your debtor can now pay on or before ${DateFormat.yMMMMd().format(DateTime.parse(newDate))}"
                  : "Please wait while your debtor refund the loan",
              onOk: ExpectedPaymentsProvider.loansListCb,
            );
          } else {
            setState(() {
              if (confirm) {
                isSending = false;
                _submitErrText = response["errors"];
                _submitErrVisible = true;
              } else {
                isCanceling = false;
                _cancelErrText = response["errors"];
                _cancelErrVisible = true;
              }
            });
          }
        }

        var appColors = Provider.of<AppColorsProvider>(context, listen: false);
        return IntrinsicHeight(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              SizedBox(height: 5),
              Text(
                content,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.body1.copyWith(
                      fontWeight: FontWeight.w300,
                    ),
              ),
              SizedBox(height: 35),
              ErrText(
                errText: _submitErrText,
                vissibility: _submitErrVisible,
              ),
              SizedBox(height: 5),
              CustomRaisedButton(
                bgColor: Theme.of(context).accentColor,
                title: isSending ? "" : "Confirm",
                onTapAction: () {
                  isSending ? () {} : onSubmit();
                },
                indicator: isSending ? CustomProgressIndicator() : Text(""),
              ),
              CustomRaisedButton(
                title: isCanceling ? "" : "Reject",
                onTapAction: () {
                  isCanceling ? () {} : onSubmit(confirm: false);
                },
                bgColor: Colors.transparent,
                color: Theme.of(context).primaryColor,
                indicator: isCanceling
                    ? CustomProgressIndicator(
                        color: Theme.of(context).primaryColor,
                      )
                    : Text(""),
              ),
              ErrText(
                errText: _cancelErrText,
                vissibility: _cancelErrVisible,
              ),
            ],
          ),
        );
      }),
    );
  }

  static void confirmPay({
    BuildContext context,
    String leadingIconPath,
    String title,
    String content,
    String buttonText,
    Map<String, dynamic> loanAction,
  }) {
    bool isSending = false;
    bool isCanceling = false;
    String _submitErrText = "";
    bool _submitErrVisible = false;
    String _cancelErrText = "";
    bool _cancelErrVisible = false;
    bool viewProof = false;
    modalDesignWrapper(
      context: context,
      title: title,
      child: StatefulBuilder(builder: (ctx, setState) {
        void onSubmit({bool confirm = true}) async {
          setState(() {
            if (confirm) {
              _submitErrText = "";
              _submitErrVisible = false;
              isSending = true;
            } else {
              _cancelErrText = "";
              _cancelErrVisible = false;
              isCanceling = true;
            }
          });

          var response;

          if (confirm) {
            response = await Provider.of<ExpectedPaymentsProvider>(context,
                    listen: false)
                .confirmPay();
          } else {
            response = await Provider.of<ExpectedPaymentsProvider>(context,
                    listen: false)
                .flagPay();
          }
          if (response["status"] == 200 || response["status"] == 201) {
            setState(() {
              if (confirm) {
                _submitErrText = "";
                _submitErrVisible = false;
                isSending = false;
              } else {
                _cancelErrText = "";
                _cancelErrVisible = false;
                isCanceling = false;
              }
            });
            Navigator.of(ctx).pop();
            showSuccess(
              title: confirm ? "Payment confirmed" : "Payment declined",
              context: context,
              content: confirm
                  ? "This loan is now completed"
                  : "Loan status active, please wait while the debtor refunds the loan",
              onOk: ExpectedPaymentsProvider.loansListCb,
            );
          } else {
            setState(() {
              if (confirm) {
                isSending = false;
                _submitErrText = response["errors"];
                _submitErrVisible = true;
              } else {
                isCanceling = false;
                _cancelErrText = response["errors"];
                _cancelErrVisible = true;
              }
            });
          }
        }

        var appColors = Provider.of<AppColorsProvider>(context, listen: false);
        return IntrinsicHeight(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              SizedBox(height: 5),
              Text(
                content,
                // textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.body1.copyWith(
                      fontWeight: FontWeight.w300,
                    ),
              ),
              SizedBox(
                height: 15,
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    viewProof = !viewProof;
                  });
                },
                child: Padding(
                  padding: const EdgeInsets.only(top: 5, bottom: 5, right: 5),
                  child: Row(
                    children: <Widget>[
                      Text(
                        "View proof",
                        style: Theme.of(context).textTheme.body1.copyWith(
                              color: Theme.of(context).primaryColor,
                            ),
                      ),
                      SizedBox(width: 5),
                      FaIcon(
                        viewProof
                            ? FontAwesomeIcons.lightChevronDown
                            : FontAwesomeIcons.lightChevronUp,
                        size: 12,
                        color: appColors.defaultIconColor,
                      )
                    ],
                  ),
                ),
              ),
              AnimatedContainer(
                constraints: BoxConstraints(
                  maxHeight: viewProof ? 400 : 0,
                ),
                duration: Duration(milliseconds: 300),
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                    child: Image.network(
                      loanAction["document"],
                      fit: BoxFit.fill,
                    )),
              ),
              SizedBox(height: 20),
              ErrText(
                errText: _submitErrText,
                vissibility: _submitErrVisible,
              ),
              SizedBox(height: 5),
              CustomRaisedButton(
                bgColor: Theme.of(context).accentColor,
                title: isSending ? "" : "Confirm",
                onTapAction: () {
                  isSending ? () {} : onSubmit();
                },
                indicator: isSending ? CustomProgressIndicator() : Text(""),
              ),
              CustomRaisedButton(
                title: isCanceling ? "" : "Decline",
                onTapAction: () {
                  isCanceling ? () {} : onSubmit(confirm: false);
                },
                bgColor: Colors.transparent,
                color: Theme.of(context).primaryColor,
                indicator: isCanceling
                    ? CustomProgressIndicator(
                        color: Theme.of(context).primaryColor,
                      )
                    : Text(""),
              ),
              ErrText(
                errText: _cancelErrText,
                vissibility: _cancelErrVisible,
              ),
            ],
          ),
        );
      }),
    );
  }

  static openRescheduledDataPicker({BuildContext context}) async {
    //
    var selectedDate = await HelperFunctions.getSelectedDate(
        context: context,
        initialDate: DateTime.now().add(Duration(days: 1)),
        firstDate: DateTime.now(),
        lastDate: DateTime(DateTime.now().year + 10));

    if (selectedDate != null) {
      DebtsProviver.rescheduledDate = selectedDate;
      rescheduleDate(
        title: "Request re-schedule",
        leadingIconPath: "public/images/reschedule.png",
        context: context,
        content:
            "You have chosen to refund this loan on or before ${DateFormat.yMMMMd().format(selectedDate)}, please endeavour to comply.",
        buttonText: "Confirm",
        altAction: () => infoDialog(
          title: "Request re-schedule",
          leadingIconPath: "public/images/reschedule.png",
          context: context,
          content: "Request sent, awaiting confirmation from lender",
        ),
      );
    }
  }

  static resolveDebtActionsList(
      {BuildContext context,
      bool hasActiveAccount,
      Function wrongClaimFn,
      Function payOnAppCb}) {
    Widget buildResolveItem(
        {String title, String iconPath, Function onPressed}) {
      return InkWell(
        onTap: () => {Navigator.of(context).pop(), onPressed()},
        child: Container(
          // width: 200,
          padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
          // margin: EdgeInsets.only(left: 40),
          decoration: BoxDecoration(
              border: Border(
            bottom: BorderSide(
              width: 0.5,
              color: Color.fromRGBO(103, 102, 165, 0.18),
            ),
          )),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                // width: 150,
                child: Text(
                  title,
                  style: Theme.of(context).textTheme.body2,
                ),
              ),
              Spacer(),
              SizedBox(
                height: 20,
                width: 20,
                // child: Image.asset("public/images/pay-icon.png"),
                child: Image.asset(iconPath),
              )
            ],
          ),
        ),
      );
    }

    modalDesignWrapper(
      context: context,
      title: "Resolve Debt",
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          if (hasActiveAccount)
            buildResolveItem(
                title: "Pay on app",
                iconPath: "public/images/pay-icon.png",
                onPressed: () => payOnAppCb()),
          buildResolveItem(
            title: "Payment made offline",
            iconPath: "public/images/checkmark.png",
            onPressed: () => uploadPloadPaymentProof(
              context: context,
              title: "Mark as paid",
              content: "Please provide proof of payment",
              resolveDebt: true,
              afterPop: () => infoDialog(
                  title: "Confirmation sent",
                  leadingIconPath: "public/images/checkmark.png",
                  context: context,
                  content: "Waiting for financer to confirm payment"),
            ),
          ),
          buildResolveItem(
            title: "Request re-schedule",
            iconPath: "public/images/reschedule.png",
            onPressed: () => infoDialog(
              title: "Request re-schedule",
              leadingIconPath: "public/images/reschedule.png",
              context: context,
              content: "When would you like to re-schedule this payment to?",
              buttonText: "Pick a date",
              altAction: () => openRescheduledDataPicker(context: context),
            ),
          ),
          buildResolveItem(
            title: "Wrong claim",
            iconPath: "public/images/error-icon.png",
            onPressed: () => wrongClaimProofChoice(
                context: context, provideProofFn: wrongClaimFn),
            // onPressed: () => infoDialog(
            //   title: "Claim updated",
            //   leadingIconPath: "public/images/error-icon.png",
            //   context: context,
            //   content:
            //       "Claim has been removed temporarily and will be removed permanently if a proof isn’t provided by the creditor",
            // ),
          )
        ],
      ),
    );
  }

  static requestLoanActions(
      {BuildContext context,
      LoanRequest loan,
      bool hasActiveAccount,
      Function payOnAppCb}) async {
    modalDesignWrapper(
      context: context,
      title: "Loan Request",
      child: StatefulBuilder(
        builder: (ctx, setState) {
          Widget buildLoanAction(
              {String title, String iconPath, Function onPressed}) {
            return InkWell(
              onTap: () => {Navigator.of(context).pop(), onPressed()},
              child: Container(
                // width: 200,
                padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                // margin: EdgeInsets.only(left: 40),
                decoration: BoxDecoration(
                    border: Border(
                  bottom: BorderSide(
                    width: 0.5,
                    color: Color.fromRGBO(103, 102, 165, 0.18),
                  ),
                )),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      // width: 150,
                      child: Text(
                        title,
                        style: Theme.of(context).textTheme.body2,
                      ),
                    ),
                    Spacer(),
                    SizedBox(
                      height: 20,
                      width: 20,
                      // child: Image.asset("public/images/pay-icon.png"),
                      child: Image.asset(iconPath),
                    )
                  ],
                ),
              ),
            );
          }

          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 10,
              ),
              // if (activeBankAccount != null)
              if (hasActiveAccount && (loan.debtor.email != null))
                buildLoanAction(
                    title: "Loan on app",
                    iconPath: "public/images/pay-icon.png",
                    onPressed: () => payOnAppCb()),
              buildLoanAction(
                title: "Payment made offline",
                iconPath: "public/images/checkmark.png",
                onPressed: () => uploadPloadPaymentProof(
                    context: context,
                    title: "Payment made offline",
                    content: "Please provide proof of payment",
                    loan: loan),
              ),
              buildLoanAction(
                title: "Deny",
                iconPath: "public/images/delete-icon.png",
                onPressed: () => denyLoanReason(
                  title: "Deny Loan",
                  // leadingIconPath: "public/images/delete-icon.png",
                  context: context,
                  content: "You have declined the loan request by Oliver Gucci",
                ),
              )
            ],
          );
        },
      ),
    );

    // showDialog(
    //   context: context,
    //   child:
    // );
  }

  static denyLoanReason({BuildContext context, String title, String content}) {
    var _selectedValue = 0;
    List<String> presetReasons = [
      "I don't have money at this moment",
      "This person currently owes me money",
      "This person does not refund on time",
      "Specify reason",
    ];
    bool isDenying = false;
    String _submitErrText = "";
    bool _submitErrVisible = false;
    String _reasonErrText = "";
    bool _reasonErrVisible = false;
    TextEditingController _reasonController = TextEditingController();
    FocusNode _reasonFocusNode = FocusNode();
    modalDesignWrapper(
      context: context,
      title: "Deny loan",
      padding: EdgeInsets.symmetric(vertical: 15),
      child: StatefulBuilder(
        builder: (ctx, setState) {
          String debtorName;
          final loan = ExpectedPaymentsProvider.selectedDebtorLoanRequest;
          debtorName = HelperFunctions.getContactFullName(contact: loan.debtor);
          var amount = HelperFunctions.getFormattedAmount(amount: loan.amount);
          Widget _buildReasonTile({int value}) {
            void onReasonClicked() {
              setState(() {
                _selectedValue = value;
                _submitErrText = "";
                _submitErrVisible = false;
                _reasonErrText = "";
                _reasonErrVisible = false;
              });
            }

            return Row(
              children: <Widget>[
                Container(
                  height: 15,
                  width: 15,
                  alignment: Alignment.centerLeft,
                  child: Radio(
                    // materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    value: value,
                    groupValue: _selectedValue,
                    activeColor: Theme.of(context).primaryColor,
                    onChanged: (value) => onReasonClicked(),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: InkWell(
                    onTap: onReasonClicked,
                    child: Container(
                      height: 20,
                      child: FittedBox(
                        alignment: Alignment.centerLeft,
                        fit: BoxFit.contain,
                        child: Text(
                          presetReasons[value],
                          style: Theme.of(context).textTheme.body1.copyWith(
                                fontWeight: FontWeight.w300,
                              ),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            );
          }

          void _onSubmit() async {
            setState(() {
              _submitErrText = "";
              _submitErrVisible = false;
            });
            if (_selectedValue == 3 && _reasonController.text.isEmpty) {
              setState(() {
                _reasonErrText = "Please provide a reason";
                _reasonErrVisible = true;
              });
              return;
            }
            //set reason here
            if (_selectedValue != 3) {
              ExpectedPaymentsProvider.loanDenyReason =
                  presetReasons[_selectedValue];
            } else {
              ExpectedPaymentsProvider.loanDenyReason = _reasonController.text;
            }
            setState(() {
              isDenying = true;
            });
            var denyResponse = await Provider.of<ExpectedPaymentsProvider>(
                    context,
                    listen: false)
                .denyLoanRequest();
            if (denyResponse["status"] == 200 ||
                denyResponse["status"] == 201) {
              setState(() {
                isDenying = false;
                _submitErrText = "";
                _submitErrVisible = false;
              });
              // ModalActions.showSuccess(
              //     context: context,
              //     content: "Phone number has been deleted successfully.");
              Navigator.of(ctx).pop();
              showSuccess(
                context: context,
                content: "You have declined the loan request by $debtorName",
                title: "Loan Denied",
                onOk: ExpectedPaymentsProvider.loanRequestCb,
              );
              // infoDialog(
              //   title: "Loan Denied",
              //   leadingIconPath: "public/images/delete-icon.png",
              //   context: context,
              //   content:
              //       "You have declined the loan request by Oliver Gucci",
              // );
            } else {
              setState(() {
                isDenying = false;
                _submitErrText = denyResponse["errors"];
                _submitErrVisible = true;
              });
            }
          }

          var appColors =
              Provider.of<AppColorsProvider>(context, listen: false);
          return IntrinsicHeight(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Why are you denying $debtorName the loan request of $amount ?",
                          style: Theme.of(context).textTheme.body1.copyWith(
                                fontWeight: FontWeight.w300,
                              ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Theme(
                    data: ThemeData(
                      unselectedWidgetColor: Theme.of(context).primaryColor,
                    ),
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                      child: Column(
                        children: <Widget>[
                          _buildReasonTile(value: 0),
                          SizedBox(
                            height: 10,
                          ),
                          _buildReasonTile(value: 1),
                          SizedBox(
                            height: 10,
                          ),
                          _buildReasonTile(value: 2),
                          SizedBox(
                            height: 10,
                          ),
                          _buildReasonTile(value: 3),
                        ],
                      ),
                    ),
                  ),
                  if (_selectedValue == 3)
                    Container(
                      padding: EdgeInsets.only(left: 20, right: 15),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          CustomTextField(
                            textEditingController: _reasonController,
                            focusNode: _reasonFocusNode,
                            maxLines: 4,
                            borderRadius: 4,
                            height: 55,
                            hintText: "At most 200 words",
                            contentPadding: EdgeInsets.symmetric(
                              horizontal: 10,
                              vertical: 5,
                            ),
                            onChanged: (value) {
                              setState(() {
                                _reasonErrText = "";
                                _reasonErrVisible = false;
                              });
                              var wordList = [];
                              wordList = value.split(" ");
                              if (wordList.length > 200) {
                                wordList = wordList.sublist(0, 200);
                                _reasonController.text = wordList.join(" ");
                                _reasonFocusNode.unfocus();
                              }
                            },
                          ),
                          ErrText(
                            errText: _reasonErrText,
                            vissibility: _reasonErrVisible,
                          )
                        ],
                      ),
                    ),
                  SizedBox(
                    height: 15,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    width: double.infinity,
                    child: CustomRaisedButton(
                        title: isDenying ? "" : "Deny",
                        bgColor: Theme.of(context).accentColor,
                        onTapAction: () {
                          isDenying ? () {} : _onSubmit();
                        },
                        indicator: isDenying
                            ? CustomProgressIndicator()
                            : Container()),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 15),
                    child: ErrText(
                      errText: _submitErrText,
                      vissibility: _submitErrVisible,
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  static closeLoanReason({BuildContext context, String title, String content}) {
    var _selectedValue = 0;
    List<String> presetReasons = [
      "This debtor has paid",
      "This debtor was unable to pay",
      "I am forfeiting the loan",
      "Specify reason",
    ];
    bool isDenying = false;
    String _submitErrText = "";
    bool _submitErrVisible = false;
    String _reasonErrText = "";
    bool _reasonErrVisible = false;
    TextEditingController _reasonController = TextEditingController();
    FocusNode _reasonFocusNode = FocusNode();
    modalDesignWrapper(
      context: context,
      title: "Close loan",
      padding: EdgeInsets.symmetric(vertical: 15),
      child: StatefulBuilder(
        builder: (ctx, setState) {
          String debtorName;
          final loan = ExpectedPaymentsProvider.selectedLoan;
          debtorName = HelperFunctions.getContactFullName(contact: loan.debtor);
          var amount = HelperFunctions.getFormattedAmount(amount: loan.amount);
          Widget _buildReasonTile({int value}) {
            void onReasonClicked() {
              setState(() {
                _selectedValue = value;
                _submitErrText = "";
                _submitErrVisible = false;
                _reasonErrText = "";
                _reasonErrVisible = false;
              });
            }

            return Row(
              children: <Widget>[
                Container(
                  height: 15,
                  width: 15,
                  alignment: Alignment.centerLeft,
                  child: Radio(
                    // materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    value: value,
                    groupValue: _selectedValue,
                    activeColor: Theme.of(context).primaryColor,
                    onChanged: (value) => onReasonClicked(),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: InkWell(
                    onTap: onReasonClicked,
                    child: Container(
                      height: 20,
                      child: FittedBox(
                        alignment: Alignment.centerLeft,
                        fit: BoxFit.contain,
                        child: Text(
                          presetReasons[value],
                          style: Theme.of(context).textTheme.body1.copyWith(
                                fontWeight: FontWeight.w300,
                              ),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            );
          }

          void _onSubmit() async {
            setState(() {
              _submitErrText = "";
              _submitErrVisible = false;
            });
            if (_selectedValue == 3 && _reasonController.text.isEmpty) {
              setState(() {
                _reasonErrText = "Please provide a reason";
                _reasonErrVisible = true;
              });
              return;
            }
            //set reason here
            if (_selectedValue != 3) {
              ExpectedPaymentsProvider.loanCloseReason =
                  presetReasons[_selectedValue];
            } else {
              ExpectedPaymentsProvider.loanCloseReason = _reasonController.text;
            }
            setState(() {
              isDenying = true;
            });
            var denyResponse = await Provider.of<ExpectedPaymentsProvider>(
                    context,
                    listen: false)
                .closeLoan();
            if (denyResponse["status"] == 200 ||
                denyResponse["status"] == 201) {
              setState(() {
                isDenying = false;
                _submitErrText = "";
                _submitErrVisible = false;
              });
              // ModalActions.showSuccess(
              //     context: context,
              //     content: "Phone number has been deleted successfully.");
              Navigator.of(ctx).pop();
              showSuccess(
                context: context,
                content: "You have closed the loan made to $debtorName",
                title: "Loan Closed",
                onOk: ExpectedPaymentsProvider.loansListCb,
              );
              // infoDialog(
              //   title: "Loan Denied",
              //   leadingIconPath: "public/images/delete-icon.png",
              //   context: context,
              //   content:
              //       "You have declined the loan request by Oliver Gucci",
              // );
            } else {
              setState(() {
                isDenying = false;
                _submitErrText = denyResponse["errors"];
                _submitErrVisible = true;
              });
            }
          }

          var appColors =
              Provider.of<AppColorsProvider>(context, listen: false);
          return IntrinsicHeight(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Why are you closing this loan ?",
                          style: Theme.of(context).textTheme.body1.copyWith(
                                fontWeight: FontWeight.w300,
                              ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Theme(
                    data: ThemeData(
                      unselectedWidgetColor: Theme.of(context).primaryColor,
                    ),
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                      child: Column(
                        children: <Widget>[
                          _buildReasonTile(value: 0),
                          SizedBox(
                            height: 10,
                          ),
                          _buildReasonTile(value: 1),
                          SizedBox(
                            height: 10,
                          ),
                          _buildReasonTile(value: 2),
                          SizedBox(
                            height: 10,
                          ),
                          _buildReasonTile(value: 3),
                        ],
                      ),
                    ),
                  ),
                  if (_selectedValue == 3)
                    Container(
                      padding: EdgeInsets.only(left: 20, right: 15),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          CustomTextField(
                            textEditingController: _reasonController,
                            focusNode: _reasonFocusNode,
                            maxLines: 4,
                            borderRadius: 4,
                            height: 55,
                            hintText: "At most 200 words",
                            contentPadding: EdgeInsets.symmetric(
                              horizontal: 10,
                              vertical: 5,
                            ),
                            onChanged: (value) {
                              setState(() {
                                _reasonErrText = "";
                                _reasonErrVisible = false;
                              });
                              var wordList = [];
                              wordList = value.split(" ");
                              if (wordList.length > 200) {
                                wordList = wordList.sublist(0, 200);
                                _reasonController.text = wordList.join(" ");
                                _reasonFocusNode.unfocus();
                              }
                            },
                          ),
                          ErrText(
                            errText: _reasonErrText,
                            vissibility: _reasonErrVisible,
                          )
                        ],
                      ),
                    ),
                  SizedBox(
                    height: 15,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    width: double.infinity,
                    child: CustomRaisedButton(
                        title: isDenying ? "" : "Close",
                        bgColor: Theme.of(context).accentColor,
                        onTapAction: () {
                          isDenying ? () {} : _onSubmit();
                        },
                        indicator: isDenying
                            ? CustomProgressIndicator()
                            : Container()),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 15),
                    child: ErrText(
                      errText: _submitErrText,
                      vissibility: _submitErrVisible,
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  static wrongClaimProofChoice(
      {BuildContext context,
      String title,
      String content,
      Function provideProofFn}) {
    var _selectedValue = 0;
    String _submitErrText = "";
    bool _submitErrVisible = false;
    bool isSending = false;
    modalDesignWrapper(
      context: context,
      title: "Wrong Claim",
      child: StatefulBuilder(
        builder: (ctx, setState) {
          void onSubmit() async {
            var loanId = DebtsProviver.selectedDebt.id;
            var response =
                await Provider.of<DebtsProviver>(context, listen: false)
                    .badClaim(witnesses: [], documents: [], loanId: loanId);
            if (response["status"] == 200 || response["status"] == 201) {
              setState(() {
                isSending = false;
                _submitErrText = "";
                _submitErrVisible = false;
                Navigator.of(ctx).pop();
              });
              showSuccess(
                context: context,
                title: "Claim updated",
                content:
                    "Claim has been removed temporarily and will be removed permanently if a proof isn’t provided by the creditor",
                onOk: DebtsProviver.debtsListCb,
              );
            } else {
              setState(() {
                isSending = false;
                _submitErrText = response["errors"];
                _submitErrVisible = true;
              });
            }
          }

          var appColors =
              Provider.of<AppColorsProvider>(context, listen: false);
          return IntrinsicHeight(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "How do you want to flag this claim?",
                  style: Theme.of(context).textTheme.body1.copyWith(
                        fontWeight: FontWeight.w300,
                      ),
                ),
                SizedBox(
                  height: 10,
                ),
                Theme(
                  data: ThemeData(
                    unselectedWidgetColor: Theme.of(context).primaryColor,
                  ),
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              height: 15,
                              width: 15,
                              alignment: Alignment.centerLeft,
                              child: Radio(
                                materialTapTargetSize:
                                    MaterialTapTargetSize.shrinkWrap,
                                value: 0,
                                groupValue: _selectedValue,
                                activeColor: Theme.of(context).primaryColor,
                                onChanged: (value) {
                                  setState(() {
                                    _selectedValue = value;
                                  });
                                },
                              ),
                            ),
                            SizedBox(
                              width: 15,
                            ),
                            Text(
                              "With proof",
                              style: Theme.of(context).textTheme.body1.copyWith(
                                    fontWeight: FontWeight.w300,
                                  ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                              height: 15,
                              width: 15,
                              child: Radio(
                                value: 1,
                                groupValue: _selectedValue,
                                activeColor: Theme.of(context).primaryColor,
                                onChanged: (value) {
                                  setState(() {
                                    _selectedValue = value;
                                  });
                                },
                              ),
                            ),
                            SizedBox(
                              width: 15,
                            ),
                            Text(
                              "Without proof",
                              style: Theme.of(context).textTheme.body1.copyWith(
                                    fontWeight: FontWeight.w300,
                                  ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  width: double.infinity,
                  child: CustomRaisedButton(
                    bgColor: Theme.of(context).accentColor,
                    title: isSending ? "" : "Continue",
                    onTapAction: () {
                      if (_selectedValue == 0) {
                        Navigator.of(ctx).pop();
                        provideProofFn();
                      } else {
                        isSending ? null : onSubmit();
                      }
                    },
                  ),
                ),
                ErrText(
                  errText: _submitErrText,
                  vissibility: _submitErrVisible,
                )
              ],
            ),
          );
        },
      ),
    );
  }

  static uploadPloadPaymentProof(
      {BuildContext context,
      String title,
      String content,
      Function afterPop,
      bool resolveDebt = false,
      dynamic loan}) async {
    Map<String, dynamic> _file = {"name": "", "file": File(""), "filePath": ""};
    bool fileUploaded = false;
    String _submitErrText = "";
    bool _submitErrVisible = false;
    bool isSending = false;
    String loanId = resolveDebt
        ? DebtsProviver.selectedDebt.id
        : ExpectedPaymentsProvider.selectedDebtorLoanRequest.id;
    Function successCb = resolveDebt
        ? DebtsProviver.debtsListCb
        : ExpectedPaymentsProvider.loanRequestCb;
    Map<String, dynamic> userData = await Provider.of<UserProvider>(context, listen: false).userData;
    modalDesignWrapper(
      context: context,
      title: "Payment made offline",
      child: StatefulBuilder(
        builder: (ctx, setState) {
          Future _pickFile() async {
            PermissionStatus permission = await Permission.storage.request();
            if (permission != null) {
              if (permission == PermissionStatus.permanentlyDenied) {
                return openAppSettings();
              } else if (permission == PermissionStatus.denied) {
                return await Permission.storage.request();
              }
              var file = await FilePicker.getFile(
                type: FileType.image,
              );
              setState(() {
                //TODO: check mimetype
                _file = {
                  "name": file.path.split("/").last,
                  "file": file,
                  "filePath": file.path
                };
                fileUploaded = true;
              });

              if (resolveDebt) {
                DebtsProviver.debtPaymentProofFilePath = _file["filePath"];
              } else {
                ExpectedPaymentsProvider.loanRequestPaymentProofFilePath =
                    _file["filePath"];
              }
            }
          }

          void _onSubmit() async {
            setState(() {
              _submitErrText = "";
              _submitErrVisible = false;
            });
            setState(() {
              isSending = true;
            });
            if (resolveDebt) {
              var response =
                  await Provider.of<DebtsProviver>(context, listen: false)
                      .offlinePayment(
                          loanId: loanId, documentPath: _file["filePath"]);
              if (response["status"] == 200 || response["status"] == 201) {
                setState(() {
                  isSending = false;
                  _submitErrText = "";
                  _submitErrVisible = false;
                });
                Navigator.of(ctx).pop();
                showSuccess(
                    context: context,
                    title: "Loan Funded",
                    content: "Your beneficiary has received the loan",
                    onOk: successCb);
              } else {
                setState(() {
                  isSending = false;
                  _submitErrText = response["errors"];
                  _submitErrVisible = true;
                });
              }
            } else {
              var response = await Provider.of<ExpectedPaymentsProvider>(
                      context,
                      listen: false)
                  .approveLoanWithReceipt(
                recipientPhone: loan.debtor.phoneNumber,
                loanRequestId: loanId,
                documentPath: _file["filePath"],
              );
              if (response["status"] == 200 || response["status"] == 201) {
                setState(() {
                  isSending = false;
                  _submitErrText = "";
                  _submitErrVisible = false;
                });
                //add loan to debtors calender
                // addLoanToUserCalender(
                //   dueDate: loan.dueDate.toIso8601String(),
                //   amount: loan.amount,
                //   debtorId: loan.debtor.id,
                //   creditorFirstName: userData["first_name"],
                // );
                Navigator.of(ctx).pop();
                showSuccess(
                    context: context,
                    title: "Loan Funded",
                    content: "Your beneficiary has received the loan",
                    onOk: successCb);
              } else {
                setState(() {
                  isSending = false;
                  _submitErrText = response["errors"];
                  _submitErrVisible = true;
                });
              }
            }
          }

          var appColors =
              Provider.of<AppColorsProvider>(context, listen: false);
          return IntrinsicHeight(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                Text(
                  content,
                  style: Theme.of(context).textTheme.body1.copyWith(
                        fontWeight: FontWeight.w300,
                      ),
                ),
                SizedBox(
                  height: 5,
                ),
                CustomUploadButton(
                  bgColor: Color(0xffDB6837),
                  title: "Upload proof",
                  leadingIconPath: "public/images/cloud-upload.png",
                  onClick: _pickFile,
                ),
                if (fileUploaded)
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      //          AnimatedContainer(
                      //   constraints: BoxConstraints(
                      //     maxHeight: viewProof ? 400 : 0,
                      //   ),
                      //   duration: Duration(milliseconds: 300),
                      //   child: ClipRRect(
                      //       borderRadius: BorderRadius.circular(5),
                      //       child: Image.network(
                      //         loanAction["document"],
                      //         fit: BoxFit.fill,
                      //       )),
                      // ),
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 10),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(6),
                          child: Container(
                            // width: 200,
                            height: 400,
                            constraints: BoxConstraints(maxHeight: 250),
                            child: Image.file(_file["file"], fit: BoxFit.fill),
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: _pickFile,
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: 8.0),
                          child: Text(
                            "Change proof",
                            style: Theme.of(context).textTheme.body1.copyWith(
                                  fontWeight: FontWeight.w400,
                                  color: Theme.of(context).primaryColor,
                                ),
                          ),
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        child: CustomRaisedButton(
                            title: isSending ? "" : "Send",
                            onTapAction: () {
                              isSending ? () {} : _onSubmit();
                            },
                            indicator: isSending
                                ? CustomProgressIndicator()
                                : Container()),
                      ),
                      ErrText(
                        errText: _submitErrText,
                        vissibility: _submitErrVisible,
                      )
                    ],
                  )
              ],
            ),
          );
        },
      ),
    );
  }

  static showMoreInfo(
      {BuildContext context, String title, String content, Function onOk}) {
    modalDesignWrapper(
      context: context,
      title: title,
      child: IntrinsicHeight(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              content,
              style: Theme.of(context).textTheme.body1.copyWith(
                    fontWeight: FontWeight.w300,
                  ),
            ),
            SizedBox(
              height: 10,
            ),
            Align(
              alignment: Alignment.centerRight,
              child: InkWell(
                onTap: () {
                  if (onOk != null) {
                    onOk();
                  }
                  Navigator.of(context).pop();
                },
                child: Text(
                  "Ok",
                  style: Theme.of(context).textTheme.body2.copyWith(
                        color: Theme.of(context).primaryColor,
                      ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  static recallAction({BuildContext context, String title, String content}) {
    String _submitErrText = "";
    bool _submitErrVisible = false;
    bool isSending = false;
    modalDesignWrapper(
      context: context,
      title: title,
      child: StatefulBuilder(builder: (ctx, setState) {
        void onSubmit() async {
          setState(() {
            _submitErrText = "";
            _submitErrVisible = false;
          });
          isSending = true;

          var denyResponse =
              await Provider.of<DebtsProviver>(context, listen: false)
                  .recallAction();
          if (denyResponse["status"] == 200 || denyResponse["status"] == 201) {
            setState(() {
              isSending = false;
              _submitErrText = "";
              _submitErrVisible = false;
            });
            Navigator.of(ctx).pop();
            showSuccess(
                title: "Action recalled",
                context: context,
                content: "You can now access other actions on this loan.",
                onOk: DebtsProviver.debtsListCb);
          } else {
            setState(() {
              isSending = false;
              _submitErrText = denyResponse["errors"];
              _submitErrVisible = true;
            });
          }
        }

        var appColors = Provider.of<AppColorsProvider>(context, listen: false);

        return IntrinsicHeight(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                content,
                style: Theme.of(context).textTheme.body1.copyWith(
                      fontWeight: FontWeight.w300,
                    ),
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  InkWell(
                    onTap: () =>
                        isSending ? () {} : Navigator.of(context).pop(),
                    child: Container(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        isSending ? "" : "Cancel",
                        style: Theme.of(context).textTheme.body2.copyWith(
                              color: Theme.of(context).primaryColor,
                            ),
                      ),
                    ),
                  ),
                  // CustomRaisedButton(
                  //   onTapAction: () => isSending ? () {} : onSubmit(),
                  //   title: isSending ? "" : "Recall action",
                  // ),
                  InkWell(
                    onTap: () => isSending ? () {} : onSubmit(),
                    child: Container(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        isSending ? "" : "Recall action",
                        style: Theme.of(context).textTheme.body2.copyWith(
                              color: Theme.of(context).primaryColor,
                            ),
                      ),
                    ),
                  ),
                  isSending
                      ? CustomProgressIndicator(
                          color: appColors.altProgressValueColor,
                        )
                      : Container(),
                ],
              ),
              ErrText(
                errText: _submitErrText,
                vissibility: _submitErrVisible,
              )
            ],
          ),
        );
      }),
    );
  }

  //use this modal for non self-contained modals confirmations
  static normalConfirmationDialog({
    BuildContext context,
    String title,
    String content,
    Function onConfirm,
    Function onUnconfirm,
    bool isColumnActions = false,
    String firstActionText = "NO",
    String secondActionText = "YES",
  }) {
    var appColors = Provider.of<AppColorsProvider>(context, listen: false);
    modalDesignWrapper(
      context: context,
      title: title,
      child: IntrinsicHeight(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              content,
              style: Theme.of(context).textTheme.body1.copyWith(
                    fontWeight: FontWeight.w300,
                  ),
            ),
            SizedBox(
              height: 20,
            ),
            if (!isColumnActions)
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        onUnconfirm();
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        padding: EdgeInsets.all(10),
                        alignment: Alignment.center,
                        child: Text(
                          "NO",
                          style: Theme.of(context)
                              .textTheme
                              .body2
                              .copyWith(color: Theme.of(context).primaryColor),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        onConfirm();
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        padding: EdgeInsets.all(10),
                        alignment: Alignment.center,
                        child: Text(
                          "YES",
                          style: Theme.of(context)
                              .textTheme
                              .body2
                              .copyWith(color: Theme.of(context).primaryColor),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            if (isColumnActions)
              Column(
                children: <Widget>[
                  CustomRaisedButton(
                    width: double.infinity,
                    title: firstActionText,
                    bgColor: Theme.of(context).accentColor,
                    onTapAction: () {
                      onUnconfirm();
                      Navigator.of(context).pop();
                    },
                  ),
                  CustomRaisedButton(
                    title: secondActionText,
                    width: double.infinity,
                    bgColor: Colors.transparent,
                    color: Theme.of(context).primaryColor,
                    onTapAction: () {
                      onConfirm();
                      Navigator.of(context).pop();
                    },
                  )
                ],
              )
          ],
        ),
      ),
    );
  }

  static showSuccess(
      {BuildContext context,
      String title = "Success",
      String content,
      Function onOk}) {
    var appColors = Provider.of<AppColorsProvider>(context, listen: false);

    modalDesignWrapper(
      context: context,
      title: "",
      onDismiss: onOk,
      showIcon: false,
      centerIconWidget: Container(
        margin: EdgeInsets.only(top: 20),
        height: 60,
        child: Image.asset(
          "public/images/ok.png",
        ),
      ),
      child: Stack(
        alignment: AlignmentDirectional.topCenter,
        children: <Widget>[
          Column(
            children: <Widget>[
              SizedBox(height: 25),
              Text(
                title,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.body2.copyWith(
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                    ),
              ),
              Text(
                content,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.body1.copyWith(
                      fontWeight: FontWeight.w300,
                    ),
              ),
              SizedBox(
                height: 10,
              ),
              Align(
                alignment: Alignment.bottomRight,
                child: InkWell(
                  onTap: () {
                    if (onOk != null) {
                      onOk();
                    }
                    Navigator.of(context).pop();
                  },
                  child: Padding(
                    padding: EdgeInsets.all(8),
                    child: Text(
                      "OK",
                      style: Theme.of(context)
                          .textTheme
                          .body2
                          .copyWith(color: Theme.of(context).primaryColor),
                    ),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

//Todo
  static warningDialog(
      {BuildContext context, String content, Function onConfirm}) {
    modalDesignWrapper(
      context: context,
      title: "",
      showIcon: false,
      centerIconWidget: Container(
        margin: EdgeInsets.only(top: 18),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.white,
        ),
        child: FaIcon(
          FontAwesomeIcons.lightExclamationCircle,
          color: Color(0xFFDB6837),
          size: 55,
        ),
      ),
      child: IntrinsicHeight(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(
              height: 25,
            ),
            Text(
              "Are you sure?",
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .textTheme
                  .body2
                  .copyWith(fontSize: 14, fontWeight: FontWeight.w600),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              content,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.body1.copyWith(
                    fontWeight: FontWeight.w300,
                  ),
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: CustomRaisedButton(
                    title: "Cancel",
                    // bgColor: Color(0xff65CA7E),
                    bgColor: Theme.of(context).accentColor,
                    onTapAction: () => Navigator.of(context).pop(),
                  ),
                ),
                SizedBox(
                  width: 15,
                ),
                Expanded(
                  child: CustomRaisedButton(
                    title: "Proceed",
                    // bgColor: Color(0xffD3514D),
                    bgColor: Theme.of(context).primaryColor,
                    onTapAction: () {
                      onConfirm();
                      Navigator.of(context).pop();
                    },
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
    // showDialog(
    //   context: context,
    //   child: Dialog(
    //     shape: RoundedRectangleBorder(
    //       borderRadius: BorderRadius.circular(6),
    //     ),
    //     child: Container(
    //       padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
    //       width: 200,
    //       constraints: BoxConstraints(
    //         minHeight: 120,
    //         maxHeight: 250,
    //       ),
    //       child:
    //     ),
    //   ),
    // );
  }

  static addAlternatePhone(
      {BuildContext context, String title, String content, Function afterPop}) {
    var _index = 0;
    modalDesignWrapper(
      context: context,
      title: "Add alternate phone",
      minHeight: 320,
      width: double.infinity,
      child: StatefulBuilder(
        builder: (ctx, setState) {
          return IntrinsicHeight(
            child: IndexedStack(
              index: _index,
              children: <Widget>[
                PhoneNumberScreen(
                  isAltPhone: true,
                  altPhoneAction: () {
                    setState(() {
                      _index = 1;
                    });
                  },
                ),
                OtpScreen(
                  isAltPhone: true,
                  altPhoneNav: () {
                    setState(() {
                      _index = 0;
                    });
                  },
                  altPhoneAction: () {
                    Navigator.of(ctx).pop();
                    showSuccess(
                        context: context,
                        content: "Alternate phone number added successfully",
                        onOk: Provider.of<UserProvider>(
                          context,
                          listen: false,
                        ).profilePageCb);
                  },
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  static sendMessage(
      {BuildContext context,
      String title,
      String contentTitle,
      String smsMessage,
      String whatsappMessage,
      String recipientPhone,
      String successMessage,
      bool hasWhatsapp,
      String loanId,
      bool isLoan = true}) {
    bool isSmsSending = false;
    bool isWhatsappSending = false;
    bool isLoanClosing = false;
    String _loanErrText = "";
    bool _loanErrVisible = false;
    String _smsErrText = "";
    bool _smsErrVisible = false;
    String _whatsappErrText = "";
    bool _whatsappErrVisible = false;
    modalDesignWrapper(
      context: context,
      title: title,
      child: StatefulBuilder(
        builder: (ctx, setState) {
          void onSmsClicked() async {
            // if (Platform.isIOS) {
            var message = smsMessage.replaceAll(" ", "%20");
            var url;
            if (Platform.isAndroid) {
              url = 'sms:$recipientPhone?body=$message';
            } else {
              url = 'sms:$recipientPhone&body=$message';
            }
            if (await canLaunch('sms:$recipientPhone')) {
              await launch(url, forceSafariVC: Platform.isIOS ? false : null);
            } else {
              setState(() {
                isSmsSending = false;
                _smsErrText = "Could not open SMS App";
                _smsErrVisible = true;
              });
            }
            // } else {
            //   setState(() {
            //     _smsErrText = "";
            //     _smsErrVisible = false;
            //     isSmsSending = true;
            //   });
            //   HelperFunctions.sendSms(
            //     recipientPhone: recipientPhone,
            //     messageText: smsMessage,
            //     smsCb: (isSent) {
            //       if (!isSent) {
            //         setState(() {
            //           isSmsSending = false;
            //           _smsErrText = "Sms could not be sent, please try again.";
            //           _smsErrVisible = true;
            //         });
            //       } else {
            //         Navigator.of(ctx).pop();
            //         showSuccess(
            //             title: "Sms sent",
            //             context: context,
            //             content: "A reminder as been sent to the defaulter.",
            //             onOk: () {});
            //       }
            //     },
            //   );
            // }
          }

          Future<void> whatsappHandler() async {
            // String phoneNumber = HelperFunctions.serializePhoneNumber(
            //     phoneNumber: loan.debtor.phoneNumber);
            // bool isLoanOverDue = DateTime.now().isAfter(loan.date);
            // String message =
            //     "Hello, here is a reminder of the loan you are currently ${isLoanOverDue ? "defaulting" : "owing"} which ${isLoanOverDue ? "was" : "is"} due for payment on ${DateFormat.yMMMMd().format(loan.date)} to ${userData["first_name"]} on PeerScore, kindly respond to it, thanks.";
            var whatsappUrl =
                "whatsapp://send?phone=$recipientPhone&text=$whatsappMessage";

            if (await canLaunch(whatsappUrl)) {
              Navigator.of(ctx).pop();
              await launch(whatsappUrl,
                  forceSafariVC: Platform.isIOS ? false : null);
            } else {
              setState(() {
                isWhatsappSending = false;
                _whatsappErrText = "Unable to send message.";
                _whatsappErrVisible = true;
              });
            }
          }

          Future<void> closeLoanHandler() async {
            Navigator.of(ctx).pop();
            closeLoanReason(context: context);
            // setState(() {
            //   isLoanClosing = true;
            //   _loanErrVisible = false;
            // });
            // var result =
            //     await Provider.of<LoansProvider>(context, listen: false)
            //         .closeLoan(body: {"loan_id": loanId});
            // if (result["status"] == 200 || result["status"] == 201) {
            //   Navigator.of(ctx).pop();
            //   showSuccess(
            //     title: "Loan closed",
            //     context: context,
            //     content: "This loan has been closed successfully",
            //     onOk: ExpectedPaymentsProvider.loansListCb,
            //   );
            // } else {
            //   setState(() {
            //     isLoanClosing = false;
            //     _loanErrText = "Could not close loan, please try again";
            //     _loanErrVisible = true;
            //   });
            // }
          }

          return HelperFunctions.buildSendMessage(
            context: context,
            title: contentTitle,
            onSmsClicked: onSmsClicked,
            whatsappHandler: whatsappHandler,
            loanHandler: closeLoanHandler,
            smsErrText: _smsErrText,
            smsErrVisible: _smsErrVisible,
            whatsappErrText: _whatsappErrText,
            whatsappErrVisible: _whatsappErrVisible,
            hasWhatsapp: hasWhatsapp,
            isSmsSending: isSmsSending,
            isWhatsappSending: isWhatsappSending,
            isLoanClosing: isLoanClosing,
            loanErrText: _loanErrText,
            loanErrVisible: _loanErrVisible,
            isLoan: isLoan,
          );
        },
      ),
    );
  }

  // static changeSIm(
  //     {BuildContext context,
  //     String title,
  //     String content,
  //     Function provideProofFn,
  //     Function onSelect}) async {
  //   PermissionStatus permission = await Permission.sms.request();
  //   if (permission != null) {
  //     if (permission == PermissionStatus.permanentlyDenied) {
  //       return openAppSettings();
  //     } else if (permission == PermissionStatus.denied) {
  //       return await Permission.sms.request();
  //     }
  //     SimCardsProvider provider = new SimCardsProvider();
  //     List<SimCard> cards = await provider.getSimCards();
  //     cards = cards.map((card) {
  //       if (card.state == SimCardState.Ready) {
  //         return card;
  //       }
  //     }).toList();

  //     modalDesignWrapper(
  //       context: context,
  //       title: "Select Sim",
  //       child: StatefulBuilder(
  //         builder: (ctx, setState) {
  //           return IntrinsicHeight(
  //             child: Column(
  //               crossAxisAlignment: CrossAxisAlignment.start,
  //               children: <Widget>[
  //                 Container(
  //                   padding: EdgeInsets.symmetric(horizontal: 15),
  //                   child: Column(
  //                     crossAxisAlignment: CrossAxisAlignment.start,
  //                     children: <Widget>[
  //                       // Text(
  //                       //   "Select Sim",
  //                       //   style: Theme.of(context).textTheme.body2,
  //                       // ),
  //                       // SizedBox(
  //                       //   height: 10,
  //                       // ),
  //                       Text(
  //                         "Choose a sim to send message",
  //                         style: Theme.of(context).textTheme.body1.copyWith(
  //                               fontWeight: FontWeight.w300,
  //                             ),
  //                       ),
  //                     ],
  //                   ),
  //                 ),
  //                 SizedBox(
  //                   height: 5,
  //                 ),
  //                 Container(
  //                   height: 110,
  //                   padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
  //                   child: ListView.builder(
  //                       shrinkWrap: true,
  //                       itemCount: cards.length,
  //                       itemBuilder: (ctx, index) {
  //                         return Container(
  //                           margin: EdgeInsets.only(bottom: 8),
  //                           padding: EdgeInsets.only(bottom: 8),
  //                           decoration: BoxDecoration(
  //                               border: Border(
  //                             bottom: BorderSide(width: 0.3),
  //                           )),
  //                           child: InkWell(
  //                             onTap: () {
  //                               onSelect(cards[index]);
  //                               Navigator.of(ctx).pop();
  //                             },
  //                             child: Row(
  //                               children: <Widget>[
  //                                 SizedBox(
  //                                   height: 25,
  //                                   child: Image.asset(
  //                                       "public/images/sim-icon.png"),
  //                                 ),
  //                                 SizedBox(
  //                                   width: 10,
  //                                 ),
  //                                 Text(
  //                                   "SIM ${cards[index].slot}",
  //                                   style: Theme.of(context)
  //                                       .textTheme
  //                                       .body2
  //                                       .copyWith(),
  //                                 )
  //                               ],
  //                             ),
  //                           ),
  //                         );
  //                       }),
  //                 ),
  //               ],
  //             ),
  //           );
  //         },
  //       ),
  //     );
  //     // showDialog(
  //     //   context: context,
  //     //   child: Dialog(
  //     //     shape: RoundedRectangleBorder(
  //     //       borderRadius: BorderRadius.circular(6),
  //     //     ),
  //     //   ),
  //     // );
  //   }
  // }

  static notification({
    BuildContext context,
    String title,
    String content,
    String proofLink,
    bool hasProof,
    bool showPage,
    int mainPageIndex,
    int subPageIndex,
  }) {
    bool viewProof = false;

    modalDesignWrapper(
      context: context,
      title: title,
      minHeight: 100,
      width: double.infinity,
      child: StatefulBuilder(
        builder: (ctx, setState) {
          var appColors =
              Provider.of<AppColorsProvider>(context, listen: false);
          return IntrinsicHeight(
              child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(content),
                SizedBox(
                  height: 15,
                ),
                if (proofLink != null)
                  InkWell(
                    onTap: () {
                      setState(() {
                        viewProof = !viewProof;
                      });
                    },
                    child: Padding(
                      padding:
                          const EdgeInsets.only(top: 5, bottom: 5, right: 5),
                      child: Row(
                        children: <Widget>[
                          Text(
                            "View proof",
                            style: Theme.of(context).textTheme.body1.copyWith(
                                  color: appColors.altColorText,
                                ),
                          ),
                          SizedBox(width: 5),
                          FaIcon(
                            viewProof
                                ? FontAwesomeIcons.lightChevronDown
                                : FontAwesomeIcons.lightChevronUp,
                            color: Theme.of(context).primaryColor,
                            size: 12,
                          )
                        ],
                      ),
                    ),
                  ),
                if (proofLink != null)
                  AnimatedContainer(
                    constraints: BoxConstraints(
                      maxHeight: viewProof ? 400 : 0,
                    ),
                    duration: Duration(milliseconds: 300),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(5),
                      child: Image.network(
                        proofLink,
                        loadingBuilder: (BuildContext context, Widget child,
                            ImageChunkEvent loadingProgress) {
                          if (loadingProgress == null) return child;
                          return
                              // CustomProgressIndicator(
                              //   color: Theme.of(context).primaryColor,
                              // );
                              Container(
                            width: 20,
                            height: 20,
                            padding: EdgeInsets.all(3),
                            child: CircularProgressIndicator(
                              strokeWidth: 2,
                              valueColor: AlwaysStoppedAnimation<Color>(
                                  Theme.of(context).primaryColor),
                              backgroundColor: Colors.transparent,
                              value: loadingProgress.expectedTotalBytes != null
                                  ? loadingProgress.cumulativeBytesLoaded /
                                      loadingProgress.expectedTotalBytes
                                  : null,
                            ),
                          );
                        },
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisAlignment: showPage
                      ? MainAxisAlignment.spaceAround
                      : MainAxisAlignment.end,
                  children: <Widget>[
                    if (showPage)
                      InkWell(
                        onTap: () {
                          //check if profile screen is opened then close
                          Navigator.of(ctx).pop();
                          Navigator.of(context).pop();
                          if (GeneralProvider.profileScaffoldKey != null) {
                            Navigator.of(context).pushReplacementNamed(
                                MainScreen.routeName,
                                arguments: {
                                  "mainPageIndex": mainPageIndex,
                                  "subPageIndex": subPageIndex
                                });
                            GeneralProvider.profileScaffoldKey = null;
                          } else {
                            if (GeneralProvider.currentSelectedPageIndex ==
                                mainPageIndex) {
                              GeneralProvider.switchSubStackIndex(
                                  index: subPageIndex);
                            } else {
                              GeneralProvider.switchMainPage(
                                index: mainPageIndex,
                                pageStackIntialIndex: subPageIndex,
                              );
                            }
                          }
                          NotificationViewSingleton.getInstance()
                              .notificationViewSubject
                              .add("Test Event");
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            "VIEW",
                            style: Theme.of(context).textTheme.body2.copyWith(
                                  color: Theme.of(context).primaryColor,
                                ),
                          ),
                        ),
                      ),
                    // Spacer(),
                    InkWell(
                      onTap: () {
                        Navigator.of(context).pop();
                        GeneralProvider.widgetType = null;
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          "OK",
                          style: Theme.of(context).textTheme.body2.copyWith(
                                color: Theme.of(context).primaryColor,
                              ),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ));
        },
      ),
    );
    // .then((_) {
    //
    //   // GeneralProvider.widgetType = null;
    //   modalNotifierCb(false);
    // });
  }

  static flagSort(
      {BuildContext context,
      String title,
      int selectedValue,
      List itemList,
      Function onSelectOption}) {
    int _selectedValue = selectedValue;
    modalDesignWrapper(
      context: context,
      title: title,
      child: StatefulBuilder(builder: (ctx, setState) {
        var appColors = Provider.of<AppColorsProvider>(context, listen: false);
        return IntrinsicHeight(
          child: Container(
            child: Column(
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: List.generate(
                    itemList.length,
                    (index) => Container(
                      margin: EdgeInsets.only(bottom: 10),
                      child: CustomRadioTile(
                        title: itemList[index],
                        value: index,
                        selectedValue: _selectedValue,
                        onSelect: (value) => setState(
                          () {
                            _selectedValue = value;
                          },
                        ),
                      ),
                    ),
                  ).toList(),
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: CustomRaisedButton(
                    title: "OK",
                    width: 50,
                    onTapAction: () {
                      Navigator.of(ctx).pop();
                      onSelectOption(_selectedValue);
                    },
                    color: Theme.of(context).primaryColor,
                    bgColor: Colors.transparent,
                  ),
                )
              ],
            ),
          ),
        );
      }),
    );
  }

  static modalDesignWrapper(
      {BuildContext context,
      String title,
      Widget child,
      Function onDismiss,
      bool showIcon = true,
      Widget centerIconWidget,
      double minHeight = 100,
      EdgeInsets padding,
      double width = 300}) {
    GeneralProvider.isModalOpened = true;
    bool myInterceptor(bool stopDefaultButtonEvent) {
      if (stopDefaultButtonEvent) return false;

      if (onDismiss != null) onDismiss();
      Navigator.of(context).pop();
      BackButtonInterceptor.remove(myInterceptor);
      return true;
    }

    BackButtonInterceptor.add(myInterceptor);
    showDialog(
        context: context,
        child: Dialog(
          backgroundColor: GeneralProvider.isLightMode
              ? Theme.of(context).primaryColor
              : Colors.grey[900],
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          child: Container(
            // padding: EdgeInsets.symmetric(vertical: 0),
            width: width,
            constraints: BoxConstraints(
              minHeight: minHeight,
              maxHeight: double.infinity,
            ),
            child: IntrinsicHeight(
              child: Stack(
                alignment: AlignmentDirectional.topCenter,
                children: <Widget>[
                  Container(
                    height: 50,
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    margin: EdgeInsets.only(right: 30),
                    child: Row(
                      // alignment: AlignmentDirectional.centerStart,
                      // mainAxisAlignment: MainAxisAlignment.center,
                      // crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        if (showIcon)
                          SizedBox(
                            height: 30,
                            child: Image.asset("public/images/logos/1.png"),
                          ),
                        Expanded(
                          child: Align(
                            child: FittedBox(
                              fit: BoxFit.contain,
                              child: Text(
                                title,
                                style: Theme.of(context)
                                    .textTheme
                                    .body2
                                    .copyWith(color: Colors.white),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  // Container(
                  //   margin: EdgeInsets.only(top: 50),
                  // ),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.only(top: 50),
                    padding: padding != null
                        ? padding
                        : EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                        bottomLeft: Radius.circular(10),
                        bottomRight: Radius.circular(10),
                      ),
                      color: GeneralProvider.isLightMode
                          ? Colors.white
                          : Colors.grey[850],
                    ),
                    constraints: BoxConstraints(minHeight: minHeight),
                    child: child,
                  ),
                  if (!showIcon) centerIconWidget,
                ],
              ),
            ),
          ),
        )).then((_) {
      if (onDismiss != null) onDismiss();
      GeneralProvider.isModalOpened = false;
      BackButtonInterceptor.remove(myInterceptor);
    });
  }
}
