import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/scheduler.dart' hide Priority;
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:peer_score/main.dart';
import 'package:peer_score/providers/contact.dart';
import 'package:peer_score/providers/expected_payments_provider.dart';
import 'package:peer_score/providers/notifications_provider.dart' as Note;
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/providers/witness_provider.dart';
import 'package:peer_score/screens/notification_screen.dart';
import 'package:peer_score/utils/enums.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NotificationUtils {
  static AppLifecycleState appLifecycleState = AppLifecycleState.resumed;

  static Note.Notification getFullNotificationObject(
      {Map<String, dynamic> notification}) {
    Contact debtor;
    Contact creditor;
    Contact witness;
    //
    Map<String, dynamic> loanOrRequestObject;
    
    switch (notification["type"]) {
      case "loan":
        loanOrRequestObject = notification["loan"];
        break;
      case "loan_request":
        loanOrRequestObject = notification["loan_request"];
        break;
      case "witness":
        loanOrRequestObject = notification["witness_stand"];
        if (loanOrRequestObject["witness"] != null) {
          witness = Contact(
            id: loanOrRequestObject["witness"]["id"],
            firstName: loanOrRequestObject["witness"]["first_name"],
            lastName: loanOrRequestObject["witness"]["last_name"],
            otherName: loanOrRequestObject["witness"]["other_name"],
            profilePicture: loanOrRequestObject["witness"]["profile_picture"],
            peerScore: "${loanOrRequestObject["witness"]["peer_score"]}",
            compliance: "${loanOrRequestObject["witness"]["compliance"]}",
            email: loanOrRequestObject["witness"]["email"],
            isWhatsApp: loanOrRequestObject["witness"]["whatsapp"],
            phoneNumber: HelperFunctions.deserializePhoneNumber(
              phoneNumber: loanOrRequestObject["witness"]["phone"],
            ),
          );
        }
        break;
      default:
    }
    debtor = loanOrRequestObject["reciever"]["id"] == null
        ? null
        : Contact(
            id: loanOrRequestObject["reciever"]["id"],
            firstName: loanOrRequestObject["reciever"]["first_name"],
            lastName: loanOrRequestObject["reciever"]["last_name"],
            otherName: loanOrRequestObject["reciever"]["other_name"],
            profilePicture: loanOrRequestObject["reciever"]["profile_picture"],
            peerScore: "${loanOrRequestObject["reciever"]["peer_score"]}",
            compliance: "${loanOrRequestObject["reciever"]["compliance"]}",
            email: loanOrRequestObject["reciever"]["email"],
            isWhatsApp: loanOrRequestObject["reciever"]["whatsapp"],
            phoneNumber: HelperFunctions.deserializePhoneNumber(
              phoneNumber: loanOrRequestObject["reciever"]["phone"],
            ),
          );
    creditor = loanOrRequestObject["provider"]["id"] == null
        ? null
        : Contact(
            id: loanOrRequestObject["provider"]["id"],
            firstName: loanOrRequestObject["provider"]["first_name"],
            lastName: loanOrRequestObject["provider"]["last_name"],
            otherName: loanOrRequestObject["provider"]["other_name"],
            profilePicture: loanOrRequestObject["provider"]["profile_picture"],
            peerScore: "${loanOrRequestObject["provider"]["peer_score"]}",
            compliance: "${loanOrRequestObject["provider"]["compliance"]}",
            email: loanOrRequestObject["provider"]["email"],
            isWhatsApp: loanOrRequestObject["provider"]["whatsapp"],
            phoneNumber: HelperFunctions.deserializePhoneNumber(
              phoneNumber: loanOrRequestObject["provider"]["phone"],
            ),
          );

    return Note.Notification(
      id: notification["id"],
      title: notification["title"],
      type: notification["type"],
      seen: notification["seen"],
      archived: notification["archived"],
      createdDate: DateTime.parse(notification["createdAt"]),
      loanRequest: notification["loan_request"] == null
          ? null
          : LoanRequest(
              id: notification["loan_request"]["id"],
              amount: notification["loan_request"]["amount"],
              privacy: notification["loan_request"]["privacy"],
              status: notification["loan_request"]["status"],
              denialReason: notification["loan_request"]["denial_reason"],
              dueDate: DateTime.parse(notification["loan_request"]["due_date"]),
              creditor: creditor,
              debtor: debtor),
      loan: notification["loan"] == null
          ? null
          : Loan(
              id: notification["loan"]["id"],
              debtor: debtor,
              creditor: creditor,
              status: notification["loan"]["status"],
              amount: notification["loan"]["amount"],
              date: DateTime.parse(notification["loan"]["due_date"]),
              loanActions: notification["loan"]["loan_action"],
              loanApprovalProof: notification["loan"]["loan_approval_proof"],
              badClaimInsistProof:
                  loanOrRequestObject["bad_claim_insist_proof"],
              badClaimInitiateProof:
                  loanOrRequestObject["bad_claim_initiate_proof"]),
      witness: notification["witness_stand"] == null
          ? null
          : Claim(
              id: loanOrRequestObject["id"],
              type: loanOrRequestObject["type"],
              verdict: loanOrRequestObject["verdict"],
              winessProvider:
                  loanOrRequestObject["type"] == "insist" ? creditor : debtor,
              claimInitiator:
                  loanOrRequestObject["type"] == "insist" ? debtor : creditor,
              witness: witness,
              loan: Loan(
                id: loanOrRequestObject["loan"]["id"],
                amount: loanOrRequestObject["loan"]["amount"],
                date: DateTime.parse(loanOrRequestObject["loan"]["due_date"]),
              ),
            ),
    );
  }

  static Contact getNotificationInstigator(
      {Note.Notification notification, String userId}) {
    Contact intigator;
    var instigatorExtractionObject;
    String type = notification.type;
    switch (type) {
      case "loan":
        instigatorExtractionObject = notification.loan;
        break;
      case "loan_request":
        instigatorExtractionObject = notification.loanRequest;
        break;
      case "witness":
        instigatorExtractionObject = notification.witness;
        break;
      default:
    }

    if (type == "loan" || type == "loan_request") {
      intigator = userId == instigatorExtractionObject.debtor.id
          ? instigatorExtractionObject.creditor
          : instigatorExtractionObject.debtor;
    } else if (type == "witness") {
      String notificationTitle = notification.title;
      switch (notificationTitle.toLowerCase()) {
        case "witness response":
          intigator = instigatorExtractionObject.witness;
          break;
        default:
          intigator = instigatorExtractionObject.winessProvider;
      }
    }
    return intigator;
  }

  static dynamic getNotificationAttachedProofLink(
      {Note.Notification notification}) {
    Map<String, dynamic> loanAction;
    Map<String, dynamic> badClaimInitiateProof;
    Map<String, dynamic> badClaimInsistProof;
    Map<String, dynamic> loanApprovalProof;
    var proofLink;
    //only do this if the notification type is loan
    if (notification.type == "loan") {
      loanAction = notification.loan.loanActions;
      badClaimInitiateProof = notification.loan.badClaimInitiateProof;
      badClaimInsistProof = notification.loan.badClaimInsistProof;
      loanApprovalProof = notification.loan.loanApprovalProof;

      if (notification.title.trim().toLowerCase() == "loan request approved") {
        proofLink = loanApprovalProof["document"];
      } else if (notification.title.trim().toLowerCase() == "loan payment") {
        proofLink = loanAction["document"];
      } else if (notification.title.trim().toLowerCase() == "loan flagged") {
        proofLink = badClaimInitiateProof["document"];
      } else if (notification.title.trim().toLowerCase() == "loan insisted") {
        proofLink = badClaimInsistProof["document"];
      }

      return proofLink;
    }
  }

  static Map<String, dynamic> getNotificationBody(
      {Note.Notification notification, String userId}) {
    String content = "";
    String loanAmount = "";
    String dueDateString = "";
    String denialReason = "";
    String newRequestedDate = "";
    Map<String, dynamic> loanAction;
    DateTime dueDate;
    String witnessProviderFullName;
    String claimInitiatorFullName;
    String witnessFullName;
    String claimType;
    Contact instigator;
    String instigatorFullName = "";
    String type = notification.type;
    String title = notification.title;
    bool isItemAvailable = false;
    bool isItemAvailableForCreditor = false;
    bool showPage;

    //pageIndex view action directs to
    int mainPageIndex;
    //subPageIndex view action directs to under main page
    int subPageIndex;

    if (type == "loan") {
      loanAmount = notification.loan.amount;
      dueDate = notification.loan.date;
      loanAction = notification.loan.loanActions;
      instigator =
          getNotificationInstigator(notification: notification, userId: userId);
      instigatorFullName =
          HelperFunctions.getContactFullName(contact: instigator);
      if (title.trim().toLowerCase() == "re-schedule request") {
        newRequestedDate =
            DateFormat.yMMMMd().format(DateTime.parse(loanAction["new_date"]));
      }
      // status = notification.loan.status;
      isItemAvailable = (notification.loan.status != "completed");
      // (notification.loan.status != "bad_claim");

      isItemAvailableForCreditor = (notification.loan.status != "completed") &&
          (notification.loan.status == "bad_claim");
      GeneralProvider.selectedItemId = notification.loan.id;
    } else if (type == "loan_request") {
      loanAmount = notification.loanRequest.amount;
      dueDate = notification.loanRequest.dueDate;
      denialReason = notification.loanRequest.denialReason;
      instigator =
          getNotificationInstigator(notification: notification, userId: userId);
      instigatorFullName =
          HelperFunctions.getContactFullName(contact: instigator);

      isItemAvailable = notification.loanRequest.status == "waiting";
      GeneralProvider.selectedItemId = notification.loanRequest.id;
    } else if (type == "witness") {
      dueDate = notification.witness.loan.date;
      loanAmount = notification.witness.loan.amount;
      witnessProviderFullName = HelperFunctions.getContactFullName(
          contact: notification.witness.winessProvider);
      claimInitiatorFullName = HelperFunctions.getContactFullName(
          contact: notification.witness.claimInitiator);
      witnessFullName = notification.witness.witness != null
          ? HelperFunctions.getContactFullName(
              contact: notification.witness.witness)
          : null;
      claimType = notification.witness.type;
      isItemAvailable = notification.witness.verdict == null;
      // GeneralProvider.selectedItemId = notification.loanRequest.id;
      //comeback to save witness voting data
      WitnesProvider.selectedClaim = notification.witness;
    }
    loanAmount = HelperFunctions.getFormattedAmount(amount: loanAmount);
    dueDateString = DateFormat.yMMMMd().format(dueDate);
    switch (title.trim().toLowerCase()) {
      case "new loan request":
        content =
            "$instigatorFullName has made a loan request of $loanAmount and has chosen to payback on $dueDateString, kindly respond to it.";
        mainPageIndex = 1;
        subPageIndex = 1;
        GeneralProvider.widgetType = ExpectedPaymentsStackEnum.loanRequests;
        break;
      case "loan request denied":
        content =
            "Your loan request of $loanAmount made to $instigatorFullName has been declined because: \n \n $denialReason";
        break;
      case "loan request funded":
        content =
            "Your loan request of $loanAmount made to $instigatorFullName has been approved and you are expected to pay back on or before $dueDateString.";
        mainPageIndex = 2;
        subPageIndex = 0;
        GeneralProvider.widgetType = CreditorsStackEnum.creditorsPage;
        break;
      case "loan request approved":
        content =
            "Your loan request of $loanAmount made to $instigatorFullName has been approved and you are expected to pay back on or before $dueDateString.";
        mainPageIndex = 2;
        subPageIndex = 0;
        GeneralProvider.widgetType = CreditorsStackEnum.creditorsPage;
        break;
      case "new loan demand":
        bool isLoanOverDue = DateTime.now().isAfter(dueDate);
        content =
            "$instigatorFullName has made a payment request of $loanAmount ${isLoanOverDue ? "which was" : "which will be"} due for payment on $dueDateString, endeavour to refund this loan.";
        mainPageIndex = 2;
        subPageIndex = 0;
        GeneralProvider.widgetType = CreditorsStackEnum.creditorsPage;
        break;
      case "loan flagged":
        content =
            "$instigatorFullName has flagged your loan of $loanAmount, loan is temporarily inactive for the debtor. Cancel this loan if you have made it in error or provide proof to uphold it.";
        mainPageIndex = 1;
        subPageIndex = 0;
        isItemAvailable = isItemAvailableForCreditor
            ? isItemAvailableForCreditor
            : isItemAvailable;
        GeneralProvider.widgetType = ExpectedPaymentsStackEnum.debtorsPage;
        break;
      case "loan paid":
        content =
            "$instigatorFullName has made payment for the loan of $loanAmount, loan is now completed.";
        // mainPageIndex = 1;
        // subPageIndex = 1;
        break;
      case "loan payment":
        content =
            "$instigatorFullName has made payment for the loan of $loanAmount, loan will be marked as complete once you confirm payment.";
        mainPageIndex = 1;
        subPageIndex = 0;
        isItemAvailable = isItemAvailableForCreditor
            ? isItemAvailableForCreditor
            : isItemAvailable;
        GeneralProvider.widgetType = ExpectedPaymentsStackEnum.debtorsPage;
        break;
      case "re-schedule request":
        content =
            "$instigatorFullName has requested for an extension of payment date for the loan of $loanAmount to $newRequestedDate, the debtor will be able to pay on new date if you approve the request.";
        mainPageIndex = 1;
        subPageIndex = 0;
        isItemAvailable = isItemAvailableForCreditor
            ? isItemAvailableForCreditor
            : isItemAvailable;
        GeneralProvider.widgetType = ExpectedPaymentsStackEnum.debtorsPage;
        break;
      case "payment confirmed":
        content =
            "$instigatorFullName has confirmed your payment for the loan of $loanAmount, the loan is now marked as complete.";
        // mainPageIndex = 2;
        // subPageIndex = 1;
        break;
      case "re-schedule confirmed":
        content =
            "$instigatorFullName has confirmed your new payment date for the loan of $loanAmount, you can now pay on or before $dueDateString.";
        mainPageIndex = 2;
        subPageIndex = 0;
        GeneralProvider.widgetType = CreditorsStackEnum.creditorsPage;
        break;
      case "re-schedule declined":
        content =
            "$instigatorFullName has declined your payment extension for the loan of $loanAmount, endeavour to refund the loan on time.";
        mainPageIndex = 2;
        subPageIndex = 0;
        GeneralProvider.widgetType = CreditorsStackEnum.creditorsPage;
        break;
      case "loan insisted":
        content =
            "$instigatorFullName has reinstated the loan of $loanAmount with a supporting proof, kindly refund this loan if it is indeed valid";
        mainPageIndex = 2;
        subPageIndex = 0;
        GeneralProvider.widgetType = CreditorsStackEnum.creditorsPage;
        break;
      case "loan closed":
        content =
            "$instigatorFullName has canceled the loan of $loanAmount and has been removed permanently from your debt list";
        break;
      case "payment flagged":
        content =
            "$instigatorFullName has declined your payment for the loan of $loanAmount, you might have done this in error please check and reupload your payment proof";
        mainPageIndex = 2;
        subPageIndex = 0;
        GeneralProvider.widgetType = CreditorsStackEnum.creditorsPage;
        break;
      case "witness invite - bad claim":
        bool isLoanOverDue = DateTime.now().isAfter(dueDate);
        content =
            "$witnessProviderFullName has flagged the claim made by $claimInitiatorFullName for a loan of $loanAmount which ${isLoanOverDue ? "was" : "will be due"} for payment on $dueDateString. You have been invited to pass a verdict based on the authenticity of this claim.";
        mainPageIndex = 0;
        subPageIndex = 2;
        GeneralProvider.widgetType = DashboardStackEnum.votePage;
        break;
      case "witness invite - claim insist":
        content =
            "$witnessProviderFullName has insisted that the loan of $loanAmount which was flagged by $claimInitiatorFullName is indeed authentic. You have been invited to pass a verdict based on the authenticity of this claim.";
        mainPageIndex = 0;
        subPageIndex = 2;
        GeneralProvider.widgetType = DashboardStackEnum.votePage;
        break;
      case "witness response":
        content = claimType == "insist"
            ? "$witnessFullName has passed a verdict on the loan of $loanAmount which was flagged by $claimInitiatorFullName but you insisted."
            : "$witnessFullName has passed a verdict on the loan of $loanAmount you flagged which was provided by $claimInitiatorFullName.";
        break;
      default:
    }

    showPage =
        (mainPageIndex != null && subPageIndex != null) && isItemAvailable;

    return {
      "content": content,
      "mainPageIndex": mainPageIndex,
      "subPageIndex": subPageIndex,
      "showPage": showPage
    };
  }

  static Future<String> downloadAndSaveFile(
      String imageUrl, String fileName) async {
    Dio dio = new Dio();
    var directory = await getApplicationDocumentsDirectory();
    var filePath = '${directory.path}/$fileName';
    var response = await dio.get(imageUrl,
        options: Options(responseType: ResponseType.bytes));
    var file = File(filePath);
    await file.writeAsBytes(response.data);
    return filePath;
  }

//   static Bitmap getCircleBitmap(Bitmap bitmap) {
//     final Bitmap output = Bitmap().buildImage() .createBitmap(bitmap.getWidth(),
//             bitmap.height, Bitmap.Config.ARGB_8888);
//     final Canvas canvas = new Canvas(bitmap);

//     final int color = Color.RED;
//     final Paint paint = new Paint();
//     final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
//     final RectF rectF = new RectF(rect);

//     paint.setAntiAlias(true);
//     canvas.drawARGB(0, 0, 0, 0);
//     paint.setColor(color);
//     canvas.drawOval(rectF, paint);

//     paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
//     canvas.drawBitmap(bitmap, rect, rect, paint);

//     bitmap.recycle();

//     return output;
// }

  static Future<void> showNotificationOnDevice() async {
    List<String> lines = List<String>();
    final sharedPrefs = await SharedPreferences.getInstance();
    var notificationsData = json.decode(sharedPrefs.get("notifications"));
    var userData =
        json.decode(sharedPrefs.get("userData")) as Map<String, dynamic>;
    List<Note.Notification> notifications = [];
    List<Note.Notification> tempNotifications = [];
    notificationsData.forEach((notification) {
      tempNotifications.add(
        NotificationUtils.getFullNotificationObject(
          notification: notification,
        ),
      );
    });

    //sort the notifications list here and other neccessities
    notifications = tempNotifications;
    //only unread notifications
    notifications.removeWhere((notification) => notification.seen == true);

    int count = 0;
    // if (appLifecycleState == AppLifecycleState.paused) {
    String groupKey = 'Activities';
    String groupChannelId = '0';
    String groupChannelName = 'Activities';
    String groupChannelDescription = 'This channel shows all notifications';
    flutterLocalNotificationsPlugin.cancelAll();

    var allNotificationsSHown = await Future.wait(notifications.map(
      (notification) async {
        // if(count > 1) return;
        Contact instigator = NotificationUtils.getNotificationInstigator(
            notification: notification, userId: userData["id"]);
        // var largeIconPath =
        // await downloadAndSaveFile(instigator.profilePicture, 'largeIcon');
        String title = notification.title +
            " - " +
            HelperFunctions.getContactFullName(contact: instigator);
        String content = NotificationUtils.getNotificationBody(
            notification: notification, userId: userData["id"])["content"];
        AndroidNotificationDetails notificationAndroidSpecifics =
            AndroidNotificationDetails(
                groupChannelId, groupChannelName, groupChannelDescription,
                // largeIcon: FilePathAndroidBitmap(largeIconPath),
                importance: Importance.Max,
                priority: Priority.High,
                groupKey: groupKey,
                groupAlertBehavior: GroupAlertBehavior.Summary,
                when: notification.createdDate.millisecondsSinceEpoch,
                styleInformation: BigTextStyleInformation(content));
        NotificationDetails firstNotificationPlatformSpecifics =
            NotificationDetails(notificationAndroidSpecifics, null);

        lines.add("$title  $content");
        flutterLocalNotificationsPlugin.show(
          count,
          title,
          content,
          firstNotificationPlatformSpecifics,
          payload: notification.id,
        );
        count++;
      },
    ).toList());
    if (allNotificationsSHown != null) {
// create the summary notification required for older devices that pre-date Android 7.0 (API level 24)

      InboxStyleInformation inboxStyleInformation = InboxStyleInformation(
        lines,
        contentTitle: '$count new notification${count > 1 ? "s" : ""}',
      );
      AndroidNotificationDetails androidPlatformChannelSpecifics =
          AndroidNotificationDetails(
        groupChannelId,
        groupChannelName,
        groupChannelDescription,
        styleInformation: inboxStyleInformation,
        groupKey: groupKey,
        setAsGroupSummary: true,
      );
      NotificationDetails platformChannelSpecifics =
          NotificationDetails(androidPlatformChannelSpecifics, null);
      await flutterLocalNotificationsPlugin.show(
          count,
          'Attention',
          '$count new notification${count > 1 ? "s" : ""}',
          platformChannelSpecifics);
    }

    //nullify the widget type selected during notification body extraction
    GeneralProvider.widgetType = null;
  }
  // }

  static openNotificationPage({String payload}) {
    var appriopriateKey;

    appriopriateKey = HelperFunctions.getAppriopriateScaffoldKey();
    if (appriopriateKey != null) {
      Note.NotificationProvider.notificationIdFromDeviceTray = payload;
      if (!Note.NotificationProvider.isOpened) {
        Navigator.of(appriopriateKey.currentContext).pushNamed(
          NotificationScreen.routeName,
        );
      } else {
        //notifications page already opened, show only the selected notification modal
        HelperFunctions.openNotificationModalWhenClickedFromTray();
      }
    }
  }

  //in memory notifications update
  static addNewNotification({Map<String, dynamic> notificationObject}) async {
    final sharedPrefs = await SharedPreferences.getInstance();
    var notificationsData = json.decode(sharedPrefs.get("notifications"));
    notificationsData.add(notificationObject);
    return sharedPrefs.setString(
        "notifications", json.encode(notificationsData));
  }

  static Future<int> countUnreadNotifications() async {
    final sharedPrefs = await SharedPreferences.getInstance();
    var notificationsData = sharedPrefs.containsKey("notifications")
        ? json.decode(sharedPrefs.get("notifications"))
        : [];
    List<Note.Notification> notifications = [];
    List<Note.Notification> tempNotifications = [];
    notificationsData.forEach((notification) {
      tempNotifications.add(
        NotificationUtils.getFullNotificationObject(
          notification: notification,
        ),
      );
    });
    //sort the notifications list here and other neccessities
    notifications = tempNotifications;
    //only unread notifications
    notifications.removeWhere((notification) => notification.seen == true);
    return notifications.length;
  }
}
