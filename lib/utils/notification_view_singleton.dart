import 'package:rxdart/rxdart.dart';

class NotificationViewSingleton {
  static final NotificationViewSingleton _instance =
      NotificationViewSingleton._internal();
  NotificationViewSingleton._internal();

  static NotificationViewSingleton getInstance() => _instance;

  BehaviorSubject<dynamic> notificationViewSubject = BehaviorSubject();

  void dispose() {
    notificationViewSubject.close();
  }
}
