import 'package:flutter/material.dart';
import 'package:peer_score/providers/temp-provider.dart';

class CustomProgressIndicator extends StatelessWidget {
  final double size;
  final Color bgColor;
  final Color color;
  final double strokeWidth;
  //might have to remove this argument
  final bool useValueCOlor;
  CustomProgressIndicator({
    this.size = 25,
    this.bgColor = Colors.transparent,
    this.color,
    this.strokeWidth = 2,
    this.useValueCOlor = false,
  });
  @override
  Widget build(BuildContext context) {
    // Color themeValueColor = color == null ? Colors.white : color;
    // if (!GeneralProvider.isLightMode && themeValueColor == Colors.white)
    //   themeValueColor = Theme.of(context).primaryColor;
    // if (!GeneralProvider.isLightMode &&
    //     themeValueColor == Theme.of(context).primaryColor)
    //   themeValueColor = Colors.white;
    // if (useValueCOlor) {
    //   themeValueColor = color == null ? Colors.white : color;
    // }
    return SizedBox(
      height: size,
      width: size,
      child: CircularProgressIndicator(
        strokeWidth: strokeWidth,
        backgroundColor: bgColor,
        valueColor:
            AlwaysStoppedAnimation<Color>(color == null ? Colors.white : color),
      ),
    );
  }
}
