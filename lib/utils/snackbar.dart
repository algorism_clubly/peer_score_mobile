import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:peer_score/providers/temp-provider.dart';

class CustomSnackBar {
  static snackBar(
      {String title,
      GlobalKey<ScaffoldState> scaffoldState,
      BuildContext context}) async {
    // await Flushbar().dismiss();
    final newFlushBar = Flushbar(
      // message: title,
      messageText: Text(
        title,
        style: Theme.of(context).textTheme.body1.copyWith(
            color: GeneralProvider.isLightMode ? Colors.white : Colors.black),
      ),
      duration: Duration(seconds: 2),
      backgroundColor: GeneralProvider.isLightMode
          ? Colors.black87
          : Colors.white.withOpacity(1),
      borderRadius: 4.0,
      margin: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 60),
      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      onTap: (currentFlushBar) {
        currentFlushBar.dismiss();
      },
    );
    newFlushBar.show(context).catchError((err) => print(err));
    // print(Flushbar().isShowing());

    // if (Flushbar().isShowing()) {
    //   await Flushbar().dismiss().then(
    //       (value) => newFlushBar.show(context).catchError((err) => print(err)));
    // } else {
    //   newFlushBar.show(context).catchError((err) => print(err));
    // }
    // Flushbar(
    //   // message: title,
    //   messageText: Text(
    //     title,
    //     style: Theme.of(context).textTheme.body1.copyWith(
    //         color: GeneralProvider.isLightMode ? Colors.white : Colors.black),
    //   ),
    //   duration: Duration(seconds: 2),
    //   backgroundColor: GeneralProvider.isLightMode
    //       ? Colors.black87
    //       : Colors.white.withOpacity(0.87),
    //   borderRadius: 4.0,
    //   margin: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 60),
    //   dismissDirection: FlushbarDismissDirection.HORIZONTAL,
    // ).show(context).catchError((err) => print(err));
    // return scaffoldState.currentState.showSnackBar(
    //   SnackBar(
    //     behavior: SnackBarBehavior.floating,
    //     // elevation: 20,
    //     backgroundColor: Colors.black87,
    //     content: Container(
    //       child: Text(
    //         title,
    //         style:
    //             Theme.of(context).textTheme.body1.copyWith(color: Colors.white),
    //       ),
    //     ),
    //     duration: Duration(seconds: 2),
    //   ),
    // );
  }
}
