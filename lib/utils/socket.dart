import 'package:peer_score/utils/api/urls.dart';
import 'package:peer_score/utils/notification_utils.dart';
import 'package:rxdart/subjects.dart';
import 'package:socket_io_client/socket_io_client.dart' as io;

class SocketConnection {
  static final SocketConnection _instance = SocketConnection._internal();
  SocketConnection._internal();

  static SocketConnection getInstance() => _instance;

  static io.Socket socket;

  BehaviorSubject<int> notificationChangeSubject = BehaviorSubject();

  void startSocket({String userId}) {
    socket = io.io(Urls.baseUrl, <String, dynamic>{
      'transports': ['websocket'],
      'autoConnect': false,
      "query": {"userId": userId}
    });
    socket.connect();
    socket.on('connect', (_) => print("socket connected"));
    socket.on('Notification', (data) {
      if (data["notification_count"] != null) {
        // HelperFunctions.getInitialNotificationsCount(
        //     data["notification_count"]);
      } else {
        NotificationUtils.addNewNotification(
          notificationObject: data["content"],
        );
        NotificationUtils.showNotificationOnDevice();
        updateUnreadNotificationCount();
      }
    });
    socket.on('disconnect', (_) => print("disconnect"));
  }

  void updateUnreadNotificationCount() async {
    int unreadCount = await NotificationUtils.countUnreadNotifications();
    notificationChangeSubject.add(unreadCount);
  }

  void destroySocket() {
    socket.close();
    socket.destroy();
  }
}
