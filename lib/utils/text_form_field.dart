import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:peer_score/providers/temp-provider.dart';

class CustomTextField extends StatelessWidget {
  final String hintText;
  final String title;
  final bool filled;
  final String iconPath;
  final double borderRadius;
  final double height;
  final int maxLines;
  final Color borderColor;
  final TextEditingController textEditingController;
  final FocusNode focusNode;
  final EdgeInsets contentPadding;
  final bool obscureText;
  final Function onTap;
  final Function onChanged;
  final bool readOnly;
  final TextInputType textInputType;
  final List<TextInputFormatter> inputFormatters;
  CustomTextField(
      {this.hintText,
      this.filled = false,
      this.iconPath,
      this.title,
      this.borderRadius,
      this.height,
      this.borderColor,
      this.textEditingController,
      this.focusNode,
      this.maxLines,
      this.contentPadding,
      this.obscureText = false,
      this.onTap,
      this.onChanged,
      this.readOnly = false,
      this.textInputType,
      this.inputFormatters});
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        if (title != null)
          Text(
            title,
            style: Theme.of(context).textTheme.body1.copyWith(
                fontSize: 10, height: 10 / 14, fontWeight: FontWeight.w300),
          ),
        if (title != null)
          SizedBox(
            height: 6,
          ),
        Container(
          height: height != null ? height : 46,
          child: TextFormField(
            inputFormatters: inputFormatters != null ? inputFormatters : null,
            readOnly: readOnly,
            maxLines: maxLines != null ? maxLines : 1,
            keyboardType:
                textInputType != null ? textInputType : TextInputType.multiline,
            controller: textEditingController,
            focusNode: focusNode,
            cursorColor:GeneralProvider.isLightMode  ? Colors.black : Colors.white,
            style: Theme.of(context).textTheme.body1.copyWith(
                // height: 12/16
                ),
            onTap: onTap != null ? onTap : null,
            onChanged: onChanged != null ? onChanged : null,
            obscureText: obscureText,
            decoration: InputDecoration(
              contentPadding: contentPadding != null
                  ? contentPadding
                  : EdgeInsets.only(left: 15, right: 15),
              hintText: hintText != null ? hintText : "",
              hintStyle: Theme.of(context).textTheme.body2.copyWith(
                  fontWeight: FontWeight.w300, color: Color(0xFF9C9C9C)),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                    width: GeneralProvider.isLightMode  ? 1 : 1.5,
                    color: filled
                        ? Colors.transparent
                        : borderColor != null
                            ? borderColor
                            : GeneralProvider.isLightMode
                                ? Color(0xFFB19CD9)
                                : Colors.grey[850]),
                borderRadius: BorderRadius.circular(
                    borderRadius != null ? borderRadius : 4),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(
                    width: GeneralProvider.isLightMode  ? 1 : 1.5,
                    color: filled
                        ? Colors.transparent
                        : borderColor != null
                            ? borderColor
                            : GeneralProvider.isLightMode
                                ? Color(0xFFB19CD9)
                                : Colors.grey[850]),
                borderRadius: BorderRadius.circular(
                    borderRadius != null ? borderRadius : 4),
              ),
              filled: filled,
              fillColor: GeneralProvider.isLightMode  ? null : Colors.white.withOpacity(0.12),
              suffixIcon: iconPath != null
                  ? Padding(
                      padding: const EdgeInsets.all(13),
                      child: SizedBox(
                        width: 16.6,
                        height: 16.6,
                        child: Image.asset(
                          iconPath,
                          fit: BoxFit.fill,
                        ),
                      ),
                    )
                  : null,
            ),
          ),
        ),
      ],
    );
  }
}
