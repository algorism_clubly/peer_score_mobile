import 'package:flutter/material.dart';

class ThemeDesign {
  static ThemeData lightMode({BuildContext context}) {
    return ThemeData(
      fontFamily: "Nunito",
      primarySwatch: MaterialColor(0xFF1F3D51, {
        50: Color(0xFF1F3D51),
        100: Color(0xFF1F3D51),
        200: Color(0xFF1F3D51),
        300: Color(0xFF1F3D51),
        400: Color(0xFF1F3D51),
        500: Color(0xFF1F3D51),
        600: Color(0xFF1F3D51),
        700: Color(0xFF1F3D51),
        800: Color(0xFF1F3D51),
        900: Color(0xFF1F3D51),
      }),
      backgroundColor: Colors.white,
      canvasColor: Colors.white,
      // primaryColor: Color(0xFF1F3D51),
      accentColor: Color(0xffE78200),
      cursorColor: Colors.black,
      unselectedWidgetColor: Colors.white,
      disabledColor: Colors.white,
      errorColor: Colors.red,
      textTheme: Theme.of(context).textTheme.copyWith(
            body1: TextStyle(
                fontSize: 13,
                // height: 1.5,
                fontWeight: FontWeight.w300,
                fontStyle: FontStyle.normal,
                color: Colors.black),
            body2: TextStyle(
                fontSize: 15,
                // height: 1.5,
                fontWeight: FontWeight.normal,
                fontStyle: FontStyle.normal,
                color: Colors.black),
            headline: TextStyle(
                fontSize: 24,
                height: 1.7,
                fontWeight: FontWeight.w300,
                fontStyle: FontStyle.normal,
                color: Colors.black),
          ),
    );
  }

  static ThemeData darkMode({BuildContext context}) {
    return ThemeData(
      fontFamily: "Nunito",
      primarySwatch: MaterialColor(0xff9db8cd, {
        50: Color(0xff9db8cd),
        100: Color(0xff9db8cd),
        200: Color(0xff9db8cd),
        300: Color(0xff9db8cd),
        400: Color(0xff9db8cd),
        500: Color(0xff9db8cd),
        600: Color(0xff9db8cd),
        700: Color(0xff9db8cd),
        800: Color(0xff9db8cd),
        900: Color(0xff9db8cd),
      }),
      backgroundColor: Color(0xff121212),
      canvasColor: Color(0xff121212),
      // primaryColor: Color(0xFF1F3D51),
      accentColor: Color(0xfff0af4b),
      cursorColor: Colors.white,
      unselectedWidgetColor: Colors.white,
      disabledColor: Colors.white,
      errorColor: Color(0xffffa28a),
      textTheme: Theme.of(context).textTheme.copyWith(
            body1: TextStyle(
                fontSize: 13,
                // height: 1.5,
                fontWeight: FontWeight.w300,
                fontStyle: FontStyle.normal,
                color: Colors.white),
            body2: TextStyle(
                fontSize: 15,
                // height: 1.5,
                fontWeight: FontWeight.normal,
                fontStyle: FontStyle.normal,
                color: Colors.white),
            headline: TextStyle(
                fontSize: 24,
                height: 1.7,
                fontWeight: FontWeight.w300,
                fontStyle: FontStyle.normal,
                color: Colors.white),
          ),
    );
  }
}
