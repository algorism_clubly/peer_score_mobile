import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum ThemeModeEnum { light, dark, system }

Window window = WidgetsBinding.instance.window;

class ThemeModeSingleton {
  static final ThemeModeSingleton _instance = ThemeModeSingleton._internal();

  ThemeModeSingleton._internal();

  static ThemeModeSingleton getInstance() => _instance;

  BehaviorSubject<bool> themeModeChangeSubject = BehaviorSubject();

  static Future<bool> isLightMode() async {
    final sharedPrefs = await SharedPreferences.getInstance();
    String themeMode = sharedPrefs.containsKey("themeMode")
        ? sharedPrefs.getString("themeMode")
        : null;
    bool isLight;
    if (themeMode != null) {
      switch (themeMode.toLowerCase()) {
        case "light":
          isLight = true;
          GeneralProvider.selectedMode = "light";
          break;
        case "dark":
          isLight = false;
          GeneralProvider.selectedMode = "dark";
          break;
        case "system":
          bool isSystemLightMode =
              window.platformBrightness == Brightness.light;
          isSystemLightMode ? isLight = true : isLight = false;
          GeneralProvider.selectedMode = "system";
          break;
        default:
      }
    } else {
      bool isSystemLightMode = window.platformBrightness == Brightness.light;
      isSystemLightMode ? isLight = true : isLight = false;
      GeneralProvider.selectedMode = "system";
    }

    GeneralProvider.isLightMode = isLight;

    return isLight;
  }

  void initialize() async {
    bool mode = await isLightMode();
    themeModeChangeSubject.add(mode);
  }

  void setThemeMode(String themeMode) async {
    final sharedPrefs = await SharedPreferences.getInstance();
    if (themeMode != null) sharedPrefs.setString("themeMode", themeMode);
    bool mode = await isLightMode();
    themeModeChangeSubject.add(mode);
  }

  void dispose() {
    themeModeChangeSubject.close();
  }
}
