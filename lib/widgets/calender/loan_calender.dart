import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:peer_score/providers/colors.dart';
import 'package:peer_score/providers/contact.dart';
import 'package:peer_score/providers/expected_payments_provider.dart';
import 'package:peer_score/providers/loans-provider.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/providers/user-provider.dart';
import 'package:peer_score/utils/custom_switch.dart';
import 'package:peer_score/utils/enums.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:peer_score/utils/main_nav_bar.dart';
import 'package:peer_score/utils/notification_view_singleton.dart';
import 'package:peer_score/utils/progress_indicator.dart';
import 'package:peer_score/utils/snackbar.dart';
import 'package:provider/provider.dart';
import 'package:table_calendar/table_calendar.dart';

class LoanCalender extends StatefulWidget {
  @override
  _LoanCalenderState createState() => _LoanCalenderState();
}

class _LoanCalenderState extends State<LoanCalender>
    with TickerProviderStateMixin {
  CalendarController _calendarController;
  AnimationController _animationController;
  Map<DateTime, List<Loan>> _events = {};
  ScrollController _scrollController;
  List<Loan> _items = [];
  String _selectedItemId;
  bool isLoading = true;
  bool isCreditors = false;
  DateTime _selectedDay;
  DateTime _currentCalenderDay;
  String loanType = "debts";
  Map<String, dynamic> userData = {
    "id": "",
    "first_name": "",
    "profile_picture": "",
    "compliance": 0
  };
  Map<String, List<Loan>> _calenderLoans = {"debts": [], "payments": []};
  Map<DateTime, List> _holidays = {};
  @override
  void initState() {
    super.initState();
    _selectedDay = DateTime.now();
    _currentCalenderDay = DateTime.now();
    Provider.of<UserProvider>(context, listen: false).userData.then((data) {
      if (this.mounted) {
        setState(() {
          userData = data;
        });
      }
      getCalenderLoans(
        calenderMonth: _selectedDay.add(Duration(days: 1)),
      );
    });
    _scrollController = ScrollController(keepScrollOffset: false);
    _calendarController = CalendarController();

    // _events = {
    //   _selectedDay.add(Duration(days: 2)): [
    //     Loan(
    //       date: DateTime.now(),
    //       creditor: Contact(
    //         firstName: "test",
    //         lastName: "tester",
    //         phoneNumber: "+2348139194625",
    //       ),
    //       amount: "10000",
    //       id: "12345",
    //     ),
    //   ],
    // };
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 400),
    );

    _animationController.forward();

    BackButtonInterceptor.add(myInterceptor);
  }

  void getCalenderLoans({DateTime calenderMonth}) async {
    setState(() {
      isLoading = true;
    });
    var resp = await Provider.of<LoansProvider>(context, listen: false)
        .getCalenderLoans(body: {
      "userId": userData["id"],
      "year": calenderMonth.year.toString(),
      "month": calenderMonth.month.toString(),
    });
    if (resp["status"] == 200) {
      _calenderLoans =
          Provider.of<LoansProvider>(context, listen: false).calenderLoans;
      List<DateTime> eventDates =
          _calenderLoans[loanType].map((e) => e.date).toSet().toList();
      print(eventDates);
      Map<DateTime, List<Loan>> lEvents = {};
      eventDates.forEach((dueDate) {
        lEvents.putIfAbsent(
          dueDate,
          () => _calenderLoans[loanType]
              .where((loan) => loan.date.day == dueDate.day)
              .toList(),
        );
      });
      _events = lEvents;
      setState(() {
        isLoading = false;
      });
    } else {
      setState(() {
        isLoading = false;
        CustomSnackBar.snackBar(
          title: "Something went wrong, please try again",
          context: context,
        );
      });
    }
  }

  void onBackButton() {
    GeneralProvider.switchMainPage(index: 0);
    GeneralProvider.widgetType = null;
  }

  bool myInterceptor(bool stopDefaultButtonEvent) {
    onBackButton();
    return true;
  }

  @override
  void dispose() {
    _calendarController.dispose();
    BackButtonInterceptor.remove(myInterceptor);
    super.dispose();
  }

  void onOutlineClicked(index) {
    NotificationViewSingleton.getInstance()
        .notificationViewSubject
        .add("Loan Event");
    GeneralProvider.widgetType = isCreditors
        ? ExpectedPaymentsStackEnum.debtorsPage
        : CreditorsStackEnum.creditorsPage;
    GeneralProvider.selectedItemId = _items[index].id;
    print("ID : ");
    print(GeneralProvider.selectedItemId);
    GeneralProvider.switchMainPage(
      index: isCreditors ? 1 : 2,
      pageStackIntialIndex: 0,
    );
  }

  void _onDaySelected(DateTime day, List events, List holidays) {
    print('CALLBACK: _onDaySelected');
    print(events);
    setState(() {
      _items = events.length > 0 ? events : [];
    });
  }

  void onLoanTypeSwitch() {
    setState(() {
      loanType = isCreditors ? "payments" : "debts";
      _items = [];
    });
    getCalenderLoans(calenderMonth: _currentCalenderDay);
    // getNotifications(isArchive: isCreditors);
  }

  @override
  Widget build(BuildContext context) {
    var appColors = Provider.of<AppColorsProvider>(context, listen: false);
    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(top: 30),
            child: MainNavBar(
              title: "Loan calendar",
              leadingImagePath: 'public/images/arrow-back.png',
              notificationImagePath: 'public/images/notification.png',
              leadingPressAction: () => onBackButton(),
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 10),
            margin: EdgeInsets.symmetric(
              horizontal: 30,
            ),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(color: Colors.grey, width: 0.5),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Spacer(),
                Row(
                  children: <Widget>[
                    Text("Debtors",
                        style: Theme.of(context)
                            .textTheme
                            .body2
                            .copyWith(fontWeight: FontWeight.w400)),
                    SizedBox(
                      width: 5,
                    ),
                    CustomSwitch(
                      value: isCreditors,
                      onClick: (value) {
                        if (this.mounted) {
                          setState(() {
                            isCreditors = value;
                          });
                        }
                        onLoanTypeSwitch();
                      },
                      padding: EdgeInsets.all(5),
                    ),
                  ],
                )
              ],
            ),
          ),
          Expanded(
              child: Stack(
            children: [
              if (isLoading)
                Container(
                  height: MediaQuery.of(context).size.height,
                  color: Color.fromRGBO(169, 169, 169, 0.1),
                  child: Center(
                    child: CustomProgressIndicator(
                      size: 45,
                      color: appColors.altProgressValueColor,
                    ),
                  ),
                ),
              Column(
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: TableCalendar(
                      calendarController: _calendarController,
                      initialSelectedDay: _selectedDay,
                      holidays: _events,
                      events: _events,
                      calendarStyle: CalendarStyle(
                        outsideDaysVisible: false,
                      ),
                      headerStyle: HeaderStyle(
                        centerHeaderTitle: true,
                        formatButtonVisible: false,
                        leftChevronIcon: Icon(
                          Icons.chevron_left,
                          color: GeneralProvider.isLightMode
                              ? Colors.black
                              : Colors.white,
                        ),
                        rightChevronIcon: Icon(
                          Icons.chevron_right,
                          color: GeneralProvider.isLightMode
                              ? Colors.black
                              : Colors.white,
                        ),
                      ),
                      builders: CalendarBuilders(
                        holidayDayBuilder: (context, date, _) {
                          return FadeTransition(
                            opacity: Tween(begin: 0.0, end: 1.0)
                                .animate(_animationController),
                            child: Container(
                              margin: const EdgeInsets.all(4.0),
                              padding:
                                  const EdgeInsets.only(top: 5.0, left: 6.0),
                              color: Colors.deepOrange[100],
                              width: 100,
                              height: 100,
                              child: Text(
                                '${date.day}',
                                style: TextStyle().copyWith(fontSize: 16.0),
                              ),
                            ),
                          );
                        },
                        selectedDayBuilder: (context, date, _) {
                          return FadeTransition(
                            opacity: Tween(begin: 0.0, end: 1.0)
                                .animate(_animationController),
                            child: Container(
                              margin: const EdgeInsets.all(4.0),
                              padding:
                                  const EdgeInsets.only(top: 5.0, left: 6.0),
                              color: Colors.deepOrange[300],
                              width: 100,
                              height: 100,
                              child: Text(
                                '${date.day}',
                                style: TextStyle().copyWith(fontSize: 16.0),
                              ),
                            ),
                          );
                        },
                        todayDayBuilder: (context, date, _) {
                          return Container(
                            margin: const EdgeInsets.all(4.0),
                            padding: const EdgeInsets.only(top: 5.0, left: 6.0),
                            color: Colors.transparent,
                            width: 100,
                            height: 100,
                            child: Center(
                              child: Text(
                                '${date.day}',
                                style: TextStyle().copyWith(),
                              ),
                            ),
                          );
                        },
                        markersBuilder: (context, date, events, holidays) {
                          final children = <Widget>[];

                          if (holidays.isNotEmpty) {
                            children.add(
                              Positioned(
                                right: 1,
                                bottom: 1,
                                child: _buildLoansMarker(events),
                              ),
                            );
                          }

                          return children;
                        },
                      ),
                      onDaySelected: (date, events, holidays) {
                        _onDaySelected(date, events, holidays);
                        _animationController.forward(from: 0.0);
                      },
                      onVisibleDaysChanged: (date, events, holidays) {
                        setState(() {
                          _currentCalenderDay = date;
                          _items = [];
                        });
                        getCalenderLoans(calenderMonth: date);
                      },
                    ),
                  ),
                  Expanded(
                    child: _items.length == 0
                        ? Center(
                            child: Text("You have no loans at selected date"),
                          )
                        : ListView.builder(
                            controller: _scrollController,
                            shrinkWrap: true,
                            itemCount: _items.length,
                            itemBuilder: (ctx, index) =>
                                HelperFunctions.buildLoanTile(
                              context: context,
                              contact: isCreditors
                                  ? _items[index].debtor
                                  : _items[index].creditor,
                              amount: _items[index].amount,
                              dueDate: _items[index].date,
                              itemId: _items[index].id,
                              selectedItemId: _selectedItemId,
                              onOutlineClicked: () => onOutlineClicked(index),
                              action: null,
                            ),
                          ),
                  )
                ],
              )
            ],
          )),
        ],
      ),
    );
  }

  Widget _buildLoansMarker(List events) {
    return Container(
      width: 16.0,
      height: 16.0,
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        color: Colors.brown[500],
      ),
      child: Center(
        child: Text(
          '${events.length}',
          style: TextStyle().copyWith(
            color: Colors.white,
            fontSize: 12.0,
          ),
        ),
      ),
    );
  }
}
