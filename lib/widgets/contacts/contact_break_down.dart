import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:peer_score/providers/contact.dart';
import 'package:peer_score/providers/expected_payments_provider.dart';
import 'package:peer_score/providers/loans-provider.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/providers/user-provider.dart';
import 'package:peer_score/screens/profile_screen.dart';
import 'package:peer_score/utils/animatedcount.dart';
import 'package:peer_score/utils/custom_raised_button.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:peer_score/utils/main_nav_bar.dart';
import 'package:peer_score/utils/modal_actions.dart';
import 'package:peer_score/utils/progress_indicator.dart';
import 'package:peer_score/utils/snackbar.dart';
import 'package:provider/provider.dart';

class ContactBreakDown extends StatefulWidget {
  final Function switchTo;
  final Function navBarLeadingFn;
  ContactBreakDown({this.switchTo, this.navBarLeadingFn});

  @override
  _ContactBreakDownState createState() => _ContactBreakDownState();
}

class _ContactBreakDownState extends State<ContactBreakDown>
    with TickerProviderStateMixin {
  bool isLightMode = GeneralProvider.isLightMode;
  bool isLoading = false;
  bool detailsOpened = true;
  Contact contact;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  List<Loan> openLoans = [];
  Map<String, dynamic> summary = {
    "loan_count": 0,
    "debt_count": 0,
    "total_loan": 0.0,
    "total_debt": 0.0
  };
  Map<String, dynamic> userData = {
    "first_name": "",
    "profile_picture": "",
    "compliance": 0
  };
  AnimationController _detailsPageAnimationController;
  Animation _detailsPageAnimation;
  AnimationController _openLoansAnimationController;
  Animation _openLoansAnimation;

  @override
  void initState() {
    super.initState();
    BackButtonInterceptor.add(myInterceptor);
    contact = LoansProvider.selectedContact;
    Provider.of<UserProvider>(context, listen: false).userData.then((data) {
      if (this.mounted) {
        setState(() {
          userData = data;
        });
        if (contact.id != null) {
          getFinaceSummary();
        }
        getOpenLoansList();
      }
    });
    _detailsPageAnimationController =
        AnimationController(duration: Duration(milliseconds: 200), vsync: this);
    _detailsPageAnimation =
        IntTween(begin: 5, end: 0).animate(_detailsPageAnimationController);
    _openLoansAnimationController =
        AnimationController(duration: Duration(milliseconds: 200), vsync: this);
    _openLoansAnimation =
        IntTween(begin: 0, end: 5).animate(_openLoansAnimationController);

    _detailsPageAnimationController.addListener(() {
      setState(() {});
    });
    _openLoansAnimationController.addListener(() {
      setState(() {});
    });
  }

  Future<void> getFinaceSummary() async {
    if (this.mounted) {
      setState(() {
        isLoading = true;
      });
    }
    var summaryResponse;
    summaryResponse = await Provider.of<LoansProvider>(context, listen: false)
        .getUserFinanceSummary(
      providerId: contact.id,
      recipientPhone: HelperFunctions.serializePhoneNumber(
        phoneNumber: contact.phoneNumber,
      ),
    );

    if (summaryResponse["status"] == 200 || summaryResponse["status"] == 201) {
      if (this.mounted) {
        setState(() {
          summary = summaryResponse["data"];
          isLoading = false;
        });
      }
    } else {
      if (this.mounted) {
        setState(() {
          isLoading = false;
          CustomSnackBar.snackBar(
            title: "Something went wrong, please reload the page",
            scaffoldState: _scaffoldKey,
            context: context,
          );
        });
      }
    }
  }

  Future<void> getOpenLoansList() async {
    if (this.mounted) {
      setState(() {
        isLoading = true;
      });
    }
    var summaryResponse;

    if (contact.id == null) {
      summaryResponse = await Provider.of<LoansProvider>(context, listen: false)
          .getUnregisteredContactOpenLoans(
        recipientPhone: HelperFunctions.serializePhoneNumber(
          phoneNumber: contact.phoneNumber,
        ),
      );
    } else {
      summaryResponse = await Provider.of<LoansProvider>(context, listen: false)
          .getContactOpenLoans(recipientId: contact.id);
    }

    if (summaryResponse["status"] == 200 || summaryResponse["status"] == 201) {
      if (this.mounted) {
        setState(() {
          isLoading = false;
          openLoans = Provider.of<LoansProvider>(context, listen: false)
              .contactOpenLoans;
          if (contact.id == null) {
            summary["debt_count"] =
                Provider.of<LoansProvider>(context, listen: false)
                    .unregisteredContactDebtsCount;
          }
        });
      }
    } else {
      if (this.mounted) {
        setState(() {
          isLoading = false;
          CustomSnackBar.snackBar(
            title: "Something went wrong, please reload the page",
            scaffoldState: _scaffoldKey,
            context: context,
          );
        });
      }
    }
  }

  bool myInterceptor(bool stopDefaultButtonEvent) {
    if (stopDefaultButtonEvent) return false;
    widget.navBarLeadingFn();
    return true;
  }

  void gotoProfilePage() {
    Navigator.of(context).pushReplacementNamed(
      ProfileScreen.routeName,
      arguments: {
        "isSlideBackward": false,
      },
    );
  }

  void toggleOpenLoans({bool isOpenDetails = false}) {
    
    setState(() {
      detailsOpened = isOpenDetails;
    });
    if (isOpenDetails) {
      _detailsPageAnimationController.reverse();
      _openLoansAnimationController.reverse();
    } else {
      _detailsPageAnimationController.forward();
      _openLoansAnimationController.forward();
    }
  }

  Widget _buildContactFinanceSummary(
      {String title, int loanCount, Color bgColor}) {
    return Container(
      padding: EdgeInsets.only(left: 15, right: 15, top: 10, bottom: 15),
      decoration:
          BoxDecoration(color: bgColor, borderRadius: BorderRadius.circular(4)),
      child: Column(
        children: <Widget>[
          FittedBox(
            child: Text(
              title,
              style: Theme.of(context)
                  .textTheme
                  .body1
                  .copyWith(fontWeight: FontWeight.w600, color: Colors.white),
            ),
          ),
          // Spacer(),
          Expanded(
            child: Center(
              child: AnimatedCount(
                count: num.parse(loanCount.toString()),
                style: Theme.of(context).textTheme.headline.copyWith(
                    fontSize: 24,
                    fontWeight: FontWeight.w600,
                    color: Colors.white),
                duration: Duration(milliseconds: 1000),
              ),
            ),
          ),
          // Text(
          //   "15",
          //   style: Theme.of(context)
          //       .textTheme
          //       .body2
          //       .copyWith(
          //           fontSize: 24,
          //           fontWeight: FontWeight.w600,
          //           color: Colors.white),
          // ),
        ],
      ),
    );
  }

  Widget _buildOpenLoansTile(
      {String amount, DateTime dueDate, String privacy}) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30),
      padding: EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.grey, width: 0.5))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                HelperFunctions.getFormattedAmount(amount: amount),
                style: Theme.of(context).textTheme.body2.copyWith(
                      fontWeight: FontWeight.w600,
                    ),
              ),
              Spacer(),
              Text(
                toBeginningOfSentenceCase(privacy),
                style: Theme.of(context).textTheme.body1.copyWith(
                      fontWeight: FontWeight.w600,
                    ),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            DateFormat('dd-MM-y').format(dueDate),
          )
        ],
      ),
    );
  }

  void onInviteClick() {
    ModalActions.sendMessage(
      context: context,
      title: "Invite contact",
      contentTitle: "Send an invitation to this contact",
      smsMessage:
          "Hello, ${userData["first_name"]} is inviting you to register on PeerScore, kindly register here bit.ly/2E9PZOm",
      whatsappMessage:
          "Hello, ${userData["first_name"]} is inviting you to register on PeerScore, kindly register here bit.ly/2E9PZOm",
      recipientPhone: HelperFunctions.serializePhoneNumber(
          phoneNumber: contact.phoneNumber),
      successMessage: "Invitation sent successfully",
      hasWhatsapp: true,
      isLoan: false
    );
  }

  @override
  void dispose() {
    BackButtonInterceptor.remove(myInterceptor);
    _detailsPageAnimationController.dispose();
    _openLoansAnimationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Container(
      height: deviceSize.height -
          MediaQuery.of(context).padding.top -
          Provider.of<GeneralProvider>(context, listen: false)
              .getBottomNavHeight,
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(vertical: 30),
            child: MainNavBar(
              title: "Contact Breakdown",
              leadingImagePath: 'public/images/arrow-back.png',
              notificationImagePath: 'public/images/notification.png',
              leadingPressAction: widget.navBarLeadingFn,
            ),
          ),
          // SizedBox(height: space_before_picture),
          Expanded(
            flex: _detailsPageAnimation.value,
            child: Container(
              height: 0,
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: Column(
                children: <Widget>[
                  // if (contact.id != null)
                  Expanded(
                    flex: 4,
                    child: Column(
                      children: <Widget>[
                        if (detailsOpened && _detailsPageAnimation.value > 4)
                          Expanded(
                            flex: 6,
                            child: Column(
                              children: <Widget>[
                                // Spacer(
                                //   flex: 1,
                                // ),
                                Expanded(
                                  // flex: 3,
                                  child: Container(
                                    alignment: Alignment.bottomCenter,
                                    constraints: BoxConstraints(
                                      minHeight: 50,
                                      minWidth: 50,
                                      maxHeight: 100,
                                      maxWidth: 100,
                                    ),
                                    child: AspectRatio(
                                      aspectRatio: 1,
                                      child: ClipOval(
                                        child: FadeInImage(
                                          placeholder: AssetImage(
                                              "public/images/profile-picture-placeholder.png"),
                                          image: NetworkImage(contact.id == null
                                              ? ""
                                              : contact.profilePicture),
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 15),
                                Text(
                                  HelperFunctions.getContactFullName(
                                      contact: contact),
                                  style: Theme.of(context)
                                      .textTheme
                                      .body2
                                      .copyWith(
                                        fontWeight: FontWeight.w600,
                                        // color: Color(0xFF646464),
                                      ),
                                ),
                                Text(
                                  contact.phoneNumber,
                                  style: Theme.of(context)
                                      .textTheme
                                      .body1
                                      .copyWith(
                                          fontWeight: FontWeight.w600,
                                          color: Color(0xFF646464)),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      "Loan Compliance - ",
                                      style: Theme.of(context)
                                          .textTheme
                                          .body2
                                          .copyWith(fontSize: 16),
                                    ),
                                    if (num.tryParse(contact.compliance) !=
                                        null)
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          AnimatedCount(
                                            count:
                                                num.parse(contact.compliance),
                                            style: Theme.of(context)
                                                .textTheme
                                                .body2
                                                .copyWith(fontSize: 16
                                                    // fontSize: 24,
                                                    // fontWeight: FontWeight.w600,
                                                    ),
                                            duration:
                                                Duration(milliseconds: 1000),
                                          ),
                                          SizedBox(width: 5),
                                          Text(
                                            "%",
                                            style: Theme.of(context)
                                                .textTheme
                                                .body2
                                                .copyWith(fontSize: 16
                                                    // fontWeight: FontWeight.w600,
                                                    ),
                                          )
                                        ],
                                      ),
                                      if (num.tryParse(contact.compliance) ==
                                        null)
                                        Text(
                                            contact.compliance,
                                            style: Theme.of(context)
                                                .textTheme
                                                .body2
                                                .copyWith(fontSize: 16
                                                    // fontWeight: FontWeight.w600,
                                                    ),
                                          )
                                  ],
                                ),
                              ],
                            ),
                          ),
                        SizedBox(
                          height: 15,
                        ),
                        Expanded(
                          flex: 2,
                          child: Container(
                            // height: 108,
                            child: Stack(
                              children: <Widget>[
                                if(contact.id != null)Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: HelperFunctions.buildSummaryCard(
                                        context: context,
                                        title: "Active Lendings",
                                        number: summary["loan_count"],
                                        borderColor:
                                            Theme.of(context).primaryColor,
                                        onTap: null,
                                      ),
                                    ),
                                    SizedBox(width: 15),
                                    Expanded(
                                      child: HelperFunctions.buildSummaryCard(
                                        context: context,
                                        title: "Active Debts",
                                        number: summary["debt_count"],
                                        borderColor: Color(0xffE78200),
                                        onTap: null,
                                      ),
                                    ),
                                  ],
                                ),
                                if(contact.id == null) Row(
                                  children: <Widget>[
                                    Spacer(),
                                    Expanded(
                                      flex: 3,
                                      child: HelperFunctions.buildSummaryCard(
                                        context: context,
                                        title: "Active Debts",
                                        number: summary["debt_count"],
                                        borderColor: Color(0xffE78200),
                                        onTap: null,
                                      ),
                                    ),
                                    Spacer(),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  // if (contact.id == null)
                  //   Expanded(
                  //     flex: 4,
                  //     child: Column(
                  //       crossAxisAlignment: CrossAxisAlignment.center,
                  //       children: <Widget>[
                  //         SizedBox(
                  //           height: 127,
                  //           width: 127,
                  //           child: Image.asset(
                  //             "public/images/not-found.png",
                  //             color: Theme.of(context).primaryColor,
                  //           ),
                  //         ),
                  //         Text(
                  //           "Phone number not found on PeerScore",
                  //           style: Theme.of(context).textTheme.body2.copyWith(
                  //               // fontWeight: FontWeight.w600,
                  //               ),
                  //         ),
                  //         SizedBox(
                  //           height: 15,
                  //         ),
                  //         Text(
                  //           "You can still request loans or payments from them, options of notifying them will be provided during the process.",
                  //           textAlign: TextAlign.center,
                  //           style: Theme.of(context).textTheme.body1.copyWith(
                  //               // fontWeight: FontWeight.w600,
                  //               ),
                  //         ),
                  //       ],
                  //     ),
                  //   ),
                  Expanded(
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: FittedBox(
                            child: CustomRaisedButton(
                              title: "Request Payment",
                              bgColor: Theme.of(context).primaryColor,
                              // bgColor: GeneralProvider.isLightMode
                              //     ? Theme.of(context).primaryColor
                              //     : Colors.white,
                              // color: GeneralProvider.isLightMode
                              //     ? Colors.white
                              //     : Theme.of(context).primaryColor,
                              onTapAction: () {
                                if (userData["first_name"] == null) {
                                  return ModalActions.showMoreInfo(
                                    context: context,
                                    title: "Update profile",
                                    content:
                                        "Please update your profile to create a request.",
                                    onOk: () => gotoProfilePage(),
                                  );
                                }
                                Provider.of<LoansProvider>(context,
                                        listen: false)
                                    .setContact(contact: contact);
                                widget.switchTo("payment");
                              },
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 25,
                        ),
                        Expanded(
                          child: FittedBox(
                            child: CustomRaisedButton(
                              title: "Request Loan",
                              bgColor: Theme.of(context).accentColor,
                              // bgColor: GeneralProvider.isLightMode
                              //     ? Color(0xffE78200)
                              //     : Colors.white,
                              // color: GeneralProvider.isLightMode
                              //     ? Colors.white
                              //     : Color(0xffE78200),
                              onTapAction: () {
                                if (userData["first_name"] == null) {
                                  return ModalActions.showMoreInfo(
                                    context: context,
                                    title: "Update profile",
                                    content:
                                        "Please update your profile to create a request.",
                                    onOk: () => gotoProfilePage(),
                                  );
                                }
                                Provider.of<LoansProvider>(context,
                                        listen: false)
                                    .setContact(contact: contact);
                                widget.switchTo("loan");
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  if (!(openLoans.length > 0))
                    Spacer(
                      flex: 1,
                    ),
                  if (openLoans.length > 0)
                    Expanded(
                      flex: 1,
                      child: Stack(
                        alignment: AlignmentDirectional.center,
                        children: <Widget>[
                          CustomRaisedButton(
                            title: " ",
                            width: double.infinity,
                            bgColor: Theme.of(context).primaryColor,
                            height: 43,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              left: 15,
                              // top: 1,
                              right: 1,

                              // bottom: 1,
                            ),
                            child: CustomRaisedButton(
                              title: "Click to view open loan records",
                              width: double.infinity,
                              height: 41,
                              fontSize: 15,
                              color: Colors.black,
                              bgColor: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                              onTapAction: () => toggleOpenLoans(),
                            ),
                          )
                        ],
                      ),
                    ),
                  if (contact.id == null)
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 5),
                        child: CustomRaisedButton(
                          title: "Invite to PeerScore",
                          fontSize: 13,
                          bgColor: Colors.transparent,
                          color: Theme.of(context).accentColor,
                          fontWeight: FontWeight.bold,
                          onTapAction: () => onInviteClick(),
                        ),
                      ),
                    )
                ],
              ),
            ),
          ),
          // if (contact.id != null)
          Expanded(
            flex: _openLoansAnimation.value,
            child: Container(
              height: _openLoansAnimation.value + 0.0,
              child: Column(
                children: <Widget>[
                  Container(
                    // height: 0,
                    padding: EdgeInsets.symmetric(horizontal: 30),
                    decoration: BoxDecoration(
                      color: GeneralProvider.isLightMode ? Colors.white : null,
                      boxShadow: !isLightMode
                          ? null
                          : [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.12),
                                spreadRadius: 0,
                                blurRadius: 15,
                                offset: Offset(0, 20),
                              )
                            ],
                      border: GeneralProvider.isLightMode
                          ? null
                          : Border(
                              bottom: BorderSide(
                                width: 0.2,
                                color: Colors.white,
                              ),
                            ),
                    ),
                    child: ListTile(
                      onTap: () => toggleOpenLoans(isOpenDetails: true),
                      contentPadding: EdgeInsets.zero,
                      isThreeLine: true,
                      leading: Container(
                        height: 50,
                        width: 50,
                        child: AspectRatio(
                          aspectRatio: 1,
                          child: ClipOval(
                            child: FadeInImage(
                              placeholder: AssetImage(
                                  "public/images/profile-picture-placeholder.png"),
                              image: NetworkImage(contact.id == null
                                  ? ""
                                  : contact.profilePicture),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                      title: Text(
                        HelperFunctions.getContactFullName(contact: contact),
                        style: Theme.of(context).textTheme.body2.copyWith(
                              fontWeight: FontWeight.w600,
                              // color: Color(0xFF646464),
                            ),
                      ),
                      subtitle: Text(
                        contact.phoneNumber,
                        style: Theme.of(context).textTheme.body1.copyWith(
                            fontWeight: FontWeight.w600,
                            color: Color(0xFF646464)),
                      ),
                      trailing: num.tryParse(contact.compliance) != null
                          ? Row(
                              // mainAxisAlignment: MainAxisAlignment.center,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                AnimatedCount(
                                  count: num.parse(contact.compliance),
                                  style: Theme.of(context)
                                      .textTheme
                                      .body2
                                      .copyWith(fontSize: 16
                                          // fontSize: 24,
                                          // fontWeight: FontWeight.w600,
                                          ),
                                  duration: Duration(milliseconds: 1000),
                                ),
                                SizedBox(width: 5),
                                Text(
                                  "%",
                                  style: Theme.of(context)
                                      .textTheme
                                      .body2
                                      .copyWith(fontSize: 16
                                          // fontWeight: FontWeight.w600,
                                          ),
                                )
                              ],
                            )
                          : Container(
                              child: Text(""),
                            ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Expanded(
                    child: ListView.builder(
                      itemCount: openLoans.length,
                      itemBuilder: (_, index) => _buildOpenLoansTile(
                          amount: openLoans[index].amount,
                          dueDate: openLoans[index].date,
                          privacy: openLoans[index].privacy),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
