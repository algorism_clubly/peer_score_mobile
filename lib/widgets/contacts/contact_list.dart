import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:contacts_service/contacts_service.dart' hide Contact;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:flutter_contact/contacts.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:peer_score/providers/colors.dart';
import 'package:peer_score/providers/contact.dart' as UserContact;
import 'package:peer_score/providers/debts_provider.dart';
import 'package:peer_score/providers/expected_payments_provider.dart';
import 'package:peer_score/providers/loans-provider.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/providers/user-provider.dart';
import 'package:peer_score/utils/custom_raised_button.dart';
import 'package:peer_score/utils/custom_switch.dart';
import 'package:peer_score/utils/enums.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:peer_score/utils/main_nav_bar.dart';
import 'package:peer_score/utils/modal_actions.dart';
import 'package:peer_score/utils/progress_indicator.dart';
import 'package:peer_score/utils/snackbar.dart';
import 'package:peer_score/utils/text_form_field.dart';
import 'package:peer_score/widgets/contacts/contact_util.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';

class ContactListPage extends StatefulWidget {
  final Function switchTo;
  final Function navBarLeadingFn;
  final String header;
  final String subHeader;
  final String selectMultipleActionText;
  final bool selectMultipleMode;
  final List<String> selectedContactIds;
  ContactListPage(
      {this.switchTo,
      this.navBarLeadingFn,
      this.header,
      this.subHeader,
      this.selectMultipleMode = false,
      this.selectMultipleActionText,
      this.selectedContactIds});

  @override
  _ContactListPageState createState() => _ContactListPageState();
}

class _ContactListPageState extends State<ContactListPage> {
  List<String> contactPhones = [];
  List<String> peerScorePhones = [];
  List<UserContact.Contact> contactList = [];
  List<UserContact.Contact> contactListCopy = [];
  List<UserContact.Contact> currentFilterdList = [];
  GlobalKey<ScaffoldState> _scaffoldKey;
  bool isLoading = false;
  bool isSyncing = false;
  bool pullRefresh = false;
  bool noResultFound = false;
  bool permissionDenied = false;
  Map<String, dynamic> userData = {"first_name": ""};
  bool autoSyncValue = false;
  List _selectedContactIds = [];
  String optionalFilterMessage;
  List<Map<String, dynamic>> filterOptions = [
    {
      "type": ContactlistFilterOptions.all,
      "title": "All",
    },
    {
      "type": ContactlistFilterOptions.peerScoreMembers,
      "title": "PeerScore members",
    },
    {
      "type": ContactlistFilterOptions.nonPeerScoreMembers,
      "title": "Non PeerScore members",
    },
  ];
  int _selectedFilterValue = 0;
  List<Map<String, dynamic>> sortOptions = [
    {
      "type": ContactlistSortOptions.complianceAsc,
      "title": "Lowest(Compliance)",
    },
    {
      "type": ContactlistSortOptions.complianceDesc,
      "title": "Highest(Compliance)",
    }
  ];
  int _selectedSortValue = 1;

  FocusNode searchFocusNode;
  @override
  void initState() {
    super.initState();

    getAutoSyncValue();
    searchFocusNode = FocusNode();
    GeneralProvider.currentFocusedNode = searchFocusNode;
    BackButtonInterceptor.add(myInterceptor);
    _scaffoldKey =
        Provider.of<GeneralProvider>(context, listen: false).getAppScaffoldKey;
    if (this.mounted) {
      setState(() {
        isLoading = true;
      });
    }
    Provider.of<UserProvider>(context, listen: false).userData.then((data) {
      if (this.mounted) {
        setState(() {
          userData = data;
        });
      }
      refreshContacts();
    });

    if (widget.selectMultipleMode) {
      _selectedContactIds = widget.selectedContactIds;
      //remove "all" and "non peerscore" filter options if selecting multiple
      filterOptions
          .retainWhere((filter) => filter["title"] == "PeerScore members");
      setState(() {});
    }
  }

  void getAutoSyncValue() async {
    autoSyncValue = await HelperFunctions.getAutoSyncValue();
    setState(() {});
  }

  void onBackButtonClicked() {
    // if (widget.selectMultipleMode) {
    widget.navBarLeadingFn();
    // } else {
    // GeneralProvider.switchMainPage(index: 0);
    // }
  }

  bool myInterceptor(bool stopDefaultButtonEvent) {
    if (stopDefaultButtonEvent) return false;
    onBackButtonClicked();
    return true;
  }

  @override
  void dispose() {
    BackButtonInterceptor.remove(myInterceptor);
    super.dispose();
  }

  refreshContacts() async {
    if (this.mounted) {
      setState(() {
        isLoading = true;
        _selectedFilterValue = 0;
        _selectedSortValue = 1;
        permissionDenied = false;
      });
    }

    List<UserContact.Contact> savedContactList =
        await PhoneContacts.getSavedContactList();
    setState(() {
      contactList = savedContactList;
      contactListCopy = [...contactList];
      removeSpecificContactsOnMultiple();
      if (contactList.length > 0) {
        isLoading = false;
        isSyncing = true;
      }
    });

    Map<String, dynamic> contactsResponse =
        await PhoneContacts.getPhoneContacts(
            context: context,
            userId: await userData["id"],
            isAutoSync: autoSyncValue);
    if (contactsResponse["status"]) {
      if (this.mounted) {
        setState(() {
          contactList = contactsResponse["contactList"];
          removeSpecificContactsOnMultiple();
          contactListCopy = [...contactList];
          isLoading = false;
          isSyncing = false;
        });
        //handle for permanently denied
        _handleInvalidPermissions(contactsResponse["permissionStatus"]);
      }
    } else if (contactsResponse["status"] == false) {
      if (this.mounted) {
        setState(() {
          isLoading = false;
          isSyncing = false;
          // permissionDenied = true;
          CustomSnackBar.snackBar(
            title: "Something went wrong, please try again.",
            scaffoldState: _scaffoldKey,
            context: context,
          );
        });
      }
    }

    // else {
    //   _handleInvalidPermissions(contactsResponse["permissionStatus"]);
    // }
  }

  void _handleInvalidPermissions(PermissionStatus permissionStatus) {
    if (this.mounted && autoSyncValue == true) {
      setState(() {
        isLoading = false;
        isSyncing = false;
        permissionDenied = false;
        if (permissionStatus == PermissionStatus.permanentlyDenied) {
          ModalActions.normalConfirmationDialog(
            title: "Contact permission denied",
            isColumnActions: true,
            firstActionText: "Grant permission",
            secondActionText: "Continue without permission",
            context: context,
            content: "Contact permission has been denied for auto sync feature",
            onConfirm: () {
              setState(() {
                autoSyncValue = false;
                HelperFunctions.setAutoSync(value: false);
              });
            },
            onUnconfirm: () => openAppSettings(),
          );
        }
      });
    }
  }

  void removeSpecificContactsOnMultiple() {
    if (widget.selectMultipleMode) {
      //removing both the debtor and creditor from witnesses list
      String creditorId = DebtsProviver.selectedDebt != null
          ? DebtsProviver.selectedDebt.creditor.id
          : null;
      String debtorId = ExpectedPaymentsProvider.selectedLoan != null
          ? ExpectedPaymentsProvider.selectedLoan.debtor.id
          : null;
      contactList.removeWhere((contact) => contact.id == null);
      contactList.removeWhere((contact) => contact.id == creditorId);
      contactList.removeWhere((contact) => contact.id == debtorId);
    }
  }

  void selectContact(String id) {
    if (this.mounted) {
      setState(() {
        if (_selectedContactIds.contains(id)) {
          _selectedContactIds.removeWhere((item) => item == id);
        } else {
          _selectedContactIds.add(id);
        }
      });
    }
  }

  List getSelectedContacts() {
    List<UserContact.Contact> selectedContacts = [];
    _selectedContactIds.forEach((id) {
      selectedContacts
          .add(contactList.firstWhere((contact) => contact.id == id));
    });

    return selectedContacts;

    // List<UserContact.Contact> selectedContacts = [];
    // _selectedContactIds.forEach(
    //   (index) {
    //     var contact = contactList[index];
    //     selectedContacts.add(contact);
    //   },
    // );
    // return selectedContacts;
  }

  bool isIndexSelected(String id) {
    return _selectedContactIds.contains(id);
  }

  Widget buildContactListTile({BuildContext context, int index}) {
    var contactid = contactListCopy[index].id;
    var appColors = Provider.of<AppColorsProvider>(context, listen: false);
    return InkWell(
      onTap: () {
        if (widget.selectMultipleMode) {
          selectContact(contactid);
        } else {
          
          Provider.of<LoansProvider>(context, listen: false)
              .setContact(contact: contactListCopy[index]);
          
          widget.switchTo();
        }
      },
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(
                horizontal: isIndexSelected(contactid) ? 5 : 0),
            decoration: BoxDecoration(
              color: isIndexSelected(contactid)
                  ? appColors.contactListSelectedHighlight
                  : Colors.transparent,
              borderRadius: BorderRadius.circular(6),
            ),
            margin: EdgeInsets.only(bottom: 3),
            child: ListTile(
              contentPadding: EdgeInsets.zero,
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    HelperFunctions.getContactFullName(
                        contact: contactListCopy[index]),
                    style: Theme.of(context).textTheme.body2.copyWith(
                          fontWeight: FontWeight.normal,
                          height: 1.36,
                          color: isIndexSelected(contactid)
                              ? Colors.white
                              : appColors.contactNameColor,
                        ),
                  ),
                  //show the compliance of the contact if they are registered
                  Text("${contactListCopy[index].compliance}",
                      style: Theme.of(context).textTheme.body1.copyWith(
                            fontWeight: FontWeight.bold,
                            height: 1.33,
                            color: isIndexSelected(contactid)
                                ? Colors.white
                                : appColors.contactComplianceColor,
                          ))
                ],
              ),
              subtitle: Row(
                children: <Widget>[
                  Text(
                    contactListCopy[index].phoneNumber,
                    style: Theme.of(context).textTheme.body1.copyWith(
                          fontWeight: FontWeight.normal,
                          // height: 1.33,
                          color: isIndexSelected(contactid)
                              ? Colors.white
                              : appColors.contactPhoneColor,
                        ),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  if (contactListCopy[index].id != null)
                    FaIcon(
                      FontAwesomeIcons.solidStar,
                      size: 12,
                      color: isIndexSelected(contactid)
                          ? Colors.white
                          : appColors.contactBadgeColor,
                    )
                ],
              ),
            ),
          ),
          Divider(
            thickness: 1.2,
            height: 0.0,
            color: !GeneralProvider.isLightMode ? Colors.grey[850] : null,
          )
        ],
      ),
    );
  }

  void searchContactList({String value}) {
    List<UserContact.Contact> loanListToSearch =
        _selectedFilterValue == 0 ? contactList : currentFilterdList;
    if (loanListToSearch.length == 0) return;
    // setState(() {
    //   _selectedFilterValue = 0;
    //   _selectedSortValue = 1;
    //   optionalFilterMessage = null;
    // });
    final _contactList = HelperFunctions.searchContacts(
      searchValue: value,
      contactList: loanListToSearch,
    );
    if (this.mounted) {
      setState(() {
        contactListCopy = _contactList;
        _contactList.length == 0 ? noResultFound = true : noResultFound = false;
      });
    }
  }

  String getEmptyListText() {
    return noResultFound
        ? "No contact matches the search criteria"
        : widget.selectMultipleMode
            ? "You currently have no contact to use as a witness"
            : "You currently have no contacts";
  }

  void filterList({int index, BuildContext context}) async {
    if (contactList.length == 0) return;
    await PhoneContacts.filterContacts(
        type: filterOptions[index]["type"],
        contactList: contactList,
        sortType: sortOptions[_selectedSortValue]["type"],
        resultCb: (status, filteredList, message) {
          if (status) {
            
            
            if (this.mounted) {
              setState(() {
                _selectedFilterValue = index;
                optionalFilterMessage = message;
                contactListCopy = filteredList;
                currentFilterdList = filteredList;
                filteredList.length == 0
                    ? noResultFound = true
                    : noResultFound = false;
              });
            }
          }
        });
  }

  void sortList({int index}) async {
    if (contactList.length == 0) return;
    setState(() {
      _selectedSortValue = index;
    });
    final _sortedList = PhoneContacts.sortContactList(
      type: sortOptions[index]["type"],
      contactList: contactListCopy,
    );
    if (this.mounted) {
      setState(() {
        contactListCopy = _sortedList;
        _sortedList.length == 0 ? noResultFound = true : noResultFound = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final deiviceSize = MediaQuery.of(context).size;

    var appColors = Provider.of<AppColorsProvider>(context, listen: false);
    return Container(
      height: deiviceSize.height -
          MediaQuery.of(context).padding.top -
          Provider.of<GeneralProvider>(context, listen: false)
              .getBottomNavHeight,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(vertical: 30),
            child: MainNavBar(
              title: widget.header,
              leadingImagePath: widget.selectMultipleMode
                  ? 'public/images/close-page.png'
                  : 'public/images/arrow-back.png',
              leadingHeight: widget.selectMultipleMode ? 30 : null,
              leadingWidth: widget.selectMultipleMode ? 25 : null,
              notificationImagePath: widget.selectMultipleMode
                  ? null
                  : 'public/images/notification.png',
              // subNav: true,

              leadingPressAction: () => onBackButtonClicked(),
            ),
          ),
          Expanded(
            child: Stack(
              children: <Widget>[
                Container(
                  // padding: EdgeInsets.symmetric(horizontal: 30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      // Container(
                      //   margin: EdgeInsets.only(bottom: 25),
                      //   child: Text(
                      //     widget.header,
                      //     style: Theme.of(context).textTheme.body1.copyWith(
                      //           fontSize: 12,
                      //           fontWeight: FontWeight.w600,
                      //           height: 12 / 16,
                      //         ),
                      //   ),
                      // ),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 30),
                        margin: EdgeInsets.only(bottom: 10),
                        child: Row(
                          children: <Widget>[
                            if (isSyncing)
                              CustomProgressIndicator(
                                size: 10,
                                color: Theme.of(context).primaryColor,
                              ),
                            Spacer(),
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Text(
                                    "Sync contacts",
                                    style: Theme.of(context)
                                        .textTheme
                                        .body2
                                        .copyWith(
                                          fontWeight: FontWeight.w600,
                                        ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  CustomSwitch(
                                    value: autoSyncValue,
                                    // height: 15,
                                    onClick: (value) {
                                      setState(() {
                                        autoSyncValue = value;
                                        HelperFunctions.setAutoSync(
                                            value: value);
                                        if (value == true) refreshContacts();
                                      });
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 30),
                        child: CustomTextField(
                          filled: true,
                          iconPath: 'public/images/search.png',
                          hintText: "Search contact by name or phone",
                          onChanged: (value) => searchContactList(value: value),
                          focusNode: searchFocusNode,
                        ),
                      ),
                      HelperFunctions.buildFilterPanel(
                        context: context,
                        optionalFilterMessage: optionalFilterMessage,
                        filterList: filterList,
                        sortList: sortList,
                        filterOptions: filterOptions
                            .map((option) => option['title'])
                            .toList(),
                        sortOptions: sortOptions
                            .map((option) => option['title'])
                            .toList(),
                        selectedSortValue: _selectedSortValue,
                        selectedFilterValue: _selectedFilterValue,
                      ),
                      SizedBox(height: widget.subHeader == null ? 12 : 25),
                      if (widget.subHeader != null)
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 30),
                          child: Text(
                            widget.subHeader,
                            style: Theme.of(context).textTheme.body1.copyWith(
                                  // fontSize: 10,
                                  fontWeight: FontWeight.bold,
                                  // height: 0.7,
                                ),
                          ),
                        ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 30),
                          child: permissionDenied
                              ? Center(
                                  child: CustomRaisedButton(
                                    onTapAction: refreshContacts,
                                    title: "Import contacts",
                                  ),
                                )
                              : HelperFunctions.renderListItemsAndRefresh(
                                  context: context,
                                  pullRefresh: pullRefresh,
                                  isLoading: isLoading,
                                  refresh: () {
                                    if (this.mounted) {
                                      setState(() {
                                        pullRefresh = true;
                                      });
                                    }
                                    return refreshContacts();
                                  },
                                  itemsCount: contactListCopy.length,
                                  itemsBuilder: ListView.builder(
                                      shrinkWrap: true,
                                      itemCount: contactListCopy.length,
                                      itemBuilder: (ctx, index) =>
                                          buildContactListTile(
                                              context: ctx, index: index)),
                                  emptyListIconPath:
                                      "public/images/empty-lists/${noResultFound ? GeneralProvider.isLightMode ? "no-results" : "no-results-white" : "contact"}.png",
                                  emptyListText: getEmptyListText(),
                                ),
                        ),
                      ),
                      if (widget.selectMultipleMode &&
                          _selectedContactIds.length > 0)
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 30),
                          margin: EdgeInsets.symmetric(vertical: 30),
                          width: double.infinity,
                          child: CustomRaisedButton(
                            title:
                                "${widget.selectMultipleActionText} (${_selectedContactIds.length})",
                            // color: appColors.primaryButtonColorText,
                            // bgColor: appColors.primaryButtonColorBg,
                            onTapAction: () {
                              widget.switchTo(getSelectedContacts());
                            },
                          ),
                        ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
