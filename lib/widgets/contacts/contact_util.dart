import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:contacts_service/contacts_service.dart' hide Contact;
import 'package:flutter/services.dart';
import 'package:peer_score/utils/enums.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:peer_score/providers/contact.dart' as UserContact;
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PhoneContacts {
  static Future<PermissionStatus> _getContactPermission() async {
    PermissionStatus permission = await Permission.contacts.request();
    return permission;
  }

  static Future<Map<String, dynamic>> getPhoneContacts(
      {BuildContext context, String userId, bool isAutoSync}) async {
    List<Map<String, String>> contactObj = [];
    List<String> peerScorePhones = [];
    List<UserContact.Contact> contactList = [];
    PermissionStatus permissionStatus = await _getContactPermission();
    if (isAutoSync && (permissionStatus == PermissionStatus.granted)) {
      var contacts =
          (await ContactsService.getContacts(withThumbnails: false)).toList();
      
      
      contacts.forEach((contact) {
        if (contact.phones.toList().length != 0 &&
            contact.phones.toList()[0].value.length > 5) {
          // 
          String phoneNumber = HelperFunctions.serializePhoneNumber(
            phoneNumber: contact.phones.toList()[0].value,
          );
          if (phoneNumber.length == 14 &&
              phoneNumber.substring(0, 4) == "+234") {
            contactObj.add({
              "first_name": contact.displayName,
              "phone": phoneNumber,
            });
          }
        }
        // if (contact.phones.toList().length != 0 &&
        //     contact.phones.toList()[0].value.length > 5) {
        //   // 
        //   contactObj.add({
        //     "first_name": contact.displayName,
        //     "phone": HelperFunctions.serializePhoneNumber(
        //       phoneNumber: contact.phones.toList()[0].value,
        //     ),
        //   });
        // }
      });
    }

    final response =
        await Provider.of<UserContact.ContactProvider>(context, listen: false)
            .syncPhone(phoneNumbers: contactObj, userId: userId);

    if (response["status"] == 200 || response["status"] == 201) {
      
      List fetchedContactList = response["data"];
      List<String> fetchedContactPhones = [];
      fetchedContactList.forEach((contact) {
        if (userId != contact["id"]) {
          if (fetchedContactPhones.contains(contact["phone"])) return;
          contactList.add(
            UserContact.Contact(
              id: contact["id"],
              otherName: contact["other_name"],
              peerScore: contact["peer_score"].toString(),
              phoneNumber: HelperFunctions.deserializePhoneNumber(
                phoneNumber: contact["phone"],
              ),
              lastName: contact["last_name"],
              compliance: contact["compliance"].toString() == "Unavailable"
                  ? "Unavailable"
                  : contact["compliance"].toString(),
              email: contact["email"],
              firstName: contact["first_name"],
              profilePicture: contact["profile_picture"],
              isWhatsApp:
                  contact["whatsapp"] == null ? false : contact["whatsapp"],
            ),
          );
          //done to remove duplicate numbers, numbers that are registered already
          fetchedContactPhones.add(contact["phone"]);
        }
      });

      // sort by Highest(Compliance) by default
      List<UserContact.Contact> tempPeerScoreMembers = [];
      List<UserContact.Contact> tempNonPeerScoreMembers = [];
      tempPeerScoreMembers =
          contactList.where((contact) => contact.id != null).toList();
      tempNonPeerScoreMembers =
          contactList.where((contact) => contact.id == null).toList();

      tempPeerScoreMembers.sort((a, b) {
        var complainceA = a.compliance;
        var complainceB = b.compliance;
        return int.parse(complainceB).compareTo(
          int.parse(complainceA),
        );
      });

      tempNonPeerScoreMembers.sort((a, b) =>
          HelperFunctions.getContactFullName(contact: a)
              .compareTo(HelperFunctions.getContactFullName(contact: b)));

      contactList = [...tempPeerScoreMembers, ...tempNonPeerScoreMembers];
      saveNewUserContactList(contacts: contactList);
      return {
        "status": true,
        "contactList": contactList,
        "permissionStatus": permissionStatus
      };
    } else {
      return {"status": false, "permission": true};
    }
    // } else {
    //   return {
    //     "status": false,
    //     "permission": false,
    //     "permissionStatus": permissionStatus
    //   };
    // }
  }

  

  static List<UserContact.Contact> sortContactList(
      {dynamic type, List<UserContact.Contact> contactList}) {
    switch (type) {
      case ContactlistSortOptions.complianceDesc:

      List<UserContact.Contact> contactsWithCompliance = [];
        List<UserContact.Contact> contactsWithoutCompliance = [];
        contactsWithCompliance =
            contactList.where((contact) => contact.compliance != "Unavailable").toList();
        contactsWithoutCompliance =
            contactList.where((contact) => contact.compliance == "Unavailable").toList();

        contactsWithCompliance.sort((a, b) {
          var complainceA = a.compliance;
          var complainceB = b.compliance;

          return int.parse(complainceB).compareTo(
            int.parse(complainceA),
          );
        });

        contactList = [...contactsWithCompliance, ...contactsWithoutCompliance];
      
        // contactList.sort((a, b) {
        //   var complainceA = a.compliance == "Unavailable" ? '0' : a.compliance;
        //   var complainceB = b.compliance == "Unavailable" ? '0' : b.compliance;
        //   return int.parse(complainceB).compareTo(
        //     int.parse(complainceA),
        //   );
        // });


        break;
      case ContactlistSortOptions.complianceAsc:
        //first remove members and sort then join with normal contacts

        List<UserContact.Contact> contactsWithCompliance = [];
        List<UserContact.Contact> contactsWithoutCompliance = [];
        contactsWithCompliance =
            contactList.where((contact) => contact.compliance != "Unavailable").toList();
        contactsWithoutCompliance =
            contactList.where((contact) => contact.compliance == "Unavailable").toList();

        contactsWithCompliance.sort((a, b) {
          var complainceA = a.compliance;
          var complainceB = b.compliance;

          return int.parse(complainceA).compareTo(
            int.parse(complainceB),
          );
        });

        contactList = [...contactsWithCompliance, ...contactsWithoutCompliance];

        break;
      default:
    }

    return contactList;
  }

  static Future<List<UserContact.Contact>> filterContacts({
    dynamic type,
    dynamic sortType,
    List<UserContact.Contact> contactList,
    Function resultCb,
  }) {
    bool status = true;
    String message;
    List<UserContact.Contact> contactListCopy = [...contactList];

    switch (type) {
      case ContactlistFilterOptions.all:
        break;
      case ContactlistFilterOptions.peerScoreMembers:
        contactListCopy.removeWhere((contact) => contact.id == null);
        break;
      case ContactlistFilterOptions.nonPeerScoreMembers:
        contactListCopy.removeWhere((contact) => contact.id != null);
        break;
      default:
    }

    resultCb(
      status,
      sortContactList(type: sortType, contactList: contactListCopy),
      message,
    );
  }

  static void saveNewUserContactList(
      {List<UserContact.Contact> contacts}) async {
    final sharedPrefs = await SharedPreferences.getInstance();

    List<Map<String, dynamic>> contactListObject = [];
    contactListObject = contacts.map((contact) => contact.toMap()).toList();

    sharedPrefs.setString("contacts", json.encode(contactListObject));
  }

  static Future<List<UserContact.Contact>> getSavedContactList() async {
    final sharedPrefs = await SharedPreferences.getInstance();
    List<UserContact.Contact> contactList = [];
    var contactListData = sharedPrefs.containsKey("contacts")
        ? json.decode(sharedPrefs.get("contacts"))
        : [];
    contactListData.forEach(
      (contact) => contactList.add(UserContact.Contact(
        id: contact["id"],
        firstName: contact["firstName"],
        lastName: contact["lastName"],
        otherName: contact["otherName"],
        phoneNumber: contact["phoneNumber"],
        peerScore: contact["peerScore"],
        compliance: contact["compliance"],
        email: contact["email"],
        profilePicture: contact["profilePicture"],
        isWhatsApp: contact["isWhatsApp"],
      )),
    );

    return contactList;
  }
}
