import 'package:flutter/material.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/widgets/contacts/contact_break_down.dart';
import 'package:peer_score/widgets/contacts/contact_list.dart';
import 'package:peer_score/widgets/dashboard/loan_details_page.dart';
import 'package:peer_score/widgets/dashboard/message_page.dart';
import 'package:peer_score/widgets/dashboard/success_page.dart';

class ContactListStack extends StatefulWidget {
  @override
  _ContactListStackState createState() => _ContactListStackState();
}

class _ContactListStackState extends State<ContactListStack> {
  bool isRequestLoan = false;
  bool isRequestPayment = false;
  PageController _pageController = PageController();
  // int pageIndex = 0;
  @override
  void initState() {
    GeneralProvider.switchSubStackIndex = switchSubStackIndex;
    super.initState();
  }

  void switchSubStackIndex({int index = 0}) {
    _pageController.jumpToPage(index);
  }

  void _switchPage(int toPageNumber) {
    if (GeneralProvider.currentFocusedNode != null) {
      GeneralProvider.currentFocusedNode.unfocus();
    }
    _pageController.animateToPage(toPageNumber,
        duration: Duration(milliseconds: 200), curve: Curves.linear);
  }

  void _goBack(int toPageNumber) {
    if (GeneralProvider.currentFocusedNode != null) {
      GeneralProvider.currentFocusedNode.unfocus();
    }
    _pageController.jumpToPage(toPageNumber);
    // _pageController.animateToPage(toPageNumber,
    //     duration: Duration(milliseconds: 200), curve: Curves.linear);
  }

  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: _pageController,
      physics: NeverScrollableScrollPhysics(),
      children: <Widget>[
        ContactListPage(
          header: "Contacts",
          navBarLeadingFn: () => GeneralProvider.switchMainPage(index: 0),
          switchTo: () => _switchPage(1),
        ),
        ContactBreakDown(
          switchTo: (mode) {
            //
            if (this.mounted) {
              setState(() {
                if (mode == "payment") {
                  isRequestPayment = true;
                  isRequestLoan = false;
                } else if (mode == "loan") {
                  isRequestPayment = false;
                  isRequestLoan = true;
                }
              });
            }
            _switchPage(2);
          },
          navBarLeadingFn: () => _goBack(0),
        ),
        LoanDetailsPage(
          header: isRequestPayment ? "Request payment" : "Request loan",
          isRequestPayment: isRequestPayment ? true : false,
          switchTo: () => _switchPage(3),
          navBarLeadingFn: () => _goBack(1),
        ),
        MessagePage(
          isRequestPayment: false,
          navBarLeadingFn: () => _switchPage(2),
          switchTo: () => _switchPage(4),
        ),
        SuccessPage(
          isRequestPayment: false,
          navBarLeadingFn: () => _goBack(0),
          switchMainPage: (index) =>
              GeneralProvider.switchMainPage(index: index),
        )
      ],
    );
  }
}
