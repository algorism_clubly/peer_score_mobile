import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/providers/witness_provider.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:peer_score/utils/main_nav_bar.dart';
import 'package:peer_score/utils/progress_indicator.dart';
import 'package:peer_score/utils/snackbar.dart';
import 'package:provider/provider.dart';

class ClaimsList extends StatefulWidget {
  final Function navBarLeadingFn;
  final Function switchTo;
  final String header;
  ClaimsList({this.header, this.navBarLeadingFn, this.switchTo});
  @override
  _ClaimsListState createState() => _ClaimsListState();
}

class _ClaimsListState extends State<ClaimsList> {
  bool isLoading = false;
  bool pullRefresh = false;
  List<Claim> claimList = [];
  List<Claim> claimListCopy = [];
  GlobalKey<ScaffoldState> _scaffoldKey;

  @override
  void initState() {
    super.initState();
    BackButtonInterceptor.add(myInterceptor);
    _scaffoldKey =
        Provider.of<GeneralProvider>(context, listen: false).getAppScaffoldKey;

    getClaims();
  }

  bool myInterceptor(bool stopDefaultButtonEvent) {
    if (stopDefaultButtonEvent) return false;
    widget.navBarLeadingFn();
    return true;
  }

  @override
  void dispose() {
    BackButtonInterceptor.remove(myInterceptor);
    super.dispose();
  }

  Future<void> getClaims() async {
    if (this.mounted) {
      setState(() {
        isLoading = true;
      });
    }
    final listResponse =
        await Provider.of<WitnesProvider>(context, listen: false).getClaims();
    if (listResponse["status"] == 200 || listResponse["status"] == 201) {
      if (this.mounted) {
        setState(() {
          claimList = [
            ...Provider.of<WitnesProvider>(context, listen: false).claims
          ];
          claimListCopy = [...claimList];
          isLoading = false;
          pullRefresh = false;
        });
      }
    } else {
      if (this.mounted) {
        setState(() {
          isLoading = false;
          pullRefresh = false;
          CustomSnackBar.snackBar(
            title: "Something went wrong, please reload the page",
            scaffoldState: _scaffoldKey,
            context: context,
          );
        });
      }
    }
  }

  Widget buildContactListTile({BuildContext context, int index}) {
    return InkWell(
      onTap: () {
        WitnesProvider.selectedClaim = claimList[index];
        widget.switchTo();
      },
      child: Column(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              color: Colors.transparent,
            ),
            margin: EdgeInsets.only(bottom: 3),
            child: ListTile(
              contentPadding: EdgeInsets.zero,
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    HelperFunctions.getContactFullName(
                        contact: claimList[index].winessProvider),
                    style: Theme.of(context).textTheme.body2.copyWith(
                          fontWeight: FontWeight.normal,
                          height: 1.36,
                        ),
                  ),
                  Text(
                      HelperFunctions.getFormattedAmount(
                          amount: claimList[index].loan.amount),
                      style: Theme.of(context).textTheme.body1.copyWith(
                            fontWeight: FontWeight.bold,
                            height: 1.33,
                          ))
                ],
              ),
              subtitle: Row(
                children: <Widget>[
                  Text(
                    claimList[index].claimInitiator.phoneNumber,
                    style: Theme.of(context).textTheme.body1.copyWith(
                        fontWeight: FontWeight.normal,
                        // height: 1.33,
                        color: GeneralProvider.isLightMode
                            ? Color(0xFF797979)
                            : Colors.white70),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  if (claimList[index].claimInitiator.id != null)
                    
                    HelperFunctions.buildUserBadge(context: context)
                ],
              ),
            ),
          ),
          Divider(
            thickness: GeneralProvider.isLightMode?  1.2 : 1.5,
            height: 0.0,
            color: !GeneralProvider.isLightMode ? Colors.grey[850] : null,
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final deiviceSize = MediaQuery.of(context).size;
    return Container(
      height: deiviceSize.height -
          MediaQuery.of(context).padding.top -
          Provider.of<GeneralProvider>(context, listen: false)
              .getBottomNavHeight,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(vertical: 30),
            child: MainNavBar(
              title: widget.header,
              leadingImagePath: 'public/images/arrow-back.png',
              notificationImagePath: 'public/images/notification.png',
              // subNav: true,
              leadingPressAction: widget.navBarLeadingFn,
            ),
          ),
          Expanded(
            child: Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      // Container(
                      //   margin: EdgeInsets.only(bottom: 25),
                      //   child: Text(
                      //     widget.header,
                      //     style: Theme.of(context).textTheme.body1.copyWith(
                      //           // fontSize: 12,
                      //           fontWeight: FontWeight.w600,
                      //           // height: 12 / 16,
                      //         ),
                      //   ),
                      // ),
                      Expanded(
                        child: HelperFunctions.renderListItemsAndRefresh(
                            context: context,
                            pullRefresh: pullRefresh,
                            isLoading: isLoading,
                            refresh: () {
                              if (this.mounted) {
                                setState(() {
                                  pullRefresh = true;
                                });
                              }
                              return getClaims();
                            },
                            itemsCount: claimListCopy.length,
                            itemsBuilder: ListView.builder(
                              itemBuilder: (ctx, index) {
                                return buildContactListTile(
                                    context: ctx, index: index);
                              },
                              itemCount: claimList.length,
                            ),
                            emptyListIconPath:
                                "public/images/empty-lists/witness.png",
                            emptyListText:
                                "You currently have no witness invitation"),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
