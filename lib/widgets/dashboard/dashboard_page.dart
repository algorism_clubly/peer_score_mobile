import 'dart:io';

import 'package:feature_discovery/feature_discovery.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:peer_score/providers/colors.dart';
import 'package:peer_score/providers/loans-provider.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/providers/user-provider.dart';
import 'package:peer_score/providers/witness_provider.dart';
import 'package:peer_score/screens/profile_screen.dart';
import 'package:peer_score/utils/animatedcount.dart';
import 'package:peer_score/utils/custom_raised_button.dart';
import 'package:peer_score/utils/functions.dart';
import 'dart:math' as math;
import 'dart:ui' as ui;
import 'package:peer_score/utils/main_nav_bar.dart';
import 'package:peer_score/utils/modal_actions.dart';
import 'package:peer_score/utils/progress_indicator.dart';
import 'package:peer_score/utils/snackbar.dart';
import 'package:peer_score/widgets/contacts/contact_util.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';

class DashBoardPage extends StatefulWidget {
  final Function switchTo;
  DashBoardPage({this.switchTo});

  @override
  _DashBoardPageState createState() => _DashBoardPageState();
}

class _DashBoardPageState extends State<DashBoardPage>
    with TickerProviderStateMixin {
  bool isLightMode = GeneralProvider.isLightMode;
  bool isLoading = false;
  bool pullRefresh = false;
  int claimsLength = 0;
  double dashboardHeight = 0;
  double avartarArchFactor = 0;
  double psArchFactor = 0;
  GlobalKey<ScaffoldState> _scaffoldKey;

  GlobalKey<RefreshIndicatorState> refreshKey;
  Map<String, dynamic> userData = {
    "first_name": "",
    "profile_picture": "",
    "compliance": 0
  };
  Map<String, dynamic> updatedUserData = {"compliance": 0};
  Map<String, dynamic> summary = {
    "loan_count": 0,
    "debt_count": 0,
    "total_loan": 0.0,
    "total_debt": 0.0
  };
  AnimationController percentageAnimationController;
  AnimationController avartarArcAnimationController;
  @override
  void initState() {
    super.initState();
    refreshKey = GlobalKey<RefreshIndicatorState>();
    percentageAnimationController = AnimationController(vsync: this);
    avartarArcAnimationController = AnimationController(vsync: this);
    _scaffoldKey =
        Provider.of<GeneralProvider>(context, listen: false).getAppScaffoldKey;

    Provider.of<UserProvider>(context, listen: false).userData.then((data) {
      if (this.mounted) {
        setState(() {
          userData = data;
          refreshDashBoard();
        });
      }
    });

    SchedulerBinding.instance.addPostFrameCallback((_) {
      dashboardHeight = refreshKey.currentContext.size.height;
      setState(() {});
      //show feature discovery
      FeatureDiscovery.discoverFeatures(
        context,
        const <String>{
          // Feature ids for every feature that you want to showcase in order.
          'profile',
          'request_payment',
          'request_loan',
          "notification"
        },
      );

      if (GeneralProvider.isNewUser) {
        autoSyncContact();
      }
    });

    // getLoansList();
    // getDebtList();
    // getWitnessList();
  }

  void autoSyncContact() async {
    //set isnew value to false to prevent subsequest modal openings for same user
    GeneralProvider.isNewUser = false;
    ModalActions.normalConfirmationDialog(
      title: "Auto sync contacts",
      context: context,
      content:
          "Do you want to automatically sync contacts on this device to your account?",
      onConfirm: () async {
        //upload contacts straight here and check for permission to enable auto syncing
        Map<String, dynamic> contactsResponse =
            await PhoneContacts.getPhoneContacts(
          context: context,
          userId: await userData["id"],
          isAutoSync: true,
        );
        if (contactsResponse["status"]) {
          PermissionStatus permissionStatus;
          permissionStatus = contactsResponse["permissionStatus"];
          if (permissionStatus == PermissionStatus.permanentlyDenied ||
              permissionStatus == PermissionStatus.denied) {
            HelperFunctions.setAutoSync(value: false);
          } else {
            HelperFunctions.setAutoSync(value: true);
          }
        } else {
          HelperFunctions.setAutoSync(value: true);
        }
      },
      onUnconfirm: () => HelperFunctions.setAutoSync(value: false),
    );
  }

  Future<void> refreshDashBoard() async {
    getLatestUserData();
    getFinaceSummary();
    return getWitnessList();
  }

  void getLatestUserData() async {
    if (this.mounted) {
      setState(() {
        isLoading = true;
      });
    }

    final updatedUserDataResp =
        await Provider.of<UserProvider>(context, listen: false)
            .getUpdatedUserData(
      userId: userData["id"],
    );

    if (updatedUserDataResp["status"] == 200 ||
        updatedUserDataResp["status"] == 201) {
      if (this.mounted) {
        setState(() {
          updatedUserData = updatedUserDataResp["data"];
          var archFactor = HelperFunctions.getUserCompletionArchAngleFactor(
            context: context,
            userData: updatedUserData,
          );

          avartarArcAnimationController = new AnimationController(
            vsync: this,
            duration: new Duration(
              milliseconds: 1000,
            ),
          )..addListener(() {
              if (this.mounted) {
                setState(() {
                  avartarArchFactor = ui.lerpDouble(
                      0.0, archFactor, avartarArcAnimationController.value);
                });
              }
            });
          avartarArcAnimationController.forward();
          percentageAnimationController = new AnimationController(
            vsync: this,
            duration: new Duration(
              milliseconds: 1000,
            ),
          )..addListener(() {
              if (this.mounted) {
                setState(() {
                  psArchFactor = ui.lerpDouble(
                      0.0,
                      (updatedUserData["compliance"] / 100 * 2),
                      percentageAnimationController.value);
                });
              }
            });
          percentageAnimationController.forward();
        });
      }
    } else {
      if (this.mounted) {
        setState(() {
          CustomSnackBar.snackBar(
            title: "Something went wrong, please reload the page",
            scaffoldState: _scaffoldKey,
            context: context,
          );
        });
      }
    }
  }

  void getFinaceSummary() async {
    final summaryResponse =
        await Provider.of<LoansProvider>(context, listen: false)
            .getUserFinanceSummary(
      providerId: userData["id"],
      recipientPhone: HelperFunctions.serializePhoneNumber(
        phoneNumber: userData["phone"],
      ),
    );
    if (summaryResponse["status"] == 200 || summaryResponse["status"] == 201) {
      if (this.mounted) {
        setState(() {
          summary = summaryResponse["data"];
        });
      }
    } else {
      if (this.mounted) {
        setState(() {
          CustomSnackBar.snackBar(
            title: "Something went wrong, please reload the page",
            scaffoldState: _scaffoldKey,
            context: context,
          );
        });
      }
    }
  }

  void getWitnessList() async {
    final listResponse =
        await Provider.of<WitnesProvider>(context, listen: false).getClaims();
    if (listResponse["status"] == 200 || listResponse["status"] == 201) {
      if (this.mounted) {
        setState(() {
          claimsLength =
              Provider.of<WitnesProvider>(context, listen: false).claims.length;
          isLoading = false;
        });
      }
    } else {
      if (this.mounted) {
        setState(() {
          isLoading = false;
          pullRefresh = false;
          CustomSnackBar.snackBar(
            title: "Something went wrong, please reload the page",
            scaffoldState: _scaffoldKey,
            context: context,
          );
        });
      }
    }
  }

  Widget buildSummaryCard(
      {BuildContext context,
      String title,
      num number,
      String amount,
      bool isDebt = false,
      Color borderColor}) {
    return InkWell(
      onTap: () =>
          GeneralProvider.switchMainPage(index: isDebt == true ? 2 : 1),
      child: Container(
        // width: 123,
        padding: EdgeInsets.symmetric(vertical: 5, horizontal: 8),
        decoration: BoxDecoration(
          color: Color(0xFFF1F1F1F1),
          borderRadius: BorderRadius.circular(8),
          border: Border.all(color: borderColor, width: 0.3),
        ),

        child: Column(
          children: <Widget>[
            Align(
              alignment: Alignment.topLeft,
              child: Text(
                title,
                style: Theme.of(context)
                    .textTheme
                    .body1
                    .copyWith(fontWeight: FontWeight.bold),
              ),
            ),
            AnimatedCount(
              count: number,
              style: Theme.of(context)
                  .textTheme
                  .headline
                  .copyWith(fontWeight: FontWeight.w300, fontSize: 36),
              duration: Duration(milliseconds: 1000),
            ),
            Spacer(),
            Align(
              alignment: Alignment.bottomRight,
              child: Text(
                "$amount",
                style: Theme.of(context)
                    .textTheme
                    .body1
                    .copyWith(fontWeight: FontWeight.w300),
              ),
            )
          ],
        ),
      ),
    );
  }

  void gotoProfilePage() {
    Navigator.of(context).pushReplacementNamed(
      ProfileScreen.routeName,
      arguments: {"isSlideBackward": false},
    );
  }

  @override
  void dispose() {
    avartarArcAnimationController.dispose();
    percentageAnimationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    var complianceSectionHeight;
    double archBarWidth;
    var appColors = Provider.of<AppColorsProvider>(context, listen: false);
    var height = HelperFunctions.getDeviceUsabaleHeigth(context: context);

    complianceSectionHeight = (height -
            100 -
            Provider.of<GeneralProvider>(context).getBottomNavHeight) /
        3;
    archBarWidth = complianceSectionHeight < 200 ? 35 : 50;
    return RefreshIndicator(
      key: refreshKey,
      onRefresh: () {
        if (this.mounted) {
          setState(() {
            pullRefresh = true;
          });
        }
        return refreshDashBoard();
      },
      color: Theme.of(context).primaryColor,
      displacement: 45,
      child: SingleChildScrollView(
        padding: EdgeInsets.zero,
        physics: AlwaysScrollableScrollPhysics(),
        child: Container(
          height: dashboardHeight,
          // height: height -
          //     Provider.of<GeneralProvider>(context).getBottomNavHeight - 20,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                // height: 100,
                padding: EdgeInsets.symmetric(vertical: 30),
                child: MainNavBar(
                  // leadingImagePath: 'public/images/new_logo.png',
                  notificationImagePath: 'public/images/notification.png',
                  useImageColor: true,
                  leadingHeight: 22,
                  leadingWidth: 22,
                  leadingWidget: Padding(
                    padding: const EdgeInsets.only(left: 15),
                    child: DescribedFeatureOverlay(
                      featureId: 'profile',
                      title: Text("Profile"),
                      description: Text(
                        "Update your profile and manage your bank accounts to boost your PeerScore",
                      ),
                      // backgroundColor:
                      //     appColors.primaryFeatureDiscoveryBackground,
                      backgroundColor: GeneralProvider.isLightMode
                          ? Theme.of(context).primaryColor
                          : Colors.grey[850],
                      contentLocation: ContentLocation.below,
                      tapTarget: CircleAvatar(
                        backgroundImage: AssetImage(
                          "public/images/profile-picture-placeholder.png",
                        ),
                      ),
                      onDismiss: () async {
                        return false;
                      },
                      onComplete: () async {
                        return true;
                      },
                      child: InkWell(
                        customBorder: CircleBorder(),
                        onTap: gotoProfilePage,
                        child: Stack(
                          alignment: AlignmentDirectional.center,
                          children: <Widget>[
                            Container(
                              height: 37,
                              width: 37,
                              child: CustomPaint(
                                foregroundPainter: UserIconArc(
                                    archFactor: avartarArchFactor,
                                    arcColor: Theme.of(context).primaryColor),
                              ),
                            ),
                            ClipRRect(
                              borderRadius: BorderRadius.circular(25),
                              child: Container(
                                height: 30,
                                width: 30,
                                decoration:
                                    BoxDecoration(shape: BoxShape.circle),
                                child: FadeInImage(
                                  placeholder: AssetImage(
                                      "public/images/profile-picture-placeholder.png"),
                                  image:
                                      NetworkImage(userData["profile_picture"]),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 5,
                child: Column(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        padding: EdgeInsets.only(left: 30, right: 30),
                        child: Row(
                          children: <Widget>[
                            Text("Hello, ",
                                style: Theme.of(context).textTheme.body2),
                            Text(
                              "${toBeginningOfSentenceCase(userData["first_name"] == null ? "" : userData["first_name"])}",
                              style: Theme.of(context).textTheme.body2.copyWith(
                                    fontWeight: FontWeight.w600,
                                  ),
                            ),
                            Spacer(),
                            if (claimsLength > 0)
                              InkWell(
                                onTap: () {
                                  widget.switchTo("witness");
                                },
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      width: 7,
                                      height: 7,
                                      margin: EdgeInsets.only(right: 6),
                                      child: Image.asset(
                                        'public/images/priority.png',
                                        color: Theme.of(context).errorColor,
                                      ),
                                    ),
                                    Text(
                                      "Dispute invites",
                                      style: Theme.of(context)
                                          .textTheme
                                          .body2
                                          .copyWith(
                                              // fontWeight: FontWeight.w600,
                                              ),
                                    ),
                                  ],
                                ),
                              ),
                          ],
                        ),
                      ),
                    ),
                    Spacer(),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 30),
                      child: Stack(
                        alignment: AlignmentDirectional.center,
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              Container(
                                height: complianceSectionHeight,
                                alignment: Alignment.center,
                                child: Stack(
                                  children: <Widget>[
                                    Align(
                                      child: Container(
                                        height: complianceSectionHeight,
                                        width: complianceSectionHeight,
                                        decoration: BoxDecoration(
                                          // color: Colors.grey[200],
                                          color: appColors.summaryBoxColor,
                                          shape: BoxShape.circle,
                                        ),
                                      ),
                                    ),
                                    Align(
                                      child: Container(
                                        height: complianceSectionHeight,
                                        width: complianceSectionHeight,
                                        child: CustomPaint(
                                          foregroundPainter: PeerScoreBar(
                                            archFactor: psArchFactor,
                                            archBarWidth: archBarWidth,
                                            goodIndicator:
                                                Theme.of(context).primaryColor,
                                            badIndicator:
                                                Theme.of(context).accentColor,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Align(
                                      child: Container(
                                        height: complianceSectionHeight -
                                            archBarWidth,
                                        width: complianceSectionHeight -
                                            archBarWidth,
                                        padding:
                                            EdgeInsets.symmetric(vertical: 8),
                                        decoration: BoxDecoration(
                                          // color: Colors.white,
                                          color: GeneralProvider.isLightMode
                                              ? Colors.white
                                              : Color(0xff121212),
                                          shape: BoxShape.circle,
                                          boxShadow: GeneralProvider.isLightMode
                                              ? [
                                                  BoxShadow(
                                                    offset: Offset(0, 2.6),
                                                    color: Colors.grey,
                                                  )
                                                ]
                                              : null,
                                        ),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Expanded(
                                              flex: 2,
                                              child: Container(
                                                width: complianceSectionHeight -
                                                    (archBarWidth * 2),
                                                child: FittedBox(
                                                  fit: BoxFit.none,
                                                  child: Text(
                                                    "Compliance",
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .body2
                                                        .copyWith(),
                                                  ),
                                                ),
                                              ),
                                            ),

                                            Expanded(
                                              flex: 3,
                                              child: FittedBox(
                                                fit: BoxFit.contain,
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    AnimatedCount(
                                                      count: updatedUserData[
                                                          "compliance"],
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .headline
                                                          .copyWith(
                                                            color: GeneralProvider
                                                                    .isLightMode
                                                                ? Color(
                                                                    0xFF1F3D51,
                                                                  )
                                                                : null,
                                                            fontSize: 36,
                                                            fontWeight:
                                                                FontWeight.w600,
                                                          ),
                                                      duration: Duration(
                                                        milliseconds: 1000,
                                                      ),
                                                    ),
                                                    SizedBox(width: 5),
                                                    Text(
                                                      "%",
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .headline
                                                          .copyWith(
                                                              color: GeneralProvider
                                                                      .isLightMode
                                                                  ? Color(
                                                                      0xFF1F3D51,
                                                                    )
                                                                  : null,
                                                              fontSize: 36,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ),
                                            // Text(
                                            //   "${userData["compliance"]}",
                                            //   style: Theme.of(context)
                                            //       .textTheme
                                            //       .headline
                                            //       .copyWith(
                                            //           color: Color(0xFF1F3D51),
                                            //           fontSize: 36,
                                            //           fontWeight: FontWeight.w600),
                                            // ),
                                            Expanded(
                                              flex: 2,
                                              child: FittedBox(
                                                fit: BoxFit.none,
                                                child: Text(
                                                  "${psArchFactor < 0.8 ? "Bad" : "Good"}",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .body2
                                                      .copyWith(
                                                        color: GeneralProvider
                                                                .isLightMode
                                                            ? Color(
                                                                0xFF1F3D51,
                                                              )
                                                            : null,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                      ),
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                height: 15,
                                margin: EdgeInsets.only(top: 5),
                                child: pullRefresh == false && isLoading
                                    ? Center(
                                        child: CustomProgressIndicator(
                                          size: 15,
                                          strokeWidth: 1,
                                          color: Theme.of(context).primaryColor,
                                        ),
                                      )
                                    : Container(),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 3,
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.symmetric(vertical: 8),
                        child: Text("Quick Summary",
                            style: Theme.of(context).textTheme.body2),
                      ),
                      Expanded(
                        // flex: 2,
                        child: Container(
                          // height: 115,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                child: HelperFunctions.buildSummaryCard(
                                    context: context,
                                    title: "Money others owe",
                                    number: summary["loan_count"],
                                    amount: HelperFunctions.getFormattedAmount(
                                      amount: summary["total_loan"].toString(),
                                    ),
                                    borderColor: Theme.of(context).primaryColor,
                                    onTap: () => GeneralProvider.switchMainPage(
                                        index: 1)),
                              ),
                              SizedBox(
                                width: 15,
                              ),
                              Expanded(
                                child: HelperFunctions.buildSummaryCard(
                                    context: context,
                                    title: "Money you owe",
                                    number: summary["debt_count"],
                                    amount: HelperFunctions.getFormattedAmount(
                                      amount: summary["total_debt"].toString(),
                                    ),
                                    borderColor: Color(0xffE78200),
                                    onTap: () => GeneralProvider.switchMainPage(
                                        index: 2)),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                  flex: 2,
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        if (deviceSize.width > 360)
                          SizedBox(
                            width: (deviceSize.width - 360) / 25 + 10,
                          ),
                        Expanded(
                          // width: 100,
                          // constraints: BoxConstraints(minWidth: 80),
                          child: DescribedFeatureOverlay(
                            featureId: 'request_payment',
                            title: Text(
                              "Request Payment ",
                            ),
                            description: Text(
                                "Click here to request loan payments from your defaulter \n \n"),
                            // backgroundColor:
                            //     appColors.primaryFeatureDiscoveryBackground,
                            backgroundColor: GeneralProvider.isLightMode
                                ? Theme.of(context).primaryColor
                                : Colors.grey[850],
                            contentLocation: ContentLocation.above,
                            tapTarget: Image.asset(
                              'public/images/money-in.png',
                              color: Colors.black,
                              height: 28,
                            ),
                            onDismiss: () async {
                              return false;
                            },
                            onComplete: () async {
                              return true;
                            },
                            child: FittedBox(
                              child: CustomRaisedButton(
                                title: "Request Payment",
                                leadingWidget: GeneralProvider.isLightMode
                                    ? null
                                    : FaIcon(
                                        Platform.isAndroid
                                            ? FontAwesomeIcons.plus
                                            : FontAwesomeIcons.lightPlus,
                                        size: 20,
                                      ),
                                leadingIconPath: GeneralProvider.isLightMode
                                    ? "public/images/request-payment-icon.png"
                                    : null,
                                onTapAction: () {
                                  if (userData["first_name"] == null) {
                                    return ModalActions.showMoreInfo(
                                      context: context,
                                      title: "Update profile",
                                      content:
                                          "Please update your profile to create a request.",
                                      onOk: () => gotoProfilePage(),
                                    );
                                  }
                                  widget.switchTo("payment");
                                },
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 25,
                        ),
                        Expanded(
                          // width: 100,
                          // constraints: BoxConstraints(minWidth: 80),
                          child: DescribedFeatureOverlay(
                            featureId: 'request_loan',
                            title: Text(
                              "Request Loan",
                              style:
                                  Theme.of(context).textTheme.body1.copyWith(),
                            ),
                            description: Text(
                              "Click here to initiate loan requests from your contacts \n \n",
                              style:
                                  Theme.of(context).textTheme.body1.copyWith(),
                            ),
                            backgroundColor: GeneralProvider.isLightMode
                                ? Theme.of(context).accentColor
                                : Colors.grey[850],
                            contentLocation: ContentLocation.above,
                            tapTarget: Image.asset(
                              'public/images/money-out.png',
                              color: Colors.black,
                              height: 28,
                            ),
                            onDismiss: () async {
                              return false;
                            },
                            onComplete: () async {
                              return true;
                            },
                            child: FittedBox(
                              child: CustomRaisedButton(
                                title: "Request Loan",
                                leadingWidget: GeneralProvider.isLightMode
                                    ? null
                                    : FaIcon(Platform.isAndroid
                                        ? FontAwesomeIcons.minus
                                        : FontAwesomeIcons.lightMinus),
                                leadingIconPath: GeneralProvider.isLightMode
                                    ? "public/images/request-loan-icon.png"
                                    : null,
                                bgColor: Theme.of(context).accentColor,
                                onTapAction: () {
                                  if (userData["first_name"] == null) {
                                    return ModalActions.showMoreInfo(
                                      context: context,
                                      title: "Update profile",
                                      content:
                                          "Please update your profile to create a request.",
                                      onOk: () => gotoProfilePage(),
                                    );
                                  }
                                  widget.switchTo("loan");
                                },
                              ),
                            ),
                          ),
                        ),
                        if (deviceSize.width > 360)
                          SizedBox(
                            width: (deviceSize.width - 360) / 25 + 10,
                          ),
                      ],
                    ),
                  ))
            ],
          ),
        ),
      ),
    );
  }
}

class PeerScoreBar extends CustomPainter {
  final double archFactor;
  final double archBarWidth;
  final Color goodIndicator;
  final Color badIndicator;
  PeerScoreBar(
      {this.archFactor = 0,
      this.archBarWidth,
      this.badIndicator,
      this.goodIndicator});
  @override
  void paint(Canvas canvas, Size size) {
    Paint circle = Paint();
    circle.color = Colors.transparent;
    Paint arcBar = Paint();
    // arcBar.color = Color(0xFF73E5AE);
    arcBar.color = archFactor < 0.8 ? badIndicator : goodIndicator;
    arcBar.strokeWidth = archBarWidth / 2;
    arcBar.strokeCap = StrokeCap.round;
    arcBar.style = PaintingStyle.stroke;
    Paint dot = Paint();
    dot.color = GeneralProvider.isLightMode ? Colors.white : Colors.black;
    dot.strokeWidth = archBarWidth * 0.3; //taking 30% from the archbar width
    dot.strokeCap = StrokeCap.round;
    Offset circleCenter = Offset(size.width / 2, size.height / 2);
    double radius = size.width / 2 - ((archBarWidth / 2) / 2);
    canvas.drawCircle(circleCenter, radius, circle);
    var startAngle = -math.pi / 1.12;
    // var sweepAngle = (math.pi / 40) * 50;
    var sweepAngle = math.pi * archFactor;
    canvas.drawArc(Rect.fromCircle(center: circleCenter, radius: radius),
        startAngle, sweepAngle, false, arcBar);
    final pointMode = ui.PointMode.points;
    var ballPointX =
        radius * math.cos(startAngle + sweepAngle) + size.width / 2;
    var ballPointY =
        radius * math.sin(startAngle + sweepAngle) + size.height / 2;
    Offset pointOffset = Offset(ballPointX, ballPointY);
    canvas.drawPoints(pointMode, [pointOffset], dot);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

class UserIconArc extends CustomPainter {
  final double archFactor;
  final Color arcColor;
  UserIconArc({this.archFactor, this.arcColor});

  @override
  void paint(Canvas canvas, Size size) {
    final double strokeWidth = 1.5;
    Paint arcBarBg = Paint();
    Paint arcBarIndicator = Paint();
    arcBarBg.color = Colors.grey[300];
    arcBarBg.strokeWidth = strokeWidth;
    arcBarBg.strokeCap = StrokeCap.round;
    arcBarBg.style = PaintingStyle.stroke;
    arcBarIndicator.color = arcColor;
    arcBarIndicator.strokeWidth = strokeWidth;
    arcBarIndicator.strokeCap = StrokeCap.round;
    arcBarIndicator.style = PaintingStyle.stroke;

    Offset circleCenter = Offset(size.width / 2, size.height / 2);
    double radius = size.width / 2 - strokeWidth / 2;
    var startAngleBg = math.pi / 2;
    var sweepAngleBg = math.pi * 2;
    var startAngleIndicator = -math.pi / 2;
    var sweepAngleIndicator = -math.pi * archFactor;
    canvas.drawArc(Rect.fromCircle(center: circleCenter, radius: radius),
        startAngleBg, sweepAngleBg, false, arcBarBg);
    canvas.drawArc(Rect.fromCircle(center: circleCenter, radius: radius),
        startAngleIndicator, sweepAngleIndicator, false, arcBarIndicator);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
