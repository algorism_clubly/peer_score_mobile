import 'package:flutter/material.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/widgets/contacts/contact_list.dart';
import 'package:peer_score/widgets/dashboard/claims_list.dart';
import 'package:peer_score/widgets/dashboard/dashboard_page.dart';
import 'package:peer_score/widgets/dashboard/loan_details_page.dart';
import 'package:peer_score/widgets/dashboard/message_page.dart';
import 'package:peer_score/widgets/dashboard/select_contact_page.dart';
import 'package:peer_score/widgets/dashboard/success_page.dart';
import 'package:peer_score/widgets/dashboard/verdict_page.dart';
import 'package:provider/provider.dart';

class DashBoardStack extends StatefulWidget {
  final int initialIndex;
  DashBoardStack({this.initialIndex});
  @override
  _DashBoardStackState createState() => _DashBoardStackState();
}

class _DashBoardStackState extends State<DashBoardStack> {
  bool isRequestLoan = false;
  bool isRequestPayment = false;
  bool witness = true;
  PageController _pageController;
  // int pageIndex = 0;
  @override
  void initState() {
    _pageController = PageController(initialPage: widget.initialIndex);
    GeneralProvider.switchSubStackIndex = switchSubStackIndex;
    super.initState();
  }

  void switchSubStackIndex({int index = 0}) {
    
    _pageController.jumpToPage(index);
  }

  void _switchPage(int toPageNumber) {
    if (GeneralProvider.currentFocusedNode != null) {
      GeneralProvider.currentFocusedNode.unfocus();
    }
    _pageController.animateToPage(toPageNumber,
        duration: Duration(milliseconds: 200), curve: Curves.linear);
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: _pageController,
      physics: NeverScrollableScrollPhysics(),
      children: <Widget>[
        DashBoardPage(
          switchTo: (mode) {
            //
            if (this.mounted) {
              setState(() {
                if (mode == "payment") {
                  isRequestPayment = true;
                  isRequestLoan = false;
                  witness = false;
                } else if (mode == "loan") {
                  isRequestPayment = false;
                  isRequestLoan = true;
                  witness = false;
                } else if (mode == "witness") {
                  isRequestPayment = false;
                  isRequestLoan = false;
                  witness = true;
                }
              });
            }
            _switchPage(1);
          },
        ),
        if (!witness)
          ContactListPage(
            header: isRequestPayment ? "Request payment" : "Request loan",
            subHeader:
              isRequestPayment ? "Create new debtor" : "Select creditor",
            navBarLeadingFn: () => _switchPage(0),
            switchTo: () => _switchPage(2),
          ),
        // SelectContactPage(
        //   switchTo: () => _switchPage(2),
        //   navBarLeadingFn: () => _switchPage(0),
        //   header: isRequestPayment ? "Request payment" : "Request loan",
        //   subHeader:
        //       isRequestPayment ? "Create new debtor" : "Select creditor",
        // ),
        if (!witness)
          LoanDetailsPage(
            switchTo: () => _switchPage(3),
            navBarLeadingFn: () => _switchPage(1),
            header: isRequestPayment ? "Request payment" : "Request loan",
            isRequestPayment: isRequestPayment ? true : false,
          ),
        if (!witness)
          MessagePage(
            isRequestPayment: isRequestPayment ? true : false,
            navBarLeadingFn: () => _switchPage(2),
            switchTo: () => _switchPage(4),
          ),
        if (witness)
          ClaimsList(
            navBarLeadingFn: () => _switchPage(0),
            switchTo: () => _switchPage(2),
            header: "Claim verdicts",
          ),
        if (witness)
          VerdictPage(
            navBarLeadingFn: () => _switchPage(1),
            switchTo: () => _switchPage(3),
          ),
        SuccessPage(
          navBarLeadingFn: () => _pageController.jumpToPage(0),
          switchMainPage: (index) => _pageController.jumpToPage(0),
        ),
      ],
    );
  }
}
