import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:peer_score/providers/colors.dart';
import 'package:peer_score/providers/contact.dart';
import 'package:peer_score/providers/loans-provider.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/utils/custom_checkbox.dart';
import 'package:peer_score/utils/custom_raised_button.dart';
import 'package:peer_score/utils/dropdown_field.dart';
import 'package:peer_score/utils/err_text_widget.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:peer_score/utils/main_nav_bar.dart';
import 'package:peer_score/utils/modal_actions.dart';
import 'package:peer_score/widgets/contacts/contact_list.dart';
import 'package:provider/provider.dart';

class LoanDetailsPage extends StatefulWidget {
  final Function switchTo;
  final Function navBarLeadingFn;
  final String header;
  bool isRequestPayment;
  LoanDetailsPage(
      {this.switchTo,
      this.navBarLeadingFn,
      this.header,
      this.isRequestPayment = true});

  @override
  _LoanDetailsPageState createState() => _LoanDetailsPageState();
}

class _LoanDetailsPageState extends State<LoanDetailsPage> {
  Contact selectedContact;
  String selectedPaymentDate = "";
  DateTime selectedPaymentDateTime;
  bool privateRequest = true;
  bool circleOfFriends = false;
  bool publicOnPeerScore = false;
  TextEditingController _amountController;
  FocusNode _amountFocusNode;
  String _amountErrText = "";
  bool _amountErrVisible = false;
  String _dateErrText = "";
  bool _dateErrVisible = false;

  @override
  void initState() {
    super.initState();
    BackButtonInterceptor.add(myInterceptor);
    _amountController = TextEditingController();
    _amountFocusNode = FocusNode();
    GeneralProvider.currentFocusedNode = _amountFocusNode;
    _amountController.addListener(() {
      if (_amountController.text.length > 0) {
        if (this.mounted) {
          setState(() {
            _amountErrText = "";
            _amountErrVisible = false;
          });
        }
      }
    });
    selectedContact = LoansProvider.selectedContact;
  }

  bool myInterceptor(bool stopDefaultButtonEvent) {
    if (stopDefaultButtonEvent) return false;
    widget.navBarLeadingFn();
    return true;
  }

  @override
  void dispose() {
    super.dispose();
    BackButtonInterceptor.remove(myInterceptor);
    _amountController.dispose();
  }

  void privateRequestCheck(bool value) {
    if (this.mounted) {
      setState(() {
        if (value) {
          circleOfFriends = false;
          publicOnPeerScore = false;
          privateRequest = true;
        }
      });
    }
  }

  void circleOfFriendsCheck(bool value) {
    if (this.mounted) {
      setState(() {
        if (value) {
          privateRequest = false;
          publicOnPeerScore = false;
          circleOfFriends = true;
        }
      });
    }
  }

  void publicOnPeerScoreCheck(bool value) {
    if (this.mounted) {
      setState(() {
        if (value) {
          privateRequest = false;
          circleOfFriends = false;
          publicOnPeerScore = true;
        }
      });
    }
  }

  Widget buildCheckBox(
      {String title,
      Function onCLick,
      String moreInfoContent,
      bool value,
      String toolTipContent}) {
    return Row(
      children: <Widget>[
        CustomCheckBox(
          title: title,
          value: value,
          onClick: (value) {
            onCLick(value);
          },
        ),
        SizedBox(width: 12),
        InkWell(
          onTap: () => ModalActions.showMoreInfo(
            context: context,
            title: title,
            content: toolTipContent,
          ),
          child: HelperFunctions.buildQuestionMark(),
        )
      ],
    );
  }

  void onSubmit() {
    _amountFocusNode.unfocus();
    final amount = _amountController.text.trim();
    var regEx = RegExp(r"^[0-9]+$", multiLine: true);
    if (!regEx.hasMatch(amount)) {
      if (this.mounted) {
        setState(() {
          _amountErrText = "Provided amount invalid";
          _amountErrVisible = true;
        });
      }
      return;
    }
    if (selectedPaymentDateTime == null) {
      if (this.mounted) {
        setState(() {
          _dateErrText = "Choose a date";
          _dateErrVisible = true;
        });
      }
      return;
    }

    Provider.of<LoansProvider>(context, listen: false).setAmountDateVisibility(
        amount: _amountController.text,
        paymentDate: selectedPaymentDateTime,
        requestVisibility: privateRequest
            ? "private"
            : circleOfFriends ? "friends" : publicOnPeerScore ? "public" : "");
    widget.switchTo();
  }

  @override
  Widget build(BuildContext context) {
    void openDatePicker() async {
      _amountFocusNode.unfocus();
      DateTime firstDate;
      DateTime lastDate;
      DateTime initialDate;
      if (widget.isRequestPayment) {
        firstDate = DateTime(DateTime.now().year - 10);
        lastDate = DateTime(DateTime.now().year + 10);
        initialDate = DateTime.now();
      } else {
        firstDate = DateTime.now();
        lastDate = DateTime(DateTime.now().year + 10);
        initialDate = DateTime.now().add(Duration(days: 1));
      }
      var selectedDate = await HelperFunctions.getSelectedDate(
          context: context,
          initialDate: initialDate,
          firstDate: firstDate,
          lastDate: lastDate);

      if (selectedDate != null) {
        if (this.mounted) {
          setState(() {
            selectedPaymentDateTime = selectedDate;
            selectedPaymentDate = DateFormat.yMMMMd().format(selectedDate);
          });
        }
      }
    }

    final deviceSize = MediaQuery.of(context).size;
    var heigth = deviceSize.height;

    var appColors = Provider.of<AppColorsProvider>(context, listen: false);

    return Container(
      height: deviceSize.height -
          MediaQuery.of(context).padding.top -
          Provider.of<GeneralProvider>(context, listen: false)
              .getBottomNavHeight,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(vertical: 30),
            child: MainNavBar(
                title: widget.header,
                leadingImagePath: 'public/images/arrow-back.png',
                notificationImagePath: 'public/images/notification.png',
                // subNav: true,
                leadingPressAction: () {
                  // _amountFocusNode.unfocus();
                  widget.navBarLeadingFn();
                }),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              height: 38,
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: 25,
                    margin: EdgeInsets.only(right: 12),
                    child: Image.asset(
                      "public/images/selected-contact.png",
                      color: !GeneralProvider.isLightMode ? Colors.white : null,
                    ),
                  ),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              HelperFunctions.getContactFullName(
                                  contact: selectedContact),
                              overflow: TextOverflow.ellipsis,
                              style: Theme.of(context)
                                  .textTheme
                                  .body2
                                  .copyWith(fontWeight: FontWeight.normal),
                            ),
                            Text(
                                "${selectedContact.id == null ? "-" : selectedContact.compliance}",
                                style:
                                    Theme.of(context).textTheme.body1.copyWith(
                                          fontWeight: FontWeight.bold,
                                        ))
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Text(
                              selectedContact.phoneNumber,
                              style: Theme.of(context).textTheme.body1.copyWith(
                                    fontWeight: FontWeight.normal,
                                    color: appColors.contactPhoneColor,
                                  ),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            if (selectedContact.id != null)
                              HelperFunctions.buildUserBadge(context: context)
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Expanded(
            child: ListView(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Loan details",
                        style: Theme.of(context).textTheme.body2.copyWith(
                            fontWeight: FontWeight.normal, height: 0.73),
                      ),
                      SizedBox(height: 10),
                      Text(
                        "fill PeerScore in on the loan details so that we can hasten the process",
                        style: Theme.of(context).textTheme.body1.copyWith(
                              // fontSize: 11,
                              fontWeight: FontWeight.bold,
                              // height: 0.73,
                              color: Color(0xFF8E8D8D),
                            ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      DropDownField(
                        defaultItem: "NGN",
                        items: ["NGN"],
                        hintText: "Amount",
                        height: 40,
                        textEditingController: _amountController,
                        focusNode: _amountFocusNode,
                        textInputType: TextInputType.number,
                        onTextInput: (value) {
                          if (value.startsWith("0")) {
                            setState(() {
                              _amountController.text = "";
                            });
                          }
                        },
                        inputFormatters: [
                          WhitelistingTextInputFormatter.digitsOnly,
                        ],
                      ),
                      ErrText(
                        errText: _amountErrText,
                        vissibility: _amountErrVisible,
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Text(
                        widget.isRequestPayment
                            ? "Expected payment date"
                            : "Payment refund date",
                        style: Theme.of(context).textTheme.body1.copyWith(
                              // fontSize: 11,
                              // height: 0.73,
                              fontWeight: FontWeight.bold,
                              color: Color(0xFFB4B4B4),
                            ),
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      InkWell(
                        onTap: () {
                          if (this.mounted) {
                            setState(() {
                              _dateErrText = "";
                              _dateErrVisible = false;
                            });
                          }
                          openDatePicker();
                        },
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text(
                              "Choose date",
                              style: Theme.of(context).textTheme.body1.copyWith(
                                    height: 0.73,
                                    fontWeight: FontWeight.normal,
                                    color: Theme.of(context).primaryColor,
                                  ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 5),
                              child: SizedBox(
                                width: 15,
                                height: 16,
                                child: Image.asset(
                                  "public/images/callender.png",
                                  fit: BoxFit.cover,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      ErrText(
                        errText: _dateErrText,
                        vissibility: _dateErrVisible,
                      ),
                      SizedBox(height: 5),
                      Text(
                        selectedPaymentDate,
                        style: Theme.of(context).textTheme.body1.copyWith(
                              height: 0.73,
                              fontWeight: FontWeight.bold,
                            ),
                      ),
                    ],
                  ),
                ),
                if (widget.isRequestPayment)
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(height: 20),
                        Text(
                          "Request Privacy",
                          style: Theme.of(context).textTheme.body1.copyWith(
                                // height: 0.75,
                                // fontSize: 12,
                                fontWeight: FontWeight.normal,
                                color: Theme.of(context).accentColor,
                              ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Define how you want PeerScore to handle the publicity of your request",
                          style: Theme.of(context).textTheme.body1.copyWith(
                                // fontSize: 11,
                                fontWeight: FontWeight.bold,
                                // height: 0.73,
                                color: Color(0xFF8E8D8D),
                              ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        buildCheckBox(
                            title: "Make request private",
                            onCLick: privateRequestCheck,
                            value: privateRequest,
                            toolTipContent:
                                "Requests marked as private are only visible to both the creditor and debtor."),
                        SizedBox(
                          height: 15,
                        ),
                        buildCheckBox(
                            title: "Public among circle of friends",
                            onCLick: circleOfFriendsCheck,
                            value: circleOfFriends,
                            toolTipContent:
                                "This request will be visible to the circle of friends of both creditor and debtor."),
                        SizedBox(
                          height: 15,
                        ),
                        buildCheckBox(
                            title: "Public on PeerScore",
                            onCLick: publicOnPeerScoreCheck,
                            value: publicOnPeerScore,
                            toolTipContent:
                                "This request will be visible to all users of PeerScore."),
                      ],
                    ),
                  ),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 25),
            padding: EdgeInsets.symmetric(horizontal: 30),
            child: CustomRaisedButton(
              title: "Complete",
              onTapAction: onSubmit,
            ),
          )
        ],
      ),
    );
  }
}
