import 'dart:io' show Platform;
import 'package:auto_size_text/auto_size_text.dart';
import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:peer_score/providers/colors.dart';
import 'package:peer_score/providers/contact.dart';
import 'package:peer_score/providers/loans-provider.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/providers/user-provider.dart';
import 'package:peer_score/utils/custom_raised_button.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:peer_score/utils/googleCalender.dart';
import 'package:peer_score/utils/main_nav_bar.dart';
import 'package:peer_score/utils/modal_actions.dart';
import 'package:peer_score/utils/progress_indicator.dart';
import 'package:peer_score/utils/snackbar.dart';
import 'package:provider/provider.dart';
// import 'package:sms_maintained/sms.dart';
import 'package:url_launcher/url_launcher.dart';

class MessagePage extends StatefulWidget {
  final Function navBarLeadingFn;
  final Function switchTo;
  final bool isRequestPayment;
  final String successSms;
  MessagePage(
      {this.navBarLeadingFn,
      this.isRequestPayment = true,
      this.successSms,
      this.switchTo});

  @override
  _MessagePageState createState() => _MessagePageState();
}

class _MessagePageState extends State<MessagePage> {
  GlobalKey<ScaffoldState> _scaffoldKey;
  Contact contact;
  bool isSmsSending = false;
  bool isWhatsappSending = false;
  Map<String, dynamic> userData = {"first_name": ""};
  String smsMessageText;

  @override
  void initState() {
    super.initState();
    BackButtonInterceptor.add(myInterceptor);
    Provider.of<UserProvider>(context, listen: false).userData.then((data) {
      if (this.mounted) {
        setState(() {
          userData = data;
          smsMessageText = widget.isRequestPayment
              ? "Hello, ${userData["first_name"]} has made a payment request from you on PeerScore, kindly respond to it${contact.id != null ? ", thanks." : " on bit.ly/2E9PZOm"}"
              : "Hello,${userData["first_name"]} has requested a loan from you on peer-score, kindly respond to it${contact.id != null ? ", thanks." : " on bit.ly/2E9PZOm"}";
        });
      }
    });
    contact = LoansProvider.selectedContact;
    _scaffoldKey =
        Provider.of<GeneralProvider>(context, listen: false).getAppScaffoldKey;
  }

  bool myInterceptor(bool stopDefaultButtonEvent) {
    if (stopDefaultButtonEvent) return false;
    widget.navBarLeadingFn();
    return true;
  }

  @override
  void dispose() {
    BackButtonInterceptor.remove(myInterceptor);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Future<void> whatsappHandler() async {
      String phoneNumber = HelperFunctions.serializePhoneNumber(
          phoneNumber: contact.phoneNumber);
      String message = widget.isRequestPayment
          ? "Hello, I am notifying you that a payment request has been initiated from me to you on PeerScore, kindly respond to it, thanks."
          : "Hello, I have initiated a loan request from you on PeerScore, will be glad if you respond to it, thanks.";
      var whatsappUrl = "whatsapp://send?phone=$phoneNumber&text=$message";
      if (await canLaunch(whatsappUrl)) {
        await launch(whatsappUrl, forceSafariVC: Platform.isIOS ? false : null);
      } else {
        CustomSnackBar.snackBar(
          title: "WhatsApp not installed.",
          scaffoldState: _scaffoldKey,
          context: context,
        );
      }
    }

    void sendRequest({bool isSms = false}) async {
      if (!isSms) {
        if (this.mounted) {
          setState(() {
            isWhatsappSending = true;
          });
        }
      }
      var createResponse;
      if (widget.isRequestPayment) {
        createResponse =
            await Provider.of<LoansProvider>(context, listen: false)
                .createRequestPayment();
      } else {
        createResponse =
            await Provider.of<LoansProvider>(context, listen: false)
                .createLoanRequest();
      }

      if (createResponse["status"] == 200 || createResponse["status"] == 201) {
        if (this.mounted) {
          setState(() {
            isSms == true ? isSmsSending = false : isWhatsappSending = false;
          });
        }
        if (widget.isRequestPayment) {
          GeneralProvider.successPageMessage =
              "Your payment request has been created successfully.";
         
        } else {
          GeneralProvider.successPageMessage =
              "Your loan request has been created successfully.";
        }
        if (!isSms) {
          await whatsappHandler();
          Future.delayed(Duration(milliseconds: 300), () {
            widget.switchTo();
          });
        } else {
          // if (Platform.isIOS) {
          var phone = HelperFunctions.serializePhoneNumber(
              phoneNumber: contact.phoneNumber);
          var message = smsMessageText.replaceAll(" ", "%20");
          var url;
          if (Platform.isAndroid) {
            url = 'sms:$phone?body=$message';
          } else {
            url = 'sms:$phone&body=$message';
          }
          if (await canLaunch('sms:$phone')) {
            await launch(url, forceSafariVC: Platform.isIOS ? false : null);
          } else {
            CustomSnackBar.snackBar(
              title: "Cannot open SMS App",
              scaffoldState: _scaffoldKey,
              context: context,
            );
          }
          // }
          widget.switchTo();
        }
      } else {
        if (this.mounted) {
          setState(() {
            isSms == true ? isSmsSending = false : isWhatsappSending = false;
            CustomSnackBar.snackBar(
              title: "Something went wrong, please try again.",
              scaffoldState: _scaffoldKey,
              context: context,
            );
          });
        }
      }
    }

    void onWhatsappClicked() {
      if (this.mounted) {
        setState(() {
          isWhatsappSending = true;
        });
      }
      sendRequest(isSms: false);
    }

    void onSmsClicked() {
      if (this.mounted) {
        setState(() {
          isSmsSending = true;
        });
      }
      sendRequest(isSms: true);
    }

    // void onSmsClicked({SimCard simCard}) {
    //   if (this.mounted) {
    //     setState(() {
    //       isSmsSending = true;
    //     });
    //   }
    //   if (Platform.isIOS) {
    //     sendRequest(isSms: true);
    //   } else {
    //     HelperFunctions.sendSms(
    //       recipientPhone: contact.phoneNumber,
    //       selectedSimCard: simCard,
    //       messageText: widget.isRequestPayment
    //           ? "Hello, ${userData["first_name"]} has made a payment request from you on PeerScore, kindly respond to it${contact.id != null ? ", thanks." : " on bit.ly/2E9PZOm"}"
    //           : "Hello,${userData["first_name"]} has requested a loan from you on peer-score, kindly respond to it${contact.id != null ? ", thanks." : " on bit.ly/2E9PZOm"}",
    //       smsCb: (isSent) {
    //         if (!isSent) {
    //           if (this.mounted) {
    //             setState(() {
    //               CustomSnackBar.snackBar(
    //                 title: "Sms not sent, please try again.",
    //                 scaffoldState: _scaffoldKey,
    //                 context: context,
    //               );
    //               isSmsSending = false;
    //             });
    //           }
    //         } else {
    //           sendRequest(isSms: true);
    //         }
    //       },
    //     );
    //   }
    // }

    final deiviceSize = MediaQuery.of(context).size;
    final height = deiviceSize.height -
        MediaQuery.of(context).padding.top -
        Provider.of<GeneralProvider>(context, listen: false).getBottomNavHeight;

    var appColors = Provider.of<AppColorsProvider>(context, listen: false);
    return Container(
      height: height,
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(vertical: 15),
            child: MainNavBar(
              leadingImagePath: 'public/images/arrow-back.png',
              notificationImagePath: 'public/images/notification.png',
              // subNav: true,
              leadingPressAction: widget.navBarLeadingFn,
            ),
          ),
          Spacer(),
          Expanded(
            flex: 4,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: AutoSizeText(
                            "One last thing...",
                            style: Theme.of(context).textTheme.body2.copyWith(
                                  color: Color(
                                    0xFF40976D,
                                  ),
                                  fontSize: 24,
                                ),
                          ),
                        ),

                        Expanded(
                          flex: 2,
                          child: ConstrainedBox(
                            constraints: BoxConstraints(
                              maxHeight: 127,
                              maxWidth: 127,
                            ),
                            child: Image.asset("public/images/next-page.png"),
                          ),
                        ),

                        // SizedBox(
                        //   height: 127,
                        //   width: 127,
                        //   child: Image.asset("public/images/next-page.png"),
                        // ),
                        // ConstrainedBox(
                        //   constraints: BoxConstraints(
                        //     minWidth: 5,
                        //     maxHeight: 40,
                        //   ),
                        // ),
                        // SizedBox(height: 40),
                        Expanded(
                          flex: 3,
                          child: Container(
                            padding: EdgeInsets.symmetric(horizontal: 40),
                            alignment: Alignment.center,
                            child: AutoSizeText(
                              widget.isRequestPayment
                                  ? "Your payment request has been prepared, send a message to your defaulter letting them know you have requested payment of your loan on PeerScore."
                                  : "Your loan request has been prepared, send a message to your creditor letting them know you have requested a loan on PeerScore.",
                              style: Theme.of(context).textTheme.body2.copyWith(
                                    color: GeneralProvider.isLightMode
                                        ? Color(
                                            0xFF6766A5,
                                          )
                                        : Colors.white,
                                    fontWeight: FontWeight.normal,
                                  ),
                              minFontSize: 10,
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),

                  // SizedBox(height: 150),
                ],
              ),
            ),
          ),
          Spacer(),
          Expanded(
            flex: 3,
            child: Container(
              padding: EdgeInsets.symmetric(
                horizontal: 30,
              ),
              width: double.infinity,
              child: ListView(
                // crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  CustomUploadButton(
                    title: isSmsSending ? "" : "SendSMS",
                    height: 40,
                    width: double.infinity,
                    style: Theme.of(context).textTheme.body1.copyWith(
                        color: appColors.primaryButtonColorText,
                        fontWeight: FontWeight.w600),
                    leadingIcon: FontAwesomeIcons.lightCommentsAlt,
                    bgColor: appColors.primaryButtonColorBg,
                    indicator:
                        isSmsSending ? CustomProgressIndicator() : Text(""),
                    onClick: () => isSmsSending ? null : onSmsClicked(),
                  ),

                  // if (Platform.isAndroid)
                  //   InkWell(
                  //     onTap: () => ModalActions.changeSIm(
                  //         context: context,
                  //         onSelect: (simCard) =>
                  //             onSmsClicked(simCard: simCard)),
                  //     child: Align(
                  //       alignment: Alignment.topRight,
                  //       child: Container(
                  //         padding: EdgeInsets.all(5),
                  //         child: Text(
                  //           "Use another sim",
                  //           style: Theme.of(context).textTheme.body1.copyWith(
                  //                 color: Theme.of(context).primaryColor,
                  //                 fontWeight: FontWeight.w600,
                  //               ),
                  //         ),
                  //       ),
                  //     ),
                  //   ),
                  SizedBox(height: 5),
                  if (contact.isWhatsApp)
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Divider(
                            thickness: GeneralProvider.isLightMode ? 1 : 1.5,
                            color: !GeneralProvider.isLightMode
                                ? Colors.grey[850]
                                : null,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 5),
                          child: Text(
                            "OR",
                            style: Theme.of(context)
                                .textTheme
                                .body2
                                .copyWith(fontWeight: FontWeight.w600),
                          ),
                        ),
                        Expanded(
                          child: Divider(
                            thickness: GeneralProvider.isLightMode ? 1 : 1.5,
                            color: !GeneralProvider.isLightMode
                                ? Colors.grey[850]
                                : null,
                          ),
                        ),
                      ],
                    ),
                  if (contact.isWhatsApp) SizedBox(height: 5),
                  if (contact.isWhatsApp)
                    CustomUploadButton(
                      title: isWhatsappSending ? "" : "WhatsApp",
                      height: 40,
                      width: double.infinity,
                      style: Theme.of(context).textTheme.body1.copyWith(
                          color: Colors.white, fontWeight: FontWeight.w600),
                      leadingIcon: FontAwesomeIcons.whatsapp,
                      indicator: isWhatsappSending
                          ? CustomProgressIndicator()
                          : Text(""),
                      bgColor: Color(0xff77E77B),
                      onClick: () =>
                          isWhatsappSending ? null : onWhatsappClicked(),
                    ),

                  // ConstrainedBox(
                  //   constraints: BoxConstraints(
                  //     minHeight: 20,
                  //     maxHeight: 150,
                  //     maxWidth: 150,
                  //   ),
                  // ),
                ],
              ),
            ),
          ),
          Spacer(),
        ],
      ),
    );
  }
}
