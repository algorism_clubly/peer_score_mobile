import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_contact/contacts.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:peer_score/providers/contact.dart' as UserContact;
import 'package:flutter/services.dart';
import 'package:peer_score/providers/loans-provider.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/providers/user-provider.dart';
import 'package:peer_score/utils/custom_raised_button.dart';
import 'package:peer_score/utils/custom_switch.dart';
import 'package:peer_score/utils/enums.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:peer_score/utils/main_nav_bar.dart';
import 'package:peer_score/utils/modal_actions.dart';
import 'package:peer_score/utils/progress_indicator.dart';
import 'package:peer_score/utils/snackbar.dart';
import 'package:peer_score/utils/text_form_field.dart';
import 'package:peer_score/widgets/contacts/contact_list.dart';
import 'package:peer_score/widgets/contacts/contact_util.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';

class SelectContactPage extends StatefulWidget {
  final Function switchTo;
  final Function navBarLeadingFn;
  final String header;
  final String subHeader;
  SelectContactPage(
      {this.switchTo, this.navBarLeadingFn, this.header, this.subHeader});

  @override
  _SelectContactPageState createState() => _SelectContactPageState();
}

class _SelectContactPageState extends State<SelectContactPage> {
  List<String> contactPhones = [];
  List<String> peerScorePhones = [];
  FocusNode _searchFocusNode;
  List<UserContact.Contact> contactListCopy = [];
  List<UserContact.Contact> contactList = [];
  List<UserContact.Contact> currentFilterdList = [];
  GlobalKey<ScaffoldState> _scaffoldKey;
  bool isLoading = false;
  bool pullRefresh = false;
  bool noResultFound = false;
  bool permissionDenied = false;
  Map<String, dynamic> userData = {"first_name": ""};
  bool autoSyncValue = false;

  String optionalFilterMessage;
  List<Map<String, dynamic>> filterOptions = [
    {
      "type": ContactlistFilterOptions.all,
      "title": "All",
    },
    {
      "type": ContactlistFilterOptions.peerScoreMembers,
      "title": "PeerScore members",
    },
    {
      "type": ContactlistFilterOptions.nonPeerScoreMembers,
      "title": "Non PeerScore members",
    },
  ];
  int _selectedFilterValue = 0;
  List<Map<String, dynamic>> sortOptions = [
    {
      "type": ContactlistSortOptions.complianceAsc,
      "title": "Lowest(Compliance)",
    },
    {
      "type": ContactlistSortOptions.complianceDesc,
      "title": "Highest(Compliance)",
    }
  ];

  int _selectedSortValue = 1;

  @override
  void initState() {
    super.initState();
    getAutoSyncValue();
    BackButtonInterceptor.add(myInterceptor);
    _searchFocusNode = FocusNode();
    GeneralProvider.currentFocusedNode = _searchFocusNode;
    _scaffoldKey =
        Provider.of<GeneralProvider>(context, listen: false).getAppScaffoldKey;
    if (this.mounted) {
      setState(() {
        isLoading = true;
      });
    }
    Provider.of<UserProvider>(context, listen: false).userData.then((data) {
      if (this.mounted) {
        setState(() {
          userData = data;
        });
      }
      refreshContacts();
    });
    // contactList = [
    //   ...Provider.of<UserContact.ContactProvider>(context, listen: false)
    //       .contacts
    // ];
    // contactListCopy = [...contactList];
  }

  void getAutoSyncValue() async {
    autoSyncValue = await HelperFunctions.getAutoSyncValue();
    setState(() {});
  }

  bool myInterceptor(bool stopDefaultButtonEvent) {
    if (stopDefaultButtonEvent) return false;
    widget.navBarLeadingFn();
    return true;
  }

  @override
  void dispose() {
    BackButtonInterceptor.remove(myInterceptor);
    super.dispose();
  }

  refreshContacts() async {
    if (this.mounted) {
      if (this.mounted) {
        setState(() {
          isLoading = true;
          _selectedFilterValue = 0;
          _selectedSortValue = 1;
          permissionDenied = false;
        });
      }
      Map<String, dynamic> contactsResponse =
          await PhoneContacts.getPhoneContacts(
              context: context,
              userId: userData["id"],
              isAutoSync: autoSyncValue);
      if (contactsResponse["status"]) {
        if (this.mounted) {
          setState(() {
            contactList = contactsResponse["contactList"];
            contactListCopy = [...contactList];
            isLoading = false;
          });

          _handleInvalidPermissions(contactsResponse["permissionStatus"]);
        }
      } else if (contactsResponse["status"] == false) {
        if (this.mounted) {
          setState(() {
            isLoading = false;
            // permissionDenied = true;
            CustomSnackBar.snackBar(
              title: "Something went wrong, please try again.",
              scaffoldState: _scaffoldKey,
              context: context,
            );
          });
        }
      }
    }
  }

  void _handleInvalidPermissions(PermissionStatus permissionStatus) {
    if (this.mounted && autoSyncValue == true) {
      setState(() {
        isLoading = false;
        permissionDenied = false;
        if (permissionStatus == PermissionStatus.permanentlyDenied) {
          ModalActions.normalConfirmationDialog(
            title: "Contact permission denied",
            isColumnActions: true,
            firstActionText: "Grant permission",
            secondActionText: "Continue without permission",
            context: context,
            content: "Contact permission has been denied for auto sync feature",
            onConfirm: () {
              setState(() {
                autoSyncValue = false;
                HelperFunctions.setAutoSync(value: false);
              });
            },
            onUnconfirm: () => openAppSettings(),
          );
        }
      });
    }
  }

  Widget buildContactListTile(
      {BuildContext context, int index, Function onPressed}) {
    bool isLightMode = GeneralProvider.isLightMode;
    return InkWell(
      onTap: () {
        Provider.of<LoansProvider>(context, listen: false)
            .setContact(contact: contactListCopy[index]);
        onPressed();
      },
      child: Column(
        children: <Widget>[
          ListTile(
            contentPadding: EdgeInsets.zero,
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  HelperFunctions.getContactFullName(
                      contact: contactListCopy[index]),
                  style: Theme.of(context).textTheme.body2.copyWith(
                        fontWeight: FontWeight.normal,
                      ),
                ),
                Text(
                  "${contactListCopy[index].id == null ? "-" : contactListCopy[index].compliance}",
                  style: Theme.of(context).textTheme.body1.copyWith(
                        fontWeight: FontWeight.bold,
                      ),
                )
              ],
            ),
            subtitle: Row(
              children: <Widget>[
                Text(
                  contactListCopy[index].phoneNumber,
                  style: Theme.of(context).textTheme.body1.copyWith(
                        fontWeight: FontWeight.normal,
                        height: 1.33,
                        color: GeneralProvider.isLightMode
                            ? Color(0xFF797979)
                            : Colors.white70,
                      ),
                ),
                SizedBox(
                  width: 5,
                ),
                if (contactListCopy[index].id != null)
                  HelperFunctions.buildUserBadge(context: context)
              ],
            ),
          ),
          Divider(
            thickness: 1.2,
            height: 0.0,
            color: !GeneralProvider.isLightMode ? Colors.grey[850] : null,
          )
        ],
      ),
    );
  }

  void searchContactList({String value}) {
    List<UserContact.Contact> loanListToSearch =
        _selectedFilterValue == 0 ? contactList : currentFilterdList;
    if (loanListToSearch.length == 0) return;
    // setState(() {
    //   _selectedFilterValue = 0;
    //   _selectedSortValue = 1;
    //   optionalFilterMessage = null;
    // });

    final _contactList = HelperFunctions.searchContacts(
      searchValue: value,
      contactList: loanListToSearch,
    );
    if (this.mounted) {
      setState(() {
        contactListCopy = _contactList;
        _contactList.length == 0 ? noResultFound = true : noResultFound = false;
      });
    }
  }

  void filterList({int index, BuildContext context}) async {
    if (contactList.length == 0) return;
    await PhoneContacts.filterContacts(
        type: filterOptions[index]["type"],
        contactList: contactList,
        sortType: sortOptions[_selectedSortValue]["type"],
        resultCb: (status, filteredList, message) {
          if (status) {
            
            
            if (this.mounted) {
              setState(() {
                _selectedFilterValue = index;
                optionalFilterMessage = message;
                contactListCopy = filteredList;
                currentFilterdList = filteredList;
                filteredList.length == 0
                    ? noResultFound = true
                    : noResultFound = false;
              });
            }
          }
        });
  }

  void sortList({int index}) async {
    if (contactList.length == 0) return;
    setState(() {
      _selectedSortValue = index;
    });
    final _sortedList = PhoneContacts.sortContactList(
      type: sortOptions[index]["type"],
      contactList: contactListCopy,
    );
    if (this.mounted) {
      setState(() {
        contactListCopy = _sortedList;
        _sortedList.length == 0 ? noResultFound = true : noResultFound = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Container(
          padding: EdgeInsets.symmetric(vertical: 30),
          child: MainNavBar(
            title: widget.header,
            leadingImagePath: 'public/images/arrow-back.png',
            notificationImagePath: 'public/images/notification.png',
            // subNav: true,
            leadingPressAction: () {
              // _searchFocusNode.unfocus();
              widget.navBarLeadingFn();
            },
          ),
        ),
        Expanded(
          child: Container(
              // padding: EdgeInsets.symmetric(horizontal: 30),
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              // Container(
              //   margin: EdgeInsets.only(bottom: 25),
              //   child: Text(
              //     widget.header,
              //     style: Theme.of(context).textTheme.body1.copyWith(
              //           fontSize: 12,
              //           fontWeight: FontWeight.w600,
              //           height: 12 / 16,
              //         ),
              //   ),
              // ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 30),
                margin: EdgeInsets.only(bottom: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      "Sync contacts",
                      style: Theme.of(context).textTheme.body1.copyWith(
                            fontWeight: FontWeight.w600,
                          ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    CustomSwitch(
                      value: autoSyncValue,
                      height: 15,
                      onClick: (value) {
                        setState(() {
                          autoSyncValue = value;
                          HelperFunctions.setAutoSync(value: value);
                          if (value == true) refreshContacts();
                        });
                      },
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: CustomTextField(
                  focusNode: _searchFocusNode,
                  filled: true,
                  iconPath: 'public/images/search.png',
                  hintText: "Search contact",
                  onChanged: (value) => searchContactList(value: value),
                ),
              ),
              HelperFunctions.buildFilterPanel(
                context: context,
                optionalFilterMessage: optionalFilterMessage,
                filterList: filterList,
                sortList: sortList,
                filterOptions:
                    filterOptions.map((option) => option['title']).toList(),
                sortOptions:
                    sortOptions.map((option) => option['title']).toList(),
                selectedSortValue: _selectedSortValue,
                selectedFilterValue: _selectedFilterValue,
              ),
              SizedBox(height: widget.subHeader == null ? 12 : 25),
              if (widget.subHeader != null)
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  child: Text(
                    widget.subHeader,
                    style: Theme.of(context).textTheme.body1.copyWith(
                          fontSize: 10,
                          fontWeight: FontWeight.bold,
                          height: 0.7,
                        ),
                  ),
                ),
              Expanded(
                child: Container(
                  // height: 430,
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  child: permissionDenied
                      ? Center(
                          child: CustomRaisedButton(
                            onTapAction: refreshContacts,
                            title: "Import contacts",
                          ),
                        )
                      : HelperFunctions.renderListItemsAndRefresh(
                          context: context,
                          pullRefresh: pullRefresh,
                          isLoading: isLoading,
                          refresh: () {
                            if (this.mounted) {
                              setState(() {
                                pullRefresh = true;
                              });
                            }
                            return refreshContacts();
                          },
                          itemsCount: contactListCopy.length,
                          itemsBuilder: ListView.builder(
                            shrinkWrap: true,
                            itemCount: contactListCopy.length,
                            itemBuilder: (ctx, index) => buildContactListTile(
                              context: ctx,
                              index: index,
                              onPressed: widget.switchTo,
                            ),
                          ),
                          emptyListIconPath:
                              "public/images/empty-lists/${noResultFound ? GeneralProvider.isLightMode ? "no-results" : "no-results-white" : "contact"}.png",
                          emptyListText:
                              "${noResultFound ? "No debtor matches the search criteria" : "You have no debtors at this time"}",
                        ),
                ),
              ),
              // SizedBox(height: 30),
              // CustomRaisedButton(
              //   title: "Next",
              //   onTapAction: switchTo,
              // ),
            ],
          )),
        ),
      ],
    );
  }
}
