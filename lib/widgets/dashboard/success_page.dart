import 'package:auto_size_text/auto_size_text.dart';
import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/utils/main_nav_bar.dart';
import 'package:provider/provider.dart';

class SuccessPage extends StatefulWidget {
  final Function navBarLeadingFn;
  final Function switchMainPage;
  final bool isRequestPayment;
  final String successMessage;
  final String successSms;
  SuccessPage(
      {this.navBarLeadingFn,
      this.isRequestPayment = true,
      this.successSms,
      this.switchMainPage,
      this.successMessage});

  @override
  _SuccessPageState createState() => _SuccessPageState();
}

class _SuccessPageState extends State<SuccessPage> {
  @override
  void initState() {
    BackButtonInterceptor.add(myInterceptor);
    super.initState();
  }

  bool myInterceptor(bool stopDefaultButtonEvent) {
    if (stopDefaultButtonEvent) return false;
    widget.switchMainPage(0);
    return true;
  }

  @override
  void dispose() {
    BackButtonInterceptor.remove(myInterceptor);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final deiviceSize = MediaQuery.of(context).size;
    return Container(
      height: deiviceSize.height -
          MediaQuery.of(context).padding.top -
          Provider.of<GeneralProvider>(context, listen: false)
              .getBottomNavHeight,
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(vertical: 30),
            child: MainNavBar(
              leadingImagePath: 'public/images/navhome.png',
              notificationImagePath: 'public/images/notification.png',
              // subNav: true,
              leadingPressAction: () => widget.switchMainPage(0),
            ),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Column(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            "Successful",
                            style: Theme.of(context).textTheme.body2.copyWith(
                                  color: Color(
                                    0xFF40976D,
                                  ),
                                  fontSize: 24,
                                ),
                          ),
                        ),
                        Expanded(
                          child: SizedBox(
                            height: 127,
                            width: 127,
                            child: Image.asset("public/images/success.png"),
                          ),
                        ),
                        SizedBox(height: 25),
                        Expanded(
                          flex: 3,
                          child: Container(
                              padding: EdgeInsets.symmetric(horizontal: 40),
                              child: AutoSizeText(
                                GeneralProvider.successPageMessage,
                                style:
                                    Theme.of(context).textTheme.body2.copyWith(
                                          color: GeneralProvider.isLightMode
                                              ? Color(
                                                  0xFF6766A5,
                                                )
                                              : Colors.white,
                                          fontWeight: FontWeight.normal,
                                        ),
                                textAlign: TextAlign.center,
                              )),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          // Spacer(),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 30, vertical: 40),
            child: InkWell(
              customBorder: CircleBorder(),
              onTap: () => widget.switchMainPage(0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Go home",
                    style: Theme.of(context).textTheme.body2.copyWith(
                          color: Theme.of(context).accentColor,
                        ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
