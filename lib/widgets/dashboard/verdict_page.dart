import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:peer_score/providers/contact.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/providers/witness_provider.dart';
import 'package:peer_score/utils/custom_raised_button.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:peer_score/utils/main_nav_bar.dart';
import 'package:peer_score/utils/progress_indicator.dart';
import 'package:peer_score/utils/snackbar.dart';
import 'package:provider/provider.dart';

class VerdictPage extends StatefulWidget {
  final Function navBarLeadingFn;
  final Function switchTo;
  VerdictPage({this.navBarLeadingFn, this.switchTo});
  @override
  _VerdictPageState createState() => _VerdictPageState();
}

class _VerdictPageState extends State<VerdictPage> {
  Claim currentClaim;
  bool falseVerdict = false;
  bool trueVerdict = false;
  bool unawareVerdict = false;
  bool isLoading = false;
  GlobalKey<ScaffoldState> _scaffoldKey;
  @override
  void initState() {
    super.initState();
    BackButtonInterceptor.add(myInterceptor);
    getClaim();
  }

  bool myInterceptor(bool stopDefaultButtonEvent) {
    if (stopDefaultButtonEvent) return false;
    widget.navBarLeadingFn();
    return true;
  }

  @override
  void dispose() {
    BackButtonInterceptor.remove(myInterceptor);
    super.dispose();
  }

  getClaim() {
    if (this.mounted) {
      setState(() {
        currentClaim = WitnesProvider.selectedClaim;
      });
    }
  }

  onSubmit({String verdict}) async {
    if (this.mounted) {
      setState(() {
        isLoading = true;
        switch (verdict) {
          case "true":
            trueVerdict = true;
            break;
          case "false":
            falseVerdict = true;
            break;
          case "unaware":
            unawareVerdict = true;
            break;
          default:
        }
      });
    }
    final response = await Provider.of<WitnesProvider>(context, listen: false)
        .passVerdict(verdict: verdict);
    if (response["status"] == 200 || response["status"] == 201) {
      if (this.mounted) {
        setState(() {
          falseVerdict = false;
          trueVerdict = false;
          unawareVerdict = false;
          isLoading = false;
        });
      }
      GeneralProvider.successPageMessage =
          "Your verdict has been passed successfully, thank you.";
      widget.switchTo();
    } else {
      if (this.mounted) {
        setState(() {
          isLoading = false;
          CustomSnackBar.snackBar(
            title: "Something went wrong, please reload the page",
            scaffoldState: _scaffoldKey,
            context: context,
          );
        });
      }
    }
  }

  Widget _buildWitnessListTile({
    Contact contact,
  }) {
    return Container(
      // height: 60,
      // padding: EdgeInsets.only(right: 35),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          // ClipRRect(
          //   child: Container(
          //     height: 80,
          //     decoration: BoxDecoration(shape: BoxShape.circle),
          //     child: Image.network(contact.profilePicture),
          //   ),
          // ),
          // CircleAvatar(
          //   radius: 30,
          //   backgroundImage: NetworkImage(contact.profilePicture),
          // ),
          Container(
            alignment: Alignment.bottomCenter,
            constraints: BoxConstraints(
              minHeight: 50,
              minWidth: 50,
              maxHeight: 80,
              maxWidth: 80,
            ),
            child: AspectRatio(
              aspectRatio: 1,
              child: ClipOval(
                child: FadeInImage(
                  placeholder: AssetImage(
                      "public/images/profile-picture-placeholder.png"),
                  image: NetworkImage(
                      contact.id == null ? "" : contact.profilePicture),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          Spacer(
            flex: 2,
          ),
          Expanded(
            flex: 4,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  HelperFunctions.getContactFullName(contact: contact),
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.body2.copyWith(
                        fontWeight: FontWeight.normal,
                        height: 1.36,
                      ),
                ),
                Row(
                  children: <Widget>[
                    Text(
                      contact.phoneNumber,
                      style: Theme.of(context).textTheme.body1.copyWith(
                          fontWeight: FontWeight.normal,
                          // height: 1.33,
                          color: GeneralProvider.isLightMode
                              ? Color(0xFF797979)
                              : Colors.white70),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    if (contact.id != null)
                      HelperFunctions.buildUserBadge(context: context)
                  ],
                ),
              ],
            ),
          ),
          // Spacer(),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (this.mounted) {
      setState(() {
        currentClaim = WitnesProvider.selectedClaim;
      });
    }
    final deiviceSize = MediaQuery.of(context).size;
    return Container(
      height: deiviceSize.height -
          MediaQuery.of(context).padding.top -
          Provider.of<GeneralProvider>(context, listen: false)
              .getBottomNavHeight,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(vertical: 30),
            child: MainNavBar(
              title: "Pass verdict",
              leadingImagePath: 'public/images/arrow-back.png',
              notificationImagePath: 'public/images/notification.png',
              // subNav: true,
              leadingPressAction: widget.navBarLeadingFn,
            ),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Expanded(
                    // flex: 2,
                    child: _buildWitnessListTile(
                      contact: currentClaim.winessProvider,
                    ),
                  ),
                  SizedBox(height: 15),
                  Divider(
                    thickness: GeneralProvider.isLightMode ? 1.2 : 1.5,
                    height: 0.0,
                    color:
                        !GeneralProvider.isLightMode ? Colors.grey[850] : null,
                  ),
                  SizedBox(height: 15),
                  Text(
                    currentClaim.type == "insist"
                        ? "Has insisted"
                        : "Has denied owing",
                    style: Theme.of(context).textTheme.body2.copyWith(
                          fontWeight: FontWeight.normal,
                          height: 1.36,
                        ),
                  ),
                  SizedBox(height: 25),
                  Expanded(
                    // flex: 2,
                    child: Container(
                      alignment: Alignment.bottomCenter,
                      child: _buildWitnessListTile(
                        contact: currentClaim.claimInitiator,
                      ),
                    ),
                  ),
                  // SizedBox(height: 55),
                  Spacer(flex: 1),
                  Container(
                    padding: EdgeInsets.only(right: 78),
                    child: Row(
                      children: <Widget>[
                        Text(
                          currentClaim.type == "insist"
                              ? "Is owing the amount of"
                              : "The amount of",
                          style: Theme.of(context).textTheme.body2.copyWith(
                              fontWeight: FontWeight.normal, height: 1.36),
                        ),
                        Spacer(),
                        Text(
                          HelperFunctions.getFormattedAmount(
                              amount: currentClaim.loan.amount),
                          style: Theme.of(context).textTheme.body1.copyWith(
                                fontWeight: FontWeight.bold,
                                height: 1.33,
                              ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 15),
                  Divider(
                    thickness: GeneralProvider.isLightMode ? 1.2 : 1.5,
                    height: 0.0,
                    color:
                        !GeneralProvider.isLightMode ? Colors.grey[850] : null,
                  ),
                  Spacer(flex: 1),
                  SizedBox(height: 30),
                  Text(
                    "How would you like to answer?",
                    style: Theme.of(context).textTheme.body2.copyWith(
                          fontWeight: FontWeight.normal,
                          height: 1.36,
                        ),
                  ),
                  Spacer(
                    flex: 1,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        child: CustomRaisedButton(
                          title: "false",
                          bgColor: Theme.of(context).primaryColor,
                          // bgColor: Color(0xFFE78200),
                          onTapAction: () =>
                              isLoading ? () {} : onSubmit(verdict: "false"),
                          indicator: isLoading && falseVerdict
                              ? CustomProgressIndicator()
                              : Text(""),
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: CustomRaisedButton(
                          title: "unaware",
                          bgColor: Color(0xFFA3A3A3),
                          onTapAction: () =>
                              isLoading ? () {} : onSubmit(verdict: "unaware"),
                          indicator: isLoading && unawareVerdict
                              ? CustomProgressIndicator()
                              : Text(""),
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: CustomRaisedButton(
                          title: "true",
                          bgColor: Theme.of(context).primaryColor,
                          onTapAction: () =>
                              isLoading ? () {} : onSubmit(verdict: "true"),
                          indicator: isLoading && trueVerdict
                              ? CustomProgressIndicator()
                              : Text(""),
                        ),
                      )
                    ],
                  ),
                  Spacer()
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
