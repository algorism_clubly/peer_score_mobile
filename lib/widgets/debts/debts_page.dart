import 'dart:async';

import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'dart:math' as math;
import 'package:intl/intl.dart';
import 'package:peer_score/providers/bank_accounts_provider.dart';
import 'package:peer_score/providers/contact.dart';
import 'package:peer_score/providers/debts_provider.dart';
import 'package:peer_score/providers/expected_payments_provider.dart';
import 'package:peer_score/providers/payment_provider.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/providers/user-provider.dart';
import 'package:peer_score/screens/profile_screen.dart';
import 'package:peer_score/utils/custom_raised_button.dart';
import 'package:peer_score/utils/enums.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:peer_score/utils/main_nav_bar.dart';
import 'package:peer_score/utils/modal_actions.dart';
import 'package:peer_score/utils/notification_view_singleton.dart';
import 'package:peer_score/utils/progress_indicator.dart';
import 'package:peer_score/utils/snackbar.dart';
import 'package:peer_score/utils/text_form_field.dart';
import 'package:provider/provider.dart';

class DebtsPage extends StatefulWidget {
  final String title;
  final String action;
  final Color actionColor;
  final Function switchTo;
  final CreditorsStackEnum type;
  DebtsPage(
      {this.action, this.title, this.actionColor, this.switchTo, this.type});
  @override
  _DebtsPageState createState() => _DebtsPageState();
}

class _DebtsPageState extends State<DebtsPage> {
  NotificationViewSingleton notificationViewSingleton =
      NotificationViewSingleton.getInstance();
  StreamSubscription notificationViewSub;
  ScrollController _scrollController;
  List<Loan> debts = [];
  List<Loan> debtsCopy = [];
  List<Loan> currentFilterdList = [];
  Loan loan;
  GlobalKey<ScaffoldState> _scaffoldKey;
  bool isLoading = true;
  bool pullRefresh = false;
  bool isOverLayLoading = false;
  String _selectedItemId;
  Map<String, dynamic> userData = {
    "id": "",
    "first_name": "",
    "profile_picture": "",
    "compliance": 0
  };
  bool noResultFound = false;

  String optionalFilterMessage;
  List<Map<String, dynamic>> filterOptions = [
    {
      "type": LoanlistFilterOptions.all,
      "title": "All",
    },
    {
      "type": LoanlistFilterOptions.paid,
      "title": "Paid loans",
    },
    {
      "type": LoanlistFilterOptions.overdue,
      "title": "Overdue loans",
    },
    {
      "type": LoanlistFilterOptions.reschedule,
      "title": "Reschedule requests",
    },
    // {
    //   "type": LoanlistFilterOptions.createdRange,
    //   "title": "Created date range",
    // },
    {
      "type": LoanlistFilterOptions.dueRange,
      "title": "Due date range",
    },
  ];
  int _selectedFilterValue = 0;
  List<Map<String, dynamic>> sortOptions = [
    {
      "type": LoanlistSortOptions.duedateAsc,
      "title": "Oldest(Due date)",
    },
    {
      "type": LoanlistSortOptions.duedateDesc,
      "title": "Newest(Due date)",
    },
    {
      "type": LoanlistSortOptions.amountAsc,
      "title": "Lowest(Amount)",
    },
    {
      "type": LoanlistSortOptions.amountDesc,
      "title": "Highest(Amount)",
    },
    // {
    //   "type": LoanlistSortOptions.createdDateAsc,
    //   "title": "Created date ASC",
    // },
    // {
    //   "type": LoanlistSortOptions.createdDateDesc,
    //   "title": "Created date DESC",
    // },
  ];
  int _selectedSortValue = 0;

  FocusNode searchFocusNode;
  @override
  void initState() {
    searchFocusNode = FocusNode();
    _scrollController = ScrollController(keepScrollOffset: false);
    GeneralProvider.currentFocusedNode = searchFocusNode;
    BackButtonInterceptor.add(myInterceptor);
    super.initState();
    _scaffoldKey =
        Provider.of<GeneralProvider>(context, listen: false).getAppScaffoldKey;

    DebtsProviver.debtsListCb = getLoansList;
    Provider.of<UserProvider>(context, listen: false).userData.then((data) {
      if (this.mounted) {
        setState(() {
          userData = data;
        });
      }
    });
    getLoansList().then((_) {
      SchedulerBinding.instance.addPostFrameCallback((_) {
        notificationViewSub =
            notificationViewSingleton.notificationViewSubject.listen((value) {
          HelperFunctions.handlePostFrameEventFromNotificationForListItems(
              widgetType: widget.type,
              items: debtsCopy,
              executable: onOutlineClicked,
              scrollController: _scrollController);
        });
      });
    });
  }

  bool myInterceptor(bool stopDefaultButtonEvent) {
    if (stopDefaultButtonEvent) return false;
    GeneralProvider.switchMainPage(index: 0);
    return true;
  }

  @override
  void dispose() {
    BackButtonInterceptor.remove(myInterceptor);
    notificationViewSub.cancel();
    super.dispose();
  }

  Future<void> getLoansList() async {
    if (this.mounted) {
      if (this.mounted) {
        setState(() {
          isLoading = true;
          _selectedFilterValue = 0;
          _selectedSortValue = 0;
          optionalFilterMessage = null;
        });
      }
      final listResponse =
          await Provider.of<DebtsProviver>(context, listen: false).getDebts();
      if (listResponse["status"] == 200 || listResponse["status"] == 201) {
        if (this.mounted) {
          setState(() {
            debts = [
              ...Provider.of<DebtsProviver>(context, listen: false).debts
            ];
            debtsCopy = [...debts];
            isLoading = false;
            pullRefresh = false;
          });
          // HelperFunctions.handlePostFrameEventFromNotificationForListItems(
          //     widgetType: widget.type,
          //     items: debtsCopy,
          //     executable: onOutlineClicked,
          //     scrollController: _scrollController);
        }
      } else {
        if (this.mounted) {
          setState(() {
            isLoading = false;
            pullRefresh = false;
            CustomSnackBar.snackBar(
              title: "Something went wrong, please reload the page",
              scaffoldState: _scaffoldKey,
              context: context,
            );
          });
        }
      }
    }
  }

  void payOnAppCb({String receiverBankAccountId}) async {
    if (this.mounted) {
      setState(() {
        isOverLayLoading = true;
      });
    }

    HelperFunctions.payWithPaystack(
      context: context,
      isLoanRequest: false,
      loan: loan,
      userData: userData,
      initializingFailedCb: (err) {
        if (this.mounted) {
          setState(() {
            isOverLayLoading = false;
          });
        }
      },
      onPayStackResponse: (success) {
        if (this.mounted) {
          setState(() {
            isOverLayLoading = false;
          });
        }

        if (success) {
          GeneralProvider.successPageMessage =
              "The loan of ${HelperFunctions.getFormattedAmount(amount: loan.amount)} provided by ${HelperFunctions.getContactFullName(contact: loan.creditor)} has been successfully refunded, the loan is now complete.";
          if (this.mounted) {
            setState(() {
              isOverLayLoading = false;
            });
          }
          widget.switchTo("success");
        }
      },
    );
  }

  Future<bool> checkRecipientAccDetails() async {
    if (this.mounted) {
      setState(() {
        isOverLayLoading = true;
      });
    }
    final response =
        await Provider.of<BankAccountProvider>(context, listen: false)
            .getActiveBankAccount(body: {"userId": loan.creditor.id});
    if (response["status"] == 200 || response["status"] == 201) {
      if (this.mounted) {
        setState(() {
          isOverLayLoading = false;
        });
      }
      return true;
    } else {
      if (this.mounted) {
        setState(() {
          isOverLayLoading = false;
        });
      }
      return false;
    }
  }

  Future onOutlineClicked(int index) async {
    setState(() {
      _selectedItemId = debtsCopy[index].id;
    });
    // return () async {
    DebtsProviver.debtsListCb = getLoansList;
    DebtsProviver.selectedDebt = debtsCopy[index];
    loan = debtsCopy[index];
    Provider.of<GeneralProvider>(context, listen: false)
        .clearSelectedContacts();
    Provider.of<GeneralProvider>(context, listen: false).clearSelectedFiles();
    if (debtsCopy[index].loanActions == null) {
      bool hasActiveAccount = await checkRecipientAccDetails();
      ModalActions.resolveDebtActionsList(
        context: context,
        hasActiveAccount: hasActiveAccount,
        payOnAppCb: payOnAppCb,
        wrongClaimFn: () => widget.switchTo("proof"),
      );
    } else {
      var type = debtsCopy[index].loanActions["type"];
      String title;
      String content;
      switch (type) {
        case "paid":
          title = "Loan funded";
          content =
              "You have paid and submitted a proof for this loan, kindly wait for the creditor to confirm payment.";
          break;
        case "shift":
          title = "Reschedule requested";
          content =
              "You have requested for a reschedule for payment, kindly wait for the creditor to accept your new date.";
          break;
        default:
      }
      ModalActions.recallAction(
        context: context,
        title: title,
        content: content,
      );
    }
    // };
  }

  void searchLoans({String value}) {
    List<Loan> loanListToSearch =
        _selectedFilterValue == 0 ? debts : currentFilterdList;
    if (loanListToSearch.length == 0) return;
    // setState(() {
    //   _selectedFilterValue = 0;
    //   _selectedSortValue = 1;
    //   optionalFilterMessage = null;
    // });
    final _searchedList = HelperFunctions.searchLoans(
        searchValue: value, loanList: loanListToSearch, isDebt: false);
    if (this.mounted) {
      setState(() {
        debtsCopy = _searchedList;
        _searchedList.length == 0
            ? noResultFound = true
            : noResultFound = false;
      });
    }
  }

  void gotoProfilePage() {
    Navigator.of(context).pushReplacementNamed(
      ProfileScreen.routeName,
      arguments: {"isSlideBackward": false},
    );
  }

  void filterList({int index, BuildContext context}) async {
    if (debts.length == 0) return;

    await HelperFunctions.filterLoans(
        context: context,
        type: filterOptions[index]["type"],
        loanList: debts,
        sortType: sortOptions[_selectedSortValue]["type"],
        resultCb: (status, filteredList, message) {
          if (status) {
            if (this.mounted) {
              setState(() {
                _selectedFilterValue = index;
                optionalFilterMessage = message;
                debtsCopy = filteredList;
                currentFilterdList = filteredList;
                filteredList.length == 0
                    ? noResultFound = true
                    : noResultFound = false;
              });
            }
          }
        });
  }

  void sortList({int index}) async {
    if (debts.length == 0) return;
    setState(() {
      _selectedSortValue = index;
    });
    final _sortedList = HelperFunctions.sortLoans(
      type: sortOptions[index]["type"],
      loanList: debtsCopy,
    );
    if (this.mounted) {
      setState(() {
        debtsCopy = _sortedList;
        _sortedList.length == 0 ? noResultFound = true : noResultFound = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // if (isLoading == false) {
    //   HelperFunctions.handlePostFrameEventFromNotificationForListItems(
    //       widgetType: widget.type,
    //       items: debtsCopy,
    //       executable: onOutlineClicked,
    //       scrollController: _scrollController);
    // }
    final deiviceSize = MediaQuery.of(context).size;
    return Container(
      height: deiviceSize.height -
          MediaQuery.of(context).padding.top -
          Provider.of<GeneralProvider>(context, listen: false)
              .getBottomNavHeight,
      child: Stack(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(vertical: 30),
                child: MainNavBar(
                  title: widget.title,
                  leadingImagePath: 'public/images/arrow-back.png',
                  notificationImagePath: 'public/images/notification.png',
                  leadingPressAction: () =>
                      GeneralProvider.switchMainPage(index: 0),
                ),
              ),
              Expanded(
                child: Container(
                  // padding: EdgeInsets.symmetric(horizontal: 30),
                  child: Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 30),
                        margin: EdgeInsets.only(bottom: 5),
                        child: Row(
                          children: <Widget>[
                            // Text(
                            //   widget.title,
                            //   style: Theme.of(context).textTheme.body1.copyWith(
                            //         fontSize: 12,
                            //         fontWeight: FontWeight.w600,
                            //         height: 12 / 16,
                            //       ),
                            // ),
                            Spacer(),
                            CustomRaisedButton(
                              onTapAction: () {
                                if (userData["first_name"] == null) {
                                  return ModalActions.showMoreInfo(
                                    context: context,
                                    title: "Update profile",
                                    content:
                                        "Please update your profile to create a request.",
                                    onOk: () => gotoProfilePage(),
                                  );
                                }
                                widget.switchTo("loan_request");
                              },
                              bgColor: Colors.transparent,
                              color: widget.actionColor,
                              title: widget.action,
                              width: 100,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 30),
                        child: CustomTextField(
                            filled: true,
                            iconPath: 'public/images/search.png',
                            hintText:
                                "Search creditor by name, phone or amount",
                            focusNode: searchFocusNode,
                            onChanged: (value) => searchLoans(value: value)),
                      ),
                      SizedBox(height: 5),
                      // SizedBox(height: 5),
                      HelperFunctions.buildFilterPanel(
                          context: context,
                          optionalFilterMessage: optionalFilterMessage,
                          filterList: filterList,
                          sortList: sortList,
                          filterOptions: filterOptions
                              .map((option) => option['title'])
                              .toList(),
                          sortOptions: sortOptions
                              .map((option) => option['title'])
                              .toList(),
                          selectedSortValue: _selectedSortValue,
                          selectedFilterValue: _selectedFilterValue),
                      Expanded(
                        child: Container(
                          // padding: EdgeInsets.symmetric(horizontal: 30),
                          child: HelperFunctions.renderListItemsAndRefresh(
                            context: context,
                            pullRefresh: pullRefresh,
                            isLoading: isLoading,
                            refresh: () {
                              if (this.mounted) {
                                setState(() {
                                  pullRefresh = true;
                                });
                              }
                              return getLoansList();
                            },
                            itemsCount: debtsCopy.length,
                            itemsBuilder: ListView.builder(
                              controller: _scrollController,
                              shrinkWrap: true,
                              itemCount: debtsCopy.length,
                              itemBuilder: (ctx, index) =>
                                  HelperFunctions.buildLoanTile(
                                context: context,
                                contact: debtsCopy[index].creditor,
                                amount: debtsCopy[index].amount,
                                dueDate: debtsCopy[index].date,
                                itemId: debtsCopy[index].id,
                                selectedItemId: _selectedItemId,
                                onOutlineClicked: () => onOutlineClicked(index),
                                action: {},
                              ),
                            ),
                            emptyListIconPath:
                                "public/images/empty-lists/${noResultFound ? GeneralProvider.isLightMode ? "no-results" : "no-results-white" : "debts"}.png",
                            emptyListText:
                                "${noResultFound ? "No creditor matches the search criteria" : "You currently have no creditors at this time"}",
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          if (isOverLayLoading)
            Container(
              color: Colors.white24,
              child: Center(
                child: CustomProgressIndicator(
                  size: 35,
                  color: Theme.of(context).primaryColor,
                ),
              ),
            )
        ],
      ),
    );
  }
}
