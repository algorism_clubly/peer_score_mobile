import 'package:flutter/material.dart';
import 'package:peer_score/providers/debts_provider.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/utils/enums.dart';
import 'package:peer_score/utils/modal_actions.dart';
import 'package:peer_score/widgets/contacts/contact_list.dart';
import 'package:peer_score/widgets/dashboard/loan_details_page.dart';
import 'package:peer_score/widgets/dashboard/message_page.dart';
import 'package:peer_score/widgets/dashboard/select_contact_page.dart';
import 'package:peer_score/widgets/dashboard/success_page.dart';
import 'package:peer_score/widgets/debts/debts_page.dart';
import 'package:peer_score/widgets/expected_payment/expected_payement_page.dart';
import 'package:peer_score/widgets/expected_payment/provide_proof_page.dart';
import 'package:provider/provider.dart';

class DebtStack extends StatefulWidget {
  
  @override
  _DebtStackState createState() => _DebtStackState();
}

class _DebtStackState extends State<DebtStack> {
  bool loanRequest = false;
  bool provideProof = false;
  bool success = false;
  PageController _pageController = PageController();
  @override
  void initState() {
    GeneralProvider.switchSubStackIndex = switchSubStackIndex;
    super.initState();
  }

  void switchSubStackIndex({int index = 0}) {
    _pageController.jumpToPage(index);
  }

  void _switchPage(int toPageNumber) {
    if (GeneralProvider.currentFocusedNode != null) {
      GeneralProvider.currentFocusedNode.unfocus();
    }
    _pageController.animateToPage(toPageNumber,
        duration: Duration(milliseconds: 200), curve: Curves.linear);
  }

  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: _pageController,
      physics: NeverScrollableScrollPhysics(),
      children: <Widget>[
        DebtsPage(
          title: "Due Loans",
          action: "Request Loan",
          // actionColor: GeneralProvider.isLightMode  ? Color(0xFFDB6837) : Colors.white,
          // actionColor: Color(0xFFDB6837),
          actionColor: Theme.of(context).accentColor,
          type: CreditorsStackEnum.creditorsPage,
          switchTo: (mode) {
            if (this.mounted) {
              setState(() {
                switch (mode) {
                  case "loan_request":
                    loanRequest = true;
                    provideProof = false;
                    success = false;
                    break;
                  case "proof":
                    loanRequest = false;
                    provideProof = true;
                    success = false;
                    break;
                  case "success":
                    loanRequest = false;
                    provideProof = false;
                    success = true;
                    break;
                  default:
                }
              });
            }
            _switchPage(1);
            // if (index > 1) {
            //   _jumpToPage(index);
            // } else {
            //   _switchPage(index);
            // }
          },
        ),
        //request loan page stack
        if (loanRequest)
          SelectContactPage(
            switchTo: () => _switchPage(2),
            navBarLeadingFn: () => _switchPage(0),
            header: "Request loan",
            subHeader: "Select creditor",
          ),
        if (loanRequest)
          LoanDetailsPage(
            switchTo: () => _switchPage(3),
            navBarLeadingFn: () => _switchPage(1),
            header: "Request loan",
            isRequestPayment: false,
          ),
        if (loanRequest)
          MessagePage(
            isRequestPayment: false,
            navBarLeadingFn: () => _switchPage(2),
            switchTo: () => _switchPage(4),
          ),
        if (loanRequest)
          SuccessPage(
            isRequestPayment: false,
            navBarLeadingFn: () => GeneralProvider.switchMainPage(index: 0),
            switchMainPage: (index) => GeneralProvider.switchMainPage(index: 0),
          ),
        //end of request loan stack

        //provide proof page stack
        if (provideProof)
          ProvideProofPage(
            switchTo: () => _switchPage(2),
            navBarLeadingFn: () => _switchPage(0),
            expectedPayment: false,
            onSubmit: () => ModalActions.showSuccess(
                title: "Claim updated",
                context: context,
                content:
                    "Claim has been removed temporarily and will be removed permanently if a proof isn’t provided by the creditor",
                onOk: () {}),
          ),
        if (provideProof)
          ContactListPage(
            header: "Select contacts",
            selectMultipleMode: true,
            selectMultipleActionText: "Invite witnesses",
            navBarLeadingFn: () => _switchPage(1),
            switchTo: () => _switchPage(1),
          ),
        //end of proof stack

        //loan payment page stack
        if (success)
          SuccessPage(
            isRequestPayment: false,
            navBarLeadingFn: () => GeneralProvider.switchMainPage(index: 0),
            switchMainPage: (index) => GeneralProvider.switchMainPage(index: 0),
          ),
        //end of loan payment page stack
      ],
    );
  }
}
