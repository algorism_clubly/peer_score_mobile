import 'dart:async';

import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:math' as math;
import 'package:intl/intl.dart';
import 'package:peer_score/providers/contact.dart';
import 'package:peer_score/providers/expected_payments_provider.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/providers/user-provider.dart';
import 'package:peer_score/utils/custom_radio_tile.dart';
import 'package:peer_score/utils/custom_raised_button.dart';
import 'package:peer_score/utils/enums.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:peer_score/utils/main_nav_bar.dart';
import 'package:peer_score/utils/modal_actions.dart';
import 'package:peer_score/utils/notification_view_singleton.dart';
import 'package:peer_score/utils/progress_indicator.dart';
import 'package:peer_score/utils/snackbar.dart';
import 'package:peer_score/utils/text_form_field.dart';
import 'package:peer_score/widgets/contacts/contact_list.dart';
import 'package:provider/provider.dart';

class ExpectedPaymentPage extends StatefulWidget {
  final String title;
  final String action;
  final bool isExpectedPayment;
  final Color actionColor;
  final Function switchTo;
  final ExpectedPaymentsStackEnum type;
  ExpectedPaymentPage(
      {this.action,
      this.isExpectedPayment = true,
      this.title,
      this.actionColor,
      this.switchTo,
      this.type});
  @override
  _ExpectedPaymentPageState createState() => _ExpectedPaymentPageState();
}

class _ExpectedPaymentPageState extends State<ExpectedPaymentPage> {
  NotificationViewSingleton notificationViewSingleton =
      NotificationViewSingleton.getInstance();
  StreamSubscription notificationViewSub;
  ScrollController _scrollController;
  Map<String, dynamic> userData = {"first_name": ""};
  var selectedContact = 2;
  int loanRequestsLength = 0;
  // List<Debtor> dummyList = [];
  List<Loan> expectedPayments = [];
  List<Loan> expectedPaymentsCopy = [];
  List<Loan> currentFilterdList = [];
  GlobalKey<ScaffoldState> _scaffoldKey;
  bool isLoading = true;
  bool pullRefresh = false;
  bool noResultFound = false;
  FocusNode searchFocusNode;
  String _selectedItemId;
  String optionalFilterMessage;
  List<Map<String, dynamic>> filterOptions = [
    {
      "type": LoanlistFilterOptions.all,
      "title": "All",
    },
    {
      "type": LoanlistFilterOptions.paid,
      "title": "Paid loans",
    },
    {
      "type": LoanlistFilterOptions.overdue,
      "title": "Overdue loans",
    },
    {
      "type": LoanlistFilterOptions.flagged,
      "title": "Flagged loans",
    },
    {
      "type": LoanlistFilterOptions.reschedule,
      "title": "Reschedule requests",
    },
    // {
    //   "type": LoanlistFilterOptions.createdRange,
    //   "title": "Created date range",
    // },
    {
      "type": LoanlistFilterOptions.dueRange,
      "title": "Due date range",
    },
  ];
  int _selectedFilterValue = 0;
  List<Map<String, dynamic>> sortOptions = [
    {
      "type": LoanlistSortOptions.duedateAsc,
      "title": "Oldest(Due date)",
    },
    {
      "type": LoanlistSortOptions.duedateDesc,
      "title": "Newest(Due date)",
    },
    {
      "type": LoanlistSortOptions.amountAsc,
      "title": "Lowest(Amount)",
    },
    {
      "type": LoanlistSortOptions.amountDesc,
      "title": "Highest(Amount)",
    },
    // {
    //   "type": LoanlistSortOptions.createdDateAsc,
    //   "title": "Created date ASC",
    // },
    // {
    //   "type": LoanlistSortOptions.createdDateDesc,
    //   "title": "Created date DESC",
    // },
  ];
  int _selectedSortValue = 0;
  @override
  void initState() {
    super.initState();

    _scrollController = ScrollController(keepScrollOffset: false);
    searchFocusNode = FocusNode();
    GeneralProvider.currentFocusedNode = searchFocusNode;
    BackButtonInterceptor.add(myInterceptor);
    Provider.of<UserProvider>(context, listen: false).userData.then((data) {
      if (this.mounted) {
        setState(() {
          userData = data;
        });
      }
    });
    _scaffoldKey =
        Provider.of<GeneralProvider>(context, listen: false).getAppScaffoldKey;
    ExpectedPaymentsProvider.loansListCb = getLoansList;
    getLoansList().then((value) {
      notificationViewSub =
          notificationViewSingleton.notificationViewSubject.listen((value) {
        SchedulerBinding.instance.addPostFrameCallback((_) {
          HelperFunctions.handlePostFrameEventFromNotificationForListItems(
              widgetType: widget.type,
              items: expectedPaymentsCopy,
              executable: onOutlineClicked,
              scrollController: _scrollController);
        });
      });

      // 
      // 
    });
  }

  bool myInterceptor(bool stopDefaultButtonEvent) {
    if (stopDefaultButtonEvent) return false;
    GeneralProvider.switchMainPage(index: 0);
    return true;
  }

  @override
  void dispose() {
    super.dispose();
    BackButtonInterceptor.remove(myInterceptor);
    _scrollController.dispose();
    searchFocusNode.unfocus();
    notificationViewSub.cancel();
  }

  Future<void> getLoansList() async {
    if (this.mounted) {
      setState(() {
        isLoading = true;
        _selectedFilterValue = 0;
        _selectedSortValue = 0;
        optionalFilterMessage = null;
      });
    }
    final listResponse =
        await Provider.of<ExpectedPaymentsProvider>(context, listen: false)
            .getLoans();
    if (listResponse["status"] == 200 || listResponse["status"] == 201) {
      if (this.mounted) {
        setState(() {
          expectedPayments = [
            ...Provider.of<ExpectedPaymentsProvider>(context, listen: false)
                .expectedPayments
          ];
          expectedPaymentsCopy = [...expectedPayments];
          loanRequestsLength =
              Provider.of<ExpectedPaymentsProvider>(context, listen: false)
                  .pendingDebtors
                  .length;
          isLoading = false;
        });
        // HelperFunctions.handlePostFrameEventFromNotificationForListItems(
        //     widgetType: widget.type,
        //     items: expectedPaymentsCopy,
        //     executable: onOutlineClicked,
        //     scrollController: _scrollController);
      }
    } else {
      if (this.mounted) {
        setState(() {
          isLoading = false;
          pullRefresh = false;
          CustomSnackBar.snackBar(
            title: "Something went wrong, please reload the page",
            scaffoldState: _scaffoldKey,
            context: context,
          );
        });
      }
    }
  }

  void onOutlineClicked(int index) {
    // return () {
    searchFocusNode.unfocus();
    setState(() {
      _selectedItemId = expectedPaymentsCopy[index].id;
    });
    var loanAction = expectedPaymentsCopy[index].loanActions;
    ExpectedPaymentsProvider.selectedLoan = expectedPaymentsCopy[index];
    ExpectedPaymentsProvider.loansListCb = getLoansList;
    Provider.of<GeneralProvider>(context, listen: false)
        .clearSelectedContacts();
    Provider.of<GeneralProvider>(context, listen: false).clearSelectedFiles();
    if (loanAction != null) {
      switch (loanAction["type"]) {
        case "shift":
          ExpectedPaymentsProvider.newReschduledDate =
              DateTime.parse(loanAction["new_date"]);
          ModalActions.confirmReschedule(
            newDate: loanAction["new_date"],
            title: "Reschedule requested",
            leadingIconPath: "public/images/reschedule.png",
            context: context,
            content:
                "The debtor has requested to refund this loan on or before ${DateFormat.yMMMMd().format(DateTime.parse(loanAction["new_date"]))}",
          );
          break;
        case "paid":
          ModalActions.confirmPay(
            title: "Loan funded",
            leadingIconPath: "public/images/pay-icon.png",
            context: context,
            loanAction: loanAction,
            content:
                "This loan has been funded by the defaulter, kindly confirm payment.",
          );
          break;
        case "bad":
          ModalActions.provideProofModal(
            context: context,
            proceed: () => widget.switchTo("proof"),
          );
          break;
        default:
      }
    } else {
      bool isLoanOverDue =
          DateTime.now().isAfter(expectedPaymentsCopy[index].date);
      Loan loan = expectedPaymentsCopy[index];
      String phoneNumber = HelperFunctions.serializePhoneNumber(
          phoneNumber: loan.debtor.phoneNumber);
      ModalActions.sendMessage(
          context: context,
          title: "Resend notification",
          contentTitle:
              "You can send a message to the defaulter to remind them of payment",
          smsMessage:
              "Hello, ${userData["first_name"]} has made a payment request from you on PeerScore, kindly respond to it${loan.debtor.id != null ? ", thanks." : " on bit.ly/2E9PZOm"}",
          whatsappMessage:
              "Hello, here is a reminder of the loan you are currently ${isLoanOverDue ? "defaulting" : "owing"} which ${isLoanOverDue ? "was" : "is"} due for payment on ${DateFormat.yMMMMd().format(loan.date)} to ${userData["first_name"]} on PeerScore, kindly respond to it, thanks.",
          recipientPhone: phoneNumber,
          successMessage: "A reminder has been sent to the defaulter.",
          hasWhatsapp: loan.debtor.isWhatsApp,
          loanId: loan.id);
    }
    // };
  }

  void searchLoans({String value}) {
    List<Loan> loanListToSearch =
        _selectedFilterValue == 0 ? expectedPayments : currentFilterdList;
    if (loanListToSearch.length == 0) return;
    final _searchedList = HelperFunctions.searchLoans(
      searchValue: value,
      loanList: loanListToSearch,
    );
    if (this.mounted) {
      setState(() {
        expectedPaymentsCopy = _searchedList;
        _searchedList.length == 0
            ? noResultFound = true
            : noResultFound = false;
      });
    }
  }

  void filterList({int index, BuildContext context}) async {
    if (expectedPayments.length == 0) return;

    await HelperFunctions.filterLoans(
        context: context,
        type: filterOptions[index]['type'],
        loanList: expectedPayments,
        sortType: sortOptions[_selectedSortValue]['type'],
        resultCb: (status, filteredList, message) {
          if (status) {
            if (this.mounted) {
              setState(() {
                _selectedFilterValue = index;
                optionalFilterMessage = message;
                expectedPaymentsCopy = filteredList;
                currentFilterdList = filteredList;
                filteredList.length == 0
                    ? noResultFound = true
                    : noResultFound = false;
              });
            }
          }
        });
  }

  void sortList({int index}) async {
    if (expectedPayments.length == 0) return;
    setState(() {
      _selectedSortValue = index;
    });
    final _sortedList = HelperFunctions.sortLoans(
      type: sortOptions[index]['type'],
      loanList: expectedPaymentsCopy,
    );
    if (this.mounted) {
      setState(() {
        expectedPaymentsCopy = _sortedList;
        _sortedList.length == 0 ? noResultFound = true : noResultFound = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // if (isLoading == false) {
    //   HelperFunctions.handlePostFrameEventFromNotificationForListItems(
    //       widgetType: widget.type,
    //       items: expectedPaymentsCopy,
    //       executable: onOutlineClicked,
    //       scrollController: _scrollController);
    // }
    final deiviceSize = MediaQuery.of(context).size;
    return Container(
      height: deiviceSize.height -
          MediaQuery.of(context).padding.top -
          Provider.of<GeneralProvider>(context, listen: false)
              .getBottomNavHeight,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(vertical: 30),
            child: MainNavBar(
              title: widget.title,
              leadingImagePath: 'public/images/arrow-back.png',
              notificationImagePath: 'public/images/notification.png',
              leadingPressAction: () =>
                  GeneralProvider.switchMainPage(index: 0),
            ),
          ),
          Expanded(
            child: Container(
              // padding: EdgeInsets.symmetric(horizontal: 30),
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 30),
                    margin: EdgeInsets.only(bottom: 5),
                    child: Row(
                      children: <Widget>[
                        // Text(
                        //   widget.title,
                        //   style: Theme.of(context).textTheme.body1.copyWith(
                        //         fontSize: 12,
                        //         fontWeight: FontWeight.w600,
                        //         height: 12 / 16,
                        //       ),
                        // ),
                        Spacer(),
                        if (widget.isExpectedPayment && loanRequestsLength > 0)
                          Container(
                            width: 7,
                            height: 7,
                            // margin: EdgeInsets.only(right: 6),
                            child: Image.asset(
                              'public/images/priority.png',
                              color: Theme.of(context).errorColor,
                            ),
                          ),
                        CustomRaisedButton(
                          onTapAction: () => widget.switchTo("loan_request"),
                          bgColor: Colors.transparent,
                          color: widget.actionColor,
                          title: widget.action,
                          width: 130,
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 30),
                    child: CustomTextField(
                      filled: true,
                      iconPath: 'public/images/search.png',
                      hintText: "Search debtor by name, phone or amount",
                      focusNode: searchFocusNode,
                      onChanged: (value) => searchLoans(value: value),
                    ),
                  ),
                  SizedBox(height: 5),
                  HelperFunctions.buildFilterPanel(
                    context: context,
                    optionalFilterMessage: optionalFilterMessage,
                    filterList: filterList,
                    sortList: sortList,
                    filterOptions:
                        filterOptions.map((option) => option['title']).toList(),
                    sortOptions:
                        sortOptions.map((option) => option['title']).toList(),
                    selectedSortValue: _selectedSortValue,
                    selectedFilterValue: _selectedFilterValue,
                  ),
                  Expanded(
                    child: Container(
                      // padding: EdgeInsets.symmetric(horizontal: 30),
                      child: HelperFunctions.renderListItemsAndRefresh(
                        context: context,
                        pullRefresh: pullRefresh,
                        isLoading: isLoading,
                        refresh: () {
                          if (this.mounted) {
                            setState(() {
                              pullRefresh = true;
                            });
                          }
                          return getLoansList();
                        },
                        itemsCount: expectedPaymentsCopy.length,
                        itemsBuilder: ListView.builder(
                          controller: _scrollController,
                          shrinkWrap: true,
                          itemCount: expectedPaymentsCopy.length,
                          itemBuilder: (ctx, index) =>
                              HelperFunctions.buildLoanTile(
                            context: context,
                            contact: expectedPaymentsCopy[index].debtor,
                            amount: expectedPaymentsCopy[index].amount,
                            dueDate: expectedPaymentsCopy[index].date,
                            itemId: expectedPaymentsCopy[index].id,
                            selectedItemId: _selectedItemId,
                            outlineCOlor: true,
                            action:
                                expectedPaymentsCopy[index].loanActions != null
                                    ? expectedPaymentsCopy[index].loanActions
                                    : {},
                            onOutlineClicked: () => onOutlineClicked(index),
                          ),
                        ),
                        emptyListIconPath:
                            "public/images/empty-lists/${noResultFound ? GeneralProvider.isLightMode ? "no-results" : "no-results-white" : "expected"}.png",
                        emptyListText:
                            "${noResultFound ? "No debtor matches the search criteria" : "You have no debtors at this time"}",
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
