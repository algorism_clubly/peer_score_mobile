import 'package:flutter/material.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/utils/enums.dart';
import 'package:peer_score/utils/modal_actions.dart';
import 'package:peer_score/widgets/contacts/contact_list.dart';
import 'package:peer_score/widgets/dashboard/success_page.dart';
import 'package:peer_score/widgets/expected_payment/expected_payement_page.dart';
import 'package:peer_score/widgets/expected_payment/loan_request_page.dart';
import 'package:peer_score/widgets/expected_payment/provide_proof_page.dart';

class ExpectedPaymentStack extends StatefulWidget {
  final int initialIndex;
  ExpectedPaymentStack({this.initialIndex});
  @override
  _ExpectedPaymentStackState createState() => _ExpectedPaymentStackState();
}

class _ExpectedPaymentStackState extends State<ExpectedPaymentStack> {
  bool proof = false;
  bool loanRequest = true;
  PageController _pageController;
  @override
  void initState() {
    GeneralProvider.switchSubStackIndex = switchSubStackIndex;
    _pageController = PageController(initialPage: widget.initialIndex);
    super.initState();
  }

  void switchSubStackIndex({int index = 0}) {
    _pageController.jumpToPage(index);
  }

  void _switchPage(int toPageNumber) {
    if (GeneralProvider.currentFocusedNode != null) {
      GeneralProvider.currentFocusedNode.unfocus();
    }
    _pageController.animateToPage(
      toPageNumber,
      duration: Duration(milliseconds: 200),
      curve: Curves.linear,
    );
  }

  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: _pageController,
      // onPageChanged: (index) {
      //   setState(() {});
      // },
      physics: NeverScrollableScrollPhysics(),
      children: <Widget>[
        ExpectedPaymentPage(
          title: "Expected Payments",
          action: "View loan requests",
          actionColor: Theme.of(context).primaryColor,
          type: ExpectedPaymentsStackEnum.debtorsPage,
          switchTo: (mode) {
            if (this.mounted) {
              setState(() {
                switch (mode) {
                  case "proof":
                    proof = true;
                    loanRequest = false;
                    break;
                  case "loan_request":
                    proof = false;
                    loanRequest = true;
                    break;
                  default:
                }
              });
            }
            _switchPage(1);
          },
        ),
        if (proof)
          ProvideProofPage(
            switchTo: () => _switchPage(2),
            navBarLeadingFn: () => _switchPage(0),
            onSubmit: () => ModalActions.showSuccess(
              title: "Claim reinstated",
              context: context,
              content:
                  "Claim has been upheld, your debtor should refund the loan",
            ),
          ),
        if (proof)
          ContactListPage(
            header: "Select contacts",
            selectMultipleMode: true,
            selectMultipleActionText: "Invite witnesses",
            navBarLeadingFn: () => _switchPage(1),
            switchTo: () => _switchPage(1),
          ),
        if (loanRequest)
          LoanRequestPage(
            title: "Loan requests",
            type: ExpectedPaymentsStackEnum.loanRequests,
            navBarLeadingFn: () => _switchPage(0),
            switchTo: () => _switchPage(2),
          ),
        if (loanRequest)
          SuccessPage(
            navBarLeadingFn: () => _pageController.jumpToPage(0),
            switchMainPage: (index) => GeneralProvider.switchMainPage(index: 0),
          ),
      ],
    );
  }
}
