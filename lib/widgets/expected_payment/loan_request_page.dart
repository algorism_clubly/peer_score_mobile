import 'dart:async';

import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:intl/intl.dart';
import 'package:peer_score/providers/bank_accounts_provider.dart';
import 'package:peer_score/providers/expected_payments_provider.dart';
import 'package:peer_score/providers/payment_provider.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/providers/user-provider.dart';
import 'package:peer_score/screens/paystack_screen.dart';
import 'package:peer_score/utils/enums.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:peer_score/utils/main_nav_bar.dart';
import 'package:peer_score/utils/modal_actions.dart';
import 'package:peer_score/utils/notification_view_singleton.dart';
import 'package:peer_score/utils/progress_indicator.dart';
import 'package:peer_score/utils/snackbar.dart';
import 'package:peer_score/widgets/contacts/contact_list.dart';
import 'package:provider/provider.dart';

class LoanRequestPage extends StatefulWidget {
  final String title;
  final String action;
  final bool isExpectedPayment;
  final Color actionColor;
  final Function switchTo;
  final Function navBarLeadingFn;
  final ExpectedPaymentsStackEnum type;
  LoanRequestPage(
      {this.action,
      this.isExpectedPayment = true,
      this.title,
      this.actionColor,
      this.switchTo,
      this.navBarLeadingFn,
      this.type});
  @override
  _LoanRequestPageState createState() => _LoanRequestPageState();
}

class _LoanRequestPageState extends State<LoanRequestPage> {
  // List dummyList = DummyData.contactList;

  NotificationViewSingleton notificationViewSingleton =
      NotificationViewSingleton.getInstance();

  StreamSubscription notificationViewSub;
  ScrollController _scrollController;
  List<LoanRequest> pendingDebtorsList = [];
  GlobalKey<ScaffoldState> _scaffoldKey;
  bool isLoading = true;
  bool isOverLayLoading = false;
  bool pullRefresh = false;
  LoanRequest loan;
  Map<String, dynamic> userData = {
    "id": "",
    "first_name": "",
    "profile_picture": "",
    "compliance": 0
  };
  String _selectedItemId;

  @override
  void initState() {
    super.initState();
    BackButtonInterceptor.add(myInterceptor);
    _scrollController = ScrollController(keepScrollOffset: false);
    _scaffoldKey =
        Provider.of<GeneralProvider>(context, listen: false).getAppScaffoldKey;
    Provider.of<UserProvider>(context, listen: false).userData.then((data) {
      if (this.mounted) {
        setState(() {
          userData = data;
        });
      }
    });
    getPendingDebtsList().then((values) {
      notificationViewSub =
          notificationViewSingleton.notificationViewSubject.listen((value) {
        SchedulerBinding.instance.addPostFrameCallback((_) {
          HelperFunctions.handlePostFrameEventFromNotificationForListItems(
              widgetType: widget.type,
              items: pendingDebtorsList,
              executable: loanInfoClick,
              scrollController: _scrollController);
        });
      });

      // 
      // 
    });
  }

  bool myInterceptor(bool stopDefaultButtonEvent) {
    if (stopDefaultButtonEvent) return false;
    widget.navBarLeadingFn();
    return true;
  }

  @override
  void dispose() {
    BackButtonInterceptor.remove(myInterceptor);
    notificationViewSub.cancel();
    super.dispose();
  }

  void payOnAppCb() async {
    if (this.mounted) {
      setState(() {
        isOverLayLoading = true;
      });
    }

    HelperFunctions.payWithPaystack(
      context: context,
      loan: loan,
      userData: userData,
      initializingFailedCb: (err) {
        if (this.mounted) {
          setState(() {
            isOverLayLoading = false;
          });
        }
      },
      onPayStackResponse: (success) {
        if (this.mounted) {
          setState(() {
            isOverLayLoading = false;
          });
        }

        if (success) {
          GeneralProvider.successPageMessage =
              "The loan request of ${HelperFunctions.getFormattedAmount(amount: loan.amount)} made by ${HelperFunctions.getContactFullName(contact: loan.debtor)} has been successfully funded, the loan is now active.";
          if (this.mounted) {
            setState(() {
              isOverLayLoading = false;
            });
          }
          widget.switchTo();
        }
      },
    );
  }

  Future<bool> checkRecipientAccDetails() async {
    if (this.mounted) {
      setState(() {
        isOverLayLoading = true;
      });
    }
    final response =
        await Provider.of<BankAccountProvider>(context, listen: false)
            .getActiveBankAccount(body: {"userId": loan.debtor.id});
    if (response["status"] == 200 || response["status"] == 201) {
      if (this.mounted) {
        setState(() {
          isOverLayLoading = false;
        });
      }
      return true;
    } else {
      if (this.mounted) {
        setState(() {
          isOverLayLoading = false;
        });
      }
      return false;
    }
  }

  Future loanInfoClick(int index) async {
    setState(() {
      _selectedItemId = pendingDebtorsList[index].id;
    });
    loan = pendingDebtorsList[index];
    ExpectedPaymentsProvider.selectedDebtorLoanRequest = loan;
    // .setSelectedDebtorLoanRequest(loan: loan);
    ExpectedPaymentsProvider.loanRequestCb = getPendingDebtsList;
    bool hasActiveAccount = await checkRecipientAccDetails();
    ModalActions.requestLoanActions(
        context: context,
        loan: loan,
        hasActiveAccount: hasActiveAccount,
        payOnAppCb: payOnAppCb);
  }

  Future<void> getPendingDebtsList() async {
    if (this.mounted) {
      setState(() {
        isLoading = true;
      });
    }
    final listResponse =
        await Provider.of<ExpectedPaymentsProvider>(context, listen: false)
            .getLoanRequests();
    if (listResponse["status"] == 200 || listResponse["status"] == 201) {
      if (this.mounted) {
        setState(() {
          pendingDebtorsList = [
            ...Provider.of<ExpectedPaymentsProvider>(context, listen: false)
                .pendingDebtors
          ];
          isLoading = false;
          pullRefresh = false;
        });
        // HelperFunctions.handlePostFrameEventFromNotificationForListItems(
        //     widgetType: widget.type,
        //     items: pendingDebtorsList,
        //     executable: loanInfoClick,
        //     scrollController: _scrollController);
      }
    } else {
      if (this.mounted) {
        setState(() {
          isLoading = false;
          pullRefresh = false;
          CustomSnackBar.snackBar(
            title: "Something went wrong, please reload the page",
            scaffoldState: _scaffoldKey,
            context: context,
          );
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    // if (isLoading == false) {
    //   HelperFunctions.handlePostFrameEventFromNotificationForListItems(
    //       widgetType: widget.type,
    //       items: pendingDebtorsList,
    //       executable: loanInfoClick,
    //       scrollController: _scrollController);
    // }
    final deiviceSize = MediaQuery.of(context).size;
    return Container(
      height: deiviceSize.height -
          MediaQuery.of(context).padding.top -
          Provider.of<GeneralProvider>(context, listen: false)
              .getBottomNavHeight,
      child: Stack(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(vertical: 30),
                child: MainNavBar(
                  title: widget.title,
                  leadingImagePath: 'public/images/arrow-back.png',
                  notificationImagePath: 'public/images/notification.png',
                  leadingPressAction: widget.navBarLeadingFn,
                ),
              ),
              Expanded(
                child: Container(
                  // padding: EdgeInsets.symmetric(horizontal: 30),
                  child: Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(bottom: 12),
                        child: Row(
                          children: <Widget>[
                            // Text(
                            //   widget.title,
                            //   style: Theme.of(context).textTheme.body1.copyWith(
                            //         fontSize: 12,
                            //         fontWeight: FontWeight.w600,
                            //         height: 12 / 16,
                            //       ),
                            // ),
                            Spacer(),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Stack(
                          children: <Widget>[
                            HelperFunctions.renderListItemsAndRefresh(
                              context: context,
                              pullRefresh: pullRefresh,
                              isLoading: isLoading,
                              refresh: () {
                                if (this.mounted) {
                                  setState(() {
                                    pullRefresh = true;
                                  });
                                }
                                return getPendingDebtsList();
                              },
                              itemsCount: pendingDebtorsList.length,
                              itemsBuilder: ListView.builder(
                                controller: _scrollController,
                                shrinkWrap: true,
                                itemCount: pendingDebtorsList.length,
                                itemBuilder: (ctx, index) =>
                                    HelperFunctions.buildLoanTile(
                                  context: context,
                                  type: "loan request",
                                  contact: pendingDebtorsList[index].debtor,
                                  amount: pendingDebtorsList[index].amount,
                                  dueDate: pendingDebtorsList[index].dueDate,
                                  itemId: pendingDebtorsList[index].id,
                                  selectedItemId: _selectedItemId,
                                  outlineCOlor: true,
                                  action: {},
                                  onOutlineClicked: () => loanInfoClick(index),
                                ),
                              ),
                              emptyListIconPath:
                                  "public/images/empty-lists/requests.png",
                              emptyListText:
                                  "You have no loan requests at this time",
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          if (isOverLayLoading)
            Container(
              color: Colors.white24,
              child: Center(
                child: CustomProgressIndicator(
                  size: 35,
                  color: Theme.of(context).primaryColor,
                ),
              ),
            )
        ],
      ),
    );
  }
}
