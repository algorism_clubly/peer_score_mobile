import 'dart:io';

import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mime_type/mime_type.dart';
import 'package:peer_score/providers/colors.dart';
import 'package:peer_score/providers/contact.dart';
import 'package:peer_score/providers/debts_provider.dart';
import 'package:peer_score/providers/expected_payments_provider.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/utils/custom_raised_button.dart';
import 'package:peer_score/utils/err_text_widget.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:peer_score/utils/main_nav_bar.dart';
import 'package:peer_score/utils/modal_actions.dart';
import 'package:peer_score/utils/progress_indicator.dart';
import 'package:peer_score/widgets/contacts/contact_list.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';

class ProvideProofPage extends StatefulWidget {
  final Function switchTo;
  final Function navBarLeadingFn;
  final Function onSubmit;
  final bool expectedPayment;
  ProvideProofPage(
      {this.switchTo,
      this.navBarLeadingFn,
      this.onSubmit,
      this.expectedPayment = true});
  @override
  _ProvideProofPageState createState() => _ProvideProofPageState();
}

class _ProvideProofPageState extends State<ProvideProofPage> {
  List<Map<String, dynamic>> _files = [];
  List<Contact> _contacts = [];
  String _submitErrText = "";
  bool _submitErrVisible = false;
  bool isSending = false;
  String loanId = "";
  // EasyContactPicker _contactPicker = EasyContactPicker();
  // NativeContactPicker _contactPicker = NativeContactPicker();
  @override
  void initState() {
    BackButtonInterceptor.add(myInterceptor);
    // SchedulerBinding.instance.addPostFrameCallback((_) {
    //   _contacts = Provider.of<GeneralProvider>(context, listen: false)
    //       .getSelectedContatcts;
    //   _files = Provider.of<GeneralProvider>(context, listen: false)
    //       .getSelectedFilesData;
    //   setState(() {});
    //   
    //   
    //   
    //   
    // });
    // TODO: implement initState
    if (widget.expectedPayment) {
      loanId = ExpectedPaymentsProvider.selectedLoan.id;
    } else {
      loanId = DebtsProviver.selectedDebt.id;
    }
    super.initState();
  }

  bool myInterceptor(bool stopDefaultButtonEvent) {
    if (stopDefaultButtonEvent) return false;
    widget.navBarLeadingFn();
    return true;
  }

  @override
  void dispose() {
    BackButtonInterceptor.remove(myInterceptor);
    super.dispose();
  }

  dynamic pickFiles() async {
    
    PermissionStatus permissionStatus = await Permission.storage.request();
    if (permissionStatus != null) {
      if (permissionStatus == PermissionStatus.permanentlyDenied) {
        return openAppSettings();
      } else if (permissionStatus == PermissionStatus.denied) {
        return await Permission.storage.request();
      }
      var file = await FilePicker.getFile(
        type: FileType.image,
      );
      

      if (this.mounted) {
        
        
        setState(() {
          //TODO: check mimetype
          // files.forEach((file) {
          var fileObj = {
            "name": file.path.split("/").last,
            "path": file.path,
            "file": file
          };
          // Provider.of<GeneralProvider>(context, listen: false)
          //     .updatedSelectedFiles(fileObj);
          return _files = [fileObj];
        });
        
      }
      // });
    }
  }

  void selectContacts() async {
    bool myInterceptor(bool stopDefaultButtonEvent) {
      if (stopDefaultButtonEvent) return false;
      Navigator.of(context).pop();
      BackButtonInterceptor.remove(myInterceptor);
      return true;
    }

    void dispose() {
      Navigator.of(context).pop();
      BackButtonInterceptor.remove(myInterceptor);
    }

    BackButtonInterceptor.add(myInterceptor);
    showBottomSheet(
        context: context,
        elevation: 10,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
        builder: (ctx) {
          return ContactListPage(
            selectMultipleMode: true,
            selectedContactIds: _contacts.map((contact) => contact.id).toList(),
            header: "Select contacts",
            selectMultipleActionText: "Invite witnesses",
            navBarLeadingFn: () {
              dispose();
            },
            switchTo: (contacts) {
              setState(() {
                _contacts = contacts;
              });
              dispose();
            },
          );
        });
    // widget.switchTo();
  }

  void removeFile({int index}) {
    if (this.mounted) {
      setState(() {
        _files.removeAt(index);
      });
    }
  }

  void removeContact({int index}) {
    if (this.mounted) {
      setState(() {
        _contacts.removeAt(index);
      });
    }
  }

  void _onSubmit() async {
    if (this.mounted) {
      setState(() {
        isSending = true;
        _submitErrText = "";
        _submitErrVisible = false;
      });
    }
    var proofUploadResponse;
    if (widget.expectedPayment) {
      proofUploadResponse = await Provider.of<ExpectedPaymentsProvider>(context,
              listen: false)
          .insistClaim(witnesses: _contacts, documents: _files, loanId: loanId);
    } else {
      proofUploadResponse =
          await Provider.of<DebtsProviver>(context, listen: false).badClaim(
              witnesses: _contacts, documents: _files, loanId: loanId);
    }

    if (proofUploadResponse["status"] == 200 ||
        proofUploadResponse["status"] == 201) {
      widget.navBarLeadingFn();
      widget.onSubmit();
      if (this.mounted) {
        setState(() {
          isSending = false;
        });
      }
    } else {
      if (this.mounted) {
        setState(() {
          isSending = false;
          _submitErrText = "Something went wrong, please try again.";
          _submitErrVisible = true;
        });
      }
    }
  }

  Widget buildUploadButton(
      {String leadingIconPath,
      double width = 119,
      Color bgColor,
      String title,
      Function onClick}) {
    return InkWell(
      onTap: onClick,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4),
        child: Container(
          width: width,
          height: 33,
          decoration: BoxDecoration(color: bgColor),
          child: Stack(
            children: <Widget>[
              Container(
                width: 35,
                color: Color.fromRGBO(0, 0, 0, 0.28),
                alignment: Alignment.center,
                child: SizedBox(
                  width: 12,
                  child: Image.asset(leadingIconPath),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Container(
                  margin: EdgeInsets.only(left: 20),
                  child: Text(
                    title,
                    style: Theme.of(context).textTheme.body1.copyWith(
                          fontWeight: FontWeight.w600,
                          fontSize: 10,
                          color: Colors.white,
                        ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget buildFileListTile({String fileName, int index, bool isFile = true}) {
    var appColors = Provider.of<AppColorsProvider>(context, listen: false);
    return Container(
      padding: EdgeInsets.only(bottom: 8, left: 10),
      margin: EdgeInsets.only(bottom: 15),
      decoration: BoxDecoration(
          border: Border(
        bottom: BorderSide(
          width: 1,
          color: Color(0xffE7E7E7),
        ),
      )),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 20,
            height: 20,
            child: Image.asset("public/images/file-icon.png"),
          ),
          SizedBox(
            width: 8,
          ),
          Expanded(
            child: Text(
              fileName,
              overflow: TextOverflow.ellipsis,
              style: Theme.of(context)
                  .textTheme
                  .body1
                  .copyWith(color: appColors.selectedFileName),
            ),
          ),
          InkWell(
            onTap: () {
              if (isFile) {
                removeFile(index: index);
              } else {
                removeContact(index: index);
              }
            },
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
              alignment: Alignment.center,
              child: FaIcon(
                Platform.isIOS
                    ? const IconData(0xF404,
                        fontFamily: "CupertinoIcons",
                        fontPackage: "cupertino_icons")
                    : FontAwesomeIcons.times,
                size: Platform.isIOS ? 18 : 12,
                color: Theme.of(context).errorColor,
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // _contacts = Provider.of<GeneralProvider>(context, listen: false)
    //     .getSelectedContatcts;
    // _files = Provider.of<GeneralProvider>(context, listen: false)
    //     .getSelectedFilesData;
    // 
    // 
    // 
    // 
    final deviceSize = MediaQuery.of(context).size;
    double space_after_title;
    double space_before_submit;
    double uploaded_items_max_height;
    var heigth = deviceSize.height;

    var appColors = Provider.of<AppColorsProvider>(context, listen: false);
    //using a step size of 70 from minimum of 640px
    if (640 <= heigth && 710 >= heigth) {
      space_after_title = 10;
      space_before_submit = 25;
      uploaded_items_max_height = 80;
    } else if (711 <= heigth && 780 >= heigth) {
      space_after_title = 20;
      space_before_submit = 40;
      uploaded_items_max_height = 100;
    } else if (781 <= heigth && 851 >= heigth) {
      space_after_title = 35;
      space_before_submit = 60;
      uploaded_items_max_height = 130;
    } else {
      space_after_title = 50;
      space_before_submit = 70;
      uploaded_items_max_height = 150;
    }
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(vertical: 30),
            child: MainNavBar(
              leadingImagePath: 'public/images/arrow-back.png',
              notificationImagePath: 'public/images/notification.png',
              title: "Provide Proof",
              leadingPressAction: widget.navBarLeadingFn,
            ),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  // Text(
                  //   "Provide Proof",
                  //   style: Theme.of(context).textTheme.body2.copyWith(
                  //         // fontSize: 12,
                  //         fontWeight: FontWeight.w600,
                  //       ),
                  // ),
                  // SizedBox(height: 20),
                  Text(
                    "Upload Documents",
                    style: Theme.of(context).textTheme.body2.copyWith(
                          fontWeight: FontWeight.w300,
                        ),
                  ),
                  Text(
                    "Provide documents to back up your proof.",
                    style: Theme.of(context).textTheme.body1.copyWith(
                        fontWeight: FontWeight.w400,
                        // fontSize: 8,
                        color: appColors.selectItemTitle),
                  ),
                  SizedBox(height: 20),
                  buildUploadButton(
                      leadingIconPath: "public/images/cloud-upload.png",
                      title: "Upload files",
                      bgColor: Color(0xFF5250B1),
                      onClick: pickFiles),
                  if (_files.length > 0)
                    SizedBox(height: 25),
                  ConstrainedBox(
                    constraints: BoxConstraints(
                        minHeight: 5, maxHeight: uploaded_items_max_height),
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: _files.length,
                      itemBuilder: (ctx, index) => buildFileListTile(
                          fileName: _files[index]["name"], index: index),
                    ),
                  ),
                  SizedBox(height: 15),
                  Text(
                    "Invite Witnesses",
                    style: Theme.of(context).textTheme.body2.copyWith(
                          fontWeight: FontWeight.w300,
                        ),
                  ),
                  Text(
                    "Add contacts to act as your witness",
                    style: Theme.of(context).textTheme.body1.copyWith(
                        fontWeight: FontWeight.w400,
                        // fontSize: 8,
                        color: GeneralProvider.isLightMode
                            ? Color(0xFF888888)
                            : Colors.white70),
                  ),
                  SizedBox(height: 20),
                  buildUploadButton(
                    leadingIconPath: "public/images/contact-upload.png",
                    title: "Select contacts",
                    bgColor: Color(0xFFDB6837),
                    width: 132,
                    onClick: selectContacts,
                  ),
                  SizedBox(height: 25),
                  Expanded(
                      // constraints: BoxConstraints(
                      //     minHeight: 5, maxHeight: uploaded_items_max_height),
                      child: ListView(
                    children: List.generate(_contacts.length, (index) {
                      return buildFileListTile(
                          isFile: false,
                          fileName: HelperFunctions.getContactFullName(
                                      contact: _contacts[index]) ==
                                  ""
                              ? _contacts[index].phoneNumber.toString()
                              : HelperFunctions.getContactFullName(
                                  contact: _contacts[index]),
                          index: index);
                    }),
                  )

                      //  ListView.builder(
                      //   shrinkWrap: true,
                      //   itemCount: _contacts.length,
                      //   itemBuilder: (ctx, index) => buildFileListTile(
                      //       isFile: false,
                      //       fileName: HelperFunctions.getContactFullName(
                      //                   contact: _contacts[index]) ==
                      //               ""
                      //           ? _contacts[index].phoneNumber.toString()
                      //           : HelperFunctions.getContactFullName(
                      //               contact: _contacts[index]),
                      //       index: index),
                      // ),
                      ),
                ],
              ),
            ),
          ),
          if (_files.length > 0 || _contacts.length > 0)
            Container(
              padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
              child: Column(
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    padding: EdgeInsets.symmetric(vertical: 8),
                    child: CustomRaisedButton(
                      title: isSending ? "" : "Submit",
                      // color: appColors.primaryButtonColorText,
                      // bgColor: appColors.primaryButtonColorBg,
                      onTapAction: () => isSending ? () {} : _onSubmit(),
                      indicator: isSending
                          ? CustomProgressIndicator(
                              // color: appColors.primaryProgressValueColor,
                            )
                          : Text(""),
                    ),
                  ),
                  ErrText(
                    errText: _submitErrText,
                    vissibility: _submitErrVisible,
                  )
                ],
              ),
            )
        ],
      ),
    );
  }
}
