import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:peer_score/providers/bank_accounts_provider.dart';
import 'package:peer_score/providers/colors.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/providers/user-provider.dart';
import 'package:peer_score/utils/custom_switch.dart';
import 'package:peer_score/utils/main_nav_bar.dart';
import 'package:peer_score/utils/progress_indicator.dart';
import 'package:peer_score/utils/snackbar.dart';
import 'package:provider/provider.dart';

class BankAccountsPage extends StatefulWidget {
  final Function switchTo;
  final Function navBarLeadingFn;
  BankAccountsPage({this.switchTo, this.navBarLeadingFn});

  @override
  _BankAccountsPageState createState() => _BankAccountsPageState();
}

class _BankAccountsPageState extends State<BankAccountsPage> {
  List<BankAccount> _bankList = [];
  GlobalKey<ScaffoldState> _scaffoldKey;
  bool isLoading = false;
  @override
  void initState() {
    super.initState();
    BackButtonInterceptor.add(myInterceptor);

    _scaffoldKey = GeneralProvider.profileScaffoldKey;
    getAccounts();
  }

  bool myInterceptor(bool stopDefaultButtonEvent) {
    if (stopDefaultButtonEvent) return false;
    widget.navBarLeadingFn();
    return true;
  }

  @override
  void dispose() {
    BackButtonInterceptor.remove(myInterceptor);
    super.dispose();
  }

  getAccounts() async {
    if (this.mounted) {
      setState(() {
        isLoading = true;
      });
    }
    final response =
        await Provider.of<BankAccountProvider>(context, listen: false)
            .getAllAccounts();

    if (response["status"] == 200 || response["status"] == 201) {
      if (this.mounted) {
        setState(() {
          _bankList = Provider.of<BankAccountProvider>(context, listen: false)
              .bankAccounts;
          isLoading = false;
        });
      }
    } else {
      if (this.mounted) {
        setState(() {
          isLoading = false;
          CustomSnackBar.snackBar(
            title: "Something went wrong, please try again.",
            scaffoldState: _scaffoldKey,
            context: context,
          );
        });
      }
    }
  }

  void deleteAccountNumber({String accountId, bool isDefault}) async {
    
    
    if (isDefault && _bankList.length > 1) {
      CustomSnackBar.snackBar(
          title: "Cannot delete default account",
          scaffoldState: _scaffoldKey,
          context: context);
      return;
    }
    if (this.mounted) {
      setState(() {
        isLoading = true;
      });
    }
    final userData =
        await Provider.of<UserProvider>(context, listen: false).userData;
    final deleteResponse = await Provider.of<BankAccountProvider>(context,
            listen: false)
        .deleteBankAccount(body: {"id": accountId, "userId": userData["id"]});

    if (deleteResponse["status"] == 200 || deleteResponse["status"] == 201) {
      if (this.mounted) {
        setState(() {
          _bankList.removeWhere((item) => item.id == accountId);
          isLoading = false;
        });
      }
    } else {
      if (this.mounted) {
        setState(() {
          isLoading = false;
          CustomSnackBar.snackBar(
            title: "Something went wrong, please try again.",
            scaffoldState: _scaffoldKey,
            context: context,
          );
        });
      }
    }
  }

  void changeDefault({String accountId, bool isDefault}) async {
    if (isDefault) {
      CustomSnackBar.snackBar(
        title: "Cannot change default account",
        scaffoldState: _scaffoldKey,
        context: context,
      );
      return;
    }
    if (this.mounted) {
      setState(() {
        isLoading = true;
      });
    }
    final userData =
        await Provider.of<UserProvider>(context, listen: false).userData;
    final deleteResponse = await Provider.of<BankAccountProvider>(context,
            listen: false)
        .changeBankAccountStatus(body: {"id": accountId, "userId": userData["id"]});

    if (deleteResponse["status"] == 200 || deleteResponse["status"] == 201) {
      if (this.mounted) {
        setState(() {
          _bankList.forEach((bank) {
            if (bank.id == accountId) {
              bank.isDefault = true;
            } else {
              bank.isDefault = false;
            }
          });
          isLoading = false;
        });
      }
    } else {
      if (this.mounted) {
        setState(() {
          isLoading = false;
          CustomSnackBar.snackBar(
            title: "Something went wrong, please try again.",
            scaffoldState: _scaffoldKey,
            context: context,
          );
        });
      }
    }
  }

  Widget buildBankListTile(
      {BuildContext context,
      String id,
      String logo,
      String accNum,
      String bank,
      bool isDefault}) {
    return Container(
      height: 85,
      child: Column(
        children: <Widget>[
          Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  SizedBox(
                    width: 50,
                    height: 50,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Image.asset(logo),
                    ),
                  ),
                  SizedBox(width: 15),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        accNum,
                        style: Theme.of(context).textTheme.body2,
                      ),
                      Text(
                        bank,
                        style: Theme.of(context).textTheme.body1.copyWith(
                            fontSize: 10,
                            fontWeight: FontWeight.w600,
                            color: Color(0xFF6766A5)),
                      )
                    ],
                  ),
                  Spacer(),
                  Column(
                    children: <Widget>[
                      Text(
                        "Default",
                        style: Theme.of(context).textTheme.body1.copyWith(
                              fontSize: 10,
                              fontWeight: FontWeight.w600,
                              color: Color(0xFF6766A5),
                            ),
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      CustomSwitch(
                        value: isDefault,
                        onClick: (value) =>
                            changeDefault(isDefault: isDefault, accountId: id),
                      ),
                    ],
                  )
                ],
              ),
              // SizedBox(height: 5,),
              Align(
                alignment: Alignment.bottomRight,
                child: InkWell(
                  // customBorder: CircleBorder(),
                  onTap: () =>
                      deleteAccountNumber(accountId: id, isDefault: isDefault),
                  child: Container(
                    alignment: Alignment.bottomRight,
                    width: 50,
                    padding: EdgeInsets.only(
                      top: 5,
                      left: 8,
                    ),
                    child: Text(
                      "Delete",
                      style: Theme.of(context).textTheme.body1.copyWith(
                            fontSize: 10,
                            fontWeight: FontWeight.w600,
                            color: Color(0xFFDB6837),
                          ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          Spacer(),
          Divider(
            thickness: 1,
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    
    var appColors = Provider.of<AppColorsProvider>(context, listen: false);
    return Container(
      child: Stack(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(vertical: 30),
                child: MainNavBar(
                  title: "Bank Accounts",
                  leadingImagePath: 'public/images/arrow-back.png',
                  notificationImagePath: 'public/images/notification.png',
                  leadingPressAction: widget.navBarLeadingFn,
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    // Text(
                    //   "Bank Accounts",
                    //   style: Theme.of(context).textTheme.body1.copyWith(
                    //         fontWeight: FontWeight.w600,
                    //         height: 12 / 16,
                    //       ),
                    // ),
                    // SizedBox(height: 10),
                    Container(
                      constraints: BoxConstraints(
                        maxHeight: 450,
                      ),
                      child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: _bankList.length,
                        itemBuilder: (ctx, index) => buildBankListTile(
                            context: context,
                            logo:
                                "public/images/banks/${_bankList[index].bankCode}.png",
                            bank: _bankList[index].bankName,
                            accNum: _bankList[index].accountNumber,
                            isDefault: _bankList[index].isDefault,
                            id: _bankList[index].id),
                      ),
                    ),
                    SizedBox(height: 20),
                    InkWell(
                      onTap: widget.switchTo,
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            SizedBox(
                              width: 11,
                              height: 11,
                              child: Image.asset("public/images/outline.png"),
                            ),
                            SizedBox(width: 8),
                            Text(
                              "Add Account",
                              style: Theme.of(context).textTheme.body1.copyWith(
                                  fontWeight: FontWeight.w600,
                                  // height: 12 / 16,
                                  color: Color(0xFFDB6837)),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
          if (isLoading)
            Container(
              height: MediaQuery.of(context).size.height,
              color: Color.fromRGBO(169, 169, 169, 0.1),
              child: Center(
                child: CustomProgressIndicator(
                  size: 45,
                  color: appColors.altProgressValueColor,
                ),
              ),
            )
        ],
      ),
    );
  }
}
