import 'dart:async';

import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:peer_score/providers/bvn_provider.dart';
import 'package:peer_score/providers/colors.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/providers/user-provider.dart';
import 'package:peer_score/utils/custom_raised_button.dart';
import 'package:peer_score/utils/err_text_widget.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:peer_score/utils/main_nav_bar.dart';
import 'package:peer_score/utils/progress_indicator.dart';
import 'package:peer_score/utils/snackbar.dart';
import 'package:peer_score/utils/text_form_field.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:provider/provider.dart';

class BvnPage extends StatefulWidget {
  final Function switchTo;
  final Function navBarLeadingFn;
  BvnPage({this.switchTo, this.navBarLeadingFn});
  @override
  _BvnPageState createState() => _BvnPageState();
}

class _BvnPageState extends State<BvnPage> {
  String bvnValue = "";
  String bvnId = "";
  TextEditingController _bvnTextEditingController;
  FocusNode _bvnFocusNode;
  TextEditingController _otpController;
  String _submitErrText = "";
  bool _submitErrVisible = false;
  String _bvnErrText = "";
  String confirmedBvnNumber = "";
  bool _bvnErrVisible = false;
  bool isInitiating = false;
  bool hasBvn = false;
  bool bvnConfirmed = false;
  bool isSubmitting = false;
  bool isLoading = true;
  double otpSectionMargin = 5.0;

  Map<String, dynamic> userData = {};

  @override
  void initState() {
    super.initState();
    _bvnTextEditingController = TextEditingController();
    _otpController = TextEditingController();
    _bvnFocusNode = FocusNode();

    Provider.of<UserProvider>(context, listen: false).userData.then((data) {
      if (this.mounted) {
        setState(() {
          userData = data;
          getBvn();
        });
      }
    });
    _bvnTextEditingController.addListener(() {
      if (this.mounted) {
        setState(() {
          if (_bvnTextEditingController.text.length > 0) {
            _bvnErrText = "";
            _bvnErrVisible = false;
          }
        });
      }
    });
    // _bvnFocusNode.addListener(() {
    //   if (_bvnFocusNode.hasFocus) {
    //     _bvnFocusNode.unfocus();
    //   }
    // });
    BackButtonInterceptor.add(myInterceptor);
  }

  bool myInterceptor(bool stopDefaultButtonEvent) {
    if (stopDefaultButtonEvent) return false;
    _bvnFocusNode.unfocus();
    widget.navBarLeadingFn();
    return true;
  }

  void getBvn() async {
    var response = await Provider.of<BvnProvider>(context, listen: false)
        .getBvn(userId: userData["id"]);
    
    
    if (response["status"] == 200) {
      if (this.mounted) {
        setState(() {
          if (response["data"] != null) {
            //update with data
            bvnValue = response["data"]["number"];
            bvnId = response["data"]["id"];
            hasBvn = true;
          }
          isLoading = false;
        });
      }
    } else {
      if (this.mounted) {
        CustomSnackBar.snackBar(context: context, title: "Could not get BVN");
      }
    }
  }

  void _onInitiate() async {
    if (_bvnTextEditingController.text.length < 11 && this.mounted) {
      setState(() {
        _bvnErrText = "BVN must be 11 characters long";
        _bvnErrVisible = true;
      });
      return;
    }
    if (this.mounted) {
      setState(() {
        isInitiating = true;
      });
    }
    _bvnFocusNode.unfocus();
    var response = await Provider.of<BvnProvider>(context, listen: false)
        .initiateBvn(body: {
      "userId": userData["id"],
      "number": _bvnTextEditingController.text
    });

    
    

    if (response["status"] == 200) {
      
      if (this.mounted) {
        setState(() {
          bvnConfirmed = true;
          isInitiating = false;
          confirmedBvnNumber = response["data"];

          //animating otp in
          Timer.run(() {
            setState(() {
              otpSectionMargin = 0.0;
            });
          });
        });
      }
    } else {
      if (this.mounted) {
        setState(() {
          isInitiating = false;
          _bvnErrText = response["errors"];
          _bvnErrVisible = true;
        });
      }
    }
  }

  void deleteBvn() async {
    if (this.mounted) {
      setState(() {
        isLoading = true;
      });
    }
    
    var response = await Provider.of<BvnProvider>(context, listen: false)
        .deleteBvn(body: {"id": bvnId});
    if (response["status"] == 200 && this.mounted) {
      setState(() {
        isLoading = false;
        hasBvn = false;
        bvnConfirmed = false;
        isSubmitting = false;
      });
    } else {
      if (this.mounted) {
        setState(() {
          isLoading = false;
          CustomSnackBar.snackBar(
              context: context, title: "Cannot delete BVN, please try again");
        });
      }
    }
  }

  void _onSubmit() async {
    if (_otpController.text.length < 4) {
      _submitErrText = "OTP must be 4 digits long";
      _submitErrVisible = true;

      return;
    }
    if (this.mounted) {
      setState(() {
        isSubmitting = true;
        _submitErrText = "";
        _submitErrVisible = false;
      });
    }

    var response = await Provider.of<BvnProvider>(context, listen: false)
        .confirmBvn(body: {
      "userId": userData["id"],
      "code": _otpController.text,
    });

    if (response["status"] == 200 && this.mounted) {
      setState(() {
        bvnValue = response["data"]["number"];
        bvnId = response["data"]["id"];
        hasBvn = true;
        _bvnTextEditingController.text = "";
        bvnConfirmed = false;
        otpSectionMargin = 5.0;
      });
    } else {
      if (this.mounted) {
        setState(() {
          isSubmitting = false;
          _submitErrText = response["errors"];
          _submitErrVisible = true;
        });
      }
    }
  }

  @override
  void dispose() {
    
    BackButtonInterceptor.remove(myInterceptor);
    _bvnTextEditingController.dispose();
    _otpController.dispose();
    _bvnFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var appColors = Provider.of<AppColorsProvider>(context, listen: false);

    return Container(
      height: HelperFunctions.getDeviceUsabaleHeigth(context: context),
      child: Stack(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(vertical: 30),
                child: MainNavBar(
                  title: "BVN",
                  leadingImagePath: 'public/images/arrow-back.png',
                  notificationImagePath: 'public/images/notification.png',
                  leadingPressAction: () {
                    _bvnFocusNode.unfocus();
                    widget.navBarLeadingFn();
                  },
                ),
              ),
              SizedBox(
                height: 30,
              ),
              if (hasBvn)
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "${bvnValue.substring(0, 7)}****",
                        style: Theme.of(context).textTheme.body2.copyWith(
                              fontWeight: FontWeight.w600,
                              fontSize: 35,
                              letterSpacing: 0.8,
                              textBaseline: TextBaseline.ideographic,
                            ),
                      ),
                      Align(
                        alignment: Alignment.bottomRight,
                        child: CustomRaisedButton(
                          onTapAction: deleteBvn,
                          leadingWidget: FaIcon(
                            FontAwesomeIcons.lightTrash,
                            color: Theme.of(context).accentColor,
                            size: 15,
                          ),
                          title: "Delete",
                          fontSize: 15,
                          fontWeight: FontWeight.w600,
                          mainAxisAlignment: MainAxisAlignment.end,
                          color: Theme.of(context).accentColor,
                          bgColor: Colors.transparent,
                        ),
                      )
                    ],
                  ),
                ),
              if (!isLoading && !hasBvn)
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Please provide your BVN",
                        style: Theme.of(context).textTheme.body2.copyWith(
                              fontWeight: FontWeight.w400,
                            ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      CustomTextField(
                        height: 40,
                        hintText: "Enter BVN",
                        textEditingController: _bvnTextEditingController,
                        focusNode: _bvnFocusNode,
                        inputFormatters: [
                          WhitelistingTextInputFormatter.digitsOnly,
                          LengthLimitingTextInputFormatter(11)
                        ],
                        filled: !hasBvn && bvnConfirmed,
                        readOnly: isInitiating || (!hasBvn && bvnConfirmed),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      ErrText(
                        errText: _bvnErrText,
                        vissibility: _bvnErrVisible,
                      ),
                      if (!hasBvn && !bvnConfirmed)
                        Align(
                          alignment: Alignment.centerRight,
                          child: CustomRaisedButton(
                            title: isInitiating ? "" : "Proceed",
                            indicator: isInitiating
                                ? CustomProgressIndicator(
                                    // color: appColors.primaryProgressValueColor,
                                  )
                                : Text(""),
                            onTapAction: () {
                              isInitiating ? null : _onInitiate();
                            },
                          ),
                        ),
                    ],
                  ),
                ),
              AnimatedContainer(
                duration: Duration(milliseconds: 500),
                height: otpSectionMargin,
                child: Container(),
              ),
              if (!isLoading && !hasBvn && bvnConfirmed)
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SizedBox(
                        height: 15,
                      ),
                      Text(
                        "Enter the OTP sent to  $confirmedBvnNumber",
                        style: Theme.of(context).textTheme.body2.copyWith(
                              fontWeight: FontWeight.w400,
                            ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      PinCodeTextField(
                        length: 4,
                        onChanged: (value) {
                          // setState(() {
                          //   _otp = value;
                          // });
                        },
                        autoDisposeControllers: false,
                        inputFormatters: [
                          WhitelistingTextInputFormatter.digitsOnly,
                        ],
                        controller: _otpController,
                        backgroundColor: Colors.transparent,
                        textStyle: Theme.of(context)
                            .textTheme
                            .body2
                            .copyWith(fontSize: 20),
                        // backgroundColor: Colors.grey[50],
                        shape: PinCodeFieldShape.box,
                        fieldWidth: 48,
                        // fieldWidth: widget.isAltPhone ? 60 : 65,
                        // fieldHeight: widget.isAltPhone ? 60 : 65,
                        animationType: AnimationType.slide,
                        // borderRadius: BorderRadius.circular(10),
                        textInputType: TextInputType.number,
                        inactiveColor: appColors.pinCodeBorderColor,
                        inactiveFillColor: appColors.pinCodeBorderColor,
                        activeColor: appColors.pinCodeBorderColor,
                        borderRadius: BorderRadius.circular(4.0),

                        borderWidth: GeneralProvider.isLightMode ? 1 : 1.5,
                      ),
                      ErrText(
                        errText: _submitErrText,
                        vissibility: _submitErrVisible,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: CustomRaisedButton(
                          title: isSubmitting ? "" : "Complete",
                          indicator: isSubmitting
                              ? CustomProgressIndicator(
                                  // color: appColors.primaryProgressValueColor,
                                )
                              : Text(""),
                          onTapAction: () {
                            isSubmitting ? null : _onSubmit();
                          },
                        ),
                      ),
                    ],
                  ),
                )
            ],
          ),
          if (isLoading)
            Container(
              height: MediaQuery.of(context).size.height,
              color: Color.fromRGBO(169, 169, 169, 0.1),
              child: Center(
                child: CustomProgressIndicator(
                  size: 45,
                  color: appColors.altProgressValueColor,
                ),
              ),
            )
        ],
      ),
    );
  }
}
