import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:peer_score/providers/bank_accounts_provider.dart';
import 'package:peer_score/providers/colors.dart';
import 'package:peer_score/providers/user-provider.dart';
import 'package:peer_score/utils/custom_raised_button.dart';
import 'package:peer_score/utils/dropdown_field.dart';
import 'package:peer_score/utils/err_text_widget.dart';
import 'package:peer_score/utils/main_nav_bar.dart';
import 'package:peer_score/utils/progress_indicator.dart';
import 'package:peer_score/utils/text_form_field.dart';
import 'package:provider/provider.dart';

class NewbankAccount extends StatefulWidget {
  final Function switchTo;
  final Function navBarLeadingFn;
  NewbankAccount({this.switchTo, this.navBarLeadingFn});
  @override
  _NewbankAccountState createState() => _NewbankAccountState();
}

class _NewbankAccountState extends State<NewbankAccount> {
  TextEditingController _accountNumberController;
  TextEditingController _bankController;
  FocusNode _accountNumberFocusNode;
  FocusNode _bankFocusNode;
  String _accountName = "";
  String _accountNumberErrText = "";
  bool _accountNumberErrVisible = false;
  String _unresolvedErrText = "";
  bool _unresolvedErrVisible = false;
  String _submitErrText = "";
  bool _submitErrVisible = false;
  String defaultValue = "-- select bank --";
  bool isSending = false;
  bool accountResolved = false;
  bool _resolvingAccount = false;
  List<BankAccount> _bankList = [];
  var regEx = RegExp(r"^[0-9]{1,10}$", multiLine: true);
  @override
  void initState() {
    BackButtonInterceptor.add(myInterceptor);
    _accountNumberController = TextEditingController(text: "");
    _bankController = TextEditingController();
    _accountNumberFocusNode = FocusNode();
    _bankFocusNode = FocusNode();
    _accountNumberController.addListener(() {
      if (this.mounted) {
        setState(() {
          if (_accountNumberController.text.length > 0) {
            _accountNumberErrVisible = false;
            _accountNumberErrText = "";
            _unresolvedErrText = "";
            _unresolvedErrVisible = false;
            _submitErrText = "";
            _submitErrVisible = false;
          }
        });
      }
    });
    _bankFocusNode.addListener(() {
      if (_bankFocusNode.hasFocus) {
        _bankFocusNode.unfocus();
      }
    });

    _bankList =
        Provider.of<BankAccountProvider>(context, listen: false).bankAccounts;
    super.initState();
  }

  void onBackButtonPressed() {
    _accountNumberController.text = null;
    _accountNumberFocusNode.unfocus();
    widget.navBarLeadingFn();
  }

  bool myInterceptor(bool stopDefaultButtonEvent) {
    if (stopDefaultButtonEvent) return false;
    onBackButtonPressed();
    return true;
  }

  List<Map<String, dynamic>> banks = [
    {
      "name": "-- select bank --",
      "slug": "access-bank",
      "code": "044",
      "longcode": "044150149",
      "gateway": "emandate",
      "pay_with_bank": false,
      "active": true,
      "is_deleted": null,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 1,
      "createdAt": "2016-07-14T10:04:29.000Z",
      "updatedAt": "2020-02-18T08:06:44.000Z"
    },
    {
      "name": "Access Bank",
      "slug": "access-bank",
      "code": "044",
      "logo": "public/images/access-bank.png",
      "longcode": "044150149",
      "gateway": "emandate",
      "pay_with_bank": false,
      "active": true,
      "is_deleted": null,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 1,
      "createdAt": "2016-07-14T10:04:29.000Z",
      "updatedAt": "2020-02-18T08:06:44.000Z"
    },
    {
      "name": "Access Bank (Diamond)",
      "slug": "access-bank-diamond",
      "code": "063",
      "longcode": "063150162",
      "gateway": "emandate",
      "pay_with_bank": false,
      "active": true,
      "is_deleted": null,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 3,
      "createdAt": "2016-07-14T10:04:29.000Z",
      "updatedAt": "2020-02-18T08:06:48.000Z"
    },
    {
      "name": "ALAT by WEMA",
      "slug": "alat-by-wema",
      "code": "035A",
      "longcode": "035150103",
      "gateway": "emandate",
      "pay_with_bank": true,
      "active": true,
      "is_deleted": null,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 27,
      "createdAt": "2017-11-15T12:21:31.000Z",
      "updatedAt": "2020-02-18T06:46:46.000Z"
    },
    {
      "name": "ASO Savings and Loans",
      "slug": "asosavings",
      "code": "401",
      "longcode": "",
      "gateway": null,
      "pay_with_bank": false,
      "active": true,
      "is_deleted": null,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 63,
      "createdAt": "2018-09-23T05:52:38.000Z",
      "updatedAt": "2019-01-30T09:38:57.000Z"
    },
    {
      "name": "CEMCS Microfinance Bank",
      "slug": "cemcs-microfinance-bank",
      "code": "50823",
      "longcode": "",
      "gateway": null,
      "pay_with_bank": false,
      "active": true,
      "is_deleted": false,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 74,
      "createdAt": "2020-03-23T15:06:13.000Z",
      "updatedAt": "2020-03-23T15:06:28.000Z"
    },
    {
      "name": "Citibank Nigeria",
      "slug": "citibank-nigeria",
      "code": "023",
      "longcode": "023150005",
      "gateway": null,
      "pay_with_bank": false,
      "active": true,
      "is_deleted": null,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 2,
      "createdAt": "2016-07-14T10:04:29.000Z",
      "updatedAt": "2020-02-18T20:24:02.000Z"
    },
    {
      "name": "Ecobank Nigeria",
      "slug": "ecobank-nigeria",
      "code": "050",
      "longcode": "050150010",
      "gateway": null,
      "pay_with_bank": false,
      "active": true,
      "is_deleted": null,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 4,
      "createdAt": "2016-07-14T10:04:29.000Z",
      "updatedAt": "2020-02-18T20:23:53.000Z"
    },
    {
      "name": "Ekondo Microfinance Bank",
      "slug": "ekondo-microfinance-bank",
      "code": "562",
      "longcode": "",
      "gateway": null,
      "pay_with_bank": false,
      "active": true,
      "is_deleted": null,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 64,
      "createdAt": "2018-09-23T05:55:06.000Z",
      "updatedAt": "2018-09-23T05:55:06.000Z"
    },
    {
      "name": "Fidelity Bank",
      "slug": "fidelity-bank",
      "code": "070",
      "longcode": "070150003",
      "gateway": "emandate",
      "pay_with_bank": false,
      "active": true,
      "is_deleted": null,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 6,
      "createdAt": "2016-07-14T10:04:29.000Z",
      "updatedAt": "2020-02-18T07:25:19.000Z"
    },
    {
      "name": "First Bank of Nigeria",
      "slug": "first-bank-of-nigeria",
      "code": "011",
      "logo": "public/images/first-bank.png",
      "longcode": "011151003",
      "gateway": "ibank",
      "pay_with_bank": true,
      "active": true,
      "is_deleted": null,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 7,
      "createdAt": "2016-07-14T10:04:29.000Z",
      "updatedAt": "2019-11-21T05:09:47.000Z"
    },
    {
      "name": "First City Monument Bank",
      "slug": "first-city-monument-bank",
      "code": "214",
      "logo": "public/images/fcmb.png",
      "longcode": "214150018",
      "gateway": "emandate",
      "pay_with_bank": false,
      "active": true,
      "is_deleted": null,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 8,
      "createdAt": "2016-07-14T10:04:29.000Z",
      "updatedAt": "2020-02-18T08:06:46.000Z"
    },
    {
      "name": "Globus Bank",
      "slug": "globus-bank",
      "code": "00103",
      "longcode": "103015001",
      "gateway": null,
      "pay_with_bank": false,
      "active": true,
      "is_deleted": false,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 70,
      "createdAt": "2020-02-11T15:38:57.000Z",
      "updatedAt": "2020-02-11T15:38:57.000Z"
    },
    {
      "name": "Guaranty Trust Bank",
      "slug": "guaranty-trust-bank",
      "code": "058",
      "logo": "public/images/gtb.png",
      "longcode": "058152036",
      "gateway": "ibank",
      "pay_with_bank": true,
      "active": true,
      "is_deleted": null,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 9,
      "createdAt": "2016-07-14T10:04:29.000Z",
      "updatedAt": "2019-03-27T07:47:17.000Z"
    },
    {
      "name": "Heritage Bank",
      "slug": "heritage-bank",
      "code": "030",
      "longcode": "030159992",
      "gateway": null,
      "pay_with_bank": false,
      "active": true,
      "is_deleted": null,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 10,
      "createdAt": "2016-07-14T10:04:29.000Z",
      "updatedAt": "2020-02-18T20:24:23.000Z"
    },
    {
      "name": "Jaiz Bank",
      "slug": "jaiz-bank",
      "code": "301",
      "longcode": "301080020",
      "gateway": null,
      "pay_with_bank": false,
      "active": true,
      "is_deleted": null,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 22,
      "createdAt": "2016-10-10T17:26:29.000Z",
      "updatedAt": "2016-10-10T17:26:29.000Z"
    },
    {
      "name": "Keystone Bank",
      "slug": "keystone-bank",
      "code": "082",
      "longcode": "082150017",
      "gateway": null,
      "pay_with_bank": false,
      "active": true,
      "is_deleted": null,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 11,
      "createdAt": "2016-07-14T10:04:29.000Z",
      "updatedAt": "2020-02-18T20:23:45.000Z"
    },
    {
      "name": "Kuda Bank",
      "slug": "kuda-bank",
      "code": "50211",
      "longcode": "",
      "gateway": null,
      "pay_with_bank": false,
      "active": true,
      "is_deleted": false,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 67,
      "createdAt": "2019-11-15T17:06:54.000Z",
      "updatedAt": "2020-02-11T15:51:11.000Z"
    },
    {
      "name": "Parallex Bank",
      "slug": "parallex-bank",
      "code": "526",
      "longcode": "",
      "gateway": null,
      "pay_with_bank": false,
      "active": true,
      "is_deleted": null,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 26,
      "createdAt": "2017-03-31T13:54:29.000Z",
      "updatedAt": "2019-01-30T09:43:56.000Z"
    },
    {
      "name": "Polaris Bank",
      "slug": "polaris-bank",
      "code": "076",
      "longcode": "076151006",
      "gateway": null,
      "pay_with_bank": false,
      "active": true,
      "is_deleted": null,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 13,
      "createdAt": "2016-07-14T10:04:29.000Z",
      "updatedAt": "2016-07-14T10:04:29.000Z"
    },
    {
      "name": "Providus Bank",
      "slug": "providus-bank",
      "code": "101",
      "longcode": "",
      "gateway": null,
      "pay_with_bank": false,
      "active": true,
      "is_deleted": null,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 25,
      "createdAt": "2017-03-27T16:09:29.000Z",
      "updatedAt": "2019-12-16T10:14:36.000Z"
    },
    {
      "name": "Rubies MFB",
      "slug": "rubies-mfb",
      "code": "125",
      "longcode": "",
      "gateway": null,
      "pay_with_bank": false,
      "active": true,
      "is_deleted": false,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 69,
      "createdAt": "2020-01-25T09:49:59.000Z",
      "updatedAt": "2020-01-25T09:49:59.000Z"
    },
    {
      "name": "Sparkle Microfinance Bank",
      "slug": "sparkle-microfinance-bank",
      "code": "51310",
      "longcode": "",
      "gateway": null,
      "pay_with_bank": false,
      "active": true,
      "is_deleted": false,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 72,
      "createdAt": "2020-02-11T18:43:14.000Z",
      "updatedAt": "2020-02-11T18:43:14.000Z"
    },
    {
      "name": "Stanbic IBTC Bank",
      "slug": "stanbic-ibtc-bank",
      "code": "221",
      "longcode": "221159522",
      "gateway": null,
      "pay_with_bank": false,
      "active": true,
      "is_deleted": null,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 14,
      "createdAt": "2016-07-14T10:04:29.000Z",
      "updatedAt": "2020-02-18T20:24:17.000Z"
    },
    {
      "name": "Standard Chartered Bank",
      "slug": "standard-chartered-bank",
      "code": "068",
      "longcode": "068150015",
      "gateway": null,
      "pay_with_bank": false,
      "active": true,
      "is_deleted": null,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 15,
      "createdAt": "2016-07-14T10:04:29.000Z",
      "updatedAt": "2020-02-18T20:23:40.000Z"
    },
    {
      "name": "Sterling Bank",
      "slug": "sterling-bank",
      "code": "232",
      "longcode": "232150016",
      "gateway": "emandate",
      "pay_with_bank": true,
      "active": true,
      "is_deleted": null,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 16,
      "createdAt": "2016-07-14T10:04:29.000Z",
      "updatedAt": "2020-02-06T13:03:01.000Z"
    },
    {
      "name": "Suntrust Bank",
      "slug": "suntrust-bank",
      "code": "100",
      "longcode": "",
      "gateway": null,
      "pay_with_bank": false,
      "active": true,
      "is_deleted": null,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 23,
      "createdAt": "2016-10-10T17:26:29.000Z",
      "updatedAt": "2016-10-10T17:26:29.000Z"
    },
    {
      "name": "TAJ Bank",
      "slug": "taj-bank",
      "code": "302",
      "longcode": "",
      "gateway": null,
      "pay_with_bank": false,
      "active": true,
      "is_deleted": false,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 68,
      "createdAt": "2020-01-20T11:20:32.000Z",
      "updatedAt": "2020-01-20T11:20:32.000Z"
    },
    {
      "name": "Titan Bank",
      "slug": "titan-bank",
      "code": "102",
      "longcode": "",
      "gateway": null,
      "pay_with_bank": false,
      "active": true,
      "is_deleted": false,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 73,
      "createdAt": "2020-03-10T11:41:36.000Z",
      "updatedAt": "2020-03-23T15:06:29.000Z"
    },
    {
      "name": "Union Bank of Nigeria",
      "slug": "union-bank-of-nigeria",
      "code": "032",
      "longcode": "032080474",
      "gateway": "emandate",
      "pay_with_bank": false,
      "active": true,
      "is_deleted": null,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 17,
      "createdAt": "2016-07-14T10:04:29.000Z",
      "updatedAt": "2020-02-18T20:22:54.000Z"
    },
    {
      "name": "United Bank For Africa",
      "slug": "united-bank-for-africa",
      "code": "033",
      "longcode": "033153513",
      "gateway": "emandate",
      "pay_with_bank": true,
      "active": true,
      "is_deleted": null,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 18,
      "createdAt": "2016-07-14T10:04:29.000Z",
      "updatedAt": "2019-05-20T21:23:20.000Z"
    },
    {
      "name": "Unity Bank",
      "slug": "unity-bank",
      "code": "215",
      "longcode": "215154097",
      "gateway": "emandate",
      "pay_with_bank": false,
      "active": true,
      "is_deleted": null,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 19,
      "createdAt": "2016-07-14T10:04:29.000Z",
      "updatedAt": "2019-07-22T12:44:02.000Z"
    },
    {
      "name": "VFD",
      "slug": "vfd",
      "code": "566",
      "longcode": "",
      "gateway": null,
      "pay_with_bank": false,
      "active": true,
      "is_deleted": false,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 71,
      "createdAt": "2020-02-11T15:44:11.000Z",
      "updatedAt": "2020-02-11T15:44:11.000Z"
    },
    {
      "name": "Wema Bank",
      "slug": "wema-bank",
      "code": "035",
      "longcode": "035150103",
      "gateway": null,
      "pay_with_bank": false,
      "active": true,
      "is_deleted": null,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 20,
      "createdAt": "2016-07-14T10:04:29.000Z",
      "updatedAt": "2020-02-18T20:23:58.000Z"
    },
    {
      "name": "Zenith Bank",
      "slug": "zenith-bank",
      "code": "057",
      "longcode": "057150013",
      "gateway": "emandate",
      "pay_with_bank": true,
      "active": true,
      "is_deleted": null,
      "country": "Nigeria",
      "currency": "NGN",
      "type": "nuban",
      "id": 21,
      "createdAt": "2016-07-14T10:04:29.000Z",
      "updatedAt": "2016-07-14T10:04:29.000Z"
    }
  ];

  void _onBankSelect(String value) async {
    _accountNumberFocusNode.unfocus();
    var accountNumber = _accountNumberController.text;
    accountNumber.trim();
    var regEx = RegExp(r"^[0-9]{10}$", multiLine: true);
    if (!regEx.hasMatch(accountNumber)) {
      if (this.mounted) {
        setState(() {
          _accountNumberErrVisible = true;
          _accountNumberErrText = "Account number is invalid";
        });
      }
      return;
    }
    //end of account validation
    if (this.mounted) {
      setState(() {
        defaultValue = value;
        _unresolvedErrText = "";
        _unresolvedErrVisible = false;
      });
    }
    if (defaultValue == "-- select bank --") {
      return;
    }

    var accountNumberFromList = _bankList.firstWhere(
        (bank) => bank.accountNumber == _accountNumberController.text,
        orElse: () => null);

    if (accountNumberFromList != null) {
      _unresolvedErrText = "Account already added";
      _unresolvedErrVisible = true;
      return;
    }

    if (this.mounted) {
      setState(() {
        _resolvingAccount = true;
        accountResolved = false;
      });
    }
    try {
      final resolveResponse =
          await Provider.of<BankAccountProvider>(context, listen: false)
              .resolveAccountNumber(
                  accountNumber: _accountNumberController.text,
                  bankCode: banks.firstWhere(
                      (bank) => bank["name"] == defaultValue)["code"]);

      if (this.mounted) {
        setState(() {
          if (resolveResponse["status"]) {
            _accountName = resolveResponse["data"]["account_name"];
            _resolvingAccount = false;
            accountResolved = true;
          } else {
            _resolvingAccount = false;
            accountResolved = false;
            _unresolvedErrText = "Could not resolve account, please try again";
            _unresolvedErrVisible = true;
          }
        });
      }
    } catch (e) {
      if (this.mounted) {
        setState(() {
          _resolvingAccount = false;
          accountResolved = false;
          _unresolvedErrText = "Something went wrong, please try again";
          _unresolvedErrVisible = true;
        });
      }
    }
  }

  void _onSubmit() async {
    if (this.mounted) {
      setState(() {
        isSending = true;
        _submitErrText = "";
        _submitErrVisible = false;
      });
    }
    var bankCode =
        banks.firstWhere((bank) => bank["name"] == defaultValue)["code"];
    final userData =
        await Provider.of<UserProvider>(context, listen: false).userData;
    final newAccountResponse =
        await Provider.of<BankAccountProvider>(context, listen: false)
            .addNewAccount(body: {
      "bank_name": defaultValue,
      "bank_code": bankCode,
      "account_name": _accountName,
      "acc_number": _accountNumberController.text,
      "userId": userData["id"]
    });

    if (newAccountResponse["status"] == 200 ||
        newAccountResponse["status"] == 201) {
      widget.switchTo();
      if (this.mounted) {
        setState(() {
          isSending = false;
        });
      }
    } else {
      if (this.mounted) {
        setState(() {
          isSending = false;
          _submitErrText = "Something went wrong, please try again.";
          _submitErrVisible = true;
        });
      }
    }
  }

  @override
  void dispose() {
    super.dispose();
    BackButtonInterceptor.remove(myInterceptor);
    _accountNumberController.dispose();
    _bankController.dispose();
    _accountNumberFocusNode.dispose();
    _bankFocusNode.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var appColors = Provider.of<AppColorsProvider>(context, listen: false);
    return SingleChildScrollView(
      child: Container(
        child: Stack(children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(vertical: 30),
                child: MainNavBar(
                  title: "Add Bank Account",
                  leadingImagePath: 'public/images/arrow-back.png',
                  notificationImagePath: 'public/images/notification.png',
                  leadingPressAction: onBackButtonPressed,
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    // Text(
                    //   "Add Bank Account",
                    //   style: Theme.of(context).textTheme.body1.copyWith(
                    //         fontWeight: FontWeight.w600,
                    //         height: 12 / 16,
                    //       ),
                    // ),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      "Account Number",
                      style: Theme.of(context).textTheme.body1.copyWith(
                            fontWeight: FontWeight.w300,
                            height: 12 / 16,
                          ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Text(
                      "Enter your 10 digits account number",
                      style: Theme.of(context).textTheme.body1.copyWith(
                          fontWeight: FontWeight.w600,
                          // fontSize: 10,
                          height: 8 / 11,
                          color: Color(0xFF888888)),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    CustomTextField(
                      textEditingController: _accountNumberController,
                      focusNode: _accountNumberFocusNode,
                      borderRadius: 2,
                      // borderColor: Color.fromRGBO(103, 102, 165, 0.5),
                      onChanged: (value) {
                        if (this.mounted) {
                          setState(() {
                            accountResolved = false;
                          });
                        }
                      },
                      textInputType: TextInputType.number,
                      inputFormatters: [
                        WhitelistingTextInputFormatter.digitsOnly,
                        LengthLimitingTextInputFormatter(10)
                      ],
                    ),
                    ErrText(
                      errText: _accountNumberErrText,
                      vissibility: _accountNumberErrVisible,
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Text(
                      "Bank",
                      style: Theme.of(context).textTheme.body1.copyWith(
                            fontWeight: FontWeight.w300,
                          ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    DropDownField(
                      defaultItem: defaultValue,
                      textEditingController: _bankController,
                      focusNode: _bankFocusNode,
                      items: banks.map((bank) => bank["name"]).toList(),
                      dropDownOnly: true,
                      borderRadius: 2,
                      // borderColor: Color.fromRGBO(103, 102, 165, 0.5),
                      onTap: () {
                        _accountNumberFocusNode.unfocus();
                      },
                      onChanged: (value) {
                        _onBankSelect(value);
                      },
                    ),
                    ErrText(
                      errText: _unresolvedErrText,
                      vissibility: _unresolvedErrVisible,
                    ),
                    if (_resolvingAccount)
                      Align(
                        child: Container(
                          margin: EdgeInsets.only(top: 25),
                          child: CustomProgressIndicator(
                            size: 40,
                            color: Theme.of(context).primaryColor,
                          ),
                        ),
                      ),
                    SizedBox(
                      height: 30,
                    ),
                    if (accountResolved)
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Text(
                            "Account Name",
                            style: Theme.of(context).textTheme.body1.copyWith(
                                  fontWeight: FontWeight.w300,
                                  height: 12 / 16,
                                ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Container(
                            height: 44,
                            decoration: BoxDecoration(
                              border: Border.all(
                                  color: Color.fromRGBO(103, 102, 165, 0.5),
                                  width: 1),
                              borderRadius: BorderRadius.circular(2),
                            ),
                            padding: EdgeInsets.only(left: 15, top: 12),
                            child: Text(_accountName),
                          ),
                          // CustomTextField(
                          //   borderRadius: 2,
                          //   borderColor: Color.fromRGBO(103, 102, 165, 0.5),
                          //   textEditingController: _accountNameController,
                          //   focusNode: _accountNameFocusNode,
                          // ),
                          SizedBox(height: 190),
                          CustomRaisedButton(
                            title: isSending ? " " : "Submit",
                            indicator: isSending
                                ? CustomProgressIndicator(
                                    // color: appColors.primaryProgressValueColor,
                                    )
                                : Text(""),
                            onTapAction: () => isSending ? () {} : _onSubmit(),
                          ),
                          ErrText(
                            errText: _submitErrText,
                            vissibility: _submitErrVisible,
                          ),
                        ],
                      ),
                  ],
                ),
              ),
            ],
          ),
          if (_resolvingAccount)
            Container(
              height: 890,
              color: Colors.transparent,
            )
        ]),
      ),
    );
  }
}
