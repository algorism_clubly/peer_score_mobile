import 'dart:io';

import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:peer_score/utils/main_nav_bar.dart';
import 'package:peer_score/utils/snackbar.dart';
import 'package:provider/provider.dart';
import 'package:get_version/get_version.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';

class PrivacyPage extends StatefulWidget {
  final Function navBarLeadingFn;
  PrivacyPage({this.navBarLeadingFn});
  @override
  _PrivacyPageState createState() => _PrivacyPageState();
}

class _PrivacyPageState extends State<PrivacyPage> {
  String projectVersion = "";

  @override
  void initState() {
    // TODO: implement initState
    BackButtonInterceptor.add(myInterceptor);
    getAppVersionName();
    super.initState();
  }

  void getAppVersionName() async {
    try {
      projectVersion = await GetVersion.projectVersion;
      setState(() {});
    } on PlatformException {
      projectVersion = '_';
      setState(() {});
    }
  }

  bool myInterceptor(bool stopDefaultButtonEvent) {
    if (stopDefaultButtonEvent) return false;
    widget.navBarLeadingFn();
    return true;
  }

  void openWebsite(String url) async {
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: Platform.isIOS ? false : null);
    } else {
      CustomSnackBar.snackBar(
        title: "Could not open url.",
        context: context,
      );
    }
  }

  Widget buildLinkTile({String title, String url}) {
    return InkWell(
      onTap: () => openWebsite(url),
      child: Container(
        padding: EdgeInsets.all(12.0),
        width: MediaQuery.of(context).size.width * 0.85,
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              width: 0.08,
            ),
          ),
        ),
        child: Text(
          title,
          textAlign: TextAlign.center,
          style: Theme.of(context)
              .textTheme
              .body1
              .copyWith(fontWeight: FontWeight.w600, fontSize: 16),
        ),
      ),
    );
  }

  @override
  void dispose() {
    BackButtonInterceptor.remove(myInterceptor);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(vertical: 30),
            child: MainNavBar(
              title: "About",
              leadingImagePath: 'public/images/arrow-back.png',
              notificationImagePath: 'public/images/notification.png',
              leadingPressAction: widget.navBarLeadingFn,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  HelperFunctions.buildLogo(context: context),
                  Container(
                    margin: EdgeInsets.only(
                      top: 8,
                    ),
                    child: Text(
                      "PeerScore",
                      style: Theme.of(context)
                          .textTheme
                          .body1
                          .copyWith(fontWeight: FontWeight.w600, fontSize: 16),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 8, bottom: 35),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Version",
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          projectVersion,
                        ),
                      ],
                    ),
                  ),
                  buildLinkTile(
                      title: "About", url: "https://peerscore.ng/about"),
                  buildLinkTile(
                      title: "Support", url: "https://peerscore.ng/support"),
                  buildLinkTile(
                      title: "Privacy", url: "https://peerscore.ng/privacy")
                ],
              ),
            ),
          ),
          Spacer(),
          Container(
            width: MediaQuery.of(context).size.width * 0.85,
            margin: EdgeInsets.only(bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                FaIcon(
                  FontAwesomeIcons.copyright,
                  color:
                      GeneralProvider.isLightMode ? Colors.black : Colors.white,
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  "PeerScore ${DateTime.now().year}",
                  style: Theme.of(context).textTheme.body1.copyWith(
                        fontWeight: FontWeight.normal,
                      ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
