import 'dart:io';

import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:dio/dio.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mime_type/mime_type.dart';
import 'package:peer_score/providers/colors.dart';
import 'package:peer_score/providers/user-provider.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/screens/main_screen.dart';
import 'package:peer_score/screens/welcome_back.dart';
import 'package:peer_score/utils/custom_checkbox.dart';
import 'package:peer_score/utils/custom_raised_button.dart';
import 'package:peer_score/utils/err_text_widget.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:peer_score/utils/main_nav_bar.dart';
import 'package:peer_score/utils/modal_actions.dart';
import 'package:peer_score/utils/progress_indicator.dart';
import 'package:peer_score/utils/snackbar.dart';
import 'package:peer_score/utils/text_form_field.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfilePage extends StatefulWidget {
  final Function switchTo;
  ProfilePage({this.switchTo});
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  TextEditingController _phoneController = TextEditingController(text: "");
  TextEditingController _firstNameController = TextEditingController(text: "");
  TextEditingController _lastNameController = TextEditingController(text: "");
  TextEditingController _emailController = TextEditingController(text: "");
  TextEditingController _otherNamesController = TextEditingController(text: "");
  FocusNode _phoneFocusNode = FocusNode();
  FocusNode _firstNameFocusNode = FocusNode();
  FocusNode _lastNameFocusNode = FocusNode();
  FocusNode _emailFocusNode = FocusNode();
  FocusNode _otherNamesFocusNode = FocusNode();
  var altPhoneNumbers = [];
  Map<String, dynamic> userData = {};
  String userImage = "";
  bool _whatsApp = false;
  File _image;
  bool isSending = false;
  bool isFirstNameFilled = false;
  bool isLastNameFilled = false;
  bool isOtherNamesFilled = false;
  bool isEmailFilled = false;
  bool isLoadingImage = false;
  String _submitErrText = "";
  bool _submitErrVisible = false;
  String _firstNameErrText = "";
  bool _firstNameErrVisible = false;
  String _emailErrText = "";
  bool _emailErrVisible = false;
  String errText;
  bool isDeletingPhone = false;
  GlobalKey<ScaffoldState> _scaffoldKey;
  List<Map<String, dynamic>> _alterNamePhoneNumbers = [];
  @override
  void initState() {
    super.initState();
    BackButtonInterceptor.add(myInterceptor);
    _scaffoldKey =
        Provider.of<GeneralProvider>(context, listen: false).getAppScaffoldKey;

    Provider.of<UserProvider>(context, listen: false).profilePageCb = loadData;
    if (this.mounted) {
      loadData();
    }
  }

  void backFunction() {
    Navigator.of(context).pushReplacementNamed(MainScreen.routeName,
        arguments: {"testing": "argument"});
    GeneralProvider.profileScaffoldKey = null;
    // Navigator.of(context).pop();
  }

  bool myInterceptor(bool stopDefaultButtonEvent) {
    if (stopDefaultButtonEvent) return false;
    backFunction();
    return true;
  }

  loadData() {
    Provider.of<UserProvider>(context, listen: false).userData.then((data) {
      if (this.mounted) {
        setState(() {
          userData = data;
          _firstNameController.text = data["first_name"];
          _phoneController.text = HelperFunctions.deserializePhoneNumber(
              phoneNumber: data["phone"]);
          _lastNameController.text = data["last_name"];
          _emailController.text = data["email"];
          _otherNamesController.text = data["other_name"];
          userImage = data["profile_picture"];
          _whatsApp = data["whatsapp"];
          fillAppropriateFeilds(
            firstName: _firstNameController.text,
            lastName: _lastNameController.text,
            emailAddress: _emailController.text,
            otherNames: _otherNamesController.text,
          );
        });
      }
      getAllPhones();
    });
  }

  void getAllPhones() async {
    final response = await Provider.of<UserProvider>(context, listen: false)
        .getAllAlternatePhones(userId: userData["id"]);
    if (response["status"] == 200 || response["status"] == 201) {
      if (this.mounted) {
        setState(() {
          _alterNamePhoneNumbers =
              Provider.of<UserProvider>(context, listen: false)
                  .alternatePhoneNumbers;
        });
      }
    } else {
      if (this.mounted) {
        setState(() {
          CustomSnackBar.snackBar(
            title: "Something went wrong, please reload the page.",
            scaffoldState: _scaffoldKey,
            context: context,
          );
        });
      }
    }
  }

  void fillAppropriateFeilds(
      {String firstName,
      String lastName,
      String otherNames,
      String emailAddress}) {
    firstName.trim().length > 0
        ? isFirstNameFilled = true
        : isFirstNameFilled = false;

    lastName.trim().length > 0
        ? isLastNameFilled = true
        : isLastNameFilled = false;
    emailAddress.trim().length > 0
        ? isEmailFilled = true
        : isEmailFilled = false;
    otherNames.trim().length > 0
        ? isOtherNamesFilled = true
        : isOtherNamesFilled = false;
  }

  void addNewPhoneField() {
    Provider.of<UserProvider>(context, listen: false).profilePageCb = loadData;
    ModalActions.addAlternatePhone(context: context);
  }

  void removePhoneField({String phoneId}) async {
    if (this.mounted) {
      setState(() {
        isDeletingPhone = true;
      });
    }
    var deleteResponse = await Provider.of<UserProvider>(context, listen: false)
        .deleteAlternatePhone(
      body: {"id": phoneId, "userId": userData["id"]},
    );
    if (deleteResponse["status"] == 200 || deleteResponse["status"] == 201) {
      if (this.mounted) {
        setState(() {
          isDeletingPhone = false;
        });
      }
      ModalActions.showSuccess(
          context: context,
          content: "Phone number has been deleted successfully.",
          onOk: loadData());
    } else {
      if (this.mounted) {
        setState(() {
          isDeletingPhone = false;
          CustomSnackBar.snackBar(
            title: "Something went wrong, please try again.",
            scaffoldState: _scaffoldKey,
            context: context,
          );
        });
      }
    }
  }

  void showMoreInfo({String title, String content}) {
    ModalActions.showMoreInfo(context: context, title: title, content: content);
  }

  Future<PermissionStatus> _getStoragePermission() async {
    PermissionStatus permission = await Permission.storage.request();
    return permission;
  }

  Future _getImage(ImageSource source) async {
    PermissionStatus permissionStatus = await _getStoragePermission();
    if (permissionStatus != null) {
      if (source == ImageSource.gallery) {
        if (permissionStatus == PermissionStatus.permanentlyDenied) {
          return openAppSettings();
        }
      }
      var image = await ImagePicker.pickImage(source: source);
      if (this.mounted) {
        setState(() {
          String mimeType = mime(image.path);
          if (mimeType == "image/jpeg" ||
              mimeType == "image/jpg" ||
              mimeType == "image/png") {
            _image = image;
          } else {
            _image = null;
            // CustomSnackBar.snackBar(
            //   context: context,
            //   scaffoldState: widget.scaffoldState,
            //   title: "Unsupported file.",
            // );
          }
        });
      }
      Navigator.of(context).pop();
      if (this.mounted) {
        setState(() {
          isLoadingImage = true;
        });
      }
      final pictureResponse =
          await Provider.of<UserProvider>(context, listen: false)
              .uploadProfilePicture(
                  body: FormData.fromMap({
        "userId": userData["id"],
        "profile_picture": await MultipartFile.fromFile(_image.path,
            filename: "profile_picture")
      }));
      if (pictureResponse["status"] == 200 ||
          pictureResponse["status"] == 201) {
        if (this.mounted) {
          setState(() {
            userImage = pictureResponse["data"]["profile_picture"];
            isLoadingImage = false;
          });
        }
      } else {
        if (this.mounted) {
          setState(() {
            isLoadingImage = false;
          });
        }
      }
    }
  }

  void _onImageOpen(BuildContext context) {
    showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20), topRight: Radius.circular(20))),
        builder: (ctx) {
          return Container(
            // color: Theme.of(context).accentColor,
            height: 100,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                InkWell(
                  onTap: () => _getImage(ImageSource.camera),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        width: 60,
                        height: 60,
                        child: Image.asset("public/images/camera-open.png"),
                      ),
                      Text(
                        "Camera",
                        style: Theme.of(context)
                            .textTheme
                            .body2
                            .copyWith(color: Colors.black),
                      )
                    ],
                  ),
                ),
                // Spacer(),
                InkWell(
                  onTap: () => _getImage(ImageSource.gallery),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        width: 60,
                        height: 60,
                        child: Image.asset("public/images/gallery-icon.png"),
                      ),
                      // Icon(Icons.file_upload, size: 35, color: Colors.black),
                      Text(
                        "Gallery",
                        style: Theme.of(context)
                            .textTheme
                            .body2
                            .copyWith(color: Colors.black),
                      )
                    ],
                  ),
                )
              ],
            ),
          );
        });
  }

  Widget _buildAlternatePhoneTile(
      {String phoneNumber, int index, String phoneId}) {
    phoneNumber =
        HelperFunctions.deserializePhoneNumber(phoneNumber: phoneNumber);

    var appColors = Provider.of<AppColorsProvider>(context, listen: false);
    return Row(
      children: <Widget>[
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Text("Alternate Phone ${index + 1}",
                  style: Theme.of(context).textTheme.body1.copyWith(
                      fontSize: 10,
                      height: 10 / 14,
                      fontWeight: FontWeight.w300)),
              SizedBox(height: 3),
              Container(
                height: 30,
                padding: EdgeInsets.only(left: 15),
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(
                    color: appColors.altPhoneContainerColor,
                    borderRadius: BorderRadius.circular(2)),
                child:
                    Text(phoneNumber, style: Theme.of(context).textTheme.body1),
              )
            ],
          ),
        ),
        InkWell(
          onTap: () => ModalActions.warningDialog(
            context: context,
            content:
                "Do you really want to delete alternate phone number $phoneNumber?",
            onConfirm: () => removePhoneField(phoneId: phoneId),
          ),
          customBorder: CircleBorder(),
          child: Container(
            width: 40,
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
            // alignment: Alignment.center,
            child: FaIcon(
              Platform.isIOS
                  ? CupertinoIcons.minus_circled
                  : FontAwesomeIcons.minusCircle,
              size: 13,
              color: Color(0xFFDB6837),
            ),
          ),
        )
      ],
    );
  }

  void updateWhatsapp() async {
    final updateResponse =
        await Provider.of<UserProvider>(context, listen: false).updateUserInfo(
      body: {
        "userId": userData["id"],
        "whatsapp": _whatsApp,
        // "first_name": _firstNameController.text.trim().length > 0
        //     ? _firstNameController.text
        //     : null,
        // "last_name": _lastNameController.text.trim().length > 0
        //     ? _lastNameController.text
        //     : null,
        // "other_name": _otherNamesController.text.trim().length > 0
        //     ? _otherNamesController.text
        //     : null,
        // "email": _emailController.text.trim().length > 0
        //     ? _emailController.text
        //     : null,
      },
    );

    if (updateResponse["status"] == 200 || updateResponse["status"] == 201) {
    } else {
      setState(() {
        _whatsApp = userData["whatsapp"];
        CustomSnackBar.snackBar(
            context: context, title: "Something went wrong, please try again");
      });
    }
  }

  void _onSubmit() async {
    //do validations here
    if (_firstNameController.text.isEmpty) {
      if (this.mounted) {
        setState(() {
          _firstNameErrText = "*First name is required";
          _firstNameErrVisible = true;
        });
      }
      return;
    }
    //check if email is not empty then validate
    if (_emailController.text.isNotEmpty) {
      if (!EmailValidator.validate(_emailController.text)) {
        if (this.mounted) {
          setState(() {
            _emailErrText = "*Invalid email";
            _emailErrVisible = true;
          });
        }
        return;
      }
    }
    if (this.mounted) {
      setState(() {
        isSending = true;
      });
    }

    _phoneFocusNode.unfocus();
    _firstNameFocusNode.unfocus();
    _lastNameFocusNode.unfocus();
    _emailFocusNode.unfocus();
    _otherNamesFocusNode.unfocus();

    final updateResponse =
        await Provider.of<UserProvider>(context, listen: false).updateUserInfo(
      body: {
        "userId": userData["id"],
        "first_name": _firstNameController.text.trim().length > 0
            ? _firstNameController.text
            : null,
        "last_name": _lastNameController.text.trim().length > 0
            ? _lastNameController.text
            : null,
        "other_name": _otherNamesController.text.trim().length > 0
            ? _otherNamesController.text
            : null,
        "email": _emailController.text.trim().length > 0
            ? _emailController.text
            : null,
      },
    );

    if (updateResponse["status"] == 200 || updateResponse["status"] == 201) {
      if (this.mounted) {
        //show success modal here
        ModalActions.showSuccess(
            context: context,
            content: "Profile information updated successfully!");
        setState(() {
          isSending = false;
          isFirstNameFilled = true;
          isLastNameFilled = true;
          isOtherNamesFilled = true;
          isEmailFilled = true;
          fillAppropriateFeilds(
            firstName: _firstNameController.text,
            lastName: _lastNameController.text,
            emailAddress: _emailController.text,
            otherNames: _otherNamesController.text,
          );
        });
      }
    } else {
      if (this.mounted) {
        setState(() {
          // errText = updateResponse["errors"];
          _submitErrText = "Something went wrong, please try again.";
          _submitErrVisible = true;
          isSending = false;
        });
      }
      final userData =
          await Provider.of<UserProvider>(context, listen: false).userData;
      if (this.mounted) {
        setState(() {
          _firstNameController.text = userData["first_name"];
          // _phoneController.text = userData["phone"];
          _lastNameController.text = userData["last_name"];
          _emailController.text = userData["email"];
          _otherNamesController.text = userData["other_name"];
          fillAppropriateFeilds(
            firstName: _firstNameController.text,
            lastName: _lastNameController.text,
            emailAddress: _emailController.text,
            otherNames: _otherNamesController.text,
          );
        });
      }
    }
  }

  @override
  void dispose() {
    BackButtonInterceptor.remove(myInterceptor);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var appColors = Provider.of<AppColorsProvider>(context, listen: false);
    return Container(
      child: SingleChildScrollView(
        child: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.symmetric(vertical: 30),
                  child: MainNavBar(
                    title: "Profile",
                    leadingImagePath: 'public/images/arrow-back.png',
                    leadingPressAction: backFunction,
                    notificationImagePath: 'public/images/notification.png',
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 30, right: 30, bottom: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: 100,
                        child: Row(
                          children: <Widget>[
                            ClipRRect(
                              borderRadius: BorderRadius.circular(8),
                              child: Stack(
                                alignment: AlignmentDirectional.center,
                                children: <Widget>[
                                  SizedBox(
                                    width: 100,
                                    height: 100,
                                    child: FadeInImage(
                                      placeholder: AssetImage(
                                          "public/images/profile-picture-placeholder.png"),
                                      image: NetworkImage(userImage),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  Positioned(
                                    bottom: 4,
                                    right: 3,
                                    child: InkWell(
                                      onTap: () {
                                        _onImageOpen(context);
                                      },
                                      customBorder: CircleBorder(),
                                      child: Container(
                                        height: 25,
                                        padding: EdgeInsets.all(5),
                                        child: Image.asset(
                                          "public/images/camera-icon.png",
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                  if (isLoadingImage)
                                    Container(
                                      width: 100,
                                      decoration: BoxDecoration(
                                          color: Color.fromRGBO(
                                              169, 169, 169, 0.5)),
                                    ),
                                  if (isLoadingImage) CustomProgressIndicator()
                                ],
                              ),
                            ),
                            SizedBox(width: 15),
                            Expanded(
                              child: Column(
                                // mainAxisAlignment:
                                //     MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  CustomTextField(
                                    textEditingController: _phoneController,
                                    focusNode: _phoneFocusNode,
                                    title: "Phone",
                                    height: 30,
                                    borderRadius: 2,
                                    filled: true,
                                    readOnly: true,
                                  ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  FittedBox(
                                    child: Row(
                                      children: <Widget>[
                                        CustomCheckBox(
                                          value: _whatsApp,
                                          onClick: (selectedValue) {
                                            setState(() {
                                              _whatsApp = selectedValue;
                                              updateWhatsapp();
                                            });
                                          },
                                          title: "WhatsApp Availability",
                                        ),
                                      ],
                                    ),
                                  ),
                                  // if (_alterNamePhoneNumbers.length > 0)
                                  //   _buildAlternatePhoneTile(
                                  //     phoneNumber: _alterNamePhoneNumbers[0]
                                  //         ["phone"],
                                  //     phoneId: _alterNamePhoneNumbers[0]["id"],
                                  //     index: 1,
                                  //   )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                          height: _alterNamePhoneNumbers.length > 0 ? 20 : 8),
                      Container(
                        // height: 300,
                        constraints:
                            BoxConstraints(maxHeight: 200, minHeight: 0),
                        child: ListView.builder(
                            shrinkWrap: true,
                            itemCount: _alterNamePhoneNumbers.length,
                            itemBuilder: (ctx, index) {
                              // if (index == _alterNamePhoneNumbers.length - 1)
                              //   return Text("");
                              return Container(
                                margin: EdgeInsets.only(bottom: 10),
                                child: _buildAlternatePhoneTile(
                                  phoneNumber: _alterNamePhoneNumbers[index]
                                      ["phone"],
                                  phoneId: _alterNamePhoneNumbers[index]["id"],
                                  index: index,
                                ),
                              );

                              // return Container(
                              //   margin: EdgeInsets.only(bottom: 10),
                              //   child: _buildAlternatePhoneTile(
                              //     phoneNumber: _alterNamePhoneNumbers[index + 1]
                              //         ["phone"],
                              //     phoneId: _alterNamePhoneNumbers[index + 1]
                              //         ["id"],
                              //     index: index + 2,
                              //   ),
                              // );
                            }),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          InkWell(
                            onTap: addNewPhoneField,
                            child: Padding(
                              padding: const EdgeInsets.all(5),
                              child: Row(
                                children: <Widget>[
                                  SizedBox(
                                    width: 13,
                                    height: 13,
                                    child: Image.asset(
                                        "public/images/phone-icon.png"),
                                  ),
                                  SizedBox(width: 5),
                                  Text(
                                    "Add Phone",
                                    style: Theme.of(context)
                                        .textTheme
                                        .body1
                                        .copyWith(
                                          // fontSize: 10,
                                          fontWeight: FontWeight.w600,
                                        ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          SizedBox(width: 5),
                          InkWell(
                            onTap: () => showMoreInfo(
                              title: "Phone Number",
                              content:
                                  "You can add alternate numbers that belong to you for ease of access and your PeerScore increases for every number you add.",
                            ),
                            child: HelperFunctions.buildQuestionMark(),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Text(
                        "About You",
                        style: Theme.of(context).textTheme.body1.copyWith(
                              fontWeight: FontWeight.w600,
                            ),
                      ),
                      SizedBox(
                        height: 25,
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: CustomTextField(
                              textEditingController: _firstNameController,
                              focusNode: _firstNameFocusNode,
                              title: "First Name",
                              borderRadius: 2,
                              height: 40,
                              filled: isFirstNameFilled,
                              onTap: () {
                                if (this.mounted) {
                                  setState(() {
                                    isFirstNameFilled = false;
                                    _submitErrText = "";
                                    _submitErrVisible = false;
                                    _firstNameErrText = "";
                                    _firstNameErrVisible = false;
                                  });
                                }
                              },
                              inputFormatters: [
                                WhitelistingTextInputFormatter(
                                    RegExp("[a-zA-Z]")),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 13,
                          ),
                          Expanded(
                            child: CustomTextField(
                              textEditingController: _lastNameController,
                              focusNode: _lastNameFocusNode,
                              title: "Last Name",
                              borderRadius: 2,
                              height: 40,
                              filled: isLastNameFilled,
                              onTap: () {
                                if (this.mounted) {
                                  setState(() {
                                    isLastNameFilled = false;
                                    _submitErrText = "";
                                    _submitErrVisible = false;
                                  });
                                }
                              },
                              inputFormatters: [
                                WhitelistingTextInputFormatter(
                                    RegExp("[a-zA-Z]")),
                              ],
                            ),
                          )
                        ],
                      ),
                      ErrText(
                        errText: _firstNameErrText,
                        vissibility: _firstNameErrVisible,
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      CustomTextField(
                        textEditingController: _otherNamesController,
                        focusNode: _otherNamesFocusNode,
                        title: "Other Name",
                        borderRadius: 2,
                        height: 40,
                        filled: isOtherNamesFilled,
                        onTap: () {
                          if (this.mounted) {
                            setState(() {
                              isOtherNamesFilled = false;
                              _submitErrText = "";
                              _submitErrVisible = false;
                            });
                          }
                        },
                        inputFormatters: [
                          WhitelistingTextInputFormatter(RegExp("[a-zA-Z]")),
                        ],
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      CustomTextField(
                        textEditingController: _emailController,
                        focusNode: _emailFocusNode,
                        title: "Email Address",
                        borderRadius: 2,
                        height: 40,
                        filled: isEmailFilled,
                        onTap: () {
                          if (this.mounted) {
                            setState(() {
                              isEmailFilled = false;
                              _emailErrText = "";
                              _emailErrVisible = false;
                              _submitErrText = "";
                              _submitErrVisible = false;
                            });
                          }
                        },
                        // inputFormatters: [
                        //   TextInputFormatter.withFunction(formatFunction)
                        // ],
                      ),
                      ErrText(
                        errText: _emailErrText,
                        vissibility: _emailErrVisible,
                      ),
                      SizedBox(
                        height: 25,
                      ),
                      CustomRaisedButton(
                        title: isSending ? "" : "Update",
                        indicator: isSending
                            ? CustomProgressIndicator(
                                // color: appColors.primaryProgressValueColor,
                                )
                            : Text(""),
                        onTapAction: () {
                          _submitErrText = "";
                          _submitErrVisible = false;
                          isSending ? null : _onSubmit();
                        },
                      ),
                      ErrText(
                        errText: _submitErrText,
                        vissibility: _submitErrVisible,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      // Row(
                      //   children: <Widget>[
                      //     Text(
                      //       "Connect Social Accounts",
                      //       style: Theme.of(context).textTheme.body1.copyWith(
                      //             fontSize: 10,
                      //             fontWeight: FontWeight.w600,
                      //           ),
                      //     ),
                      //     SizedBox(width: 6),
                      //     InkWell(
                      //       onTap: () => showMoreInfo(
                      //         title: "Social Accounts",
                      //         content:
                      //             "PeerScore can determine your accurate score if granted access to your social media accounts and importantly your circle of friends.",
                      //       ),
                      //       child: CircleAvatar(
                      //         radius: 8.5,
                      //         backgroundColor:
                      //             Color.fromRGBO(103, 102, 165, 0.8),
                      //         child: FaIcon(
                      //           FontAwesomeIcons.solidQuestion,
                      //           size: 10,
                      //           color: Colors.white,
                      //         ),
                      //       ),
                      //     )
                      //   ],
                      // ),
                      // SizedBox(
                      //   height: 15,
                      // ),
                      // Row(
                      //   children: <Widget>[
                      //     SizedBox(
                      //       width: 29,
                      //       height: 29,
                      //       child: Image.asset("public/images/linkedin.png"),
                      //     ),
                      //     SizedBox(width: 10),
                      //     SizedBox(
                      //       width: 29,
                      //       height: 29,
                      //       child: Image.asset("public/images/facebook.png"),
                      //     ),
                      //     SizedBox(width: 10),
                      //     SizedBox(
                      //       width: 29,
                      //       height: 29,
                      //       child: Image.asset("public/images/twitter.png"),
                      //     ),
                      //   ],
                      // ),
                      SizedBox(
                        height: 15,
                      ),
                      InkWell(
                        onTap: () => widget.switchTo(),
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 8, right: 8, bottom: 8),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text(
                                "Settings and privacy",
                                style:
                                    Theme.of(context).textTheme.body1.copyWith(
                                          fontWeight: FontWeight.w600,
                                        ),
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              FaIcon(
                                FontAwesomeIcons.solidCog,
                                size: 12,
                                color: appColors.defaultIconColor,
                              )
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      InkWell(
                        onTap: () {
                          Provider.of<UserProvider>(context, listen: false)
                              .logout();
                          Navigator.of(context).pushReplacementNamed(
                              WelcomeBackScreen.routeName);
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 8, right: 8, bottom: 8),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text(
                                "Logout",
                                style:
                                    Theme.of(context).textTheme.body1.copyWith(
                                          fontWeight: FontWeight.w600,
                                        ),
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              FaIcon(
                                FontAwesomeIcons.lightSignOut,
                                size: 12,
                                color: appColors.defaultIconColor,
                              )
                            ],
                          ),
                        ),
                      ),
                      // Spacer(),
                      SizedBox(
                        height: 15,
                      ),
                    ],
                  ),
                )
              ],
            ),
            if (isDeletingPhone)
              Container(
                height: MediaQuery.of(context).size.height,
                color: Color.fromRGBO(169, 169, 169, 0.1),
                child: Center(
                  child: CustomProgressIndicator(
                    size: 45,
                    color: Theme.of(context).primaryColor,
                  ),
                ),
              )
          ],
        ),
      ),
    );
  }
}
