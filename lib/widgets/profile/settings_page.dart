import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get_version/get_version.dart';
import 'package:googleapis/calendar/v3.dart' as googleCal;
import 'package:googleapis_auth/auth_io.dart';
import 'package:peer_score/providers/colors.dart';
import 'package:peer_score/providers/temp-provider.dart';
import 'package:peer_score/providers/user-provider.dart';
import 'package:peer_score/utils/custom_radio_tile.dart';
import 'package:peer_score/utils/custom_switch.dart';
import 'package:peer_score/utils/functions.dart';
import 'package:peer_score/utils/googleCalender.dart';
import 'package:peer_score/utils/main_nav_bar.dart';
import 'package:peer_score/utils/modal_actions.dart';
import 'package:peer_score/utils/theme_mode_singleton.dart';
import 'package:peer_score/widgets/contacts/contact_util.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:http/src/client.dart';

class SettingsPage extends StatefulWidget {
  final Function switchTo;
  final Function navBarLeadingFn;
  SettingsPage({this.switchTo, this.navBarLeadingFn});
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  bool autoSyncValue = false;
  bool googleCalenderValue = false;
  Map<String, dynamic> userData = {};
  String projectVersion = "";
  @override
  void initState() {
    getAutoSyncValue();
    getAppVersionName();
    BackButtonInterceptor.add(myInterceptor);
    Provider.of<UserProvider>(context, listen: false).userData.then((data) {
      setState(() {
        userData = data;
      });
      getCalendarSwitchValue();
    });
    super.initState();
  }

  void getAppVersionName() async {
    try {
      projectVersion = await GetVersion.projectVersion;
      setState(() {});
    } on PlatformException {
      projectVersion = '_';
      setState(() {});
    }
  }

  void showMoreInfo({String title, String content}) {
    ModalActions.showMoreInfo(context: context, title: title, content: content);
  }

  bool myInterceptor(bool stopDefaultButtonEvent) {
    if (stopDefaultButtonEvent) return false;
    widget.navBarLeadingFn();
    return true;
  }

  void getAutoSyncValue() async {
    autoSyncValue = await HelperFunctions.getAutoSyncValue();
    setState(() {});
  }

  void getCalendarSwitchValue() async {
    googleCalenderValue = await checkUserCalendaData(userId: userData["id"]);
    setState(() {});
  }

  void onSwitchSync({bool selectedSwitchValue}) async {
    bool value = selectedSwitchValue;
    if (selectedSwitchValue) {
      Map<String, dynamic> contactsResponse =
          await PhoneContacts.getPhoneContacts(
        context: context,
        userId: await userData["id"],
        isAutoSync: true,
      );
      if (contactsResponse["status"]) {
        PermissionStatus permissionStatus;
        permissionStatus = contactsResponse["permissionStatus"];
        if (permissionStatus == PermissionStatus.permanentlyDenied ||
            permissionStatus == PermissionStatus.denied) {
          value = false;
        } else {
          value = true;
        }
      } else {
        value = true;
      }
    }

    setState(() {
      autoSyncValue = value;
      HelperFunctions.setAutoSync(value: value);
    });
  }

  void onSwitchGoogleCal({bool selectedSwitchValue}) async {
    var isDone;
    if (selectedSwitchValue == false) {
      // nullify user credentials
      isDone = await removeUserCalenderData(userId: userData["id"]);
    } else {
      // set user credentials
      isDone = await addUserCalenderData(userId: userData["id"]);
    }

    if (isDone) {
      setState(() {
        googleCalenderValue = selectedSwitchValue;
      });
    }
  }

  void openThemeChangeBottomSheet() {
    bool myInterceptor(bool stopDefaultButtonEvent) {
      if (stopDefaultButtonEvent) return false;
      Navigator.of(context).pop();
      BackButtonInterceptor.remove(myInterceptor);
      return true;
    }

    BackButtonInterceptor.add(myInterceptor);

    ThemeModeSingleton themeModeSingleton = ThemeModeSingleton.getInstance();
    void onSelect({String themeMode}) {
      themeModeSingleton.setThemeMode(themeMode);
    }

    var _selectedValue = 0;
    switch (GeneralProvider.selectedMode.toLowerCase()) {
      case "light":
        _selectedValue = 0;
        break;
      case "dark":
        _selectedValue = 1;
        break;
      case "system":
        _selectedValue = 2;
        break;
      default:
    }
    Widget modeTile({
      String title,
      int value,
    }) {
      return Container(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        child: CustomRadioTile(
          title: title,
          style: Theme.of(context).textTheme.body2.copyWith(
                fontWeight: FontWeight.w400,
              ),
          value: value,
          selectedValue: _selectedValue,
          onSelect: (value) => setState(
            () {
              _selectedValue = value;
              onSelect(themeMode: title);
            },
          ),
        ),
      );
    }

    showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        )),
        builder: (ctx) {
          var appColors =
              Provider.of<AppColorsProvider>(context, listen: false);
          return StatefulBuilder(builder: (_, setState) {
            return Container(
              padding: EdgeInsets.symmetric(vertical: 8.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    height: 6,
                    width: 45,
                    decoration: BoxDecoration(
                        color: appColors.swipeModeIndicatorColor,
                        borderRadius: BorderRadius.circular(15)),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Change mode",
                    style: Theme.of(context).textTheme.body2.copyWith(
                          fontWeight: FontWeight.bold,
                        ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  //  HelperFunctions.divider(),
                  modeTile(title: "Light", value: 0),
                  // HelperFunctions.divider(),
                  modeTile(title: "Dark", value: 1),
                  // HelperFunctions.divider(),
                  modeTile(title: "System", value: 2),
                  // HelperFunctions.divider(),
                ],
              ),
            );
          });
        }).then((_) {
      BackButtonInterceptor.remove(myInterceptor);
    });
  }

  Widget buildLinksToOtherPage(
      {String iconPath,
      String title,
      String helperTitle,
      String helperContent,
      Function onCLick}) {
    var appColors = Provider.of<AppColorsProvider>(context, listen: false);
    return InkWell(
      onTap: onCLick,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 5.0),
        child: Row(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8.0),
              child: Row(
                children: <Widget>[
                  SizedBox(
                    height: 20,
                    width: 20,
                    child: Image.asset(
                      iconPath,
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                  SizedBox(width: 10),
                  Text(
                    title,
                    style: Theme.of(context).textTheme.body1.copyWith(
                          fontWeight: FontWeight.w600,
                          color: Theme.of(context).primaryColor,
                        ),
                  ),
                ],
              ),
            ),

            // SizedBox(width: 6),
            Spacer(),
            InkWell(
                onTap: () => showMoreInfo(
                      title: helperTitle,
                      content: helperContent,
                    ),
                child: HelperFunctions.buildQuestionMark())
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(vertical: 30),
            child: MainNavBar(
              title: "Settings",
              leadingImagePath: 'public/images/arrow-back.png',
              notificationImagePath: 'public/images/notification.png',
              leadingPressAction: widget.navBarLeadingFn,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                  children: <Widget>[
                    buildLinksToOtherPage(
                      title: "Manage your bank accounts",
                      iconPath: "public/images/bank-icon.png",
                      helperTitle: "Bank Accounts",
                      helperContent:
                          "PeerScore needs your bank account to easily manage your transactions, this also increase your score",
                      onCLick: () => widget.switchTo("account"),
                    ),
                    HelperFunctions.divider(),
                    buildLinksToOtherPage(
                      title: "BVN",
                      iconPath: "public/images/bvn-pin.png",
                      helperTitle: "BVN",
                      helperContent:
                          "PeerScore needs your BVN to identify you and secure your transactions, this also increase your score",
                      onCLick: () => widget.switchTo("bvn"),
                    ),
                    HelperFunctions.divider(),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(
                        children: <Widget>[
                          Text(
                            "Auto sync contacts",
                            style: Theme.of(context).textTheme.body1.copyWith(
                                  fontWeight: FontWeight.w600,
                                ),
                          ),
                          Spacer(),
                          CustomSwitch(
                            value: autoSyncValue,
                            onClick: (value) =>
                                onSwitchSync(selectedSwitchValue: value),
                          ),
                        ],
                      ),
                    ),
                    HelperFunctions.divider(),
                    // Container(
                    //   padding: EdgeInsets.symmetric(vertical: 8.0),
                    //   child: Row(
                    //     children: <Widget>[
                    //       Text(
                    //         "Google calender",
                    //         style: Theme.of(context).textTheme.body1.copyWith(
                    //               fontWeight: FontWeight.w600,
                    //             ),
                    //       ),
                    //       Spacer(),
                    //       CustomSwitch(
                    //         value: googleCalenderValue,
                    //         onClick: (value) =>
                    //             onSwitchGoogleCal(selectedSwitchValue: value),
                    //       ),
                    //     ],
                    //   ),
                    // ),
                    HelperFunctions.divider(),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 5.0),
                      child: InkWell(
                        onTap: openThemeChangeBottomSheet,
                        child: Container(
                          padding:
                              EdgeInsets.only(top: 8.0, bottom: 8.0, right: 3),
                          child: Row(
                            children: <Widget>[
                              Text(
                                "Change mode",
                                style:
                                    Theme.of(context).textTheme.body1.copyWith(
                                          fontWeight: FontWeight.w600,
                                        ),
                              ),
                              Spacer(),
                              FaIcon(
                                GeneralProvider.isLightMode
                                    ? FontAwesomeIcons.lightLightbulbOn
                                    : FontAwesomeIcons.lightLightbulb,
                                color: GeneralProvider.isLightMode
                                    ? Colors.black
                                    : Colors.white,
                                size: 20,
                              )
                              // CustomSwitch(
                              //   value: autoSyncValue,
                              //   onClick: (value) =>
                              //       onSwitchSync(selectedSwitchValue: value),
                              // ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    HelperFunctions.divider(),
                    SizedBox(
                      height: 30,
                    ),
                    // InkWell(
                    //   onTap: () => widget.switchTo("privacy"),
                    //   child: Container(
                    //     padding: EdgeInsets.symmetric(vertical: 8.0),
                    //     child: Text(
                    //       "About PeerScore",
                    //       style: Theme.of(context).textTheme.body1.copyWith(
                    //             fontWeight: FontWeight.w600,
                    //           ),
                    //     ),
                    //   ),
                    // ),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 5.0),
                      child: InkWell(
                        onTap: () => widget.switchTo("privacy"),
                        child: Container(
                          padding:
                              EdgeInsets.only(top: 8.0, bottom: 8.0, right: 3),
                          child: Row(
                            children: <Widget>[
                              Text(
                                "About PeerScore",
                                style:
                                    Theme.of(context).textTheme.body1.copyWith(
                                          fontWeight: FontWeight.w600,
                                        ),
                              ),
                              Spacer(),
                              FaIcon(
                                FontAwesomeIcons.infoCircle,
                                color: GeneralProvider.isLightMode
                                    ? Colors.black
                                    : Colors.white,
                                size: 20,
                              )
                              // CustomSwitch(
                              //   value: autoSyncValue,
                              //   onClick: (value) =>
                              //       onSwitchSync(selectedSwitchValue: value),
                              // ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    // Spacer(),

                    // Divider()
                  ],
                ),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Version",
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  projectVersion,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
