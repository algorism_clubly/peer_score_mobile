// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:peer_score/main.dart';

void main() {
  testWidgets('Counter increments smoke test', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(PeerScore());

    // Verify that our counter starts at 0.
    expect(find.text('0'), findsOneWidget);
    expect(find.text('1'), findsNothing);

    // Tap the '+' icon and trigger a frame.
    await tester.tap(find.byIcon(Icons.add));
    await tester.pump();

    // Verify that our counter has incremented.
    expect(find.text('0'), findsNothing);
    expect(find.text('1'), findsOneWidget);
  });
}


// 254817545309-bithohopbgt41ji72a47gn9r2l89kud3.apps.googleusercontent.com

// { 
//   type: "Bearer",
//   data: "ya29.a0AfH6SMBm7x19_XoS_n8u2Lu7PJpIPYO9lfGbiXhKh-K-VJcjHYpV4V9sKK6hpKz5JuBuk562Rlj4LXs868uIhgPbJw49X5DLSdcvNYMRgF8w7LlRtp7GWWFSTA6N6tLEtMfTZm1gbNbuoPPfHTZ3LYBYnDgd57nZTCGQNl99wh0"
//   expiry: "2020-12-09 12:14:21.084812Z"
//   refreshToken: "1//03qzLjpmrgM2qCgYIARAAGAMSNwF-L9IrHeGtpYaX4RzCysQWl1W_zVycIKrk3tk-t_lylavZKKSIa1c0pBFcheGYHThtKJEa3qM"
// }